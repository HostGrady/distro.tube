#+TITLE: Knowledge Base
#+DESCRIPTION: Learn About Linux and Related Tools
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* Command Line Tools
+ [[file:awk.org][Awk]]
+ [[file:cut.org][Cut]]
+ [[file:ed.org][Ed]]
+ Find
+ [[file:fdisk.org][Fdisk]]
+ Grep
+ [[file:sed.org][Sed]]
+ [[file:shuf.org][Shuf]]
+ [[file:sort.org][Sort]]
+ [[file:ssh.org][Ssh]]
+ [[file:tr.org][Tr]]
+ [[file:ufw.org][Ufw]]
+ [[file:uniq.org][Uniq]]
+ [[file:xargs.org][Xargs]]
+ Xprop
+ Xwallpaper
* Emacs
+ [[file:doom-emacs.org][Doom Emacs]]
+ GNU Emacs
+ Org Mode
+ [[file:org-export.org][Org Export]]
+ [[file:org-publish.org][Org Publish]]
* Free Software Movement
* Linux Installation Guides
+ [[file:arch-linux-install.org][Arch Linux]]
* Open Source
* Package Management
+ Pacman
+ AppImage
+ Flatpak
+ Snap
* Programming and Scripting
* Shells
+ Bash
+ [[file:fish.org][Fish]]
+ Nushell
+ Zsh
* Software Tutorials
+ [[file:aura.org][Aura]]
+ Conky
+ Dmenu
+ Dunst
+ Gimp
+ Htop
+ Kdenlive
+ [[file:nextcloud.org][Nextcloud]]
+ Polkit
+ [[file:qutebrowser.org][Qutebrowser]]
+ Sxiv
+ Trayer
+ [[file:virt-manager.org][Virt-manager]]
* Vim
* Window Managers
+ Awesome
+ Dwm
+ Exwm
+ Openbox
+ Qtile
+ Xmonad

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
