#+TITLE: Sort
#+DESCRIPTION: Knowledge Base - sort
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* sort
Sort lines of text files.

** Sort a file in ascending order:
#+begin_example
sort path/to/file
#+end_example

** Sort a file in descending order:
#+begin_example
sort -r path/to/file
#+end_example

** Sort a file in case-insensitive way:
#+begin_example
sort -f path/to/file
#+end_example

** Ignore non-printable characters
#+begin_example
sort -i path/to/file
#+end_example

** Sort a file using numeric rather than alphabetic order:
#+begin_example
sort -n path/to/file
#+end_example

** Sort `/etc/passwd` by the 3rd field of each line numerically, using ":" as a field separator:
#+begin_example
sort -t : -k 3n /etc/passwd
#+end_example

** Sort a file preserving only unique lines:
#+begin_example
sort -u path/to/file
cat /etc/shells | awk -F "/" '/^\// {print $NF}' | sort -u
#+end_example

** Sort a file, printing the output to the specified output file (can be used to sort a file in-place):
#+begin_example
sort path/to/file/read -o path/to/file/output
#+end_example
=NOTE:= the output file is the one immediately after the -o flag!

** Sort randomly:
#+begin_example
sort -R .bashrc
#+end_example

=NOTE:=. This is like a random permutation of the inputs (see shuf invocation), except that keys with the same value sort together.

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
