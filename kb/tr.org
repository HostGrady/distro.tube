#+TITLE: Tr
#+DESCRIPTION: Knowledge Base - tr
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* tr
** The basics of tr
+ echo "This is a line of text" | tr 'a' 'A'
+ echo "This is a line of text" | tr 'aeio' 'AEIO'
+ echo "I'M A LEET HAXOR!" | tr 'AEO' '430'
+ echo "This is a line of text" | tr -d 'aeio'
+ echo "This is a line of text" | tr -d 'aeio '
+ echo "Thiis iis aa liinee oof teext" | tr -s 'aeio'
+ echo "Thiis iis aa liinee oof teeeeext" | tr -s 'aeio'
+ echo "Thiis iis aa liinee oof teeeeext" | tr -s '[:lower:]' '[:upper:]'
+ head /dev/urandom
+ head /dev/urandom | tr -cd '[:print:]'
+ echo "my super secure password is 1234 " | tr -cd [:digit:]
** Compared to sed (or awk)
tr works on characters, not strings, and converts characters from one set to characters in another (as in tr 'A-Z' 'a-z' to lowercase input).

sed has a y command that is analogous to tr:
+ echo "This is a line of text" | sed -e 'y/ai/AI/'

'y' does not support character ranges a-z or classes [:alpha:] like tr does, nor the complement modes -c and -C.
+ echo "This is a line of text" | sed -e 'y/[:lower:]/[:upper:]/'

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
