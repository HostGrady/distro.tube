#!/usr/bin/env bash

### man3p ###
# An array of all files in the 'mandb' directory
readarray -t manfiles < <(find ../mandb/man3p -iname "*.*")

# For each file, convert to from 'man' to 'org'.
for file in "${manfiles[@]}"; do
   name=$(echo "$file" | awk -F / '{print $NF}')
   pandoc -o ../man-org/man3p/"$name".org -f man -t org "$file" && \
      echo "$file.org created." || echo "Error converting $file."
done

cd ../man-org/man3p/ || exit
yes | rm ./*.gz
