#+TITLE: Manpages - sigwait.3p
#+DESCRIPTION: Linux manpage for sigwait.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
sigwait --- wait for queued signals

* SYNOPSIS
#+begin_example
  #include <signal.h>
  int sigwait(const sigset_t *restrict set, int *restrict sig);
#+end_example

* DESCRIPTION
The /sigwait/() function shall select a pending signal from /set/,
atomically clear it from the system's set of pending signals, and return
that signal number in the location referenced by /sig/. If prior to the
call to /sigwait/() there are multiple pending instances of a single
signal number, it is implementation-defined whether upon successful
return there are any remaining pending signals for that signal number.
If the implementation supports queued signals and there are multiple
signals queued for the signal number selected, the first such queued
signal shall cause a return from /sigwait/() and the remainder shall
remain queued. If no signal in /set/ is pending at the time of the call,
the thread shall be suspended until one or more becomes pending. The
signals defined by /set/ shall have been blocked at the time of the call
to /sigwait/(); otherwise, the behavior is undefined. The effect of
/sigwait/() on the signal actions for the signals in /set/ is
unspecified.

If more than one thread is using /sigwait/() to wait for the same
signal, no more than one of these threads shall return from /sigwait/()
with the signal number. If more than a single thread is blocked in
/sigwait/() for a signal when that signal is generated for the process,
it is unspecified which of the waiting threads returns from /sigwait/().
If the signal is generated for a specific thread, as by
/pthread_kill/(), only that thread shall return.

Should any of the multiple pending signals in the range SIGRTMIN to
SIGRTMAX be selected, it shall be the lowest numbered one. The selection
order between realtime and non-realtime signals, or between multiple
pending non-realtime signals, is unspecified.

* RETURN VALUE
Upon successful completion, /sigwait/() shall store the signal number of
the received signal at the location referenced by /sig/ and return zero.
Otherwise, an error number shall be returned to indicate the error.

* ERRORS
The /sigwait/() function may fail if:

- *EINVAL* :: The /set/ argument contains an invalid or unsupported
  signal number.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
To provide a convenient way for a thread to wait for a signal, this
volume of POSIX.1‐2017 provides the /sigwait/() function. For most cases
where a thread has to wait for a signal, the /sigwait/() function should
be quite convenient, efficient, and adequate.

However, requests were made for a lower-level primitive than /sigwait/()
and for semaphores that could be used by threads. After some
consideration, threads were allowed to use semaphores and /sem_post/()
was defined to be async-signal-safe.

In summary, when it is necessary for code run in response to an
asynchronous signal to notify a thread, /sigwait/() should be used to
handle the signal. Alternatively, if the implementation provides
semaphores, they also can be used, either following /sigwait/() or from
within a signal handling routine previously registered with
/sigaction/().

* FUTURE DIRECTIONS
None.

* SEE ALSO
/Section 2.4/, /Signal Concepts/, /Section 2.8.1/, /Realtime Signals/,
//pause/ ( )/, //pthread_sigmask/ ( )/, //sigaction/ ( )/,
//sigpending/ ( )/, //sigsuspend/ ( )/, //sigtimedwait/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<signal.h>*/,
/*<time.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
