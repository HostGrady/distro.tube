#+TITLE: Manpages - tcsetpgrp.3p
#+DESCRIPTION: Linux manpage for tcsetpgrp.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
tcsetpgrp --- set the foreground process group ID

* SYNOPSIS
#+begin_example
  #include <unistd.h>
  int tcsetpgrp(int fildes, pid_t pgid_id);
#+end_example

* DESCRIPTION
If the process has a controlling terminal, /tcsetpgrp/() shall set the
foreground process group ID associated with the terminal to /pgid_id/.
The application shall ensure that the file associated with /fildes/ is
the controlling terminal of the calling process and the controlling
terminal is currently associated with the session of the calling
process. The application shall ensure that the value of /pgid_id/
matches a process group ID of a process in the same session as the
calling process.

Attempts to use /tcsetpgrp/() from a process which is a member of a
background process group on a /fildes/ associated with its controlling
terminal shall cause the process group to be sent a SIGTTOU signal. If
the calling thread is blocking SIGTTOU signals or the process is
ignoring SIGTTOU signals, the process shall be allowed to perform the
operation, and no signal is sent.

* RETURN VALUE
Upon successful completion, 0 shall be returned. Otherwise, -1 shall be
returned and /errno/ set to indicate the error.

* ERRORS
The /tcsetpgrp/() function shall fail if:

- *EBADF* :: The /fildes/ argument is not a valid file descriptor.

- *EINVAL* :: This implementation does not support the value in the
  /pgid_id/ argument.

- *EIO* :: The process group of the writing process is orphaned, the
  calling thread is not blocking SIGTTOU, and the process is not
  ignoring SIGTTOU.

- *ENOTTY* :: The calling process does not have a controlling terminal,
  or the file is not the controlling terminal, or the controlling
  terminal is no longer associated with the session of the calling
  process.

- *EPERM* :: The value of /pgid_id/ is a value supported by the
  implementation, but does not match the process group ID of a process
  in the same session as the calling process.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//tcgetpgrp/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<sys_types.h>*/,
/*<unistd.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
