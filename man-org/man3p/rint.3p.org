#+TITLE: Manpages - rint.3p
#+DESCRIPTION: Linux manpage for rint.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
rint, rintf, rintl --- round-to-nearest integral value

* SYNOPSIS
#+begin_example
  #include <math.h>
  double rint(double x);
  float rintf(float x);
  long double rintl(long double x);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

These functions shall return the integral value (represented as a
*double*) nearest /x/ in the direction of the current rounding mode. The
current rounding mode is implementation-defined.

If the current rounding mode rounds toward negative infinity, then
/rint/() shall be equivalent to //floor/ ( )/. If the current rounding
mode rounds toward positive infinity, then /rint/() shall be equivalent
to //ceil/ ( )/. If the current rounding mode rounds towards zero, then
/rint/() shall be equivalent to //trunc/ ( )/. If the current rounding
mode rounds towards nearest, then /rint/() differs from //round/ ( )/ in
that halfway cases are rounded to even rather than away from zero.

These functions differ from the /nearbyint/(), /nearbyintf/(), and
/nearbyintl/() functions only in that they may raise the inexact
floating-point exception if the result differs in value from the
argument.

An application wishing to check for error situations should set /errno/
to zero and call /feclearexcept/(FE_ALL_EXCEPT) before calling these
functions. On return, if /errno/ is non-zero or
/fetestexcept/(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW)
is non-zero, an error has occurred.

* RETURN VALUE
Upon successful completion, these functions shall return the integer
(represented as a double precision number) nearest /x/ in the direction
of the current rounding mode. The result shall have the same sign as
/x/.

If /x/ is NaN, a NaN shall be returned.

If /x/ is ±0 or ±Inf, /x/ shall be returned.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
The integral value returned by these functions need not be expressible
as an *intmax_t*. The return value should be tested before assigning it
to an integer type to avoid the undefined results of an integer
overflow.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//abs/ ( )/, //ceil/ ( )/, //feclearexcept/ ( )/, //fetestexcept/ ( )/,
//floor/ ( )/, //isnan/ ( )/, //nearbyint/ ( )/

The Base Definitions volume of POSIX.1‐2017, /Section 4.20/, /Treatment
of Error Conditions for Mathematical Functions/, /*<math.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
