#+TITLE: Manpages - posix_spawnattr_getsigdefault.3p
#+DESCRIPTION: Linux manpage for posix_spawnattr_getsigdefault.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
posix_spawnattr_getsigdefault, posix_spawnattr_setsigdefault --- get and
set the spawn-sigdefault attribute of a spawn attributes object
(*ADVANCED REALTIME*)

* SYNOPSIS
#+begin_example
  #include <signal.h>
  #include <spawn.h>
  int posix_spawnattr_getsigdefault(const posix_spawnattr_t
      *restrict attr, sigset_t *restrict sigdefault);
  int posix_spawnattr_setsigdefault(posix_spawnattr_t *restrict attr,
      const sigset_t *restrict sigdefault);
#+end_example

* DESCRIPTION
The /posix_spawnattr_getsigdefault/() function shall obtain the value of
the /spawn-sigdefault/ attribute from the attributes object referenced
by /attr/.

The /posix_spawnattr_setsigdefault/() function shall set the
/spawn-sigdefault/ attribute in an initialized attributes object
referenced by /attr/.

The /spawn-sigdefault/ attribute represents the set of signals to be
forced to default signal handling in the new process image (if
POSIX_SPAWN_SETSIGDEF is set in the /spawn-flags/ attribute) by a spawn
operation. The default value of this attribute shall be an empty signal
set.

* RETURN VALUE
Upon successful completion, /posix_spawnattr_getsigdefault/() shall
return zero and store the value of the /spawn-sigdefault/ attribute of
/attr/ into the object referenced by the /sigdefault/ parameter;
otherwise, an error number shall be returned to indicate the error.

Upon successful completion, /posix_spawnattr_setsigdefault/() shall
return zero; otherwise, an error number shall be returned to indicate
the error.

* ERRORS
These functions may fail if:

- *EINVAL* :: The value specified by /attr/ is invalid.

The /posix_spawnattr_setsigdefault/() function may fail if:

- *EINVAL* :: The value of the attribute being set is not valid.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
These functions are part of the Spawn option and need not be provided on
all implementations.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//posix_spawn/ ( )/, //posix_spawnattr_destroy/ ( )/,
//posix_spawnattr_getflags/ ( )/, //posix_spawnattr_getpgroup/ ( )/,
//posix_spawnattr_getschedparam/ ( )/,
//posix_spawnattr_getschedpolicy/ ( )/,
//posix_spawnattr_getsigmask/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<signal.h>*/,
/*<spawn.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
