#+TITLE: Manpages - mq_getattr.3p
#+DESCRIPTION: Linux manpage for mq_getattr.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
mq_getattr --- get message queue attributes (*REALTIME*)

* SYNOPSIS
#+begin_example
  #include <mqueue.h>
  int mq_getattr(mqd_t mqdes, struct mq_attr *mqstat);
#+end_example

* DESCRIPTION
The /mq_getattr/() function shall obtain status information and
attributes of the message queue and the open message queue description
associated with the message queue descriptor.

The /mqdes/ argument specifies a message queue descriptor.

The results shall be returned in the *mq_attr* structure referenced by
the /mqstat/ argument.

Upon return, the following members shall have the values associated with
the open message queue description as set when the message queue was
opened and as modified by subsequent /mq_setattr/() calls: /mq_flags/.

The following attributes of the message queue shall be returned as set
at message queue creation: /mq_maxmsg/, /mq_msgsize/.

Upon return, the following members within the *mq_attr* structure
referenced by the /mqstat/ argument shall be set to the current state of
the message queue:

- mq_curmsgs :: The number of messages currently on the queue.

* RETURN VALUE
Upon successful completion, the /mq_getattr/() function shall return
zero. Otherwise, the function shall return -1 and set /errno/ to
indicate the error.

* ERRORS
The /mq_getattr/() function may fail if:

- *EBADF* :: The /mqdes/ argument is not a valid message queue
  descriptor.

/The following sections are informative./

* EXAMPLES
See //mq_notify/ ( )/.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//mq_notify/ ( )/, //mq_open/ ( )/, //mq_send/ ( )/, //mq_setattr/ ( )/,
//msgctl/ ( )/, //msgget/ ( )/, //msgrcv/ ( )/, //msgsnd/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<mqueue.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
