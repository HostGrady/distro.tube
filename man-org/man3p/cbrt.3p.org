#+TITLE: Manpages - cbrt.3p
#+DESCRIPTION: Linux manpage for cbrt.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
cbrt, cbrtf, cbrtl --- cube root functions

* SYNOPSIS
#+begin_example
  #include <math.h>
  double cbrt(double x);
  float cbrtf(float x);
  long double cbrtl(long double x);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

These functions shall compute the real cube root of their argument /x/.

* RETURN VALUE
Upon successful completion, these functions shall return the cube root
of /x/.

If /x/ is NaN, a NaN shall be returned.

If /x/ is ±0 or ±Inf, /x/ shall be returned.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
For some applications, a true cube root function, which returns negative
results for negative arguments, is more appropriate than /pow/( /x/,
1.0/3.0), which returns a NaN for /x/ less than 0.

* FUTURE DIRECTIONS
None.

* SEE ALSO
The Base Definitions volume of POSIX.1‐2017, /*<math.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
