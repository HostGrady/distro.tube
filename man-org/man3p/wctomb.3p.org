#+TITLE: Manpages - wctomb.3p
#+DESCRIPTION: Linux manpage for wctomb.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
wctomb --- convert a wide-character code to a character

* SYNOPSIS
#+begin_example
  #include <stdlib.h>
  int wctomb(char *s, wchar_t wchar);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

The /wctomb/() function shall determine the number of bytes needed to
represent the character corresponding to the wide-character code whose
value is /wchar/ (including any change in the shift state). It shall
store the character representation (possibly multiple bytes and any
special bytes to change shift state) in the array object pointed to by
/s/ (if /s/ is not a null pointer). At most {MB_CUR_MAX} bytes shall be
stored. If /wchar/ is 0, a null byte shall be stored, preceded by any
shift sequence needed to restore the initial shift state, and /wctomb/()
shall be left in the initial shift state.

The behavior of this function is affected by the /LC_CTYPE/ category of
the current locale. For a state-dependent encoding, this function shall
be placed into its initial state by a call for which its character
pointer argument, /s/, is a null pointer. Subsequent calls with /s/ as
other than a null pointer shall cause the internal state of the function
to be altered as necessary. A call with /s/ as a null pointer shall
cause this function to return a non-zero value if encodings have state
dependency, and 0 otherwise. Changing the /LC_CTYPE/ category causes the
shift state of this function to be unspecified.

The /wctomb/() function need not be thread-safe.

The implementation shall behave as if no function defined in this volume
of POSIX.1‐2017 calls /wctomb/().

* RETURN VALUE
If /s/ is a null pointer, /wctomb/() shall return a non-zero or 0 value,
if character encodings, respectively, do or do not have state-dependent
encodings. If /s/ is not a null pointer, /wctomb/() shall return -1 if
the value of /wchar/ does not correspond to a valid character, or return
the number of bytes that constitute the character corresponding to the
value of /wchar/.

In no case shall the value returned be greater than the value of the
{MB_CUR_MAX} macro.

* ERRORS
The /wctomb/() function shall fail if:

- *EILSEQ* :: An invalid wide-character code is detected.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//mblen/ ( )/, //mbtowc/ ( )/, //mbstowcs/ ( )/, //wcstombs/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<stdlib.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
