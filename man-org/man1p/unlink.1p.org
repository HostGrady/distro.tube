#+TITLE: Manpages - unlink.1p
#+DESCRIPTION: Linux manpage for unlink.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
unlink --- call the /unlink/() function

* SYNOPSIS
#+begin_example
  unlink file
#+end_example

* DESCRIPTION
The /unlink/ utility shall perform the function call:

#+begin_quote
  #+begin_example

    unlink(file);
  #+end_example
#+end_quote

A user may need appropriate privileges to invoke the /unlink/ utility.

* OPTIONS
None.

* OPERANDS
The following operands shall be supported:

- file :: The pathname of an existing file.

* STDIN
Not used.

* INPUT FILES
Not used.

* ENVIRONMENT VARIABLES
The following environment variables shall affect the execution of
/unlink/:

- LANG :: Provide a default value for the internationalization variables
  that are unset or null. (See the Base Definitions volume of
  POSIX.1‐2017, /Section 8.2/, /Internationalization Variables/ for the
  precedence of internationalization variables used to determine the
  values of locale categories.)

- LC_ALL :: If set to a non-empty string value, override the values of
  all the other internationalization variables.

- LC_CTYPE :: Determine the locale for the interpretation of sequences
  of bytes of text data as characters (for example, single-byte as
  opposed to multi-byte characters in arguments).

- LC_MESSAGES :: \\
  Determine the locale that should be used to affect the format and
  contents of diagnostic messages written to standard error.

- NLSPATH :: Determine the location of message catalogs for the
  processing of /LC_MESSAGES/.

* ASYNCHRONOUS EVENTS
Default.

* STDOUT
None.

* STDERR
The standard error shall be used only for diagnostic messages.

* OUTPUT FILES
None.

* EXTENDED DESCRIPTION
None.

* EXIT STATUS
The following exit values shall be returned:

-  0 :: Successful completion.

- >0 :: An error occurred.

* CONSEQUENCES OF ERRORS
Default.

/The following sections are informative./

* APPLICATION USAGE
None.

* EXAMPLES
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//link/ /, //rm/ /

The Base Definitions volume of POSIX.1‐2017, /Chapter 8/, /Environment
Variables/

The System Interfaces volume of POSIX.1‐2017, //unlink/ ( )/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
