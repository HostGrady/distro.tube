#+TITLE: Man1 - vifm-screen-split.1
#+DESCRIPTION: Linux manpage for vifm-screen-split.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vifm-screen-split - a vifm helper script to run in GNU screen split

* SYNOPSIS
Only called automagically from within vifm

vifm-screen-split

* DESCRIPTION
A helper script for vifm, not for standalone use.

Prints "Press any key to close this region..." message and waits to
close the split.

* SEE ALSO
*screen*(1), *vifm*(1)

* AUTHOR
This manual page was written by xaizek <xaizek@posteo.net>.
