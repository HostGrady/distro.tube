#+TITLE: Man1 - mpd.1
#+DESCRIPTION: Linux manpage for mpd.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mpd - MPD documentation

* SYNOPSIS
*mpd* [options] [CONF_FILE]

* DESCRIPTION
MPD is a daemon for playing music. Music is played through the
configured audio output(s) (which are generally local, but can be
remote). The daemon stores info about all available music, and this info
can be easily searched and retrieved. Player control, info retrieval,
and playlist management can all be managed remotely.

MPD searches for a config file in *$XDG_CONFIG_HOME/mpd/mpd.conf* then
*~/.mpdconf* then *~/.mpd/mpd.conf* then */etc/mpd.conf* or uses
*CONF_FILE*.

Read more about MPD at /http://www.musicpd.org//

* OPTIONS

#+begin_quote
  - *--help* :: Output a brief help message.
#+end_quote

#+begin_quote
  - *--kill* :: Kill the currently running mpd session. The pid_file
    parameter must be specified in the config file for this to work.
#+end_quote

#+begin_quote
  - *--no-config* :: Don't read from the configuration file.
#+end_quote

#+begin_quote
  - *--no-daemon* :: Don't detach from console.
#+end_quote

#+begin_quote
  - *--stderr* :: Print messages to stderr.
#+end_quote

#+begin_quote
  - *--verbose* :: Verbose logging.
#+end_quote

#+begin_quote
  - *--version* :: Print version information.
#+end_quote

* FILES

#+begin_quote
  - **$XDG_CONFIG_HOME/mpd/mpd.conf** :: User configuration file
    (usually *~/.config/mpd/mpd.conf*).

  - **/etc/mpd.conf** :: Global configuration file.
#+end_quote

* SEE ALSO
*mpd.conf(5)*, *mpc(1)*

* BUGS
If you find a bug, please report it at
/https://github.com/MusicPlayerDaemon/MPD/issues//

* AUTHOR
Max Kellermann

* COPYRIGHT
2003-2021 The Music Player Daemon Project
