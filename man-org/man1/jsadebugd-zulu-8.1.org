#+TITLE: Man1 - jsadebugd-zulu-8.1
#+DESCRIPTION: Linux manpage for jsadebugd-zulu-8.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jsadebugd - Attaches to a Java process or core file and acts as a debug
server. This command is experimental and unsupported.

* SYNOPSIS
#+begin_example

  jsadebugd pid [ server-id ]
#+end_example

#+begin_example

  jsadebugd executable core [ server-id ]
#+end_example

- /pid/ :: The process ID of the process to which the debug server
  attaches. The process must be a Java process. To get a list of Java
  processes running on a machine, use the jps(1) command. At most one
  instance of the debug server can be attached to a single process.

- /executable/ :: The Java executable from which the core dump was
  produced.

- /core/ :: The core file to which the debug server should attach.

- /server-id/ :: An optional unique ID that is needed when multiple
  debug servers are started on the same machine. This ID must be used by
  remote clients to identify the particular debug server to which to
  attach. Within a single machine, this ID must be unique.

* DESCRIPTION
The jsadebugd command attaches to a Java process or core file and acts
as a debug server. Remote clients such as jstack, jmap, and jinfo can
attach to the server through Java Remote Method Invocation (RMI). Before
you start the jsadebugd command, start the RMI registry with the
rmiregistry command as follows where /$JAVA_HOME/ is the JDK
installation directory:

#+begin_example
  rmiregistry -J-Xbootclasspath/p:$JAVA_HOME/lib/sajdi.jar
#+end_example

#+begin_example
#+end_example

If the RMI registry was not started, then the jsadebugd command starts
an RMI registry in a standard (1099) port internally. The debug server
can be stopped by sending a SIGINT to it. To send a SIGINT press
/Ctrl+C/.

/Note:/ This utility is unsupported and may or may not be available in
future releases of the JDK. In Windows Systems where dbgeng.dll is not
present, Debugging Tools For Windows must be installed to have these
tools working. The PATH environment variable should contain the location
of jvm.dll used by the target process or the location from which the
crash dump file was produced. For example, set
PATH=%JDK_HOME%\jre\bin\client;%PATH%.

* SEE ALSO
- · :: jinfo(1)

- · :: jmap(1)

- · :: jps(1)

- · :: jstack(1)

- · :: rmiregistry(1)

\\
