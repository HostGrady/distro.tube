#+TITLE: Man1 - jack_wait.1
#+DESCRIPTION: Linux manpage for jack_wait.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jack_wait - JACK toolkit client to check and wait for existence/exit of
jackd.

* SYNOPSIS
*jack_wait* [ /-s/ | /--server/ servername ] [ /-t/ | /--timeout/
timeout_seconds [ /-cqwhv/ ]

* DESCRIPTION
*jack_wait* When invoked with /-c/ it only checks for the existence of a
jack server. When invoked with /-w/ the program will wait for a jackd to
be available. The /-q/ makes it wait for the jackd to exit.

* OPTIONS
- *-w*, *--wait* :: \\
  Wait for jackd to be available.

- *-q*, *--quit* :: \\
  Wait for jackd quit.

- *-c*, *--check* :: \\
  Only check for existence of jackd, and exit.

- *-s*, *--server* /servername/ :: \\
  Connect to the jack server named /servername/

- *-t*, *--timeout* /timeout_seconds/ :: \\
  Only wait /timeout_seconds/.

- *-h*, *--help* :: \\
  Display help/usage message

- *-v*, *--version* :: \\
  Output version information and exit
