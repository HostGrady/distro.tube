#+TITLE: Man1 - xkbcli-interactive-x11.1
#+DESCRIPTION: Linux manpage for xkbcli-interactive-x11.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a commandline tool to interactively debug XKB keymaps by listening to
X11 events.

This requires an X server to be running.

Press the

key to exit.

This is a debugging tool, its behavior or output is not guaranteed to be
stable.

Print help and exit
