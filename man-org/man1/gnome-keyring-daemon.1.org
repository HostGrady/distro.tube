#+TITLE: Man1 - gnome-keyring-daemon.1
#+DESCRIPTION: Linux manpage for gnome-keyring-daemon.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnome-keyring-daemon - The gnome-keyring daemon

* SYNOPSIS
*gnome-keyring-daemon* [OPTION...]

* DESCRIPTION
The *gnome-keyring-daemon* is a service that stores your passwords and
secrets. It is normally started automatically when a user logs into a
desktop session.

The *gnome-keyring-daemon* implements the DBus Secret Service API, and
you can use tools like *seahorse* or *secret-tool* to interact with it.

The daemon also implements a GnuPG and SSH agent both of which
automatically load the users keys, and prompt for passwords when
necessary.

The daemon will print out various environment variables which should be
set in the users environment, in order to interact with the daemon.

* OPTIONS
The various startup arguments below can be used:

*-c*, *--components=*/ssh,secrets,pkcs11/

#+begin_quote
  Ask the daemon to only initialize certain components. Valid components
  are ssh, secrets, pkcs11.

  By default all components are initialized.
#+end_quote

*-C*, *--control-directory=*//path/to/directory/

#+begin_quote
  Use this directory for creating communication sockets. By default a
  temporary directory is automatically created.
#+end_quote

*-d*, *--daemonize*

#+begin_quote
  Run as a real daemon, disconnected from the terminal.
#+end_quote

*-f*, *--foreground*

#+begin_quote
  Run in the foreground, and do not fork or become a daemon.
#+end_quote

*-l*, *--login*

#+begin_quote
  This argument tells the daemon it is being run by PAM. It reads all of
  stdin (including any newlines) as a login password and does not
  complete actual initialization.

  The daemon should later be initialized with a *gnome-keyring-daemon*
  *--start* invocation.

  This option implies *--unlock*. It may not be used together with
  either the *--replace* or *--start* arguments.
#+end_quote

*-r*, *--replace*

#+begin_quote
  Try to replace a running keyring daemon, and assume its environment
  variables. A successful replacement depends on the
  GNOME_KEYRING_CONTROL environment variable being set by an earlier
  daemon.

  This option may not be used together with either the *--login* or
  *--start* arguments.
#+end_quote

*-s*, *--start*

#+begin_quote
  Connect to an already running daemon and initialize it. This is often
  used to complete initialization of a daemon that was started by PAM
  using the *--login* argument.

  This option may not be used together with either the *--login* or
  *--replace* arguments.
#+end_quote

*--unlock*

#+begin_quote
  Read a password from stdin, and use it to unlock the login keyring or
  create it if the login keyring does not exist.
#+end_quote

*-V*, *--version*

#+begin_quote
  Print out the gnome-keyring version and then exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Show help options and exit.
#+end_quote

* BUGS
Please send bug reports to either the distribution bug tracker or the
upstream bug tracker at
*https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-keyring*

* SEE ALSO
*secret-tool*(1), *seahorse*(1)

Further details available in the gnome-keyring online documentation at
*https://wiki.gnome.org/GnomeKeyring* and in the secret-service online
documentation at *http://standards.freedesktop.org/secret-service/*
