#+TITLE: Man1 - pdfmom.1
#+DESCRIPTION: Linux manpage for pdfmom.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pdfmom - produce PDF documents using the mom macro package for groff

* SYNOPSIS
*pdfmom* [ *-Tpdf* ] [/groff-options/] /file/ . . . *pdfmom* *-Tps*
[/pdfroff-options/] [/groff-options/] /file/ . . . *pdfmom* *-v*

* DESCRIPTION
/pdfmom/ is a wrapper around /groff/(1) that facilitates the production
of PDF documents from files formatted with the /mom/ macros.

/pdfmom/ prints to the standard output, so output must usually be
redirected to a destination file. The size of the final PDF can be
reduced by piping the output through /ps2pdf/(1).

If called with the *-Tpdf* option (which is the default), /pdfmom/
processes files using groff's native PDF driver, /gropdf/(1). If *-Tps*
is given, processing is passed over to /pdfroff/, which uses /groff/'s
PostScript driver. In either case, multiple runs of the source file are
performed in order to satisfy any forward references in the document.

/pdfmom/ accepts all the same options as /groff/. If *-Tps* is given,
the options associated with /pdfroff/ are accepted as well. Please note
that when /pdfmom/ calls /pdfroff/, the

#+begin_quote
  *-mpdfmark -mom --no-toc*
#+end_quote

options are implied and should not be given on the command line.
Equally, it is not necessary to supply the *-mom* or *-m mom* options
when *-Tps* is absent.

PDF integration with the /mom/ macros is discussed in full in the manual
/Producing PDFs with groff and mom/, which was itself produced with
/pdfmom/.

If called with the *-v* option, /pdfmom/ simply displays its version
information and exits.

* BUGS
/pdfmom/ sometimes issues warnings of the type

#+begin_quote
  . . .: can't transparently output node at top level
#+end_quote

but this is more of an annoyance than a bug, and may safely be ignored.

* AUTHORS
/pfdmom/ was written by [[mailto:deri@chuzzlewit.demon.co.uk][Deri
James]] and [[mailto:peter@schaffter.ca][Peter Schaffter]].

* SEE ALSO
/Producing PDFs with groff and mom/, by Deri James and Peter Schaffter;
a copy is installed at //usr/share/doc/groff-1.22.4/pdf/mom-pdf.pdf/.

/groff/(1), /gropdf/(1), /pdfroff/(1), /ps2pdf/(1)
