#+TITLE: Man1 - i3-sensible-editor.1
#+DESCRIPTION: Linux manpage for i3-sensible-editor.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
i3-sensible-editor - launches $EDITOR with fallbacks

* SYNOPSIS
i3-sensible-editor [arguments]

* DESCRIPTION
i3-sensible-editor is used by i3-nagbar(1) when you click on the edit
button.

It tries to start one of the following (in that order):

#+begin_quote
  ·

  $VISUAL
#+end_quote

#+begin_quote
  ·

  $EDITOR
#+end_quote

#+begin_quote
  ·

  nano
#+end_quote

#+begin_quote
  ·

  nvim
#+end_quote

#+begin_quote
  ·

  vim
#+end_quote

#+begin_quote
  ·

  vi
#+end_quote

#+begin_quote
  ·

  emacs
#+end_quote

#+begin_quote
  ·

  pico
#+end_quote

#+begin_quote
  ·

  qe
#+end_quote

#+begin_quote
  ·

  mg
#+end_quote

#+begin_quote
  ·

  jed
#+end_quote

#+begin_quote
  ·

  gedit
#+end_quote

#+begin_quote
  ·

  mcedit
#+end_quote

#+begin_quote
  ·

  gvim
#+end_quote

Please don't complain about the order: If the user has any preference,
they will have $VISUAL or $EDITOR set.

* SEE ALSO
i3(1)

* AUTHOR
Michael Stapelberg and contributors
