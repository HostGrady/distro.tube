#+TITLE: Man1 - psidtopgm.1
#+DESCRIPTION: Linux manpage for psidtopgm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
psidtopgm - convert PostScript 'image' data to a PGM image

* SYNOPSIS
*psidtopgm* /width/ /height/ /bits/sample/ [/imagedata/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*psidtopgm* reads the 'image' data from a PostScript file as input and
produces a PGM image as output.

This program is obsoleted by *pstopnm*.

What follows was written before *pstopnm * existed.

This is a very simple and limited program, and is here only because so
many people have asked for it. To use it you have to /manually/ extract
the readhexstring data portion from your PostScript file, and then give
the width, height, and bits/sample on the command line. Before you
attempt this, you should /at/ least read the description of the 'image'
operator in the PostScript Language Reference Manual.

It would probably not be too hard to write a script that uses this
filter to read a specific variety of PostScript image, but the variation
is too great to make a general-purpose reader. Unless, of course, you
want to write a full-fledged PostScript interpreter...

* SEE ALSO
*pnmtops*(1) , *pgm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
