#+TITLE: Man1 - gigmerge.1
#+DESCRIPTION: Linux manpage for gigmerge.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gigmerge - Merges several Gigasampler (.gig) files to one Gigasampler
file.

* SYNOPSIS
*gigmerge* [ -v ] GIGFILE1 GIGFILE2 [ ... ] NEWGIGFILE

* DESCRIPTION
Takes a list of Gigasampler (.gig) files as input and merges their
content to one new single Gigasampler file. Note: There is no check for
duplicate (equivalent) samples yet. That means, if for example 2 input
files use the exact same sample, then this sample will exist twice in
the output file.

* OPTIONS
- * GIGFILE1* :: filename of input Gigasampler file

- * GIGFILE2* :: filename of input Gigasampler file

- * NEWGIGFILE* :: filename of merged output Gigasampler file

- * -v* :: print version and exit

* SEE ALSO
*gigextract(1),* *gigdump(1),* *gig2mono(1),* *gig2stereo(1),*
*dlsdump(1),* *rifftree(1)*

* BUGS
Check and report bugs at http://bugs.linuxsampler.org

* Author
Application and manual page written by Christian Schoenebeck
<cuse@users.sf.net>
