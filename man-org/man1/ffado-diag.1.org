#+TITLE: Man1 - ffado-diag.1
#+DESCRIPTION: Linux manpage for ffado-diag.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ffado-diag - print system diagnostic information related to FFADO.

* SYNOPSIS
*ffado-diag*[ *--static* | *-V* | *--version* | *--usage* ]

* DESCRIPTION
*ffado-diag* prints out an extensive collection of diagnostic
information about the computer it is run on. Information included is the
FFADO version number, the version number of libraries which FFADO
depends on, the firewire interface card, interrupt usage, and so on.
This is useful for developers to know when giving support via the FFADO
mailing lists. For anything other than trivial issues, the output of
*ffado-diag* will be one of the first things asked fro when debugging a
problem.

* OPTIONS
*-V*, *--version* Display version information. *--usage* Print a short
usage message and exit. *--static* Only display executable paths and
libraries.
