#+TITLE: Man1 - ppmcolormask.1
#+DESCRIPTION: Linux manpage for ppmcolormask.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmcolormask - produce mask of areas of a certain color in a PPM file

* SYNOPSIS
*ppmcolormask* *-color=*/color_list/ [/ppmfile/]

*ppmcolormask* /color/ [/ppmfile/]

* EXAMPLES
#+begin_example
      ppmcolormask -color red testimg.ppm >redmask.pbm
      pamcomp background.ppm testimg.ppm -alpha=redmask.pbm >test.ppm

      ppmcolormask -color=red,pink,salmon testimg.ppm >reddishmask.pbm

      ppmcolormask -color=bk:red,bk:orange,bk:yellow testimg.ppm >firemask.pbm
#+end_example

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmcolormask* reads a PPM file as input and produces a PBM (bitmap)
file as output. The output file is the same dimensions as the input file
and is black in all places where the input file is a color indicated by
the *-color* option, and white everywhere else.

The output of *ppmcolormask* is useful as an alpha mask input to
*pamcomp*. Note that you can generate such an alpha mask automatically
as you convert to PNG format with *pnmtopng*(1)

option.

/ppmfile/ is the input file. If you don't specify /ppmfile/, the input
is from Standard Input.

The output goes to Standard Output.

In the obsolete alternative syntax, specifying the /color/ names a
single exact color to be masked.

*ppmchange* does a similar thing: it modifies an image by changing
colors you specify to other colors you specify. The two programs give
you somewhat different means of specifying colors in the input image.

To make a mask of an image's background, without having to tell it what
color it is, use *pambackground*.

* OPTIONS
- *-color=*/color_list/ :: This mandatory option specifies the colors
  that are to be masked (where the image is one of these colors, the
  output mask will be black).

Examples:

- *-color=red*

- *-color=red,pink,salmon*

- *-color=rgb:80/80/ff*

- *-color=bk:red,bk:orange,bk:yellow*

/color_list/ is a list of colors separated by commas. Each color is
either an exact color name as described for the
[[file:libppm.html#colorname][argument of the *ppm_parsecolor()* library
routine]] or one of the [[file:libppm.html#berlinkay][Berlin-Kay color
names]] . In the latter case, all colors that are better described by
that Berlin-Kay color name than any other are in the mask set.

The algorithm *ppmcolormask* uses to determine to which colors a
Berlin-Kay color name applies is based on a Sugeno-type fuzzy inference
system developed by /Kenan Kalajdzic/ in 2006. The fuzzy model consists
of partially linear membership functions defined in the HSV color space.
Although more complex algorithms for fuzzy color matching exist, this
algorithm is intentionally simplified to achieve a satisfactory speed
using relatively compact code.

This option was new in Netpbm 10.34 (June 2006). Before that, you must
use the /color/ argument and cannot specify a Berlin-Kay color.

* SEE ALSO
*pambackground*(1) , *ppmchange*(1) , *pgmtoppm*(1) , *pamcomp*(1) ,
*pbmmask*(1) , *ppm*(5)
