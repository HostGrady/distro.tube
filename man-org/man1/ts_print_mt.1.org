#+TITLE: Man1 - ts_print_mt.1
#+DESCRIPTION: Linux manpage for ts_print_mt.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_print_mt - A very basic multitouch test routine for tslib.

* SYNOPSIS
*ts_print_mt [OPTION]*

* DESCRIPTION
ts_print_mt simply reads tslib multitouch input samples and prints them
to the console.

*-i, --idev*

#+begin_quote
  Explicitly choose the original input event device for tslib to use.
  Default: the environment variable *TSLIB_TSDEVICE*'s value.
#+end_quote

*-r, --raw*

#+begin_quote
  Don't use the filters. This uses ts_read_raw_mt() instead of
  ts_read_mt(). This replaces the ts_print_raw_mt program.
#+end_quote

*-n, --non-blocking*

#+begin_quote
  Opens the input device in non-blocking mode and indefinitely prints
  the error codes while no samples are received.
#+end_quote

*-s, --samples*

#+begin_quote
  Number of samples tslib should read before returning.
#+end_quote

*-j, --slots*

#+begin_quote
  Override the number of concurrent touch contacts to allocate.
#+end_quote

*-h, --help*

#+begin_quote
  Print usage help and exit.
#+end_quote

* SEE ALSO
ts.conf (5), ts_test (1), ts_test_mt (1), ts_calibrate (1)
