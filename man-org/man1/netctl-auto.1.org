#+TITLE: Man1 - netctl-auto.1
#+DESCRIPTION: Linux manpage for netctl-auto.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
netctl-auto - Control automatic selection of wireless netctl profiles

* SYNOPSIS
*netctl-auto* {*COMMAND*} ...

*netctl-auto* [--help | --version]

* DESCRIPTION
*netctl-auto* may be used to control the automatic network profile
selection offered by the /netctl-auto@.service/ file. See
*netctl.special*(7) for details about the service file.

* OPTIONS
The following commands are understood:

*list*

#+begin_quote
  List all profiles which are currently available for automatic
  selection. Active profiles will be marked with a ‘*', disabled
  profiles will be marked with a ‘!'.
#+end_quote

*switch-to [PROFILE]*

#+begin_quote
  Switch to the network profile specified on the command line. The
  specified profile will be enabled if necessary, the state of all other
  profiles is not changed. This command does not force *netctl-auto* to
  use the specified profile. If a disconnect occurs, *netctl-auto* may
  select an alternative profile.
#+end_quote

*is-active [PROFILE]*

#+begin_quote
  Check whether the network profile specified on the command line is
  active. Prints the current state.
#+end_quote

*enable [PROFILE]*

#+begin_quote
  Enable a previously disabled network profile for automatic selection.
  Every time the *netctl-auto* service is started, all available
  profiles are enabled by default.
#+end_quote

*disable [PROFILE]*

#+begin_quote
  Disable the specified profile for automatic selection. This will only
  take effect until the *netctl-auto* service is stopped. To permanently
  exclude a profile from automatic selection, use the /ExcludeAuto=yes/
  option in the profile.
#+end_quote

*enable-all*

#+begin_quote
  Enable all profiles for automatic selection.
#+end_quote

*disable-all*

#+begin_quote
  Disable all profiles for automatic selection.
#+end_quote

*is-enabled [PROFILE]*

#+begin_quote
  Check whether the specified profile is enabled for the *netctl-auto*
  service that is currently running. Prints the current enable status.
#+end_quote

*start [INTERFACE]*

#+begin_quote
  Start automatic profile selection on the specified interface.

  This command should not be invoked directly, use the following command
  instead:
#+end_quote

#+begin_quote
  #+begin_example
        systemctl start netctl-auto@<interface>
  #+end_example
#+end_quote

*stop [INTERFACE]*

#+begin_quote
  Stop automatic profile selection on the specified interface. This will
  disconnect the currently active profile on the interface.

  This command should not be invoked directly, use the following command
  instead:
#+end_quote

#+begin_quote
  #+begin_example
        systemctl stop netctl-auto@<interface>
  #+end_example
#+end_quote

* EXIT STATUS
On success 0 is returned, a non-zero failure code otherwise.

* ENVIRONMENT
/$NETCTL_DEBUG/

#+begin_quote
  If set to "yes", debugging output is generated.
#+end_quote

* SEE ALSO
*netctl*(1), *netctl.profile*(5), *netctl.special*(7)
