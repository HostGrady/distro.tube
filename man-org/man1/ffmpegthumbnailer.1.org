#+TITLE: Man1 - ffmpegthumbnailer.1
#+DESCRIPTION: Linux manpage for ffmpegthumbnailer.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ffmpegthumbnailer - fast and lightweight video thumbnailer

* SYNOPSIS
*ffmpegthumbnailer* [/options/]

* DESCRIPTION
/Ffmpegthumbnailer/ is a lightweight video thumbnailer that can be used
by file managers to create thumbnails for your video files. The
thumbnailer uses ffmpeg to decode frames from the video files, so
supported videoformats depend on the configuration flags of ffmpeg.

* OPTIONS
- *-i*<s> :: : input file

- *-o*<s> :: : output file

- *-s*<n> :: : thumbnail size (use 0 for original size) (default: 128)

- *-q*<n> :: : image quality (0 = bad, 10 = best) (default: 8) (only for
  jpeg)

- *-c*<s> :: : override image format (jpeg or png) (default: determined
  by filename)

- *-t*<n|s> :: : time to seek to (percentage or absolute time hh:mm:ss)
  (default: 10%)

- *-a* :: : ignore aspect ratio and generate square thumbnail

- *-f* :: : create a movie strip overlay

- *-m* :: : prefer embedded image metadata over video content

- *-h* :: : display this help

* AUTHOR
Written by Dirk Vanden Boer.

* REPORTING BUGS
Report bugs to <http://code.google.com/p/ffmpegthumbnailer/issues/list>

* COPYRIGHT
Copyright © 2008 Dirk Vanden Boer <dirk.vdb@gmail.com>\\
This is free software. You may redistribute copies of it under the terms
of the GNU General Public License (version 2 or later)
<http://www.gnu.org/licenses/gpl.html>. There is NO WARRANTY, to the
extent permitted by law.
