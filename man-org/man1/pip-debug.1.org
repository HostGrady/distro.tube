#+TITLE: Man1 - pip-debug.1
#+DESCRIPTION: Linux manpage for pip-debug.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pip-debug - description of pip debug command

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know why you
    came to this page and what on it helped you and what did not. (/Read
    more about this research/)
  #+end_quote
#+end_quote

* DESCRIPTION
Display debug information.

* USAGE

#+begin_quote

  #+begin_quote
    #+begin_example
      python -m pip debug <options>
    #+end_example
  #+end_quote
#+end_quote

*WARNING:*

#+begin_quote

  #+begin_quote
    This command is only meant for debugging. Its options and outputs
    are provisional and may change without notice.
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *--platform <platform>* :: Only use wheels compatible with
    <platform>. Defaults to the platform of the running system. Use this
    option multiple times to specify multiple platforms supported by the
    target interpreter.
#+end_quote

#+begin_quote
  - *--python-version <python_version>* :: The Python interpreter
    version to use for wheel and "Requires-Python" compatibility checks.
    Defaults to a version derived from the running interpreter. The
    version can be specified using up to three dot-separated integers
    (e.g. "3" for 3.0.0, "3.7" for 3.7.0, or "3.7.3"). A major-minor
    version can also be given as a string without dots (e.g. "37" for
    3.7.0).
#+end_quote

#+begin_quote
  - *--implementation <implementation>* :: Only use wheels compatible
    with Python implementation <implementation>, e.g. 'pp', 'jy', 'cp',
    or 'ip'. If not specified, then the current interpreter
    implementation is used. Use 'py' to force implementation-agnostic
    wheels.
#+end_quote

#+begin_quote
  - *--abi <abi>* :: Only use wheels compatible with Python abi <abi>,
    e.g. 'pypy_41'. If not specified, then the current interpreter abi
    tag is used. Use this option multiple times to specify multiple abis
    supported by the target interpreter. Generally you will need to
    specify --implementation, --platform, and --python-version when
    using this option.
#+end_quote

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know:

    #+begin_quote

      1. What problem were you trying to solve when you came to this
         page?

      2. What content was useful?

      3. What content was not useful?
    #+end_quote
  #+end_quote
#+end_quote

* AUTHOR
pip developers

* COPYRIGHT
2008-2021, PyPA
