#+TITLE: Man1 - snmpvacm.1
#+DESCRIPTION: Linux manpage for snmpvacm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
snmpvacm - creates and maintains SNMPv3 View-based Access Control
entries on a network entity

* SYNOPSIS
*snmpvacm* [COMMON OPTIONS] AGENT *createSec2Group* MODEL SECURITYNAME
GROUPNAME\\
*snmpvacm* [COMMON OPTIONS] AGENT *deleteSec2Group* MODEL SECURITYNAME\\
*snmpvacm* [COMMON OPTIONS] AGENT *createView* [-Ce] NAME SUBTREE MASK\\
*snmpvacm* [COMMON OPTIONS] AGENT *deleteView* NAME SUBTREE\\
*snmpvacm* [COMMON OPTIONS] AGENT *createAccess* GROUPNAME
[CONTEXTPREFIX] MODEL LEVEL CONTEXTMATCH READVIEW WRITEVIEW NOTIFYVIEW\\
*snmpvacm* [COMMON OPTIONS] AGENT *deleteAccess* GROUPNAME
[CONTEXTPREFIX] MODEL LEVEL\\
*snmpvacm* [COMMON OPTIONS] AGENT *createAuth* GROUPNAME [CONTEXTPREFIX]
MODEL LEVEL AUTHTYPE CONTEXTMATCH VIEW\\
*snmpvacm* [COMMON OPTIONS] AGENT *deleteAuth* GROUPNAME [CONTEXTPREFIX]
MODEL LEVEL AUTHTYPE

* DESCRIPTION
*snmpvacm* is an SNMP application that can be used to do simple
maintenance on the View-based Control Module (VACM) tables of an SNMP
agent. The SNMPv3 VACM specifications (see RFC2575) define assorted
tables to specify groups of users, MIB views, and authorised access
settings. These *snmpvacm* commands effectively create or delete rows in
the appropriate one of these tables, and match the equivalent configure
directives which are documented in the /snmpd.conf(5)/ man page.

A fuller explanation of how these operate can be found in the project
FAQ.

* SUB-COMMANDS
** createSec2Group MODEL SECURITYNAME GROUPNAME
Create an entry in the SNMPv3 security name to group table. This table
allows a single access control entry to be applied to a number of users
(or 'principals'), and is indexed by the security model and security
name values.

MODEL

#+begin_quote
  An integer representing the security model, taking one of the
  following values:\\
  1 - reserved for SNMPv1\\
  2 - reserved for SNMPv2c\\
  3 - User-based Security Model (USM)
#+end_quote

SECURITYNAME

#+begin_quote
  A string representing the security name for a principal (represented
  in a security-model-independent format). For USM-based requests, the
  security name is the same as the username.
#+end_quote

GROUPNAME

#+begin_quote
  A string identifying the group that this entry (i.e. security
  name/model pair) should belong to. This group name will then be
  referenced in the access table (see *createAccess* below).
#+end_quote

** deleteSec2Group MODEL SECURITYNAME
Delete an entry from the SNMPv3 security name to group table, thus
removing access control settings for the given principal. The entry to
be removed is indexed by the MODEL and SECURITYNAME values, which should
match those used in the corresponding *createSec2Group* command (or
equivalent).

** createView [-Ce] NAME SUBTREE MASK
Create an entry in the SNMPv3 MIB view table. A MIB view consists of a
family of view subtrees which may be individually included in or
(occasionally) excluded from the view. Each view subtree is defined by a
combination of an OID subtree together with a bit string mask. The view
table is indexed by the view name and subtree OID values.

[-Ce]

#+begin_quote
  An optional flag to indicate that this view subtree should be excluded
  from the named view. If not specified, the default is to include the
  subtree in the view. When constructing a view from a mixture of
  included and excluded subtrees, the excluded subtrees should be
  defined first - particularly if the named view is already referenced
  in one or more access entries.
#+end_quote

NAME

#+begin_quote
  A string identifying a particular MIB view, of which this OID
  subtree/mask forms part (possibly the only part).
#+end_quote

SUBTREE

#+begin_quote
  The OID defining the root of the subtree to add to (or exclude from)
  the named view.
#+end_quote

MASK

#+begin_quote
  A bit mask indicating which sub-identifiers of the associated subtree
  OID should be regarded as significant.
#+end_quote

** deleteView NAME SUBTREE
Delete an entry from the SNMPv3 view table, thus removing the subtree
from the given MIB view. Removing the final (or only) subtree will
result in the deletion of the view. The entry to be removed is indexed
by the NAME and SUBTREE values, which should match those used in the
corresponding *createView* command (or equivalent).

When removing subtrees from a mixed view (i.e. containing both included
and excluded subtrees), the included subtrees should be removed first.

** createAccess GROUPNAME [CONTEXTPREFIX] MODEL LEVEL CONTEXTMATCH
READVIEW WRITEVIEW NOTIFYVIEW
Create an entry in the SNMPv3 access table, thus allowing a certain
level of access to particular MIB views for the principals in the
specified group (given suitable security model and levels in the
request). The access table is indexed by the group name, context prefix,
security model and security level values.

GROUPNAME

#+begin_quote
  The name of the group that this access entry applies to (as set up by
  a *createSec2Group* command, or equivalent)
#+end_quote

CONTEXTPREFIX

#+begin_quote
  A string representing a context name (or collection of context names)
  which this access entry applies to. The interpretation of this string
  depends on the value of the CONTEXTMATCH field (see below).

  If omitted, this will default to the null context "".
#+end_quote

MODEL

#+begin_quote
  An integer representing the security model, taking one of the
  following values:\\
  1 - reserved for SNMPv1\\
  2 - reserved for SNMPv2c\\
  3 - User-based Security Model (USM)
#+end_quote

LEVEL

#+begin_quote
  An integer representing the minimal security level, taking one of the
  following values:\\
  1 - noAuthNoPriv\\
  2 - authNoPriv\\
  3 - authPriv

  This access entry will be applied to requests of this level or higher
  (where authPriv is higher than authNoPriv which is in turn higher than
  noAuthNoPriv).
#+end_quote

CONTEXTMATCH

#+begin_quote
  Indicates how to interpret the CONTEXTPREFIX value. If this field has
  the value '1' (representing 'exact') then the context name of a
  request must match the CONTEXTPREFIX value exactly for this access
  entry to be applicable to that request.

  If this field has the value '2' (representing 'prefix') then the
  initial substring of the context name of a request must match the
  CONTEXTPREFIX value for this access entry to be applicable to that
  request. This provides a simple form of wildcarding.
#+end_quote

READVIEW

#+begin_quote
  The name of the MIB view (as set up by *createView* or equivalent)
  defining the MIB objects for which this request may request the
  current values.

  If there is no view with this name, then read access is not granted.
#+end_quote

WRITEVIEW

#+begin_quote
  The name of the MIB view (as set up by *createView* or equivalent)
  defining the MIB objects for which this request may potentially SET
  new values.

  If there is no view with this name, then read access is not granted.
#+end_quote

NOTIFYVIEW

#+begin_quote
  The name of the MIB view (as set up by *createView* or equivalent)
  defining the MIB objects which may be included in notification
  request.

  Note that this aspect of access control is not currently supported.
#+end_quote

** deleteAccess GROUPNAME [CONTEXTPREFIX] MODEL LEVEL
Delete an entry from the SNMPv3 access table, thus removing the
specified access control settings. The entry to be removed is indexed by
the group name, context prefix, security model and security level
values, which should match those used in the corresponding
*createAccess* command (or equivalent).

** createAuth GROUPNAME [CONTEXTPREFIX] MODEL LEVEL AUTHTYPE
CONTEXTMATCH VIEW
Create an entry in the Net-SNMP extension to the standard access table,
thus allowing a certain type of access to the MIB view for the
principals in the specified group. The interpretation of GROUPNAME,
CONTEXTPREFIX, MODEL, LEVEL and CONTEXTMATCH are the same as for the
*createAccess* directive. The extension access table is indexed by the
group name, context prefix, security model, security level and authtype
values.

AUTHTYPE

#+begin_quote
  The style of access that this entry should be applied to. See
  /snmpd.conf(5)/ and /snmptrapd.conf(5)/ for details of valid tokens.
#+end_quote

VIEW

#+begin_quote
  The name of the MIB view (as set up by *createView* or equivalent)
  defining the MIB objects for which this style of access is authorized.
#+end_quote

** deleteAuth GROUPNAME [CONTEXTPREFIX] MODEL LEVEL AUTHTYPE
Delete an entry from the extension access table, thus removing the
specified access control settings. The entry to be removed is indexed by
the group name, context prefix, security model, security level and
authtype values, which should match those used in the corresponding
*createAuth* command (or equivalent).

Note that *snmpget* REQUIRES an argument specifying the agent to query
as described in the .I snmpcmd(1) manual page.

* EXAMPLES
Given a pre-existing user /dave/ (which could be set up using the
/snmpusm(1)/ command), we could configure full read-write access to the
whole OID tree using the commands:

#+begin_quote
  snmpvacm localhost createSec2Group 3 dave RWGroup

  snmpvacm localhost createView all .1 80

  snmpvacm localhost createAccess RWGroup 3 1 1 all all none
#+end_quote

This creates a new security group named "RWGroup" containing the SNMPv3
user "dave", a new view "all" containing the full OID tree based on
/.iso(1)/ , and then allows those users in the group "RWGroup" (i.e.
"dave") both read- and write-access to the view "all" (i.e. the full OID
tree) when using authenticated SNMPv3 requests.

As a second example, we could set up read-only access to a portion of
the OID tree using the commands:

#+begin_quote
  snmpvacm localhost createSec2Group 3 wes ROGroup

  snmpvacm localhost createView sysView system fe

  snmpvacm localhost createAccess ROGroup 3 0 1 sysView none none
#+end_quote

This creates a new security group named "ROGroup" containing the
(pre-existing) user "wes", a new view "sysView" containing just the OID
tree based on /.iso(1).org(3).dod(6).inet(1).mgmt(2).mib-2(1).system(1)/
, and then allows those users in the group "ROGroup" (i.e. "wes")
read-access, but not write-access to the view "sysView" (i.e. the system
group).

* EXIT STATUS
The following exit values are returned:

0 - Successful completion

1 - A usage syntax error (which displays a suitable usage message) or a
request timeout.

2 - An error occurred while executing the command (which also displays a
suitable error message).

* LIMITATIONS
This utility does not support the configuration of new community
strings, so is only of use for setting up new access control for SNMPv3
requests. It can be used to amend the access settings for existing
community strings, but not to set up new ones.

The use of numeric parameters for *secLevel* and *contextMatch*
parameters is less than intuitive. These commands do not provide the
full flexibility of the equivalent config file directives.

There is (currently) no equivalent to the one-shot configure directives
/rouser/ and /rwuser./

* SEE ALSO
snmpcmd(1), snmpusm(1), snmpd.conf(5), snmp.conf(5), RFC 2575, Net-SNMP
project FAQ
