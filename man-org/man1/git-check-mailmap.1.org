#+TITLE: Man1 - git-check-mailmap.1
#+DESCRIPTION: Linux manpage for git-check-mailmap.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-check-mailmap - Show canonical names and email addresses of contacts

* SYNOPSIS
#+begin_example
  git check-mailmap [<options>] <contact>...
#+end_example

* DESCRIPTION
For each “Name <user@host>” or “<user@host>” from the command-line or
standard input (when using *--stdin*), look up the person's canonical
name and email address (see "Mapping Authors" below). If found, print
them; otherwise print the input as-is.

* OPTIONS
--stdin

#+begin_quote
  Read contacts, one per line, from the standard input after exhausting
  contacts provided on the command-line.
#+end_quote

* OUTPUT
For each contact, a single line is output, terminated by a newline. If
the name is provided or known to the /mailmap/, “Name <user@host>” is
printed; otherwise only “<user@host>” is printed.

* CONFIGURATION
See *mailmap.file* and *mailmap.blob* in *git-config*(1) for how to
specify a custom *.mailmap* target file or object.

* MAPPING AUTHORS
See *gitmailmap*(5).

* GIT
Part of the *git*(1) suite
