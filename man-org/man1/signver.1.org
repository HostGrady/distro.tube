#+TITLE: Man1 - signver.1
#+DESCRIPTION: Linux manpage for signver.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
signver - Verify a detached PKCS#7 signature for a file.

* SYNOPSIS
*signtool* -A | -V -d /directory/ [-a] [-i /input_file/] [-o
/output_file/] [-s /signature_file/] [-v]

* STATUS
This documentation is still work in progress. Please contribute to the
initial review in *Mozilla NSS bug 836477*[1]

* DESCRIPTION
The Signature Verification Tool, *signver*, is a simple command-line
utility that unpacks a base-64-encoded PKCS#7 signed object and verifies
the digital signature using standard cryptographic techniques. The
Signature Verification Tool can also display the contents of the signed
object.

* OPTIONS
-A

#+begin_quote
  Displays all of the information in the PKCS#7 signature.
#+end_quote

-V

#+begin_quote
  Verifies the digital signature.
#+end_quote

-d /directory/

#+begin_quote
  Specify the database directory which contains the certificates and
  keys.

  *signver* supports two types of databases: the legacy security
  databases (cert8.db, key3.db, and secmod.db) and new SQLite databases
  (cert9.db, key4.db, and pkcs11.txt). If the prefix *dbm:* is not used,
  then the tool assumes that the given databases are in the SQLite
  format.
#+end_quote

-a

#+begin_quote
  Sets that the given signature file is in ASCII format.
#+end_quote

-i /input_file/

#+begin_quote
  Gives the input file for the object with signed data.
#+end_quote

-o /output_file/

#+begin_quote
  Gives the output file to which to write the results.
#+end_quote

-s /signature_file/

#+begin_quote
  Gives the input file for the digital signature.
#+end_quote

-v

#+begin_quote
  Enables verbose output.
#+end_quote

* EXTENDED EXAMPLES
** Verifying a Signature
The *-V* option verifies that the signature in a given signature file is
valid when used to sign the given object (from the input file).

#+begin_quote
  #+begin_example
    signver -V -s signature_file -i signed_file -d /home/my/sharednssdb

    signatureValid=yes
  #+end_example
#+end_quote

** Printing Signature Data
The *-A* option prints all of the information contained in a signature
file. Using the *-o* option prints the signature file information to the
given output file rather than stdout.

#+begin_quote
  #+begin_example
    signver -A -s signature_file -o output_file
  #+end_example
#+end_quote

* NSS DATABASE TYPES
NSS originally used BerkeleyDB databases to store security information.
The last versions of these /legacy/ databases are:

#+begin_quote
  ·

  cert8.db for certificates
#+end_quote

#+begin_quote
  ·

  key3.db for keys
#+end_quote

#+begin_quote
  ·

  secmod.db for PKCS #11 module information
#+end_quote

BerkeleyDB has performance limitations, though, which prevent it from
being easily used by multiple applications simultaneously. NSS has some
flexibility that allows applications to use their own, independent
database engine while keeping a shared database and working around the
access issues. Still, NSS requires more flexibility to provide a truly
shared security database.

In 2009, NSS introduced a new set of databases that are SQLite databases
rather than BerkleyDB. These new databases provide more accessibility
and performance:

#+begin_quote
  ·

  cert9.db for certificates
#+end_quote

#+begin_quote
  ·

  key4.db for keys
#+end_quote

#+begin_quote
  ·

  pkcs11.txt, which is listing of all of the PKCS #11 modules contained
  in a new subdirectory in the security databases directory
#+end_quote

Because the SQLite databases are designed to be shared, these are the
/shared/ database type. The shared database type is preferred; the
legacy format is included for backward compatibility.

By default, the tools (*certutil*, *pk12util*, *modutil*) assume that
the given security databases use the SQLite type Using the legacy
databases must be manually specified by using the *dbm:* prefix with the
given security directory. For example:

#+begin_quote
  #+begin_example
    # signver -A -s signature -d dbm:/home/my/sharednssdb
  #+end_example
#+end_quote

To set the legacy database type as the default type for the tools, set
the *NSS_DEFAULT_DB_TYPE* environment variable to *dbm*:

#+begin_quote
  #+begin_example
    export NSS_DEFAULT_DB_TYPE="dbm"
  #+end_example
#+end_quote

This line can be added to the ~/.bashrc file to make the change
permanent for the user.

#+begin_quote
  ·

  https://wiki.mozilla.org/NSS_Shared_DB_Howto
#+end_quote

For an engineering draft on the changes in the shared NSS databases, see
the NSS project wiki:

#+begin_quote
  ·

  https://wiki.mozilla.org/NSS_Shared_DB
#+end_quote

* SEE ALSO
signtool (1)

The NSS wiki has information on the new database design and how to
configure applications to use it.

#+begin_quote
  ·

  Setting up the shared NSS database

  https://wiki.mozilla.org/NSS_Shared_DB_Howto
#+end_quote

#+begin_quote
  ·

  Engineering and technical information about the shared NSS database

  https://wiki.mozilla.org/NSS_Shared_DB
#+end_quote

* ADDITIONAL RESOURCES
For information about NSS and other tools related to NSS (like JSS),
check out the NSS project wiki at
*http://www.mozilla.org/projects/security/pki/nss/*. The NSS site
relates directly to NSS code changes and releases.

Mailing lists: https://lists.mozilla.org/listinfo/dev-tech-crypto

IRC: Freenode at #dogtag-pki

* AUTHORS
The NSS tools were written and maintained by developers with Netscape,
Red Hat, Sun, Oracle, Mozilla, and Google.

Authors: Elio Maldonado <emaldona@redhat.com>, Deon Lackey
<dlackey@redhat.com>.

* LICENSE
Licensed under the Mozilla Public License, v. 2.0. If a copy of the MPL
was not distributed with this file, You can obtain one at
http://mozilla.org/MPL/2.0/.

* NOTES
-  1. :: Mozilla NSS bug 836477

  https://bugzilla.mozilla.org/show_bug.cgi?id=836477
