#+TITLE: Man1 - doas.1
#+DESCRIPTION: Linux manpage for doas.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

utility executes the given command as another user. The

argument is mandatory unless

or

is specified.

The user will be required to authenticate by entering their password,
unless configured otherwise.

By default, a new environment is created. The variables

and

and the

are set to values appropriate for the target user.

is set to the name of the user executing

The variables

and

are inherited from the current environment. This behavior may be
modified by the config file. The working directory is not changed.

The options are as follows:

Parse and check the configuration file

then exit. If

is supplied,

will also perform command matching. In the latter case either

or

will be printed on standard output, depending on command matching
results. No command is executed.

Clear any persisted authentications from previous invocations, then
immediately exit. No command is executed.

Non interactive mode, fail if the matching rule doesn't have the

option.

Execute the shell from

or

Execute the command as

The default is root.

It may fail for one of the following reasons:

The config file

could not be parsed.

The user attempted to run a command which is not permitted.

The password was incorrect.

The specified command was not found or is not executable.

The

command first appeared in
