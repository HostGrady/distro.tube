#+TITLE: Man1 - isql.1
#+DESCRIPTION: Linux manpage for isql.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
isql, iusql - unixODBC command-line interactive SQL tool

* SYNOPSIS
*isql* /DSN/ [/USER/ [/PASSWORD/]] [/options/]

* DESCRIPTION
*isql* is a command line tool which allows the user to execute SQL in
batch or interactively. It has some interesting options such as an
option to generate output wrapped in an HTML table.

*iusql* is the same tool with built-in Unicode support. Some datasources
only work with *iusql*.

* ARGUMENTS
- DSN :: The Data Source Name, which should be used to make connection
  to the database. The data source is looked for in the /etc/odbc.ini
  and $HOME/.odbc.ini files in that order, with the latter overwriting
  the former.

A bare name is looked up in the above files. If the DSN name begins with
a semicolon then it's treated as a connection string instead. The
connection string may contain a DSN name and/or other
semicolon-separated parameters.

- USER :: Specifies the database user/role under which the connection
  should be made.

Overrides any *UID* specified in the DSN.

- PASSWORD :: Password for the specified *USER*.

Overrides any *Password* specified in the DSN.

* OPTIONS
- -b :: Run isql in non-interactive batch mode. In this mode, the isql
  processes its standard input, expecting one SQL command per line.

- -dDELIMITER :: Delimits columns with /delimiter/.

- -xHEX :: Delimits columns with /HEX/, which is a hexadecimal code of
  the delimiting character in the format 0xNN - i.e. 0x09 for the TAB
  character.

- -w :: Format the result as HTML table.

- -c :: Output the names of the columns on the first row. Has any effect
  only with the *-d* or *-x* options.

- -mNUM :: Limit the column display width to /NUM/ characters.

- -lLOCALE :: Sets locale to /LOCALE/.

- -q :: Wrap the character fields in double quotes.

- -3 :: Use the ODBC 3 calls.

- -n :: Use the new line processing. (multiple lines of SQL, terminated
  with command GO).

- -e :: Use the SQLExecDirect instead of Prepare.

- -k :: Use SQLDriverConnect.

- -v :: Turn on the verbose mode, where the errors are fully described.
  Useful for debugging.

- --version :: Prints the program version and exits.

- -LNUM :: Alter the maximum number of characters displayed from a
  character field to /NUM/ characters. Default is 300.

* COMMANDS
This section briefly describes some isql runtime commands.

*help*

#+begin_quote
  List all tables in the database.
#+end_quote

*help /table/*

#+begin_quote
  List all columns in the /table/.
#+end_quote

*help help*

#+begin_quote
  List all help options.
#+end_quote

* EXAMPLES
- A bare DSN name: :: 

#+begin_example
  $ iusql WebDB MyID MyPWD -w -b < My.sql
#+end_example

Connects to the WebDB as user MyID with password MyPWD, then execute the
commands in the My.sql file and returns the results wrapped in HTML
table. Each line in My.sql must contain exactly 1 SQL command, except
for the last line, which must be blank (unless the *-n* option is
specified).

- A DSN name in a connection string: :: Note the leading semicolon on
  the connection string:

#+begin_example
  $ iusql ";DSN=WebDB" MyID MyPWD -w -b < My.sql
#+end_example

Options in the DSN may be overridden in the connection string:

#+begin_example
  $ iusql ";DSN=WebDB;Driver=PostgreSQL ODBC;UID=MyID;PASSWORD=secret;Debug=1;CommLog=1" -v
#+end_example

- A string DSN: :: A string DSN may be provided in its entirety, with no
  file DSN reference at all:

#+begin_example
  $ iusql ";Driver=PostgreSQL Unicode;UID=MyID;PASSWORD=secret" -v
#+end_example

* TROUBLESHOOTING
- Cryptic error messages :: Re-run *iusql* or *isql* with the *-v* flag
  to get more detail from errors, and/or enable *Trace* mode in
  *odbcinst.ini*.

- Missing driver definition :: Check that the driver name specified by
  the *Driver* entry in the *odbc.ini* data-source definition is present
  in *odbcinst.ini* and exactly matches the odbcinst /[section name]/.

- Unloadable or incompatible driver :: If the driver is properly
  specified for the datasource it's possible that the driver may not be
  loadable. Check for mixups between Unicode and ANSI drivers. Verify
  the driver paths in the *odbcinst.ini* section name.

- Unicode datasources with ANSI clients :: Some datasources are
  Unicode-only and only work with *iusql*. If *isql* reports

#+begin_example
    [IM002][unixODBC][Driver Manager]Data source name not found and no default driver specified
    [ISQL]ERROR: Could not SQLConnect
#+end_example

but the datasource is listed by

#+begin_example
    odbcinst -q -d
#+end_example

and the driver it uses is listed by

#+begin_example
    odbcinst -q -d
#+end_example

then try *iusql*.

* FILES
//etc/odbc.ini/

#+begin_quote
  System-wide DSN definitions. See *odbc.ini*(5) for details.
#+end_quote

/$HOME/.odbc.ini/

#+begin_quote
  User-specific DSN definitions. See *odbc.ini*(5) for details.
#+end_quote

* SEE ALSO
*unixODBC*(7), *odbcinst*(1), *odbc.ini*(5)

®

* AUTHORS
The authors of unixODBC are Peter Harvey </pharvey@codebydesign.com/>
and Nick Gorham </nick@lurcher.org/>. For the full list of contributors
see the AUTHORS file.

* COPYRIGHT
unixODBC is licensed under the GNU Lesser General Public License. For
details about the license, see the COPYING file.
