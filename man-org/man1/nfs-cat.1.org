#+TITLE: Man1 - nfs-cat.1
#+DESCRIPTION: Linux manpage for nfs-cat.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
nfs-cat - Utility to read a file off NFS

* SYNOPSIS
*nfs-cat <NFS-URL>*

* DESCRIPTION
nfs-cat is a utility to read a file off an NFS server.

Example: Print the content of a file:

#+begin_quote
  #+begin_example
    $ nfs-cat nfs://127.0.0.1/data/tmp/foo.c
          
  #+end_example
#+end_quote

* SEE ALSO
*http://github.com/sahlberg/libnfs*
