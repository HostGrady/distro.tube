#+TITLE: Man1 - xdg-open.1
#+DESCRIPTION: Linux manpage for xdg-open.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xdg-open - opens a file or URL in the users preferred application

* SYNOPSIS
*xdg-open* {/file/ | /URL/}

*xdg-open* {*--help* | *--manual* | *--version*}

* DESCRIPTION
xdg-open opens a file or URL in the users preferred application. If a
URL is provided the URL will be opened in the users preferred web
browser. If a file is provided the file will be opened in the preferred
application for files of that type. xdg-open supports file, ftp, http
and https URLs.

xdg-open is for use inside a desktop session only. It is not recommended
to use xdg-open as root.

* OPTIONS
*--help*

#+begin_quote
  Show command synopsis.
#+end_quote

*--manual*

#+begin_quote
  Show this manual page.
#+end_quote

*--version*

#+begin_quote
  Show the xdg-utils version information.
#+end_quote

* EXIT CODES
An exit code of 0 indicates success while a non-zero exit code indicates
failure. The following failure codes can be returned:

*1*

#+begin_quote
  Error in command line syntax.
#+end_quote

*2*

#+begin_quote
  One of the files passed on the command line did not exist.
#+end_quote

*3*

#+begin_quote
  A required tool could not be found.
#+end_quote

*4*

#+begin_quote
  The action failed.
#+end_quote

* SEE ALSO
*xdg-mime*(1), *xdg-settings*(1), *MIME applications associations
specification*[1]

* EXAMPLES

#+begin_quote
  #+begin_example
    xdg-open http://www.freedesktop.org/
  #+end_example
#+end_quote

Opens the freedesktop.org website in the users default browser.

#+begin_quote
  #+begin_example
    xdg-open /tmp/foobar.png
  #+end_example
#+end_quote

Opens the PNG image file /tmp/foobar.png in the users default image
viewing application.

* AUTHORS
*Kevin Krammer*

#+begin_quote
  Author.
#+end_quote

*Jeremy White*

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2006\\

* NOTES
-  1. :: MIME applications associations specification

  http://www.freedesktop.org/wiki/Specifications/mime-apps-spec/
