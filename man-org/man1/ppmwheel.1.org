#+TITLE: Man1 - ppmwheel.1
#+DESCRIPTION: Linux manpage for ppmwheel.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmwheel - make a PPM image of a color wheel

* SYNOPSIS
*ppmwheel* /diameter/

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmwheel* produces a PPM image of a color wheel of the specified
diameter inside a white square just large enough to hold it.

The color wheel is based on the HSV color model. Hues are distributed
angularly around the circle and the values are distributed radially and
the saturation is zero everywhere. The values are zero at the center,
increasing linearly to maximum at the edge. The maximum value
corresponds to the maxval of the PPM image.

Hence, the image contains all of the secondary colors based on the red,
green, and blue primary colors. A secondary color is one that is
composed of light of at most two of the three primary colors.

* OPTIONS
None.

* SEE ALSO
*ppmcie*(1) , *ppmrainbow*(1) , *ppm*(5)

* HISTORY
*ppmwheel* was added to Netpbm in Release 10.14 (March 2003).

* AUTHOR
Copyright (C) 1995 by Peter Kirchgessner
