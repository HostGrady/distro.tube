#+TITLE: Man1 - broadwayd.1
#+DESCRIPTION: Linux manpage for broadwayd.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
broadwayd - Broadway display server

* SYNOPSIS
*broadwayd* [--port /PORT/] [--address /ADDRESS/] [--unixsocket
/ADDRESS/] [/:DISPLAY/]

* DESCRIPTION
*broadwayd* is a display server for the Broadway GDK backend. It allows
multiple GTK+ applications to display their windows in the same web
browser, by connecting to broadwayd.

When using broadwayd, specify the display number to use, prefixed with a
colon, similar to X. The default display number is 0.

#+begin_quote
  #+begin_example
    broadwayd :5
  #+end_example
#+end_quote

Then point your web browser at http://127.0.0.1:8085. Start your
applications like this:

#+begin_quote
  #+begin_example
    GDK_BACKEND=broadway BROADWAY_DISPLAY=:5 gtk3-demo
  #+end_example
#+end_quote

You can add password protection for your session by creating a file in
$XDG_CONFIG_HOME/broadway.passwd or $HOME/.config/broadway.passwd with a
crypt(3) style password hash. A simple way to generate it is with
openssl:

#+begin_quote
  #+begin_example
    openssl passwd -1  > ~/.config/broadway.passwd
  #+end_example
#+end_quote

* OPTIONS
--port

#+begin_quote
  Use /PORT/ as the HTTP port, instead of the default 8080 +
  (/DISPLAY/ - 1).
#+end_quote

--address

#+begin_quote
  Use /ADDRESS/ as the HTTP address, instead of the default
  http://127.0.0.1:/PORT/.
#+end_quote

--unixsocket

#+begin_quote
  Use /ADDRESS/ as the unix domain socket address. This option overrides
  --address and --port. It is available only on Unix-like systems.
#+end_quote
