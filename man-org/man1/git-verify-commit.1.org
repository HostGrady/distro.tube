#+TITLE: Man1 - git-verify-commit.1
#+DESCRIPTION: Linux manpage for git-verify-commit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-verify-commit - Check the GPG signature of commits

* SYNOPSIS
#+begin_example
  git verify-commit <commit>...
#+end_example

* DESCRIPTION
Validates the GPG signature created by /git commit -S/.

* OPTIONS
--raw

#+begin_quote
  Print the raw gpg status output to standard error instead of the
  normal human-readable output.
#+end_quote

-v, --verbose

#+begin_quote
  Print the contents of the commit object before validating it.
#+end_quote

<commit>...

#+begin_quote
  SHA-1 identifiers of Git commit objects.
#+end_quote

* GIT
Part of the *git*(1) suite
