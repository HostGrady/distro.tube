#+TITLE: Man1 - pgmoil.1
#+DESCRIPTION: Linux manpage for pgmoil.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*pgmoil* - replaced by pamoil

* DESCRIPTION
This program is part of *Netpbm*(1)

*pgmoil* was replaced in Netpbm 9.16 (July 2001) by *pamoil*(1)

*pamoil* is backward compatible with *pgmoil*, but works on color images
too.
