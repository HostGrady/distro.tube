#+TITLE: Man1 - llvm-readobj.1
#+DESCRIPTION: Linux manpage for llvm-readobj.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-readobj - LLVM Object Reader

* SYNOPSIS
*llvm-readobj* [/options/] [/input.../]

* DESCRIPTION
The *llvm-readobj* tool displays low-level format-specific information
about one or more object files.

If *input* is "*-*", *llvm-readobj* reads from standard input.
Otherwise, it will read from the specified *filenames*.

* DIFFERENCES TO LLVM-READELF
*llvm-readelf* is an alias for the *llvm-readobj* tool with a slightly
different command-line interface and output that is GNU compatible.
Following is a list of differences between *llvm-readelf* and
*llvm-readobj*:

#+begin_quote

  - *llvm-readelf* uses /GNU/ for the /--elf-output-style/ option by
    default. *llvm-readobj* uses /LLVM/.

  - *llvm-readelf* allows single-letter grouped flags (e.g.
    *llvm-readelf -SW* is the same as *llvm-readelf -S -W*).
    *llvm-readobj* does not allow grouping.

  - *llvm-readelf* provides /-s/ as an alias for /--symbols/, for GNU
    *readelf* compatibility, whereas it is an alias for
    /--section-headers/ in *llvm-readobj*.

  - *llvm-readobj* provides *-t* as an alias for /--symbols/.
    *llvm-readelf* does not.

  - *llvm-readobj* provides *--sr*, *--sd*, *--st* and *--dt* as aliases
    for /--section-relocations/, /--section-data/, /--section-symbols/
    and /--dyn-symbols/ respectively. *llvm-readelf* does not provide
    these aliases, to avoid conflicting with grouped flags.
#+end_quote

* GENERAL AND MULTI-FORMAT OPTIONS
These options are applicable to more than one file format, or are
unrelated to file formats.

#+begin_quote
  - *--all* :: Equivalent to specifying all the main display options
    relevant to the file format.
#+end_quote

#+begin_quote
  - *--addrsig* :: Display the address-significance table.
#+end_quote

#+begin_quote
  - *--expand-relocs* :: When used with /--relocs/, display each
    relocation in an expanded multi-line format.
#+end_quote

#+begin_quote
  - *--file-header, -h* :: Display file headers.
#+end_quote

#+begin_quote
  - *--headers, -e* :: Equivalent to setting: /--file-header/,
    /--program-headers/, and /--sections/.
#+end_quote

#+begin_quote
  - *--help* :: Display a summary of command line options.
#+end_quote

#+begin_quote
  - *--hex-dump=<section[,section,...]>, -x* :: Display the specified
    section(s) as hexadecimal bytes. *section* may be a section index or
    section name.
#+end_quote

#+begin_quote
  - *--needed-libs* :: Display the needed libraries.
#+end_quote

#+begin_quote
  - *--relocations, --relocs, -r* :: Display the relocation entries in
    the file.
#+end_quote

#+begin_quote
  - *--sections, --section-headers, -S* :: Display all sections.
#+end_quote

#+begin_quote
  - *--section-data, --sd* :: When used with /--sections/, display
    section data for each section shown. This option has no effect for
    GNU style output.
#+end_quote

#+begin_quote
  - *--section-relocations, --sr* :: When used with /--sections/,
    display relocations for each section shown. This option has no
    effect for GNU style output.
#+end_quote

#+begin_quote
  - *--section-symbols, --st* :: When used with /--sections/, display
    symbols for each section shown. This option has no effect for GNU
    style output.
#+end_quote

#+begin_quote
  - *--stackmap* :: Display contents of the stackmap section.
#+end_quote

#+begin_quote
  - *--string-dump=<section[,section,...]>, -p* :: Display the specified
    section(s) as a list of strings. *section* may be a section index or
    section name.
#+end_quote

#+begin_quote
  - *--string-table* :: Display contents of the string table.
#+end_quote

#+begin_quote
  - *--symbols, --syms, -s* :: Display the symbol table.
#+end_quote

#+begin_quote
  - *--unwind, -u* :: Display unwind information.
#+end_quote

#+begin_quote
  - *--version* :: Display the version of the *llvm-readobj* executable.
#+end_quote

#+begin_quote
  - *@<FILE>* :: Read command-line options from response file /<FILE>/.
#+end_quote

* ELF SPECIFIC OPTIONS
The following options are implemented only for the ELF file format.

#+begin_quote
  - *--arch-specific, -A* :: Display architecture-specific information,
    e.g. the ARM attributes section on ARM.
#+end_quote

#+begin_quote
  - *--bb-addr-map* :: Display the contents of the basic block address
    map section(s), which contain the address of each function, along
    with the relative offset of each basic block.
#+end_quote

#+begin_quote
  - *--demangle, -C* :: Display demangled symbol names in the output.
#+end_quote

#+begin_quote
  - *--dependent-libraries* :: Display the dependent libraries section.
#+end_quote

#+begin_quote
  - *--dyn-relocations* :: Display the dynamic relocation entries.
#+end_quote

#+begin_quote
  - *--dyn-symbols, --dyn-syms, --dt* :: Display the dynamic symbol
    table.
#+end_quote

#+begin_quote
  - *--dynamic-table, --dynamic, -d* :: Display the dynamic table.
#+end_quote

#+begin_quote
  - *--cg-profile* :: Display the callgraph profile section.
#+end_quote

#+begin_quote
  - *--histogram, -I* :: Display a bucket list histogram for dynamic
    symbol hash tables.
#+end_quote

#+begin_quote
  - *--elf-linker-options* :: Display the linker options section.
#+end_quote

#+begin_quote
  - *--elf-output-style=<value>* :: Format ELF information in the
    specified style. Valid options are *LLVM* and *GNU*. *LLVM* output
    (the default) is an expanded and structured format, whilst *GNU*
    output mimics the equivalent GNU *readelf* output.
#+end_quote

#+begin_quote
  - *--section-groups, -g* :: Display section groups.
#+end_quote

#+begin_quote
  - *--gnu-hash-table* :: Display the GNU hash table for dynamic
    symbols.
#+end_quote

#+begin_quote
  - *--hash-symbols* :: Display the expanded hash table with dynamic
    symbol data.
#+end_quote

#+begin_quote
  - *--hash-table* :: Display the hash table for dynamic symbols.
#+end_quote

#+begin_quote
  - *--notes, -n* :: Display all notes.
#+end_quote

#+begin_quote
  - *--program-headers, --segments, -l* :: Display the program headers.
#+end_quote

#+begin_quote
  - *--raw-relr* :: Do not decode relocations in RELR relocation
    sections when displaying them.
#+end_quote

#+begin_quote
  - *--section-mapping* :: Display the section to segment mapping.
#+end_quote

#+begin_quote
  - *--stack-sizes* :: Display the contents of the stack sizes
    section(s), i.e. pairs of function names and the size of their stack
    frames. Currently only implemented for GNU style output.
#+end_quote

#+begin_quote
  - *--version-info, -V* :: Display version sections.
#+end_quote

* MACH-O SPECIFIC OPTIONS
The following options are implemented only for the Mach-O file format.

#+begin_quote
  - *--macho-data-in-code* :: Display the Data in Code command.
#+end_quote

#+begin_quote
  - *--macho-dsymtab* :: Display the Dsymtab command.
#+end_quote

#+begin_quote
  - *--macho-indirect-symbols* :: Display indirect symbols.
#+end_quote

#+begin_quote
  - *--macho-linker-options* :: Display the Mach-O-specific linker
    options.
#+end_quote

#+begin_quote
  - *--macho-segment* :: Display the Segment command.
#+end_quote

#+begin_quote
  - *--macho-version-min* :: Display the version min command.
#+end_quote

* PE/COFF SPECIFIC OPTIONS
The following options are implemented only for the PE/COFF file format.

#+begin_quote
  - *--codeview* :: Display CodeView debug information.
#+end_quote

#+begin_quote
  - *--codeview-ghash* :: Enable global hashing for CodeView type stream
    de-duplication.
#+end_quote

#+begin_quote
  - *--codeview-merged-types* :: Display the merged CodeView type
    stream.
#+end_quote

#+begin_quote
  - *--codeview-subsection-bytes* :: Dump raw contents of CodeView debug
    sections and records.
#+end_quote

#+begin_quote
  - *--coff-basereloc* :: Display the .reloc section.
#+end_quote

#+begin_quote
  - *--coff-debug-directory* :: Display the debug directory.
#+end_quote

#+begin_quote
  - *--coff-tls-directory* :: Display the TLS directory.
#+end_quote

#+begin_quote
  - *--coff-directives* :: Display the .drectve section.
#+end_quote

#+begin_quote
  - *--coff-exports* :: Display the export table.
#+end_quote

#+begin_quote
  - *--coff-imports* :: Display the import table.
#+end_quote

#+begin_quote
  - *--coff-load-config* :: Display the load config.
#+end_quote

#+begin_quote
  - *--coff-resources* :: Display the .rsrc section.
#+end_quote

* EXIT STATUS
*llvm-readobj* returns 0 under normal operation. It returns a non-zero
exit code if there were any errors.

* SEE ALSO
*llvm-nm(1)*, *llvm-objdump(1)*, *llvm-readelf(1)*

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
