#+TITLE: Man1 - pnminvert.1
#+DESCRIPTION: Linux manpage for pnminvert.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnminvert - invert a PNM image

* SYNOPSIS
*pnminvert*

[/pnmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnminvert* reads a PNM image as input, inverts it black for white, and
produces a PNM image as output.

If the image is grayscale, *pnminvert* replaces a pixel with one of
complementary brightness, i.e. if the original pixel has gamma-adjusted
gray value G, the output pixel has gray value maxval - G.

If the image is color, *pnminvert* inverts each individual RGB component
the same as for a grayscale image.

* SEE ALSO
*pnm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
