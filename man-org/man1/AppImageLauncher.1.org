#+TITLE: Man1 - AppImageLauncher.1
#+DESCRIPTION: Linux manpage for AppImageLauncher.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
AppImageLauncher - Desktop integration helper for AppImages, for use by
Linux distributions.

* SYNOPSIS
AppImageLauncher [PATH]

* DESCRIPTION
Helper application for Linux distributions serving as a kind of "entry
point" for running and integrating AppImages.

AppImage provides a way for upstream developers to provide “native”
binaries for Linux users just the same way they could do for other
operating systems. It allows for packaging applications for any common
Linux based operating system, e.g., Debian. AppImages come with all
dependencies that cannot be assumed to be part of each target system in
a recent enough version and will run on most Linux distributions without
further modifications.

AppImageLauncher makes your Linux desktop AppImage ready™. You can
integrate AppImages with a single mouse click, and manage them from your
application launcher. Install AppImageLauncher today for your
distribution and enjoy using AppImages as easy as never before!

* OPTIONS
- --appimagelauncher-help :: Display this help and exit

- --appimagelauncher-version :: Display version and exit

* ARGUMENTS
Path to AppImage (mandatory)

* BUGS
No known bugs.

* AUTHOR
TheAssassin <theassassin@assassinate-you.net>
