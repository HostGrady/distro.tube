#+TITLE: Man1 - lxclipboard.1
#+DESCRIPTION: Linux manpage for lxclipboard.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxclipboard - clipboard utility for LXSession and LXDE

* SYNOPSIS
*lxclipboard*

* DESCRIPTION
*lxclipboard* is a clipboard utility, to be able to do copy/paste
operation between GTK+ applications. lxcliboard is only useful when
lxsession is not compile with clipboard build-in support. If lxsession
is compile with clipboard build-in lxclipboard functionality will be
automatically launched, without the need to launch lxclipboard.

* AUTHORS
William Jon McCann <mccann@jhu.edu>

Matthias Clasen

Anders Carlsson

Rodrigo Moya

Nick Schermer <nick@xfce.org>

Julien Lavergne (gilir@ubuntu.com)

Man page written to conform with Debian by Julien Lavergne.
