#+TITLE: Man1 - calibre-customize.1
#+DESCRIPTION: Linux manpage for calibre-customize.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
calibre-customize - calibre-customize

#+begin_quote

  #+begin_quote
    #+begin_example
      calibre-customize options
    #+end_example
  #+end_quote
#+end_quote

Customize calibre by loading external plugins.

Whenever you pass arguments to *calibre-customize* that have spaces in
them, enclose the arguments in quotation marks. For example: "/some
path/with spaces"

* [OPTIONS]

#+begin_quote
  - *--add-plugin, -a* :: Add a plugin by specifying the path to the ZIP
    file containing it.
#+end_quote

#+begin_quote
  - *--build-plugin, -b* :: For plugin developers: Path to the folder
    where you are developing the plugin. This command will automatically
    zip up the plugin and update it in calibre.
#+end_quote

#+begin_quote
  - *--customize-plugin* :: Customize plugin. Specify name of plugin and
    customization string separated by a comma.
#+end_quote

#+begin_quote
  - *--disable-plugin* :: Disable the named plugin
#+end_quote

#+begin_quote
  - *--enable-plugin* :: Enable the named plugin
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--list-plugins, -l* :: List all installed plugins
#+end_quote

#+begin_quote
  - *--remove-plugin, -r* :: Remove a custom plugin by name. Has no
    effect on builtin plugins
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
