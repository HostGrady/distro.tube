#+TITLE: Man1 - t1reencode.1
#+DESCRIPTION: Linux manpage for t1reencode.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
t1reencode - re-encode a PostScript Type 1 font

* SYNOPSIS
*t1reencode* -e ENCODING [OPTIONS...] /font/ [/outputfile/]

* DESCRIPTION
*T1reencode* changes a PostScript Type 1 font's embedded encoding. The
re-encoded font is written to the standard output (but see the
*--output* option). If no input font file is supplied, *t1reencode*
reads a PFA or PFB font from the standard input.

* OPTIONS
- *--encoding*=/file/, *-e* /file/ :: Read the encoding from /file/,
  which must contain an encoding in **() format. Alternatively, /file/
  can be one of the following special names, in which case the
  corresponding standard encoding is used.

  - *Name* :: *Source*

  - StandardEncoding :: Adobe

  - ISOLatin1Encoding :: Adobe/ISO (synonym: ISO_8859_1_Encoding)

  - ExpertEncoding :: Adobe

  - ExpertSubsetEncoding :: Adobe

  - SymbolEncoding :: Adobe

  - ISOLatin2Encoding :: ISO (synonym: ISO_8859_2_Encoding)

  - ISOLatin3Encoding :: ISO (synonym: ISO_8859_3_Encoding)

  - ISOLatin4Encoding :: ISO (synonym: ISO_8859_4_Encoding)

  - ISOCyrillicEncoding :: ISO (synonym: ISO_8859_5_Encoding)

  - ISOGreekEncoding :: ISO (synonym: ISO_8859_7_Encoding)

  - ISOLatin5Encoding :: ISO (synonym: ISO_8859_9_Encoding)

  - ISOLatin6Encoding :: ISO (synonym: ISO_8859_10_Encoding)

  - ISOThaiEncoding :: ISO (synonym: ISO_8859_11_Encoding)

  - ISOLatin7Encoding :: ISO (synonym: ISO_8859_13_Encoding)

  - ISOLatin8Encoding :: ISO (synonym: ISO_8859_14_Encoding)

  - ISOLatin9Encoding :: ISO (synonym: ISO_8859_15_Encoding)

  - KOI8REncoding :: -

- *--encoding-text*=/text/, *-E* /text/ :: Use the encoding in the
  /text/ argument, which must be formatted as a **() encoding. One of
  *--encoding* and *--encoding-text* must be supplied.

- *--name*=/name/, *-n* /name/ :: Set the output font's PostScript name
  to /name/. The default is the input font name followed by the
  encoding's name.

- *--full-name*=/name/, *-N* /name/ :: Set the output font's FullName to
  /name/. The default is the input FullName followed by the encoding's
  name.

- *--output*=/file/, *-o* /file/ :: Send output to /file/ instead of
  standard output.

- *--pfb*, *-b* :: Output a PFB font. This is the default.

- *--pfa*, *-a* :: Output a PFA font.

- *-h*, *--help* :: Print usage information and exit.

- *--version* :: Print the version number and some short non-warranty
  information and exit.

* RETURN VALUES
*T1reencode* exits with value 0 if a re-encoded font was successfully
generated, and 1 otherwise.

* NOTES
*T1reencode* should be used only in special situations. It's generally
much better to use PostScript commands to re-encode a font; for
instance, executing the PostScript commands to generate two
differently-encoded versions of a single font will take up much less
memory than loading two *t1reencode*d fonts.

* EXAMPLES
This command re-encodes Frutiger Roman in the ISO Latin 1 encoding. The
new font will have the PostScript name Frutiger-RomanISOLatin1Encoding.

#+begin_example
      t1reencode -e ISOLatin1Encoding FrutiRom.pfb \
              -o FrutiRomISOL1.pfb
#+end_example

This series of commands, which use **() and **() as well as *t1reencode*
itself, generate a version of Warnock Pro Regular with old-style figures
in the slots for numbers (because of *otftotfm*'s *-f*onum option). The
new font will be called WarnockPro-RegularOsF.

#+begin_example
      otftotfm -fonum WarnockPro-Regular.otf \
              --output-encoding /tmp/osf.enc
      cfftot1 WarnockPro-Regular.otf | t1reencode -e /tmp/osf.enc \
              -n WarnockPro-RegularOsF -N "Warnock Pro Regular OsF" \
              -o WarnoProRegOsF.pfb
#+end_example

* SEE ALSO
/Adobe Type 1 Font Format/, **() **() **()

* AUTHOR
Eddie Kohler (ekohler@gmail.com)
