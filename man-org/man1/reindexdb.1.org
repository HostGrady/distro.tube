#+TITLE: Man1 - reindexdb.1
#+DESCRIPTION: Linux manpage for reindexdb.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
reindexdb - reindex a PostgreSQL database

* SYNOPSIS
*reindexdb* [/connection-option/...] [/option/...] [ *-S* | *--schema*
/schema/ ]... [ *-t* | *--table* /table/ ]... [ *-i* | *--index* /index/
]... [/dbname/]

*reindexdb* [/connection-option/...] [/option/...] *-a* | *--all*

*reindexdb* [/connection-option/...] [/option/...] *-s* | *--system*
[/dbname/]

* DESCRIPTION
reindexdb is a utility for rebuilding indexes in a PostgreSQL database.

reindexdb is a wrapper around the SQL command *REINDEX*(7). There is no
effective difference between reindexing databases via this utility and
via other methods for accessing the server.

* OPTIONS
reindexdb accepts the following command-line arguments:

*-a*\\
*--all*

#+begin_quote
  Reindex all databases.
#+end_quote

*--concurrently*

#+begin_quote
  Use the CONCURRENTLY option. See *REINDEX*(7), where all the caveats
  of this option are explained in detail.
#+end_quote

*[-d] */dbname/\\
*[--dbname=]*/dbname/

#+begin_quote
  Specifies the name of the database to be reindexed, when *-a*/*--all*
  is not used. If this is not specified, the database name is read from
  the environment variable *PGDATABASE*. If that is not set, the user
  name specified for the connection is used. The /dbname/ can be a
  connection string. If so, connection string parameters will override
  any conflicting command line options.
#+end_quote

*-e*\\
*--echo*

#+begin_quote
  Echo the commands that reindexdb generates and sends to the server.
#+end_quote

*-i */index/\\
*--index=*/index/

#+begin_quote
  Recreate /index/ only. Multiple indexes can be recreated by writing
  multiple *-i* switches.
#+end_quote

*-j */njobs/\\
*--jobs=*/njobs/

#+begin_quote
  Execute the reindex commands in parallel by running /njobs/ commands
  simultaneously. This option may reduce the processing time but it also
  increases the load on the database server.

  reindexdb will open /njobs/ connections to the database, so make sure
  your max_connections setting is high enough to accommodate all
  connections.

  Note that this option is incompatible with the *--index* and
  *--system* options.
#+end_quote

*-q*\\
*--quiet*

#+begin_quote
  Do not display progress messages.
#+end_quote

*-s*\\
*--system*

#+begin_quote
  Reindex databases system catalogs.
#+end_quote

*-S */schema/\\
*--schema=*/schema/

#+begin_quote
  Reindex /schema/ only. Multiple schemas can be reindexed by writing
  multiple *-S* switches.
#+end_quote

*-t */table/\\
*--table=*/table/

#+begin_quote
  Reindex /table/ only. Multiple tables can be reindexed by writing
  multiple *-t* switches.
#+end_quote

*-v*\\
*--verbose*

#+begin_quote
  Print detailed information during processing.
#+end_quote

*-V*\\
*--version*

#+begin_quote
  Print the reindexdb version and exit.
#+end_quote

*-?*\\
*--help*

#+begin_quote
  Show help about reindexdb command line arguments, and exit.
#+end_quote

reindexdb also accepts the following command-line arguments for
connection parameters:

*-h */host/\\
*--host=*/host/

#+begin_quote
  Specifies the host name of the machine on which the server is running.
  If the value begins with a slash, it is used as the directory for the
  Unix domain socket.
#+end_quote

*-p */port/\\
*--port=*/port/

#+begin_quote
  Specifies the TCP port or local Unix domain socket file extension on
  which the server is listening for connections.
#+end_quote

*-U */username/\\
*--username=*/username/

#+begin_quote
  User name to connect as.
#+end_quote

*-w*\\
*--no-password*

#+begin_quote
  Never issue a password prompt. If the server requires password
  authentication and a password is not available by other means such as
  a .pgpass file, the connection attempt will fail. This option can be
  useful in batch jobs and scripts where no user is present to enter a
  password.
#+end_quote

*-W*\\
*--password*

#+begin_quote
  Force reindexdb to prompt for a password before connecting to a
  database.

  This option is never essential, since reindexdb will automatically
  prompt for a password if the server demands password authentication.
  However, reindexdb will waste a connection attempt finding out that
  the server wants a password. In some cases it is worth typing *-W* to
  avoid the extra connection attempt.
#+end_quote

*--maintenance-db=*/dbname/

#+begin_quote
  Specifies the name of the database to connect to to discover which
  databases should be reindexed, when *-a*/*--all* is used. If not
  specified, the postgres database will be used, or if that does not
  exist, template1 will be used. This can be a connection string. If so,
  connection string parameters will override any conflicting command
  line options. Also, connection string parameters other than the
  database name itself will be re-used when connecting to other
  databases.
#+end_quote

* ENVIRONMENT
*PGDATABASE*\\
*PGHOST*\\
*PGPORT*\\
*PGUSER*

#+begin_quote
  Default connection parameters
#+end_quote

*PG_COLOR*

#+begin_quote
  Specifies whether to use color in diagnostic messages. Possible values
  are always, auto and never.
#+end_quote

This utility, like most other PostgreSQL utilities, also uses the
environment variables supported by libpq (see Section 33.14).

* DIAGNOSTICS
In case of difficulty, see *REINDEX*(7) and *psql*(1) for discussions of
potential problems and error messages. The database server must be
running at the targeted host. Also, any default connection settings and
environment variables used by the libpq front-end library will apply.

* NOTES
reindexdb might need to connect several times to the PostgreSQL server,
asking for a password each time. It is convenient to have a ~/.pgpass
file in such cases. See Section 33.15 for more information.

* EXAMPLES
To reindex the database test:

#+begin_quote
  #+begin_example
    $ reindexdb test
  #+end_example
#+end_quote

To reindex the table foo and the index bar in a database named abcd:

#+begin_quote
  #+begin_example
    $ reindexdb --table=foo --index=bar abcd
  #+end_example
#+end_quote

* SEE ALSO
*REINDEX*(7)
