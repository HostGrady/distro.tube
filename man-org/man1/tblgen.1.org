#+TITLE: Man1 - tblgen.1
#+DESCRIPTION: Linux manpage for tblgen.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tblgen - Description to C++ Code

* SYNOPSIS
*clang-tblgen* [/options/] [/filename/]

*lldb-tblgen* [/options/] [/filename/]

*llvm-tblgen* [/options/] [/filename/]

*mlir-tblgen* [/options/] [/filename/]

* DESCRIPTION
**-tblgen* is a family of programs that translates target description
(*.td*) files into C++ code and other output formats. Most users of LLVM
will not need to use this program. It is used only for writing parts of
the compiler, debugger, and LLVM target backends.

The details of the input and output of the **-tblgen* programs is beyond
the scope of this short introduction; please see the TableGen Overview
for an introduction and for references to additional TableGen documents.

The /filename/ argument specifies the name of the Target Description
(*.td*) file that TableGen processes.

* OPTIONS
** General Options

#+begin_quote
  - *-help* :: Print a description of the command line options.
#+end_quote

#+begin_quote
  - *-help-list* :: Print a description of the command line options in a
    simple list format.
#+end_quote

#+begin_quote
  - *-D=macroname* :: Specify the name of a macro to be defined. The
    name is defined, but it has no particular value.
#+end_quote

#+begin_quote
  - *-d=filename* :: Specify the name of the dependency filename.
#+end_quote

#+begin_quote
  - *-debug* :: Enable debug output.
#+end_quote

#+begin_quote
  - *-dump-json* :: Print a JSON representation of all records, suitable
    for further automated processing.
#+end_quote

#+begin_quote
  - *-I directory* :: Specify where to find other target description
    files for inclusion. The *directory* value should be a full or
    partial path to a directory that contains target description files.
#+end_quote

#+begin_quote
  - *-null-backend* :: Parse the source files and build the records, but
    do not run any backend. This is useful for timing the frontend.
#+end_quote

#+begin_quote
  - *-o filename* :: Specify the output file name. If *filename* is *-*,
    then **-tblgen* sends its output to standard output.
#+end_quote

#+begin_quote
  - *-print-records* :: Print all classes and records to standard output
    (default backend option).
#+end_quote

#+begin_quote
  - *-print-detailed-records* :: Print a detailed report of all global
    variables, classes, and records to standard output.
#+end_quote

#+begin_quote
  - *-stats* :: Print a report with any statistics collected by the
    backend.
#+end_quote

#+begin_quote
  - *-time-phases* :: Time the parser and backend phases and print a
    report.
#+end_quote

#+begin_quote
  - *-version* :: Show the version number of the program.
#+end_quote

#+begin_quote
  - *-write-if-changed* :: Write the output file only if it is new or
    has changed.
#+end_quote

** clang-tblgen Options

#+begin_quote
  - *-gen-clang-attr-classes* :: Generate Clang attribute clases.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-parser-string-switches* :: Generate all
    parser-related attribute string switches.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-subject-match-rules-parser-string-switches* :: Generate
    all parser-related attribute subject match rule string switches.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-impl* :: Generate Clang attribute implementations.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-list"* :: Generate a Clang attribute list.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-subject-match-rule-list* :: Generate a Clang
    attribute subject match rule list.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-pch-read* :: Generate Clang PCH attribute reader.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-pch-write* :: Generate Clang PCH attribute writer.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-has-attribute-impl* :: Generate a Clang attribute
    spelling list.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-spelling-index* :: Generate a Clang attribute
    spelling index.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-ast-visitor* :: Generate a recursive AST visitor
    for Clang attributes.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-template-instantiate* :: Generate a Clang template
    instantiate code.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-parsed-attr-list* :: Generate a Clang parsed
    attribute list.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-parsed-attr-impl* :: Generate the Clang parsed
    attribute helpers.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-parsed-attr-kinds* :: Generate a Clang parsed
    attribute kinds.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-text-node-dump* :: Generate Clang attribute text
    node dumper.
#+end_quote

#+begin_quote
  - *-gen-clang-attr-node-traverse* :: Generate Clang attribute
    traverser.
#+end_quote

#+begin_quote
  - *-gen-clang-diags-defs* :: Generate Clang diagnostics definitions.
#+end_quote

#+begin_quote
  - *-clang-component component* :: Only use warnings from specified
    component.
#+end_quote

#+begin_quote
  - *-gen-clang-diag-groups* :: Generate Clang diagnostic groups.
#+end_quote

#+begin_quote
  - *-gen-clang-diags-index-name* :: Generate Clang diagnostic name
    index.
#+end_quote

#+begin_quote
  - *-gen-clang-basic-reader* :: Generate Clang BasicReader classes.
#+end_quote

#+begin_quote
  - *-gen-clang-basic-writer* :: Generate Clang BasicWriter classes.
#+end_quote

#+begin_quote
  - *-gen-clang-comment-nodes* :: Generate Clang AST comment nodes.
#+end_quote

#+begin_quote
  - *-gen-clang-decl-nodes* :: Generate Clang AST declaration nodes.
#+end_quote

#+begin_quote
  - *-gen-clang-stmt-nodes* :: Generate Clang AST statement nodes.
#+end_quote

#+begin_quote
  - *-gen-clang-type-nodes* :: Generate Clang AST type nodes.
#+end_quote

#+begin_quote
  - *-gen-clang-type-reader* :: Generate Clang AbstractTypeReader class.
#+end_quote

#+begin_quote
  - *-gen-clang-type-writer* :: Generate Clang AbstractTypeWriter class.
#+end_quote

#+begin_quote
  - *-gen-clang-opcodes* :: Generate Clang constexpr interpreter
    opcodes.
#+end_quote

#+begin_quote
  - *-gen-clang-sa-checkers* :: Generate Clang static analyzer checkers.
#+end_quote

#+begin_quote
  - *-gen-clang-comment-html-tags* :: Generate efficient matchers for
    HTML tag names that are used in documentation comments.
#+end_quote

#+begin_quote
  - *-gen-clang-comment-html-tags-properties* :: Generate efficient
    matchers for HTML tag properties.
#+end_quote

#+begin_quote
  - *-gen-clang-comment-html-named-character-references* :: Generate
    function to translate named character references to UTF-8 sequences.
#+end_quote

#+begin_quote
  - *-gen-clang-comment-command-info* :: Generate command properties for
    commands that are used in documentation comments.
#+end_quote

#+begin_quote
  - *-gen-clang-comment-command-list* :: Generate list of commands that
    are used in documentation comments.
#+end_quote

#+begin_quote
  - *-gen-clang-opencl-builtins* :: Generate OpenCL builtin declaration
    handlers.
#+end_quote

#+begin_quote
  - *-gen-arm-neon* :: Generate *arm_neon.h* for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-fp16* :: Generate *arm_fp16.h* for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-bf16* :: Generate *arm_bf16.h* for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-neon-sema* :: Generate ARM NEON sema support for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-neon-test* :: Generate ARM NEON tests for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-sve-header* :: Generate *arm_sve.h* for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-sve-builtins* :: Generate *arm_sve_builtins.inc* for
    Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-sve-builtin-codegen* :: Generate
    *arm_sve_builtin_cg_map.inc* for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-sve-typeflags* :: Generate *arm_sve_typeflags.inc* for
    Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-sve-sema-rangechecks* :: Generate
    *arm_sve_sema_rangechecks.inc* for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-mve-header* :: Generate *arm_mve.h* for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-mve-builtin-def* :: Generate ARM MVE builtin definitions
    for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-mve-builtin-sema* :: Generate ARM MVE builtin sema checks
    for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-mve-builtin-codegen* :: Generate ARM MVE builtin
    code-generator for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-mve-builtin-aliases* :: Generate list of valid ARM MVE
    builtin aliases for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-cde-header* :: Generate *arm_cde.h* for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-cde-builtin-def* :: Generate ARM CDE builtin definitions
    for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-cde-builtin-sema* :: Generate ARM CDE builtin sema checks
    for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-cde-builtin-codegen* :: Generate ARM CDE builtin
    code-generator for Clang.
#+end_quote

#+begin_quote
  - *-gen-arm-cde-builtin-aliases* :: Generate list of valid ARM CDE
    builtin aliases for Clang.
#+end_quote

#+begin_quote
  - *-gen-riscv-vector-header* :: Generate *riscv_vector.h* for Clang.
#+end_quote

#+begin_quote
  - *-gen-riscv-vector-builtins* :: Generate *riscv_vector_builtins.inc*
    for Clang.
#+end_quote

#+begin_quote
  - *-gen-riscv-vector-builtin-codegen* :: Generate
    *riscv_vector_builtin_cg.inc* for Clang.
#+end_quote

#+begin_quote
  - *-gen-attr-docs* :: Generate attribute documentation.
#+end_quote

#+begin_quote
  - *-gen-diag-docs* :: Generate diagnostic documentation.
#+end_quote

#+begin_quote
  - *-gen-opt-docs* :: Generate option documentation.
#+end_quote

#+begin_quote
  - *-gen-clang-data-collectors* :: Generate data collectors for AST
    nodes.
#+end_quote

#+begin_quote
  - *-gen-clang-test-pragma-attribute-supported-attributes* :: Generate
    a list of attributes supported by *#pragma* Clang attribute for
    testing purposes.
#+end_quote

** lldb-tblgen Options

#+begin_quote
  - *gen-lldb-option-defs* :: Generate lldb OptionDefinition values.
#+end_quote

#+begin_quote
  - *gen-lldb-property-defs* :: Generate lldb PropertyDefinition values.
#+end_quote

#+begin_quote
  - *gen-lldb-property-enum-defs* :: Generate lldb PropertyDefinition
    enum values.
#+end_quote

** llvm-tblgen Options

#+begin_quote
  - *-gen-asm-matcher* :: Generate assembly instruction matcher.
#+end_quote

#+begin_quote
  - *-match-prefix=prefix* :: Make -gen-asm-matcher match only
    instructions with the given /prefix/.
#+end_quote

#+begin_quote
  - *-gen-asm-parser* :: Generate assembly instruction parser.
#+end_quote

#+begin_quote
  - *-asmparsernum=n* :: Make -gen-asm-parser emit assembly parser
    number /n/.
#+end_quote

#+begin_quote
  - *-gen-asm-writer* :: Generate assembly writer.
#+end_quote

#+begin_quote
  - *-asmwriternum=n* :: Make -gen-asm-writer emit assembly writer
    number /n/.
#+end_quote

#+begin_quote
  - *-gen-attrs* :: Generate attributes.
#+end_quote

#+begin_quote
  - *-gen-automata* :: Generate generic automata.
#+end_quote

#+begin_quote
  - *-gen-callingconv* :: Generate calling convention descriptions.
#+end_quote

#+begin_quote
  - *-gen-compress-inst-emitter* :: Generate RISC-V compressed
    instructions.
#+end_quote

#+begin_quote
  - *-gen-ctags* :: Generate ctags-compatible index.
#+end_quote

#+begin_quote
  - *-gen-dag-isel* :: Generate a DAG (directed acyclic graph)
    instruction selector.
#+end_quote

#+begin_quote
  - *-instrument-coverage* :: Make -gen-dag-isel generate tables to help
    identify the patterns matched.
#+end_quote

#+begin_quote
  - *-omit-comments* :: Make -gen-dag-isel omit comments. The default is
    false.
#+end_quote

#+begin_quote
  - *-gen-dfa-packetizer* :: Generate DFA Packetizer for VLIW targets.
#+end_quote

#+begin_quote
  - *-gen-directive-decl* :: Generate directive related declaration code
    (header file).
#+end_quote

#+begin_quote
  - *-gen-directive-gen* :: Generate directive related implementation
    code part.
#+end_quote

#+begin_quote
  - *-gen-directive-impl* :: Generate directive related implementation
    code.
#+end_quote

#+begin_quote
  - *-gen-disassembler* :: Generate disassembler.
#+end_quote

#+begin_quote
  - *-gen-emitter* :: Generate machine code emitter.
#+end_quote

#+begin_quote
  - *-gen-exegesis* :: Generate llvm-exegesis tables.
#+end_quote

#+begin_quote
  - *-gen-fast-isel* :: Generate a "fast" instruction selector.
#+end_quote

#+begin_quote
  - *-gen-global-isel* :: Generate GlobalISel selector.
#+end_quote

#+begin_quote
  - *-gisel-coverage-file=filename* :: Specify the file from which to
    retrieve coverage information.
#+end_quote

#+begin_quote
  - *-instrument-gisel-coverage* :: Make -gen-global-isel generate
    coverage instrumentation.
#+end_quote

#+begin_quote
  - *-optimize-match-table* :: Make -gen-global-isel generate an
    optimized version of the match table.
#+end_quote

#+begin_quote
  - *-warn-on-skipped-patterns* :: Make -gen-global-isel explain why a
    pattern was skipped for inclusion.
#+end_quote

#+begin_quote
  - *-gen-global-isel-combiner* :: Generate GlobalISel combiner.
#+end_quote

#+begin_quote
  - *-combiners=list* :: Make -gen-global-isel-combiner emit the
    specified combiners.
#+end_quote

#+begin_quote
  - *-gicombiner-show-expansions* :: Make -gen-global-isel-combiner use
    C++ comments to indicate occurrences of code expansion.
#+end_quote

#+begin_quote
  - *-gicombiner-stop-after-build* :: Make -gen-global-isel-combiner
    stop processing after building the match tree.
#+end_quote

#+begin_quote
  - *-gicombiner-stop-after-parse* :: Make -gen-global-isel-combiner
    stop processing after parsing rules and dump state.
#+end_quote

#+begin_quote
  - *-gen-instr-info* :: Generate instruction descriptions.
#+end_quote

#+begin_quote
  - *-gen-instr-docs* :: Generate instruction documentation.
#+end_quote

#+begin_quote
  - *-gen-intrinsic-enums* :: Generate intrinsic enums.
#+end_quote

#+begin_quote
  - *-intrinsic-prefix=prefix* :: Make -gen-intrinsic-enums generate
    intrinsics with this target /prefix/.
#+end_quote

#+begin_quote
  - *-gen-intrinsic-impl* :: Generate intrinsic information.
#+end_quote

#+begin_quote
  - *-gen-opt-parser-defs* :: Generate options definitions.
#+end_quote

#+begin_quote
  - *-gen-opt-rst* :: Generate option RST.
#+end_quote

#+begin_quote
  - *-gen-pseudo-lowering* :: Generate pseudo instruction lowering.
#+end_quote

#+begin_quote
  - *-gen-register-bank* :: Generate register bank descriptions.
#+end_quote

#+begin_quote
  - *-gen-register-info* :: Generate registers and register classes
    info.
#+end_quote

#+begin_quote
  - *-register-info-debug* :: Make -gen-register-info dump register
    information for debugging.
#+end_quote

#+begin_quote
  - *-gen-searchable-tables* :: Generate generic searchable tables. See
    TableGen BackEnds for a detailed description.
#+end_quote

#+begin_quote
  - *-gen-subtarget* :: Generate subtarget enumerations.
#+end_quote

#+begin_quote
  - *-gen-x86-EVEX2VEX-tables* :: Generate X86 EVEX to VEX compress
    tables.
#+end_quote

#+begin_quote
  - *-gen-x86-fold-tables* :: Generate X86 fold tables.
#+end_quote

#+begin_quote
  - *-long-string-literals* :: When emitting large string tables, prefer
    string literals over comma-separated char literals. This can be a
    readability and compile-time performance win, but upsets some
    compilers.
#+end_quote

#+begin_quote
  - *-print-enums* :: Print enumeration values for a class.
#+end_quote

#+begin_quote
  - *-class=classname* :: Make -print-enums print the enumeration list
    for the specified class.
#+end_quote

#+begin_quote
  - *-print-sets* :: Print expanded sets for testing DAG exprs.
#+end_quote

** mlir-tblgen Options

#+begin_quote
  - *-gen-avail-interface-decls* :: Generate availability interface
    declarations.
#+end_quote

#+begin_quote
  - *-gen-avail-interface-defs* :: Generate op interface definitions.
#+end_quote

#+begin_quote
  - *-gen-dialect-doc* :: Generate dialect documentation.
#+end_quote

#+begin_quote
  - *-dialect* :: The dialect to generate.
#+end_quote

#+begin_quote
  - *-gen-directive-decl* :: Generate declarations for directives
    (OpenMP, etc.).
#+end_quote

#+begin_quote
  - *-gen-enum-decls* :: Generate enum utility declarations.
#+end_quote

#+begin_quote
  - *-gen-enum-defs* :: Generate enum utility definitions.
#+end_quote

#+begin_quote
  - *-gen-enum-from-llvmir-conversions* :: Generate conversions of
    EnumAttrs from LLVM IR.
#+end_quote

#+begin_quote
  - *-gen-enum-to-llvmir-conversions* :: Generate conversions of
    EnumAttrs to LLVM IR.
#+end_quote

#+begin_quote
  - *-gen-llvmir-conversions* :: Generate LLVM IR conversions.
#+end_quote

#+begin_quote
  - *-gen-llvmir-intrinsics* :: Generate LLVM IR intrinsics.
#+end_quote

#+begin_quote
  - *-llvmir-intrinsics-filter* :: Only keep the intrinsics with the
    specified substring in their record name.
#+end_quote

#+begin_quote
  - *-dialect-opclass-base* :: The base class for the ops in the dialect
    we are to emit.
#+end_quote

#+begin_quote
  - *-gen-op-decls* :: Generate operation declarations.
#+end_quote

#+begin_quote
  - *-gen-op-defs* :: Generate operation definitions.
#+end_quote

#+begin_quote
  - *-asmformat-error-is-fatal* :: Emit a fatal error if format parsing
    fails.
#+end_quote

#+begin_quote
  - *-op-exclude-regex* :: Regular expression of name of ops to exclude
    (no filter if empty).
#+end_quote

#+begin_quote
  - *-op-include-regex* :: Regular expression of name of ops to include
    (no filter if empty).
#+end_quote

#+begin_quote
  - *-gen-op-doc* :: Generate operation documentation.
#+end_quote

#+begin_quote
  - *-gen-pass-decls* :: Generate operation documentation.
#+end_quote

#+begin_quote
  - *-name namestring* :: The name of this group of passes.
#+end_quote

#+begin_quote
  - *-gen-pass-doc* :: Generate pass documentation.
#+end_quote

#+begin_quote
  - *-gen-rewriters* :: Generate pattern rewriters.
#+end_quote

#+begin_quote
  - *-gen-spirv-avail-impls* :: Generate SPIR-V operation utility
    definitions.
#+end_quote

#+begin_quote
  - *-gen-spirv-capability-implication* :: Generate utility function to
    return implied capabilities for a given capability.
#+end_quote

#+begin_quote
  - *-gen-spirv-enum-avail-decls* :: Generate SPIR-V enum availability
    declarations.
#+end_quote

#+begin_quote
  - *-gen-spirv-enum-avail-defs* :: Generate SPIR-V enum availability
    definitions.
#+end_quote

#+begin_quote
  - *-gen-spirv-op-utils* :: Generate SPIR-V operation utility
    definitions.
#+end_quote

#+begin_quote
  - *-gen-spirv-serialization* :: Generate SPIR-V (de)serialization
    utilities and functions.
#+end_quote

#+begin_quote
  - *-gen-struct-attr-decls* :: Generate struct utility declarations.
#+end_quote

#+begin_quote
  - *-gen-struct-attr-defs* :: Generate struct utility definitions.
#+end_quote

#+begin_quote
  - *-gen-typedef-decls* :: Generate TypeDef declarations.
#+end_quote

#+begin_quote
  - *-gen-typedef-defs* :: Generate TypeDef definitions.
#+end_quote

#+begin_quote
  - *-typedefs-dialect name* :: Generate types for this dialect.
#+end_quote

* EXIT STATUS
If **-tblgen* succeeds, it will exit with 0. Otherwise, if an error
occurs, it will exit with a non-zero value.

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
