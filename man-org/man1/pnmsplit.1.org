#+TITLE: Man1 - pnmsplit.1
#+DESCRIPTION: Linux manpage for pnmsplit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnmsplit - split a multi-image PNM file into multiple single-image files

* DESCRIPTION
This program is part of *Netpbm*(1)

Starting with Netpbm 10.31 (December 2005), *pnmsplit* is obsolete. Use
**pamsplit**(1) instead.

*pamsplit* is backward compatible with *pnmsplit*. You can use the
*pamsplit* manual for *pnmsplit* as long as you ignore features that
were added after Netpbm 10.30.
