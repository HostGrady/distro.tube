#+TITLE: Man1 - git-sh-i18n--envsubst.1
#+DESCRIPTION: Linux manpage for git-sh-i18n--envsubst.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-sh-i18n--envsubst - Gits own envsubst(1) for i18n fallbacks

* SYNOPSIS
#+begin_example
  eval_gettext () {
          printf "%s" "$1" | (
                  export PATH $(git sh-i18n--envsubst --variables "$1");
                  git sh-i18n--envsubst "$1"
          )
  }
#+end_example

* DESCRIPTION
This is not a command the end user would want to run. Ever. This
documentation is meant for people who are studying the plumbing scripts
and/or are writing new ones.

/git sh-i18n--envsubst/ is Git's stripped-down copy of the GNU
*envsubst(1)* program that comes with the GNU gettext package. It's used
internally by *git-sh-i18n*(1) to interpolate the variables passed to
the *eval_gettext* function.

No promises are made about the interface, or that this program won't
disappear without warning in the next version of Git. Don't use it.

* GIT
Part of the *git*(1) suite
