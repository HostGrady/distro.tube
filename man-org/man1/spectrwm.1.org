#+TITLE: Manpages - spectrwm.1
#+DESCRIPTION: Linux manpage for spectrwm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
Specify a configuration file to load instead of scanning for one.

Print version and exit.

is a minimalistic window manager that tries to stay out of the way so
that valuable screen real estate can be used for much more important
stuff. It has sane defaults and does not require one to learn a language
to do any configuration. It was written by hackers for hackers and it
strives to be small, compact and fast.

When

starts up, it reads settings from its configuration file,

See the

section below.

The following notation is used throughout this page:

Meta

Shift

Named key or button

is very simple in its use. Most of the actions are initiated via key or
pointer bindings. See the

section below for defaults and customizations.

looks for the user-configuration file in the following order:

(if

is either not set or empty)

If the user-configuration file is not found,

then looks for the global configuration file in the following order:

(each colon-separated directory in

(if

is either not set or empty)

The format of the file is

For example:

Enabling or disabling an option is done by using 1 or 0 respectively.

Colors need to be specified per the

specification.

Comments begin with a #. When a literal

is desired in an option, then it must be escaped with a backslash, i.e.
\#

The file supports the following keywords:

Launch an application in a specified workspace at start-of-day. Defined
in the format

e.g. ws[2]:xterm launches an

in workspace 2.

Note that workspace mapping is handled via

When

spawns windows via a daemon, ensure the daemon is started with the
correct

in its environment.

For example, starting

via

LD_PRELOAD=/usr/lib/libswmhack.so.0.0 urxvtd -q -o -f

Spawned programs automatically have

set when executed.

It is advised to check the man page of

as

is sometimes ignored by some operating systems. A workaround is
available, e.g. launch an xterm(1) in workspace 2:

autorun = ws[2]:xterm -name ws2 quirk[XTerm:ws2] = WS[2]

External script that populates additional information in the status bar,
such as battery life.

Process

character sequences in

output; default is 0.

Place the statusbar at the bottom of each region instead of the top.

Border color of the status bar(s) in screen

Border color of the status bar(s) on unfocused region(s) in screen

Set status bar border thickness in pixels. Disable border by setting
to 0.

Background color of the status bar(s) in screen

A comma separated list of up to 10 colors can be specified. The first
value is used as the default background color. Any of these colors can
then be selected as a background color in the status bar through the use
of the markup sequence

where n is between 0 and 9.

Background color for selections on the status bar(s) in screen

Defaults to the value of

Set default

state; default is 1.

Set default

state on workspace

default is 1.

Fonts used in the status bar. Either Xft or X Logical Font Description
(XLFD) may be used to specify fonts. Fallback fonts may be specified by
separating each font with a comma. If all entries are in XLFD syntax,
font set will be used. If at least one entry is Xft, Xft will be used.

The default is to use font set.

If Xft is used, a comma-separated list of up to 10 fonts can be
specified. The first entry is the default font. Any font defined here
can then be selected in the status bar through the use of the markup
sequence

where n is between 0 and 9.

Also note that

does not support Xft fonts.

Xft examples:

bar_font = Terminus:style=Regular:pixelsize=14:antialias=true

bar_font =
-*-profont-medium-*-*-*-11-*-*-*-*-*-*-*,Terminus:pixelsize=14,-*-clean-medium-*-*-*-12-*-*-*-*-*-*-*

Font set examples:

bar_font = -*-terminus-medium-*-*-*-14-*-*-*-*-*-*-*

bar_font =
-*-profont-medium-*-*-*-11-*-*-*-*-*-*-*,-*-terminus-medium-*-*-*-14-*-*-*-*-*-*-*,-*-clean-medium-*-*-*-12-*-*-*-*-*-*-*

To list the available fonts in your system see

or

manpages. The

application can help with the XLFD setting.

Foreground color of the status bar(s) in screen

A comma separated list of up to 10 colors can be specified. The first
value is used as the default foreground color. Any of these colors can
then be selected as a foreground color in the status bar through the use
of the markup sequence

where n is between 0 and 9.

Foreground color for selections on the status bar(s) in screen

Defaults to the value of

Specify a font which uses the Unicode Private Use Area (U+E000 ->
U+F8FF). Some fonts use these code points to provide special icon
glyphs. Available only with Xft fonts.

Set the bar format string, overriding

and all of the

options. The format is passed through

before being used. It may contain the following character sequences:

sequence effects.

is a positive integer used to allocate horizontal space between 'L', 'C'
and 'R' sections (see justify). The default weight is 1.

can have the value L, C, R or T. L, C, R are for left, center and right
justified sections respectively. A 'T' section will limit its space
usage to fit to the text. If no value is specified for a given section,
the setting from

is used.

The currently recognized text markup sequences are:

Any markup sequence found after +@stp will appear as normal characters
in the status bar.

Note that markup sequences in

script output will only be processed if

is enabled.

All character sequences may limit its output to a specific length, for
example +64A. By default, no padding/alignment is done in case the
length of the replaced string is less than the specified length (64 in
the example). The padding/alignment can be enabled using a '_' character
in the sequence. For example: +_64W, +64_W and +_64_W enable padding
before (right alignment), after (left alignment), and both before and
after (center alignment) window name, respectively. Any characters that
don't match the specification are copied as-is.

Justify the status bar text. Possible values are

and

Note that if the output is not left justified, it may not be properly
aligned in some circumstances, due to the white-spaces in the default
static format. See the

option for more details.

Bind key or button combo to action

See the

section below.

Set window border thickness in pixels. Disable all borders by setting
to 0.

Set region containment boundary width in pixels. This is how far a
window must be dragged/resized (with the pointer) beyond the region edge
before it is allowed outside the region. Disable the window containment
effect by setting to 0.

Enable or disable displaying the clock in the status bar. Disable by
setting to 0 so a custom clock could be used in the

script.

Border color of the currently focused window. Default is red.

Border color of the currently focused, maximized window. Defaults to the
value of

Border color of unfocused windows, default is rgb:88/88/88.

Border color of unfocused, maximized windows. Defaults to the value of

Some applications have dialogue windows that are too small to be useful.
This ratio is the screen size to what they will be resized. For example,
0.6 is 60% of the physical screen size.

Remove border when bar is disabled and there is only one window on the
region. Enable by setting to 1. Setting this to

removes border from lone tiled windows, regardless of the bar being
enabled/disabled. Defaults to 0.

Window to put focus when the focused window is closed. Possible values
are

(default) and

and

are relative to the window that is closed.

Whether to allow the focus to jump to the last window when the first
window is closed or vice versa. Disable by setting to 0.

Window to put focus when no window has been focused. Possible values are

and

(default).

Window focus behavior with respect to the pointer. Possible values:

Set window focus on border crossings caused by cursor motion and window
interaction.

Set window focus on all cursor border crossings, including workspace
switches and changes to layout.

Set window focus on window interaction only.

Display the number of iconic (minimized) windows in the status bar.
Enable by setting to 1.

Clear all key bindings (not button bindings) and load new bindings from
the specified file. This allows you to load pre-defined key bindings for
your keyboard layout. See the

section below for a list of keyboard mapping files that have been
provided for several keyboard layouts.

Note that

can be specified if you only want to clear bindings.

Select layout to use at start-of-day. Defined in the format

e.g. ws[2]:-4:0:1:0:horizontal sets worskspace 2 to the horizontal stack
mode, shrinks the master area by 4 ticks and adds one window to the
stack, while maintaining default floating window behavior. Possible

values are

and

See

and

for more information. Note that the stacking options are complicated and
have side-effects. One should familiarize oneself with these commands
before experimenting with the

option.

This setting is not retained at restart.

When set to 1,

will also hide/restore the bar visibility of the affected workspace.
Defaults to 0.

Change mod key. Mod1 is generally the ALT key and Mod4 is the windows
key on a PC.

Set the name of a workspace at start-of-day. Defined in the format

e.g. ws[1]:Console sets the name of workspace 1 to

Define new action to spawn a program

See the

section below.

Add "quirk" for windows with class

instance

(optional) and name

(optional). See the

section below.

Allocates a custom region, removing any autodetected regions which
occupy the same space on the screen. Defined in the format

e.g. screen[1]:800x1200+0+0.

To make a region span multiple monitors, create a region big enough to
cover them all, e.g. screen[1]:2048x768+0+0 makes the region span two
monitors with 1024x768 resolution sitting one next to the other.

Pixel width of empty space within region borders. Disable by setting
to 0.

Position in stack to place newly spawned windows. Possible values are

and

(default).

and

are relative to the focused window.

Enable or disable displaying the current stacking algorithm in the
status bar.

Set a preferred minimum width for the terminal. If this value is greater
than 0,

will attempt to adjust the font sizes in the terminal to keep the
terminal width above this number as the window is resized. Only

is currently supported. The

binary must not be setuid or setgid, which it is by default on most
systems. Users may need to set program[term] (see the

section) to use an alternate copy of the

binary without the setgid bit set.

Pixel width of empty space between tiled windows. Negative values cause
overlap. Set this to the opposite of

to collapse the border between tiles. Disable by setting to 0.

Minimizes the space consumed by the urgency hint indicator by removing
the placeholders for non-urgent workspaces, the trailing space when
there are urgent windows and the default leading space. Enable by
setting to 1.

Enable or disable the urgency hint indicator in the status bar. Note
that many terminal emulators require an explicit setting for the bell
character to trigger urgency on the window. In

for example, one needs to add the following line to

xterm.bellIsUrgent: true

Enable or disable displaying the current master window count and stack
column/row count in the status bar. Enable by setting to 1. See

and

for more information.

Focus on the target window/workspace/region when clamped. For example,
when attempting to switch to a workspace that is mapped on another
region and

is enabled, focus on the region with the target workspace. Enable by
setting to 1.

Centers the pointer on the focused window when using bindings to change
focus, switch workspaces, change regions, etc. Enable by setting to 1.

Enable or disable displaying the window class name (from WM_CLASS) in
the status bar. Enable by setting to 1.

Enable or disable displaying the window instance name (from WM_CLASS) in
the status bar. Enable by setting to 1.

Enable or disable displaying the window display name (from
_NET_WM_NAME/WM_NAME) in the status bar. Enable by setting to 1.

To prevent excessively large window names from pushing the remaining
text off the bar, it's limited to 64 characters, by default. See the

option for more details.

Prevents workspaces from being swapped when attempting to switch to a
workspace that is mapped to another region. Use

if you want to focus on the region containing the workspace and

if you want to also send the pointer. Enable by setting to 1.

Configure the status bar workspace indicator. One or more of the
following options may be specified in a comma-separated list:

Include the current workspace.

Include workspaces with windows.

Include empty workspaces.

Include named workspaces.

Include workspaces with urgent window(s).

Include all workspaces.

Always exclude the current workspace from the list.

Indicate the current workspace if it is in the list.

Indicate workspaces in the list that contain urgent window(s).

Display the names of named workspaces in the list.

The default is

Set the total number of workspaces available. Minimum is 1, maximum is
22, default is 10.

allows you to define custom actions to launch programs of your choice
and then bind them the same as with built-in actions. See the

section below.

Custom programs in the configuration file are specified as follows:

is any identifier that does not conflict with a built-in action or
keyword,

is the desired program, and

is zero or more arguments to the program.

With the exception of '~' expansion, program calls are executed as-is
without any interpretation. A shell can be called to execute shell
commands. (e.g. sh -c 'command string').

Remember that when using

in your program call, it must be escaped with a backslash, i.e. \#

The following argument variables will be substituted for values at the
time the program is spawned:

-b if

is enabled.

Example:

program[ff] = /usr/local/bin/firefox http://spectrwm.org/ bind[ff] =
MOD+Shift+b # Now M-S-b launches firefox

To cancel the previous, unbind it:

bind[] = MOD+Shift+b

Default programs:

xterm

xlock

dmenu_run $dmenu_bottom -fn $bar_font -nb $bar_color -nf $bar_font_color
-sb $bar_color_selected -sf $bar_font_color_selected

dmenu $dmenu_bottom -i -fn $bar_font -nb $bar_color -nf $bar_font_color
-sb $bar_color_selected -sf $bar_font_color_selected

dmenu $dmenu_bottom -p Workspace -fn $bar_font -nb $bar_color -nf
$bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected

initscreen.sh # optional

screenshot.sh full # optional

screenshot.sh window # optional

Note that optional default programs will not be validated unless
overridden. If a default program fails validation, you can resolve the
exception by installing the program, modifying the program call or
disabling the program by freeing the respective binding.

For example, to override

program[lock] = xscreensaver-command -lock

To unbind

and prevent it from being validated:

bind[] = MOD+Shift+Delete

provides many functions (or actions) accessed via key or pointer
bindings.

The default bindings are listed below:

focus

move

resize

resize_centered

term

menu

quit

restart

restart_of_day

cycle_layout

flip_layout

layout_vertical

layout_horizontal

layout_max

stack_reset

stack_balance

master_shrink

master_grow

master_add

master_del

stack_inc

stack_dec

swap_main

focus_next

focus_prev

focus_main

focus_urgent

swap_next

swap_prev

bar_toggle

bar_toggle_ws

wind_del

wind_kill

mvrg_next

mvrg_prev

ws_empty

ws_empty_move

ws_next

ws_prev

ws_next_all

ws_prev_all

ws_prior

ws_prev_move

ws_next_move

rg_next

rg_prev

rg_move_next

rg_move_prev

screenshot_all

screenshot_wind

version

float_toggle

lock

initscr

iconify

uniconify

maximize_toggle

fullscreen_toggle

raise

always_raise

button2

width_shrink

width_grow

height_shrink

height_grow

move_left

move_right

move_up

move_down

name_workspace

search_workspace

search_win

The action names and descriptions are listed below:

Focus window/region under pointer.

Move window with pointer while binding is pressed.

Resize window with pointer while binding is pressed.

Same as

but keep window centered.

Spawn a new terminal (see

above).

Menu (see

above).

Quit

Restart

Same as

but configuration file is loaded in full.

Cycle layout.

Swap the master and stacking areas.

Switch to vertical layout.

Switch to horizontal layout.

Switch to max layout.

Reset layout.

Balance master/stacking area.

Shrink master area.

Grow master area.

Add windows to master area.

Remove windows from master area.

Add columns/rows to stacking area.

Remove columns/rows from stacking area.

Move current window to master area.

Focus next window in workspace.

Focus previous window in workspace.

Focus on main window in workspace.

Focus on next window with the urgency hint flag set. The workspace is
switched if needed.

Swap with next window in workspace.

Swap with previous window in workspace.

Toggle overall visibility of status bars.

Toggle status bar on current workspace.

Delete current window in workspace.

Destroy current window in workspace.

Switch to workspace

where

is 1 through

Move current window to workspace

where

is 1 through

Focus on region

where

is 1 through 9.

Move current window to region

where

is 1 through 9.

Move current window to workspace in next region.

Move current window to workspace in previous region.

Switch to the first empty workspace.

Switch to the first empty workspace and move current window.

Switch to next workspace with a window in it.

Switch to previous workspace with a window in it.

Switch to next workspace.

Switch to previous workspace.

Switch to next workspace with the current window.

Switch to previous workspace with the current window.

Switch to last visited workspace.

Switch to next region.

Switch to previous region.

Switch region to next screen.

Switch region to previous screen.

Take screenshot of entire screen (if enabled) (see

above).

Take screenshot of selected window (if enabled) (see

above).

Toggle version in status bar.

Toggle focused window between tiled and floating.

Lock screen (see

above).

Reinitialize physical screens (see

above).

Minimize (unmap) currently focused window.

Restore (map) window returned by

selection.

Toggle maximization of focused window.

Toggle fullscreen state of focused window.

Raise the current window.

When set tiled windows are allowed to obscure floating windows.

Fake a middle mouse button click (Button2).

Shrink the width of a floating window.

Grow the width of a floating window.

Shrink the height of a floating window.

Grow the height of a floating window.

Move a floating window a step to the left.

Move a floating window a step to the right.

Move a floating window a step upwards.

Move a floating window a step downwards.

Name the current workspace.

Search for a workspace.

Search the windows in the current workspace.

Custom bindings in the configuration file are specified as follows:

is one of the actions listed above (or empty to unbind) and

is in the form of zero or more modifier keys and/or special arguments
(Mod1, Shift, MOD, etc.) and a normal key (b, Space, etc) or a button
(Button1 .. Button255), separated by

Multiple key/button combinations may be bound to the same action.

Special arguments:

Substituted for the currently defined

Select all modifier combinations not handled by another binding.

Reprocess binding press/release events for other programs to handle.
Unavailable for

and

example:

bind[reset] = Mod4+q # bind Windows-key + q to reset bind[] = Mod1+q #
unbind Alt + q bind[move] = MOD+Button3 # Bind move to M-Button3 bind[]
= MOD+Button1 # Unbind default move binding.

example:

bind[focus] = ANYMOD+Button3 bind[move] = MOD+Button3

In the above example,

initiates

and

pressed with any other combination of modifiers sets focus to the
window/region under the pointer.

example:

bind[focus] = REPLAY+Button3

In the above example, when

is pressed without any modifier(s), focus is set to the window under the
pointer and the button press is passed to the window.

To bind non-latin characters such as å or π you must enter the xkb
character name instead of the character itself. Run

focus the window and press the specific key and in the terminal output
read the symbol name. In the following example for å:

KeyPress event, serial 41, synthetic NO, window 0x2600001, root 0x15a,
subw 0x0, time 106213808, (11,5), root:(359,823), state 0x0, keycode 24
(keysym 0xe5, aring), same_screen YES, XLookupString gives 2 bytes: (c3
a5) "å" XmbLookupString gives 2 bytes: (c3 a5) "å" XFilterEvent returns:
False

The xkb name is aring. In other words, in

add:

bind[program] = MOD+aring

To clear all default keyboard bindings and specify your own, see the

option.

Keyboard mapping files for several keyboard layouts are listed below.
These files can be used with the

setting to load pre-defined key bindings for the specified keyboard
layout.

Czech Republic keyboard layout

Spanish keyboard layout

French keyboard layout

Swiss French keyboard layout

Swedish keyboard layout

United States keyboard layout

provides "quirks" which handle windows that must be treated specially in
a tiling window manager, such as some dialogs and fullscreen apps.

The default quirks are described below:

TRANSSZ

FLOAT

FLOAT + ANYWHERE

FLOAT + FULLSCREEN + FOCUSPREV

FLOAT

FLOAT

FLOAT

FLOAT + ANYWHERE

FLOAT + ANYWHERE

FULLSCREEN + FLOAT

FLOAT + ANYWHERE

FLOAT + ANYWHERE

XTERM_FONTADJ

The quirks themselves are described below:

Allow window to position itself, uncentered.

This window should not be tiled, but allowed to float freely.

When the window first appears on the screen, change focus to the window
if there are no other windows on the workspace with the same WM_CLASS
class/instance value. Has no effect when

is set to

On exit force focus on previously focused application not previous
application in the stack.

Remove border to allow window to use full region size.

Ignore the PID when determining the initial workspace for a new window.
Especially useful for terminal windows that share a process.

Ignore the spawn workspace when determining the initial workspace for a
new window.

Remove border when window is unfocused and floating.

Remove from normal focus cycle (focus_prev or focus_next). The window
can still be focused using search_win.

Don't change focus to the window when it first appears on the screen.
Has no effect when

is set to

When an application requests focus on the window via a
_NET_ACTIVE_WINDOW client message (source indication of 1), comply with
the request. Note that a source indication of 0 (unspecified) or 2
(pager) are always obeyed.

Adjusts size on transient windows that are too small using

(see

Force a new window to appear on workspace

Adjust

fonts when resizing.

Custom quirks in the configuration file are specified as follows:

(optional) and

(optional) are patterns used to determine which window(s) the quirk(s)
apply to and

is one of the quirks from the list above.

Note that patterns are interpreted as POSIX Extended Regular
Expressions. Any ':', '[' or ']' must be escaped with '\'. See

for more information on POSIX Extended Regular Expressions.

For example:

quirk[MPlayer] = FLOAT + FULLSCREEN + FOCUSPREV # Float all windows
having a class of 'MPlayer' quirk[.*] = FLOAT # Float all windows by
default. quirk[.*:.*:.*] = FLOAT # Same as above.
quirk[Firefox:Navigator] = FLOAT # Float all Firefox browser windows.
quirk[::Console] = FLOAT # Float windows with WM_CLASS not set and a
window name of 'Console'. quirk[\[0-9\].*:.*:\[\[\:alnum\:\]\]*] = FLOAT
# Float windows with WM_CLASS class beginning with a number, any
WM_CLASS instance and a _NET_WM_NAME/WM_NAME either blank or containing
alphanumeric characters without spaces. quirk[pcb:pcb] = NONE # remove
existing quirk

You can obtain

and

by running

and then clicking on the desired window. In the following example the
main window of Firefox was clicked:

$ xprop | grep -E "^(WM_CLASS|_NET_WM_NAME|WM_NAME)" WM_CLASS(STRING) =
"Navigator", "Firefox" WM_NAME(STRING) = "spectrwm -
ConformalOpenSource" _NET_WM_NAME(UTF8_STRING) = "spectrwm -
ConformalOpenSource"

Note that

displays WM_CLASS as:

WM_CLASS(STRING) = "<instance>", "<class>"

In the example above the quirk entry would be:

quirk[Firefox:Navigator] = FLOAT

also automatically assigns quirks to windows based on the value of the
window's _NET_WM_WINDOW_TYPE property as follows:

FLOAT + ANYWHERE

FLOAT + ANYWHERE

FLOAT + ANYWHERE

FLOAT

FLOAT

In all other cases, no automatic quirks are assigned to the window.
Quirks specified in the configuration file override the automatic
quirks.

partially implements the Extended Window Manager Hints (EWMH)
specification. This enables controlling windows as well as

itself from external scripts and programs. This is achieved by

responding to certain ClientMessage events. From the terminal these
events can be conveniently sent using tools such as

and

For the actual format of these ClientMessage events, see the EWMH
specification.

The id of the currently focused window is stored in the
_NET_ACTIVE_WINDOW property of the root window. This can be used for
example to retrieve the title of the currently active window with

and

$ WINDOWID=`xprop -root _NET_ACTIVE_WINDOW | grep -o "0x.*"` $ xprop -id
$WINDOWID _NET_WM_NAME | grep -o "\".*\""

A window can be focused by sending a _NET_ACTIVE_WINDOW client message
to the root window. For example, using

to send the message (assuming 0x4a0000b is the id of the window to be
focused):

$ wmctrl -i -a 0x4a0000b

Windows can be closed by sending a _NET_CLOSE_WINDOW client message to
the root window. For example, using

to send the message (assuming 0x4a0000b is the id of the window to be
closed):

$ wmctrl -i -c 0x4a0000b

Windows can be floated and un-floated by adding or removing the
_NET_WM_STATE_ABOVE atom from the _NET_WM_STATE property of the window.
This can be achieved by sending a _NET_WM_STATE client message to the
root window. For example, the following toggles the floating state of a
window using

to send the message (assuming 0x4a0000b is the id of the window to be
floated or un-floated):

$ wmctrl -i -r 0x4a0000b -b toggle,_NET_WM_STATE_ABOVE

Windows can also be iconified and un-iconified by substituting
_NET_WM_STATE_HIDDEN for _NET_WM_STATE_ABOVE in the previous example:

$ wmctrl -i -r 0x4a0000b -b toggle,_NET_WM_STATE_HIDDEN

Floating windows can also be resized and moved by sending a
_NET_MOVERESIZE_WINDOW client message to the root window. For example,
using

to send the message (assuming 0x4a0000b is the id of the window to be
resize/moved):

$ wmctrl -i -r 0x4a0000b -e 0,100,50,640,480

This moves the window to (100,50) and resizes it to 640x480.

Any _NET_MOVERESIZE_WINDOW events received for stacked windows are
ignored.

Sending

a HUP signal will restart it.

user specific settings.

global settings.

was inspired by xmonad & dwm.

was written by:

Information about spectrwm.1 is found in manpage for: [[../libswmhack.so .][libswmhack.so .]]