#+TITLE: Man1 - ndctl-start-scrub.1
#+DESCRIPTION: Linux manpage for ndctl-start-scrub.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ndctl-start-scrub - start an Address Range Scrub (ARS) operation

* SYNOPSIS
#+begin_example
  ndctl start-scrub [<bus-id> <bus-id2> ... <bus-idN>] [<options>]
#+end_example

\\

* DESCRIPTION
NVDIMM Address Range Scrub is a capability provided by platform firmware
that allows for the discovery of memory errors by system software. It
enables system software to pre-emptively avoid accesses that could lead
to uncorrectable memory error handling events, and it otherwise allows
memory errors to be enumerated.

The kernel provides a sysfs file (/scrub/) that when written with the
string "1\n" initiates an ARS operation. The /ndctl start-scrub/
operation starts an ARS, across all specified buses, and the kernel in
turn proceeds to scrub every persistent memory address region on the
specified buses.

* EXAMPLE
Start a scrub on all nvdimm buses in the system. The json listing report
only includes the buses that support ARS operations.

#+begin_quote
  #+begin_example
    # ndctl start-scrub
    [
      {
        "provider":"nfit_test.1",
        "dev":"ndbus3",
        "scrub_state":"active"
      },
      {
        "provider":"nfit_test.0",
        "dev":"ndbus2",
        "scrub_state":"active"
      }
    ]
  #+end_example
#+end_quote

When specifying an individual bus, or if there is only one bus in the
system, the command reports whether ARS support is available.

#+begin_quote
  #+begin_example
    # ndctl start-scrub e820
    error starting scrub: Operation not supported
  #+end_example
#+end_quote

* OPTIONS
-v, --verbose

#+begin_quote
  Emit debug messages for the ARS start process
#+end_quote

* COPYRIGHT
Copyright © 2016 - 2020, Intel Corporation. License GPLv2: GNU GPL
version 2 <http://gnu.org/licenses/gpl.html>. This is free software: you
are free to change and redistribute it. There is NO WARRANTY, to the
extent permitted by law.

* SEE ALSO
ndctl-wait-scrub(1), /"ACPI/
<http://www.uefi.org/sites/default/files/resources/ACPI%206_2_A_Sept29.pdf>
6.2 Specification Section 9.20.7.2 Address Range Scrubbing (ARS)
Overview"
