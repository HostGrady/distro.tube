#+TITLE: Man1 - sogrep.1
#+DESCRIPTION: Linux manpage for sogrep.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sogrep - Find shared library links in an Arch Linux repository

* SYNOPSIS
sogrep [options] repo libname

* DESCRIPTION
Check the soname links database for Arch Linux repositories containing
packages linked to a given shared library. If the repository specified
is "all", then all repositories will be searched, otherwise only the
named repository will be searched.

If the links database does not exist, it will be downloaded first.

* OPTIONS
*-v, --verbose*

#+begin_quote
  Provide detailed output containing the matched links for each package,
  the repository it came from (in the event that all repositories are
  being searched), and, in combination with -r, a progress bar for the
  links database download.
#+end_quote

*-r, --refresh*

#+begin_quote
  Refresh the links databases
#+end_quote

*-h, --help*

#+begin_quote
  Show a help text
#+end_quote

* ENVIRONMENT VARIABLES
*SOLINKS_MIRROR*="https://mirror.foo.com" Alternative mirror to use for
downloading soname links database.

*SOCACHE_DIR*="/path/to/directory"

#+begin_quote
  Directory where soname links database is stored, overrides the default
  directory set by the *XDG_CACHE_HOME* environment variable or the
  *HOME* environment variable if *XDG_CACHE_HOME* is not set.
#+end_quote

* BUGS
Bugs can be reported on the bug tracker /https://bugs.archlinux.org/ in
the Arch Linux category and title prefixed with [devtools] or via
arch-projects@archlinux.org.

* AUTHORS
Maintainers:

#+begin_quote
  ·

  Aaron Griffin <aaronmgriffin@gmail.com>
#+end_quote

#+begin_quote
  ·

  Allan McRae <allan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dan McGee <dan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Evangelos Foutras <evangelos@foutrelis.com>
#+end_quote

#+begin_quote
  ·

  Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
#+end_quote

#+begin_quote
  ·

  Jelle van der Waa <jelle@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Levente Polyak <anthraxx@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Pierre Schmitz <pierre@archlinux.de>
#+end_quote

#+begin_quote
  ·

  Sébastien Luttringer <seblu@seblu.net>
#+end_quote

#+begin_quote
  ·

  Sven-Hendrik Haase <svenstaro@gmail.com>
#+end_quote

#+begin_quote
  ·

  Thomas Bächler <thomas@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the devtools.git
repository.
