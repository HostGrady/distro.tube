#+TITLE: Man1 - sndioctl.1
#+DESCRIPTION: Linux manpage for sndioctl.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

utility can display or manipulate controls of

audio devices, like the output level. The options are as follows:

Dump the raw list of available controls and exit. Useful as a debugging
tool.

Use this

audio device.

Display characteristics of requested controls instead of their values.

Monitor and display audio controls changes.

Suppress printing of the variable name.

Suppress all printing when setting a variable.

Enable verbose mode, a.k.a. multi-channel mode. By default controls
affecting different channels of the same stream are disguised as a
single mono control to hide details that are not essential.

If no commands are specified all valid controls are displayed on

Unless

or

are used, displayed lines are valid commands. The set of available
controls depends on the audio device.

Commands use the following two formats to display and change controls
respectively:

On the left-hand side are specified the control group (if any), the
affected stream name, and the optional channel number. Examples of
left-hand side terms:

If the channel number (including the brackets) is omitted, the command
is applied to all channels.

Values are numbers between 0 and 1. Two-state controls (switches) take
either 0 or 1 as value, typically corresponding to the

and

states respectively.

If a decimal is prefixed by the plus (minus) sign then the given value
is added to (subtracted from) the current value of the control. If

is used instead of a number, then the switch is toggled.

Increase the

control affecting all

channels by 10% of the maximum:

Mute all

channels:

Toggle the above

control:

Allow audio recording and set all

channels to 50%:

# sysctl kern.audio.record=1 $ sndioctl input.mute=0 input.level=0.5
