#+TITLE: Man1 - lxlock.1
#+DESCRIPTION: Linux manpage for lxlock.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxlock - locking utility for LXDE

* SYNOPSIS
*lxlock*

* DESCRIPTION
*lxlock* is a simple script to lock the session, using third
application. It currently those applications, in this order (try the
next one if the application is not available) : light-locker
xscreensaver gnome-screensaver slock xlock i3lock

* AUTHORS
Olivier Fourdan (fourdan@xfce.org)

Julien Lavergne (gilir@ubuntu.com)

Jarno Suni (8@iki.fi)

Man page written to conform with Debian by Julien Lavergne.
