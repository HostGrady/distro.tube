#+TITLE: Man1 - cargo-publish.1
#+DESCRIPTION: Linux manpage for cargo-publish.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-publish - Upload a package to the registry

* SYNOPSIS
*cargo publish* [/options/]

* DESCRIPTION
This command will create a distributable, compressed *.crate* file with
the source code of the package in the current directory and upload it to
a registry. The default registry is <https://crates.io>. This performs
the following steps:

#+begin_quote
  1.Performs a few checks, including:

  #+begin_quote
    ·Checks the *package.publish* key in the manifest for restrictions
    on which registries you are allowed to publish to.
  #+end_quote
#+end_quote

#+begin_quote
  2.Create a *.crate* file by following the steps in *cargo-package*(1).
#+end_quote

#+begin_quote
  3.Upload the crate to the registry. Note that the server will perform
  additional checks on the crate.
#+end_quote

This command requires you to be authenticated with either the *--token*
option or using *cargo-login*(1).

See /the reference/
<https://doc.rust-lang.org/cargo/reference/publishing.html> for more
details about packaging and publishing.

* OPTIONS
** Publish Options
*--dry-run*

#+begin_quote
  Perform all checks without uploading.
#+end_quote

*--token* /token/

#+begin_quote
  API token to use when authenticating. This overrides the token stored
  in the credentials file (which is created by *cargo-login*(1)).

  /Cargo config/ <https://doc.rust-lang.org/cargo/reference/config.html>
  environment variables can be used to override the tokens stored in the
  credentials file. The token for crates.io may be specified with the
  *CARGO_REGISTRY_TOKEN* environment variable. Tokens for other
  registries may be specified with environment variables of the form
  *CARGO_REGISTRIES_NAME_TOKEN* where *NAME* is the name of the registry
  in all capital letters.
#+end_quote

*--no-verify*

#+begin_quote
  Don't verify the contents by building them.
#+end_quote

*--allow-dirty*

#+begin_quote
  Allow working directories with uncommitted VCS changes to be packaged.
#+end_quote

*--index* /index/

#+begin_quote
  The URL of the registry index to use.
#+end_quote

*--registry* /registry/

#+begin_quote
  Name of the registry to publish to. Registry names are defined in
  /Cargo/ config files
  <https://doc.rust-lang.org/cargo/reference/config.html>. If not
  specified, and there is a /*package.publish*/
  <https://doc.rust-lang.org/cargo/reference/manifest.html#the-publish-field>
  field in *Cargo.toml* with a single registry, then it will publish to
  that registry. Otherwise it will use the default registry, which is
  defined by the /*registry.default*/
  <https://doc.rust-lang.org/cargo/reference/config.html#registrydefault>
  config key which defaults to *crates-io*.
#+end_quote

** Package Selection
By default, the package in the current working directory is selected.
The *-p* flag can be used to choose a different package in a workspace.

*-p* /spec/, *--package* /spec/

#+begin_quote
  The package to publish. See *cargo-pkgid*(1) for the SPEC format.
#+end_quote

** Compilation Options
*--target* /triple/

#+begin_quote
  Publish for the given architecture. The default is the host
  architecture. The general format of the triple is
  *<arch><sub>-<vendor>-<sys>-<abi>*. Run *rustc --print target-list*
  for a list of supported targets.

  This may also be specified with the *build.target* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.

  Note that specifying this flag makes Cargo run in a different mode
  where the target artifacts are placed in a separate directory. See the
  /build cache/ <https://doc.rust-lang.org/cargo/guide/build-cache.html>
  documentation for more details.
#+end_quote

*--target-dir* /directory/

#+begin_quote
  Directory for all generated artifacts and intermediate files. May also
  be specified with the *CARGO_TARGET_DIR* environment variable, or the
  *build.target-dir* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  *target* in the root of the workspace.
#+end_quote

** Feature Selection
The feature flags allow you to control which features are enabled. When
no feature options are given, the *default* feature is activated for
every selected package.

See /the features documentation/
<https://doc.rust-lang.org/cargo/reference/features.html#command-line-feature-options>
for more details.

*--features* /features/

#+begin_quote
  Space or comma separated list of features to activate. Features of
  workspace members may be enabled with *package-name/feature-name*
  syntax. This flag may be specified multiple times, which enables all
  specified features.
#+end_quote

*--all-features*

#+begin_quote
  Activate all available features of all selected packages.
#+end_quote

*--no-default-features*

#+begin_quote
  Do not activate the *default* feature of the selected packages.
#+end_quote

** Manifest Options
*--manifest-path* /path/

#+begin_quote
  Path to the *Cargo.toml* file. By default, Cargo searches for the
  *Cargo.toml* file in the current directory or any parent directory.
#+end_quote

*--frozen*, *--locked*

#+begin_quote
  Either of these flags requires that the *Cargo.lock* file is
  up-to-date. If the lock file is missing, or it needs to be updated,
  Cargo will exit with an error. The *--frozen* flag also prevents Cargo
  from attempting to access the network to determine if it is
  out-of-date.

  These may be used in environments where you want to assert that the
  *Cargo.lock* file is up-to-date (such as a CI build) or want to avoid
  network access.
#+end_quote

*--offline*

#+begin_quote
  Prevents Cargo from accessing the network for any reason. Without this
  flag, Cargo will stop with an error if it needs to access the network
  and the network is not available. With this flag, Cargo will attempt
  to proceed without the network if possible.

  Beware that this may result in different dependency resolution than
  online mode. Cargo will restrict itself to crates that are downloaded
  locally, even if there might be a newer version as indicated in the
  local copy of the index. See the *cargo-fetch*(1) command to download
  dependencies before going offline.

  May also be specified with the *net.offline* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Miscellaneous Options
*-j* /N/, *--jobs* /N/

#+begin_quote
  Number of parallel jobs to run. May also be specified with the
  *build.jobs* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  the number of CPUs.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Publish the current package:

  #+begin_quote
    #+begin_example
      cargo publish
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-package*(1), *cargo-login*(1)
