#+TITLE: Man1 - pianobar.1
#+DESCRIPTION: Linux manpage for pianobar.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pianobar - console pandora.com music player

* SYNOPSIS
*pianobar*

* DESCRIPTION
*pianobar* is a lightweight console music player for the personalized
online radio pandora.com.

* FILES
/$XDG_CONFIG_HOME/pianobar/config/ or /~/.config/pianobar/config/

#+begin_quote
  Per-user configuration file. See *CONFIGURATION.*
#+end_quote

//etc/libao.conf/ or /~/.libao/

#+begin_quote
  Global/per-user audio output configuration. See libao documentation at
  http://xiph.org/ao/doc/config.html
#+end_quote

* CONFIGURATION
The configuration file consists of simple *key = value* lines, each
terminated with a newline (\n) character. Keys are case sensitive.

act_* keys control *pianobar's* key-bindings. Every one-byte character
except for \x00 and the special value *disabled* are allowed here.

- *act_help = ?* :: Show keybindings.

- *act_songlove = +* :: Love currently played song.

- *act_songban = -* :: Ban current track. It will not be played again
  and can only removed using the pandora.com web interface.

- *act_stationaddmusic = a* :: Add more music to current station. You
  will be asked for a search string. Just follow the instructions. If
  you're clueless try '?' (without quotes).

- *act_bookmark = b* :: Bookmark current song or artist.

- *act_stationcreate = c* :: Create new station. You have to enter a
  search string and select the song or artist of your choice.

- *act_stationdelete = d* :: Delete current station.

- *act_songexplain = e* :: Explain why this song is played.

- *act_stationaddbygenre = g* :: Add genre station provided by pandora.

- *act_history = h* :: Show history.

- *act_songinfo = i* :: Print information about currently played
  song/station.

- *act_addshared = j* :: Add shared station by id. id is a very long
  integer without "sh" at the beginning.

- *act_managestation = =* :: Delete artist/song seeds or feedback.

- *act_songmove = m* :: Move current song to another station

- *act_songnext = n* :: Skip current song.

- *act_songpause = S* :: Pause playback

- *act_songpausetoggle = p* :: \\

- *act_songpausetoggle2 = <Space>* :: Pause/resume playback

- *act_songplay = P* :: Resume playback

- *act_quit = q* :: Quit *pianobar.*

- *act_stationrename = r* :: Rename currently played station.

- *act_stationchange = s* :: Select another station. The station list
  can be filtered like most lists by entering a search string instead of
  a station number.

- *act_songtired = t* :: Ban song for one month.

- *act_upcoming = u* :: Show next songs in playlist.

- *act_stationcreatefromsong = v* :: Create new station from the current
  song or artist.

- *act_stationselectquickmix = x* :: Select quickmix stations. You can
  toggle the selection with 't', select all with 'a' or select none with
  'n'.

- *act_voldown = (* :: Decrease volume.

- *act_volreset = ^* :: Reset volume.

- *act_volup = )* :: Increase volume.

- *act_settings = !* :: Change Pandora settings.

- *at_icon = @* :: Replacement for %@ in station format string. It's " @
  " by default.

- *audio_quality = {high, medium, low}* :: Select audio quality.

- *audio_pipe = /path/to/fifo* :: Stream decoded, raw audio samples to a
  pipe instead of the default audio device. Use *sample_rate* to enforce
  a fixed sample rate.

- *autoselect = {1,0}* :: Auto-select last remaining item of filtered
  list. Currently enabled for station selection only.

- *autostart_station = stationid* :: Play this station when starting up.
  You can get the *stationid* by pressing *i* or the key you defined in
  *act_songinfo.*

- *ban_icon = </3* :: Icon for banned songs.

- *bind_to = {if!tunX,host!x.x.x.x,..}* :: This sets the interface name
  to use as outgoing network interface. The name can be an interface
  name, an IP address, or a host name. (from CURLOPT_INTERFACE)

It can be used as a replacement for *control_proxy* in conjunction with
OpenVPN's option *route-nopull.*

- *buffer_seconds = 5* :: Audio buffer size in seconds.

- *ca_bundle = /etc/ssl/certs/ca-certificates.crt* :: Path to CA
  certifiate bundle, containing the root and intermediate certificates
  required to validate Pandora's SSL certificate.

- *control_proxy = http://user:password@host:port/* :: Non-american
  users need a proxy to use pandora.com. Only the xmlrpc interface will
  use this proxy. The music is streamed directly.

- *decrypt_password = R=U!LH$O2B#* :: 

- *device = android-generic* :: 

- *encrypt_password = 6#26FRL$ZWD* :: 

- *event_command = path* :: File that is executed when event occurs. See
  section *EVENTCMD*

- *fifo = $XDG_CONFIG_HOME/pianobar/ctl* :: Location of control fifo.
  See section *REMOTE CONTROL*

- *format_list_song = %i) %a - %t%r* :: Available format characters:

*%i* Song number

*%a* Song artist

*%t* Song title

*%r* Rating icon

*%d* Song duration

*%@*

The at_icon

*%s*

Song's station, if not the current station.

- *format_msg_none = %s* :: \\

- *format_msg_info = (i) %s* :: \\

- *format_msg_nowplaying = |> %s* :: \\

- *format_msg_time = # %s* :: \\

- *format_msg_err = /!\\ %s* :: \\

- *format_msg_question = [?] %s* :: \\

- *format_msg_list = \t%s* :: Message format strings. *%s* is replaced
  with the actual message.

- *format_nowplaying_song = "%t" by "%a" on "%l"%r%@%s* :: Now playing
  song message format. Available format characters are:

*%t* Song title

*%a* Song artist

*%l* Album name

*%r* Rating icon

*%@* *at_icon* if station is quickmix, empty otherwise.

*%s* Real station name if quickmix

*%u* Song detail url

- *format_nowplaying_station = Station "%n" (%i)* :: Now playing station
  format. Available format characters are:

*%n* Station name

*%i* Station id

- *format_time = %s%r/%t* :: Time format.

*%e* Elapsed time

*%r* Remaining time

*%s* Sign

*%t* Total time

- *gain_mul = 1.0* :: Pandora sends a ReplayGain value with every song.
  This sets a multiplier so that the gain adjustment can be reduced. 0.0
  means no gain adjustment, 1.0 means full gain adjustment, values
  inbetween reduce the magnitude of gain adjustment.

- *history = 5* :: Keep a history of the last n songs (5, by default).
  You can rate these songs.

- *love_icon = <3* :: Icon for loved songs.

- *max_retry = 3* :: Max failures for several actions before giving up.

- *partner_password = AC7IBG09A3DTSYM4R41UJWL07VLN8JI7* :: 

- *partner_user = android* :: 

- *password = plaintext_password* :: Your pandora.com password.
  Plain-text.

- *password_command = gpg --decrypt ~/password* :: Use output of command
  as password. This setting is overridden by specifying a password with
  *password.*

- *proxy = http://user:password@host:port/* :: Use a http proxy. Note
  that this setting overrides the http_proxy environment variable. Only
  "Basic" http authentication is supported.

- *rpc_host = tuner.pandora.com* :: 

- *rpc_tls_port = 443* :: 

- *sample_rate = 0* :: Force fixed output sample rate. The default, 0,
  uses the stream's sample rate.

- *sort = {name_az, name_za, quickmix_01_name_az, quickmix_01_name_za,
  quickmix_10_name_az, quickmix_10_name_za}* :: Sort station list by
  name or type (is quickmix) and name. name_az for example sorts by name
  from a to z, quickmix_01_name_za by type (quickmix at the bottom) and
  name from z to a.

- *timeout = 30* :: Network operation timeout.

- *tired_icon = zZ* :: Icon for temporarily suspended songs.

- *user = your@user.name* :: Your pandora.com username.

- *volume = 0* :: Initial volume correction in dB. Usually between -30
  and +5.

* REMOTE CONTROL
*pianobar* can be controlled through a fifo. You have to create it
yourself by executing

mkfifo ~/.config/pianobar/ctl

Adjust the path if you set up a $XDG_CONFIG_HOME or changed the fifo
setting. Afterwards you can write commands directly into the fifo.
Example (next song):

echo -n 'n' > ~/.config/pianobar/ctl

*n* is the keybinding for "next song". If you customized your
keybindings you have to use these characters to control *pianobar.*
/This behaviour may change in the future!/

Another example:

while true; do; nc -l -p 12345 -s localhost localhost >
~/.config/pianobar/ctl; sleep 1; done

echo -ne 'n\x1a' | nc -q 0 127.0.0.1 12345

* EVENTCMD
*pianobar* can report certain "events" to an external application (see
*CONFIGURATION* ). This application is started with the event name as
it's first argument. More information like error code and description,
was well as song information related to the current event, is supplied
through stdin.

Currently supported events are: artistbookmark, songban, songbookmark,
songexplain, songfinish, songlove, songmove, songshelf, songstart,
stationaddgenre, stationaddmusic, stationaddshared, stationcreate,
stationdelete, stationdeleteartistseed, stationdeletefeedback,
stationdeletesongseed, stationfetchinfo, stationfetchplaylist,
stationfetchgenre stationquickmixtoggle, stationrename, userlogin,
usergetstations

An example script can be found in the contrib/ directory of *pianobar's*
source distribution.

* AUTHOR
Lars-Dominik Braun <lars@6xq.net>
