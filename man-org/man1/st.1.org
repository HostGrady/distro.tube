#+TITLE: Man1 - st.1
#+DESCRIPTION: Linux manpage for st.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
st - simple terminal

* SYNOPSIS
*st* [*-aiv*] [*-c* /class/] [*-d* /path/] [*-f* /font/] [*-g*
/geometry/] [*-n* /name/] [*-o* /iofile/] [*-T* /title/] [*-t* /title/]
[*-l* /line/] [*-w* /windowid/] [[*-e*] /command/ [/arguments/...]]

*st* [*-aiv*] [*-c* /class/] [*-d* /path/] [*-f* /font/] [*-g*
/geometry/] [*-n* /name/] [*-o* /iofile/] [*-T* /title/] [*-t* /title/]
[*-w* /windowid/] -l /line/ [/stty_args/...]

* DESCRIPTION
*st* is a simple terminal emulator.

* OPTIONS
- *-a* :: disable alternate screens in terminal

- *-c*/ class/ :: defines the window class (default $TERM).

- *-d*/ path/ :: changes the working directory to /path/.

- *-f*/ font/ :: defines the /font/ to use when st is run.

- *-g*/ geometry/ :: defines the X11 geometry string. The form is
  [=][<cols>{xX}<rows>][{+-}<xoffset>{+-}<yoffset>]. See
  *XParseGeometry*(3) for further details.

- *-i* :: will fixate the position given with the -g option.

- *-n*/ name/ :: defines the window instance name (default $TERM).

- *-o*/ iofile/ :: writes all the I/O to /iofile./ This feature is
  useful when recording st sessions. A value of "-" means standard
  output.

- *-T*/ title/ :: defines the window title (default 'st').

- *-t*/ title/ :: defines the window title (default 'st').

- *-w*/ windowid/ :: embeds st within the window identified by
  /windowid/

- *-l*/ line/ :: use a tty /line/ instead of a pseudo terminal. /line/
  should be a (pseudo-)serial device (e.g. /dev/ttyS0 on Linux for
  serial port 0). When this flag is given remaining arguments are used
  as flags for *stty(1).* By default st initializes the serial line to 8
  bits, no parity, 1 stop bit and a 38400 baud rate. The speed is set by
  appending it as last argument (e.g. 'st -l /dev/ttyS0 115200').
  Arguments before the last one are *stty(1)* flags. If you want to set
  odd parity on 115200 baud use for example 'st -l /dev/ttyS0 parenb
  parodd 115200'. Set the number of bits by using for example 'st -l
  /dev/ttyS0 cs7 115200'. See *stty(1)* for more arguments and cases.

- *-v* :: prints version information to stderr, then exits.

- *-e*/ command /*[*/ arguments /*... ]* :: st executes /command/
  instead of the shell. If this is used it *must be the last option* on
  the command line, as in xterm / rxvt. This option is only intended for
  compatibility, and all the remaining arguments are used as a command
  even without it.

* SHORTCUTS
- *Break* :: Send a break in the serial line. Break key is obtained in
  PC keyboards pressing at the same time control and pause.

- *Ctrl-Print Screen* :: Toggle if st should print to the /iofile./

- *Shift-Print Screen* :: Print the full screen to the /iofile./

- *Print Screen* :: Print the selection to the /iofile./

- *Ctrl-Shift-Page Up* :: Increase font size.

- *Ctrl-Shift-Page Down* :: Decrease font size.

- *Ctrl-Shift-Home* :: Reset to default font size.

- *Ctrl-Shift-y* :: Paste from primary selection (middle mouse button).

- *Ctrl-Shift-c* :: Copy the selected text to the clipboard selection.

- *Ctrl-Shift-v* :: Paste from the clipboard selection.

* CUSTOMIZATION
*st* can be customized by creating a custom config.h and (re)compiling
the source code. This keeps it fast, secure and simple.

* AUTHORS
See the LICENSE file for the authors.

* LICENSE
See the LICENSE file for the terms of redistribution.

* SEE ALSO
*tabbed*(1), *utmp*(1), *stty*(1), *scroll*(1)

* BUGS
See the TODO file in the distribution.
