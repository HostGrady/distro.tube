#+TITLE: Man1 - lxpolkit.1
#+DESCRIPTION: Linux manpage for lxpolkit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxpolkit - PolicyKit agent for LXDE

* SYNOPSIS
*lxpolkit*

* DESCRIPTION
*lxpolkit* is a PolicyKit agent which interacts with PolicyKit daemon to
provide extended privileges to applications.

* OPTIONS
*lxpolkit* does not support any options.

* AUTHORS
This manual page is written by Andriy Grytsenko <andrej@rep.kiev.ua>.
