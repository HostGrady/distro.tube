#+TITLE: Man1 - pl2pm.1perl
#+DESCRIPTION: Linux manpage for pl2pm.1perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
pl2pm - Rough tool to translate Perl4 .pl files to Perl5 .pm modules.

* SYNOPSIS
*pl2pm* /files/

* DESCRIPTION
*pl2pm* is a tool to aid in the conversion of Perl4-style .pl library
files to Perl5-style library modules. Usually, your old .pl file will
still work fine and you should only use this tool if you plan to update
your library to use some of the newer Perl 5 features, such as
AutoLoading.

* LIMITATIONS
It's just a first step, but it's usually a good first step.

* AUTHOR
Larry Wall <larry@wall.org>
