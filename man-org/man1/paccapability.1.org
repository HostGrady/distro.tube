#+TITLE: Man1 - paccapability.1
#+DESCRIPTION: Linux manpage for paccapability.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
paccapability - query libalpm capabilities

* SYNOPSIS
paccapability [options] [<capability>...] paccapability
(--help|--version)

* DESCRIPTION
*paccapability* provides a way to query which features libalpm was built
with. Recognized capabilities are:

- ALPM_CAPABILITY_NLS

- ALPM_CAPABILITY_DOWNLOADER

- ALPM_CAPABILITY_SIGNATURES

Capabilities are case-insensitive and may omit the leading
=ALPM_CAPABILITY_=. If no capabilities are provided, all enabled
capabilities will be printed, one per line. Otherwise, each provided
capability will printed, one per line, followed by a one or zero to
indicate that the feature is enabled or disabled, respectively. If any
given capabilities are disabled *paccapability* will exit with a
non-zero value.

* OPTIONS
- --help :: Display usage information and exit.

- --version :: Display version information and exit.

* EXAMPLES
- Check if libalpm was built with signature checking: ::  paccapability
  signatures >/dev/null && ...
