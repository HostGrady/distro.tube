#+TITLE: Man1 - jack_samplerate.1
#+DESCRIPTION: Linux manpage for jack_samplerate.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jack_samplerate - JACK toolkit client to print current samplerate

* SYNOPSIS
*jack_samplerate*

* DESCRIPTION
*jack_samplerate prints the current samplerate, and exits.*
