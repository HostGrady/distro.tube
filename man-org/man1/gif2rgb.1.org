#+TITLE: Man1 - gif2rgb.1
#+DESCRIPTION: Linux manpage for gif2rgb.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gif2rgb - convert images saved as GIF to 24-bit RGB triplets

* SYNOPSIS
*gif2rgb* [-v] [-1] [-c /colors/] [-s /width/ /height/] [-o /outfile/]
[-h] [/gif-file/]

* OPTIONS
-v

#+begin_quote
  Verbose mode (show progress). Enables printout of running scan lines.
#+end_quote

-1

#+begin_quote
  Only one file in the format of RGBRGB... triplets (Each of R, G,B is a
  byte) is being read or written. This file size is 3 * Width * Height.
  If stdin is used for input or stdout for output, this option is
  implicitly applied. The default (if not `-1) is 3 files with the names
  OutFileName.R, OutFileName.G, OutFileName.B, each of which is Width *
  Height bytes.
#+end_quote

-c colors

#+begin_quote
  Specifies number of colors to use in RGB-to-GIF conversions, in bits
  per pixels, so -c 8 actually specifies 256 colors (maximum and
  default).
#+end_quote

-s width height

#+begin_quote
  Sets RGB-to-GIF conversion mode and specifies the size of the image to
  read.
#+end_quote

-o

#+begin_quote
  specifies the name of the out file (see also `-1 above).
#+end_quote

-h

#+begin_quote
  Print one line of command line help, similar to Usage above.
#+end_quote

By default, convert a GIF input file to RGB triplets. If -s is
specified, convert RGB input to a GIF.

If no input file is given, gif2rgb will try to read data from stdin.

* AUTHOR
Gershon Elber.
