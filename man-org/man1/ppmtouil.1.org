#+TITLE: Man1 - ppmtouil.1
#+DESCRIPTION: Linux manpage for ppmtouil.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*ppmtouil* - replaced by pamtouil

* DESCRIPTION
This program is part of *Netpbm*(1) In May 2002, *ppmtouil* was extended
and renamed to *pamtouil*(1)
