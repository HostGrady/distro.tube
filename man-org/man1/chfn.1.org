#+TITLE: Man1 - chfn.1
#+DESCRIPTION: Linux manpage for chfn.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
chfn - change your finger information

* SYNOPSIS
*chfn* [*-f* /full-name/] [*-o* /office/] [*-p* /office-phone/] [*-h*
/home-phone/] [*-u*] [*-v*] [/username/]

* DESCRIPTION
*chfn* is used to change your finger information. This information is
stored in the //etc/passwd/ file, and is displayed by the *finger*
program. The Linux *finger* command will display four pieces of
information that can be changed by *chfn*: your real name, your work
room and phone, and your home phone.

Any of the four pieces of information can be specified on the command
line. If no information is given on the command line, *chfn* enters
interactive mode.

In interactive mode, *chfn* will prompt for each field. At a prompt, you
can enter the new information, or just press return to leave the field
unchanged. Enter the keyword "none" to make the field blank.

*chfn* supports non-local entries (kerberos, LDAP, etc.) if linked with
libuser, otherwise use *ypchfn*(1), *lchfn*(1) or any other
implementation for non-local entries.

* OPTIONS
*-f*, *--full-name* /full-name/

#+begin_quote
  Specify your real name.
#+end_quote

*-o*, *--office* /office/

#+begin_quote
  Specify your office room number.
#+end_quote

*-p*, *--office-phone* /office-phone/

#+begin_quote
  Specify your office phone number.
#+end_quote

*-h*, *--home-phone* /home-phone/

#+begin_quote
  Specify your home phone number.
#+end_quote

*-u*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-v*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

* CONFIG FILE ITEMS
*chfn* reads the //etc/login.defs/ configuration file (see
*login.defs*(5)). Note that the configuration file could be distributed
with another package (e.g., shadow-utils). The following configuration
items are relevant for *chfn*:

*CHFN_RESTRICT* /string/

#+begin_quote
  Indicate which fields are changeable by *chfn*.

  The boolean setting *"yes"* means that only the Office, Office Phone
  and Home Phone fields are changeable, and boolean setting *"no"* means
  that also the Full Name is changeable.

  Another way to specify changeable fields is by abbreviations: f = Full
  Name, r = Office (room), w = Office (work) Phone, h = Home Phone. For
  example, *CHFN_RESTRICT "wh"* allows changing work and home phone
  numbers.

  If *CHFN_RESTRICT* is undefined, then all finger information is
  read-only. This is the default.
#+end_quote

* EXIT STATUS
Returns 0 if operation was successful, 1 if operation failed or command
syntax was not valid.

* AUTHORS
* SEE ALSO
*chsh*(1), *finger*(1), *login.defs*(5), *passwd*(5)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *chfn* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
