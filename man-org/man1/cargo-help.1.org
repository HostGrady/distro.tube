#+TITLE: Man1 - cargo-help.1
#+DESCRIPTION: Linux manpage for cargo-help.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-help - Get help for a Cargo command

* SYNOPSIS
*cargo help* [/subcommand/]

* DESCRIPTION
Prints a help message for the given command.

* EXAMPLES

#+begin_quote
  1.Get help for a command:

  #+begin_quote
    #+begin_example
      cargo help build
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  2.Help is also available with the *--help* flag:

  #+begin_quote
    #+begin_example
      cargo build --help
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1)
