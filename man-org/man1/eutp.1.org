#+TITLE: Man1 - eutp.1
#+DESCRIPTION: Linux manpage for eutp.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
eutp - EuroBraille file transferring

* SYNOPSIS
eutp

* DESCRIPTION
/eutp/ lets you exchange files with a Clio terminal from EuroBraile.

* COMMAND-LINE OPTIONS
None so far

* RETURNED VALUE
| 1 | error |

* SHELL EXPANSIONS
Beware of special chars: * and . are often expanded by your shell, hence
/sending files */ will probably do what you want, putting every file
existing in the current directory onto the terminal, but / receiving
files */ may not do what you want: it will only get every file which
already exist in the current directory, skipping those you just created
on your terminal ! If you want to get every file which exist in the
terminal, you should use /'*'/ or something similar (please read your
shell manual).

The same warning applies to other special chars, such as $, ~, &,...
which should be protected by surrounding arguments by quotes (') or by
using single backslashes (\) just before them (please read your shell
manual).

* AUTHOR
Olivier Bert <olivier.bert@laposte.net>
