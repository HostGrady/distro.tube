#+TITLE: Man1 - gts2stl.1
#+DESCRIPTION: Linux manpage for gts2stl.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gts2stl - converts a GTS file to STL format

* SYNOPSIS
*gts2stl* [ /OPTIONS/ ] < /input.gts /> /output.stl/

* DESCRIPTION
This manual page documents briefly the *gts2stl* command.

* OPTIONS
This program follow the usual GNU command line syntax, with long options
starting with two dashes (`-'). A summary of options is included below.

- *-r*, *--revert* :: Revert face normals.

- *-v*, *--verbose* :: Display surface statistics.

- *-h*, *--help* :: Display the help and exit.

* AUTHOR
gts2stl was written by Stephane Popinet <popinet@users.sourceforge.net>.

This manual page was written by Ruben Molina <rmolina@udea.edu.co>, for
the Debian project (but may be used by others).
