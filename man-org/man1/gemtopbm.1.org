#+TITLE: Man1 - gemtopbm.1
#+DESCRIPTION: Linux manpage for gemtopbm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*gemtopbm* - replaced by gemtopnm

* DESCRIPTION
This program is part of *Netpbm*(1)

*gemtopbm* was replaced in Netpbm 9.1 (May 2000) by *gemtopnm*(1)

*gemtopnm* is backward compatible with *gemtopbm*, but works on color
images as well.
