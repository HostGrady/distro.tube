#+TITLE: Manpages - libbsd.7
#+DESCRIPTION: Linux manpage for libbsd.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

library provides a set of compatibility macros and functions commonly
found on BSD-based systems. Its purpose is to make those available on
non-BSD based systems to ease portability.

The library can be used in an overlay mode, which is the preferred way,
so that the code is portable and requires no modification to the
original BSD code. This can be done easily with the

library named

Or by adding the system-specific include directory with the

suffix to the list of system include paths. With

this could be

In addition the

pre-processor variable needs to be defined. The includes in this case
should be the usual system ones, such as

The other way to use the library is to use the namespaced headers, this
is less portable as it makes using

mandatory and it will not work on BSD-based systems, and requires
modifying original BSD code. This can be done with the

library named

The includes in this case should be namespaced with

such as

The package also provides a

static library that can be used to inject automatic constructors into a
program so that the

function gets invoked automatically at startup time. This can be done
with the

library named

The following are the headers provided by

that extend the standard system headers. They can work in normal or
overlay modes, for the former they need to be prefixed with

The following is a

specific convenience header, that includes some of the extended headers.
It only works in non-overlay mode.

Some functions have different prototypes depending on the BSD where they
originated from, and these various implementations provided are
selectable at build-time.

This is the list of functions that provide multiple implementations:

added

and

but unfortunately made it incompatible with the existing one in

and Freedesktop's libbsd (the former having existed for over ten years).
Despite this incompatibility being reported during development (see
http://gnats.netbsd.org/44977) they still shipped it. Even more
unfortunately

and later MacOS picked up this incompatible implementation.

Provide both implementations and default for now to the historical one
to avoid breakage, but we will switch to the

one in a later release, which is internally consistent with the other

functions and is now more widespread. Define

to switch to the

Define

to keep using the

one.

Some functions have been deprecated, they will emit warnings at compile
time and possibly while being linked at run-time. This might be due to
the functions not being portable at all to other systems, making the
package not buildable there; not portable in a correct or non-buggy way;
or because there are better more portable replacements now.

This is the list of currently deprecated macros and functions:

Unportable, requires assistance from the stdio layer. An implementation
has to choose between leaking buffers or being reentrant for a limited
amount of streams (this implementation chose the latter with a limit of
32). Use

instead, which is available in many systems and required by

Unportable, requires assistance from the stdio layer. An implementation
has to choose between leaking buffers or being reentrant for a limited
amount of streams (this implementation chose the latter with a limit of
32). Use

instead, which is available in many systems and required by

and

Unportable, requires assistance from the stdio layer or some hook
framework. On GNU systems the

function can be used. Otherwise the code needs to be prepared for
neither of these functions being available.

Some functions have been superseded by implementations in other system
libraries, and might disappear on the next SONAME bump, assuming those
other implementation have widespread deployment, or the implementations
are present in all major

for example.

The set of MD5 digest functions are now proxies for the implementations
provided by the

companion library, so it is advised to switch to use that directly
instead.

This function is provided by

2.25.

This function is provided by

2.26.

The

project started in the Debian GNU/kFreeBSD port as a way to ease porting
code from FreeBSD to the GNU-based system. Pretty early on it was
generalized and a project created on FreeDesktop.org for other
distributions and projects to use.

It is now distributed as part of most non-BSD distributions.
