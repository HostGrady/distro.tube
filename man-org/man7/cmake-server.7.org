#+TITLE: Manpages - cmake-server.7
#+DESCRIPTION: Linux manpage for cmake-server.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cmake-server - CMake Server

The *cmake(1)* server mode has been removed since CMake 3.20. Clients
should use the *cmake-file-api(7)* instead.

* COPYRIGHT
2000-2021 Kitware, Inc. and Contributors
