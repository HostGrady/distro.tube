#+TITLE: Manpages - grpck.8
#+DESCRIPTION: Linux manpage for grpck.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grpck - verify integrity of group files

* SYNOPSIS
*grpck* [options] [/group/ [ /shadow/ ]]

* DESCRIPTION
The *grpck* command verifies the integrity of the groups information. It
checks that all entries in /etc/group and /etc/gshadow have the proper
format and contain valid data. The user is prompted to delete entries
that are improperly formatted or which have other uncorrectable errors.

Checks are made to verify that each entry has:

#+begin_quote
  ·

  the correct number of fields
#+end_quote

#+begin_quote
  ·

  a unique and valid group name
#+end_quote

#+begin_quote
  ·

  a valid group identifier (/etc/group only)
#+end_quote

#+begin_quote
  ·

  a valid list of members and administrators
#+end_quote

#+begin_quote
  ·

  a corresponding entry in the /etc/gshadow file (respectively
  /etc/group for the gshadow checks)
#+end_quote

The checks for correct number of fields and unique group name are fatal.
If an entry has the wrong number of fields, the user will be prompted to
delete the entire line. If the user does not answer affirmatively, all
further checks are bypassed. An entry with a duplicated group name is
prompted for deletion, but the remaining checks will still be made. All
other errors are warnings and the user is encouraged to run the
*groupmod* command to correct the error.

The commands which operate on the /etc/group and /etc/gshadow files are
not able to alter corrupted or duplicated entries. *grpck* should be
used in those circumstances to remove the offending entries.

* OPTIONS
The *-r* and *-s* options cannot be combined.

The options which apply to the *grpck* command are:

*-h*, *--help*

#+begin_quote
  Display help message and exit.
#+end_quote

*-r*, *--read-only*

#+begin_quote
  Execute the *grpck* command in read-only mode. This causes all
  questions regarding changes to be answered /no/ without user
  intervention.
#+end_quote

*-R*, *--root* /CHROOT_DIR/

#+begin_quote
  Apply changes in the /CHROOT_DIR/ directory and use the configuration
  files from the /CHROOT_DIR/ directory.
#+end_quote

*-s*, *--sort*

#+begin_quote
  Sort entries in /etc/group and /etc/gshadow by GID.
#+end_quote

By default, *grpck* operates on /etc/group and /etc/gshadow. The user
may select alternate files with the /group/ and /shadow/ parameters.

* CONFIGURATION
The following configuration variables in /etc/login.defs change the
behavior of this tool:

*MAX_MEMBERS_PER_GROUP* (number)

#+begin_quote
  Maximum members per group entry. When the maximum is reached, a new
  group entry (line) is started in /etc/group (with the same name, same
  password, and same GID).

  The default value is 0, meaning that there are no limits in the number
  of members in a group.

  This feature (split group) permits to limit the length of lines in the
  group file. This is useful to make sure that lines for NIS groups are
  not larger than 1024 characters.

  If you need to enforce such limit, you can use 25.

  Note: split groups may not be supported by all tools (even in the
  Shadow toolsuite). You should not use this variable unless you really
  need it.
#+end_quote

* FILES
/etc/group

#+begin_quote
  Group account information.
#+end_quote

/etc/gshadow

#+begin_quote
  Secure group account information.
#+end_quote

/etc/passwd

#+begin_quote
  User account information.
#+end_quote

* EXIT VALUES
The *grpck* command exits with the following values:

/0/

#+begin_quote
  success
#+end_quote

/1/

#+begin_quote
  invalid command syntax
#+end_quote

/2/

#+begin_quote
  one or more bad group entries
#+end_quote

/3/

#+begin_quote
  cant open group files
#+end_quote

/4/

#+begin_quote
  cant lock group files
#+end_quote

/5/

#+begin_quote
  cant update group files
#+end_quote

* SEE ALSO
*group*(5), *groupmod*(8), *gshadow*(5), *passwd*(5), *pwck*(8),
*shadow*(5).
