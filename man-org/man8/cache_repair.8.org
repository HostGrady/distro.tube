#+TITLE: Manpages - cache_repair.8
#+DESCRIPTION: Linux manpage for cache_repair.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*cache_repair *- repair cache binary metadata from device/file to
device/file.

* SYNOPSIS
#+begin_example
  cache_repair [options] -i {device|file} -o {device|file}
#+end_example

* DESCRIPTION
*cache_repair reads binary cache metadata created by the respective*
device-mapper target from one device or file, repairs it and writes it
to another device or file. If written to a metadata device, the metadata
can be processed by the device-mapper target.

This tool cannot be run on live metadata.

* OPTIONS
- **-h, --help** :: Print help and exit.

- **-V, --version** :: Print version information and exit.

- **-i, --input {device|file}** :: Input file or device containing
  binary metadata.

- **-o, --output {device|file}** :: Output file or device for repaired
  binary metadata.

#+begin_example
      If a file is then it must be preallocated, and large enough to hold the
      metadata.
#+end_example

* EXAMPLE
Reads the binary cache metadata from file metadata, repairs it and
writes it to logical volume /dev/vg/metadata for further processing by
the respective device-mapper target:

#+begin_example
      $ cache_repair -i metadata -o /dev/vg/metadata
#+end_example

* DIAGNOSTICS
*cache_repair returns an exit code of 0 for success or 1 for error.*

* SEE ALSO
*cache_dump(8), cache_check(8), cache_restore(8)*

* AUTHOR
Joe Thornber <ejt@redhat.com>, Heinz Mauelshagen <HeinzM@RedHat.com>
