#+TITLE: Manpages - addgnupghome.8
#+DESCRIPTION: Linux manpage for addgnupghome.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*addgnupghome* - Create .gnupg home directories

* SYNOPSIS
*addgnupghome* /account_1/ /account_2/.../account_n/

* DESCRIPTION
If GnuPG is installed on a system with existing user accounts, it is
sometimes required to populate the GnuPG home directory with existing
files. Especially a ‘/trustlist.txt/' and a keybox with some initial
certificates are often desired. This script helps to do this by copying
all files from ‘//etc/skel/.gnupg/' to the home directories of the
accounts given on the command line. It takes care not to overwrite
existing GnuPG home directories.

*addgnupghome* is invoked by root as:

#+begin_quote
  #+begin_example
    addgnupghome account1 account2 ... accountn
  #+end_example
#+end_quote
