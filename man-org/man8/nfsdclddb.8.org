#+TITLE: Manpages - nfsdclddb.8
#+DESCRIPTION: Linux manpage for nfsdclddb.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
nfsdclddb - Tool for manipulating the nfsdcld sqlite database

* SYNOPSIS
*nfsdclddb* [*-h*|*--help*]

*nfsdclddb* [*-p*|*--path* /dbpath/] *fix-table-names* [*-h*|*--help*]

*nfsdclddb* [*-p*|*--path* /dbpath/] *downgrade-schema* [*-h*|*--help*]
[*-v*|*--version* /to-version/]

*nfsdclddb* [*-p*|*--path* /dbpath/] *print* [*-h*|*--help*]
[*-s*|*--summary*]

* DESCRIPTION
The *nfsdclddb* command is provided to perform some manipulation of the
nfsdcld sqlite database schema and to print the contents of the
database.

** Sub-commands
Valid *nfsdclddb* subcommands are:

- fix-table-names :: A previous version of *nfsdcld*(8) contained a bug
  that corrupted the reboot epoch table names. This sub-command will fix
  those table names.

- downgrade-schema :: Downgrade the database schema. Currently the
  schema can only to downgraded from version 4 to version 3.

- print :: Display the contents of the database. Prints the schema
  version and the values of the current and recovery epochs. If the
  *-s*|*--summary* option is not given, also prints the clients in the
  reboot epoch tables.

* OPTIONS
** Options valid for all sub-commands
- *-h, --help* :: Show the help message and exit

- *-p */dbpath/, *--path */dbpath/ :: Open the sqlite database located
  at /dbpath/ instead of //var/lib/nfs/nfsdcld/main.sqlite/. This is
  mainly for testing purposes.

** Options specific to the downgrade-schema sub-command
- *-v */to-version/, *--version */to-version/ :: The schema version to
  downgrade to. Currently the schema can only be downgraded to
  version 3.

** Options specific to the print sub-command
- *-s, --summary* :: Do not list the clients in the reboot epoch tables
  in the output.

* NOTES
The *nfsdclddb* command will not allow the *fix-table-names* or
*downgrade-schema* subcommands to be used if *nfsdcld*(8) is running.

* FILES
- */var/lib/nfs/nfsdcld/main.sqlite* :: 

* SEE ALSO
*nfsdcld*(8)

* AUTHOR
Scott Mayhew <smayhew@redhat.com>
