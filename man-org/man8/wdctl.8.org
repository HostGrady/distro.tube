#+TITLE: Manpages - wdctl.8
#+DESCRIPTION: Linux manpage for wdctl.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wdctl - show hardware watchdog status

* SYNOPSIS
*wdctl* [options] [/device/...]

* DESCRIPTION
Show hardware watchdog status. The default device is //dev/watchdog/. If
more than one device is specified then the output is separated by one
blank line.

If the device is already used or user has no permissions to read from
the device, then *wdctl* reads data from sysfs. In this case information
about supported features (flags) might be missing.

Note that the number of supported watchdog features is hardware
specific.

* OPTIONS
*-f*, *--flags* /list/

#+begin_quote
  Print only the specified flags.
#+end_quote

*-F*, *--noflags*

#+begin_quote
  Do not print information about flags.
#+end_quote

*-I*, *--noident*

#+begin_quote
  Do not print watchdog identity information.
#+end_quote

*-n*, *--noheadings*

#+begin_quote
  Do not print a header line for flags table.
#+end_quote

*-o*, *--output* /list/

#+begin_quote
  Define the output columns to use in table of watchdog flags. If no
  output arrangement is specified, then a default set is used. Use
  *--help* to get list of all supported columns.
#+end_quote

*-O*, *--oneline*

#+begin_quote
  Print all wanted information on one line in key="value" output format.
#+end_quote

*-r*, *--raw*

#+begin_quote
  Use the raw output format.
#+end_quote

*-s*, *-settimeout* /seconds/

#+begin_quote
  Set the watchdog timeout in seconds.
#+end_quote

*-T*, *--notimeouts*

#+begin_quote
  Do not print watchdog timeouts.
#+end_quote

*-x*, *--flags-only*

#+begin_quote
  Same as *-I -T*.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* AUTHORS
* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *wdctl* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
