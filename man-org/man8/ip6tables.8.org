#+TITLE: Manpages - ip6tables.8
#+DESCRIPTION: Linux manpage for ip6tables.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ip6tables.8 is found in manpage for: [[../man8/iptables.8][man8/iptables.8]]