#+TITLE: Manpages - systemd-network-generator.service.8
#+DESCRIPTION: Linux manpage for systemd-network-generator.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-network-generator.service, systemd-network-generator - Generate
network configuration from the kernel command line

* SYNOPSIS
systemd-network-generator.service

/usr/lib/systemd/systemd-network-generator

* DESCRIPTION
systemd-network-generator.service is a system service that translates
/ip=/ and the related settings on the kernel command line (see below)
into *systemd.network*(5), *systemd.netdev*(5), and *systemd.link*(5)
configuration files understood by *systemd-networkd.service*(8) and
*systemd-udevd.service*(8).

Files are generated in /run/systemd/network/.

* KERNEL COMMAND LINE OPTIONS
This tool understands the following options:

/ip=/, /rd.route=/, /rd.peerdns=/

#+begin_quote
  --- translated into *systemd.network*(5) files.
#+end_quote

/ifname=/

#+begin_quote
  --- translated into *systemd.link*(5) files.
#+end_quote

/vlan=/, /bond=/, /bridge=/, /bootdev=/

#+begin_quote
  --- translated into *systemd.netdev*(5) files.
#+end_quote

See *dracut.kernel*(7) for option syntax and details.

* SEE ALSO
*systemd*(1), *systemd-networkd.service*(8), *dracut*(8)
