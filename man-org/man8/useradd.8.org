#+TITLE: Manpages - useradd.8
#+DESCRIPTION: Linux manpage for useradd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
useradd - create a new user or update default new user information

* SYNOPSIS
*useradd* [/options/] /LOGIN/

*useradd* -D

*useradd* -D [/options/]

* DESCRIPTION
When invoked without the *-D* option, the *useradd* command creates a
new user account using the values specified on the command line plus the
default values from the system. Depending on command line options, the
*useradd* command will update system files and may also create the new
users home directory and copy initial files.

By default, a group will also be created for the new user (see *-g*,
*-N*, *-U*, and *USERGROUPS_ENAB*).

* OPTIONS
The options which apply to the *useradd* command are:

*--badname*

#+begin_quote
  Allow names that do not conform to standards.
#+end_quote

*-b*, *--base-dir* /BASE_DIR/

#+begin_quote
  The default base directory for the system if *-d* /HOME_DIR/ is not
  specified. /BASE_DIR/ is concatenated with the account name to define
  the home directory. If the *-m* option is not used, /BASE_DIR/ must
  exist.

  If this option is not specified, *useradd* will use the base directory
  specified by the *HOME* variable in /etc/default/useradd, or /home by
  default.
#+end_quote

*-c*, *--comment* /COMMENT/

#+begin_quote
  Any text string. It is generally a short description of the login, and
  is currently used as the field for the users full name.
#+end_quote

*-d*, *--home-dir* /HOME_DIR/

#+begin_quote
  The new user will be created using /HOME_DIR/ as the value for the
  users login directory. The default is to append the /LOGIN/ name to
  /BASE_DIR/ and use that as the login directory name. The directory
  /HOME_DIR/ does not have to exist but will not be created if it is
  missing.
#+end_quote

*-D*, *--defaults*

#+begin_quote
  See below, the subsection "Changing the default values".
#+end_quote

*-e*, *--expiredate* /EXPIRE_DATE/

#+begin_quote
  The date on which the user account will be disabled. The date is
  specified in the format /YYYY-MM-DD/.

  If not specified, *useradd* will use the default expiry date specified
  by the *EXPIRE* variable in /etc/default/useradd, or an empty string
  (no expiry) by default.
#+end_quote

*-f*, *--inactive* /INACTIVE/

#+begin_quote
  The number of days after a password expires until the account is
  permanently disabled. A value of 0 disables the account as soon as the
  password has expired, and a value of -1 disables the feature.

  If not specified, *useradd* will use the default inactivity period
  specified by the *INACTIVE* variable in /etc/default/useradd, or -1 by
  default.
#+end_quote

*-g*, *--gid* /GROUP/

#+begin_quote
  The group name or number of the users initial login group. The group
  name must exist. A group number must refer to an already existing
  group.

  If not specified, the behavior of *useradd* will depend on the
  *USERGROUPS_ENAB* variable in /etc/login.defs. If this variable is set
  to /yes/ (or *-U/--user-group* is specified on the command line), a
  group will be created for the user, with the same name as her
  loginname. If the variable is set to /no/ (or *-N/--no-user-group* is
  specified on the command line), useradd will set the primary group of
  the new user to the value specified by the *GROUP* variable in
  /etc/default/useradd, or 100 by default.
#+end_quote

*-G*, *--groups* /GROUP1/[/,GROUP2,.../[/,GROUPN/]]]

#+begin_quote
  A list of supplementary groups which the user is also a member of.
  Each group is separated from the next by a comma, with no intervening
  whitespace. The groups are subject to the same restrictions as the
  group given with the *-g* option. The default is for the user to
  belong only to the initial group.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help message and exit.
#+end_quote

*-k*, *--skel* /SKEL_DIR/

#+begin_quote
  The skeleton directory, which contains files and directories to be
  copied in the users home directory, when the home directory is created
  by *useradd*.

  This option is only valid if the *-m* (or *--create-home*) option is
  specified.

  If this option is not set, the skeleton directory is defined by the
  *SKEL* variable in /etc/default/useradd or, by default, /etc/skel.

  If possible, the ACLs and extended attributes are copied.
#+end_quote

*-K*, *--key* /KEY/=/VALUE/

#+begin_quote
  Overrides /etc/login.defs defaults (*UID_MIN*, *UID_MAX*, *UMASK*,
  *PASS_MAX_DAYS* and others).

  Example: *-K* /PASS_MAX_DAYS/=/-1/ can be used when creating system
  account to turn off password aging, even though system account has no
  password at all. Multiple *-K* options can be specified, e.g.: *-K*
  /UID_MIN/=/100/ *-K* /UID_MAX/=/499/
#+end_quote

*-l*, *--no-log-init*

#+begin_quote
  Do not add the user to the lastlog and faillog databases.

  By default, the users entries in the lastlog and faillog databases are
  reset to avoid reusing the entry from a previously deleted user.
#+end_quote

*-m*, *--create-home*

#+begin_quote
  Create the users home directory if it does not exist. The files and
  directories contained in the skeleton directory (which can be defined
  with the *-k* option) will be copied to the home directory.

  By default, if this option is not specified and *CREATE_HOME* is not
  enabled, no home directories are created.
#+end_quote

*-M*, *--no-create-home*

#+begin_quote
  Do no create the users home directory, even if the system wide setting
  from /etc/login.defs (*CREATE_HOME*) is set to /yes/.
#+end_quote

*-N*, *--no-user-group*

#+begin_quote
  Do not create a group with the same name as the user, but add the user
  to the group specified by the *-g* option or by the *GROUP* variable
  in /etc/default/useradd.

  The default behavior (if the *-g*, *-N*, and *-U* options are not
  specified) is defined by the *USERGROUPS_ENAB* variable in
  /etc/login.defs.
#+end_quote

*-o*, *--non-unique*

#+begin_quote
  Allow the creation of a user account with a duplicate (non-unique)
  UID.

  This option is only valid in combination with the *-u* option.
#+end_quote

*-p*, *--password* /PASSWORD/

#+begin_quote
  The encrypted password, as returned by *crypt*(3). The default is to
  disable the password.

  *Note:* This option is not recommended because the password (or
  encrypted password) will be visible by users listing the processes.

  You should make sure the password respects the systems password
  policy.
#+end_quote

*-r*, *--system*

#+begin_quote
  Create a system account.

  System users will be created with no aging information in /etc/shadow,
  and their numeric identifiers are chosen in the
  *SYS_UID_MIN*-*SYS_UID_MAX* range, defined in /etc/login.defs, instead
  of *UID_MIN*-*UID_MAX* (and their *GID* counterparts for the creation
  of groups).

  Note that *useradd* will not create a home directory for such a user,
  regardless of the default setting in /etc/login.defs (*CREATE_HOME*).
  You have to specify the *-m* options if you want a home directory for
  a system account to be created.
#+end_quote

*-R*, *--root* /CHROOT_DIR/

#+begin_quote
  Apply changes in the /CHROOT_DIR/ directory and use the configuration
  files from the /CHROOT_DIR/ directory.
#+end_quote

*-P*, *--prefix* /PREFIX_DIR/

#+begin_quote
  Apply changes in the /PREFIX_DIR/ directory and use the configuration
  files from the /PREFIX_DIR/ directory. This option does not chroot and
  is intended for preparing a cross-compilation target. Some
  limitations: NIS and LDAP users/groups are not verified. PAM
  authentication is using the host files. No SELINUX support.
#+end_quote

*-s*, *--shell* /SHELL/

#+begin_quote
  The name of the users login shell. The default is to leave this field
  blank, which causes the system to select the default login shell
  specified by the *SHELL* variable in /etc/default/useradd, or an empty
  string by default.
#+end_quote

*-u*, *--uid* /UID/

#+begin_quote
  The numerical value of the users ID. This value must be unique, unless
  the *-o* option is used. The value must be non-negative. The default
  is to use the smallest ID value greater than or equal to *UID_MIN* and
  greater than every other user.

  See also the *-r* option and the *UID_MAX* description.
#+end_quote

*-U*, *--user-group*

#+begin_quote
  Create a group with the same name as the user, and add the user to
  this group.

  The default behavior (if the *-g*, *-N*, and *-U* options are not
  specified) is defined by the *USERGROUPS_ENAB* variable in
  /etc/login.defs.
#+end_quote

*-Z*, *--selinux-user* /SEUSER/

#+begin_quote
  The SELinux user for the users login. The default is to leave this
  field blank, which causes the system to select the default SELinux
  user.
#+end_quote

** Changing the default values
When invoked with only the *-D* option, *useradd* will display the
current default values. When invoked with *-D* plus other options,
*useradd* will update the default values for the specified options.
Valid default-changing options are:

*-b*, *--base-dir* /BASE_DIR/

#+begin_quote
  The path prefix for a new users home directory. The users name will be
  affixed to the end of /BASE_DIR/ to form the new users home directory
  name, if the *-d* option is not used when creating a new account.

  This option sets the *HOME* variable in /etc/default/useradd.
#+end_quote

*-e*, *--expiredate* /EXPIRE_DATE/

#+begin_quote
  The date on which the user account is disabled.

  This option sets the *EXPIRE* variable in /etc/default/useradd.
#+end_quote

*-f*, *--inactive* /INACTIVE/

#+begin_quote
  The number of days after a password has expired before the account
  will be disabled.

  This option sets the *INACTIVE* variable in /etc/default/useradd.
#+end_quote

*-g*, *--gid* /GROUP/

#+begin_quote
  The group name or ID for a new users initial group (when the
  *-N/--no-user-group* is used or when the *USERGROUPS_ENAB* variable is
  set to /no/ in /etc/login.defs). The named group must exist, and a
  numerical group ID must have an existing entry.

  This option sets the *GROUP* variable in /etc/default/useradd.
#+end_quote

*-s*, *--shell* /SHELL/

#+begin_quote
  The name of a new users login shell.

  This option sets the *SHELL* variable in /etc/default/useradd.
#+end_quote

* NOTES
The system administrator is responsible for placing the default user
files in the /etc/skel/ directory (or any other skeleton directory
specified in /etc/default/useradd or on the command line).

* CAVEATS
You may not add a user to a NIS or LDAP group. This must be performed on
the corresponding server.

Similarly, if the username already exists in an external user database
such as NIS or LDAP, *useradd* will deny the user account creation
request.

Usernames must start with a lower case letter or an underscore, followed
by lower case letters, digits, underscores, or dashes. They can end with
a dollar sign. In regular expression terms: [a-z_][a-z0-9_-]*[$]?

Usernames may only be up to 32 characters long.

* CONFIGURATION
The following configuration variables in /etc/login.defs change the
behavior of this tool:

*CREATE_HOME* (boolean)

#+begin_quote
  Indicate if a home directory should be created by default for new
  users.

  This setting does not apply to system users, and can be overridden on
  the command line.
#+end_quote

*GID_MAX* (number), *GID_MIN* (number)

#+begin_quote
  Range of group IDs used for the creation of regular groups by
  *useradd*, *groupadd*, or *newusers*.

  The default value for *GID_MIN* (resp. *GID_MAX*) is 1000 (resp.
  60000).
#+end_quote

*HOME_MODE* (number)

#+begin_quote
  The mode for new home directories. If not specified, the *UMASK* is
  used to create the mode.

  *useradd* and *newusers* use this to set the mode of the home
  directory they create.
#+end_quote

*LASTLOG_UID_MAX* (number)

#+begin_quote
  Highest user ID number for which the lastlog entries should be
  updated. As higher user IDs are usually tracked by remote user
  identity and authentication services there is no need to create a huge
  sparse lastlog file for them.

  No *LASTLOG_UID_MAX* option present in the configuration means that
  there is no user ID limit for writing lastlog entries.
#+end_quote

*MAIL_DIR* (string)

#+begin_quote
  The mail spool directory. This is needed to manipulate the mailbox
  when its corresponding user account is modified or deleted. If not
  specified, a compile-time default is used.
#+end_quote

*MAIL_FILE* (string)

#+begin_quote
  Defines the location of the users mail spool files relatively to their
  home directory.
#+end_quote

The *MAIL_DIR* and *MAIL_FILE* variables are used by *useradd*,
*usermod*, and *userdel* to create, move, or delete the users mail
spool.

If *MAIL_CHECK_ENAB* is set to /yes/, they are also used to define the
*MAIL* environment variable.

*MAX_MEMBERS_PER_GROUP* (number)

#+begin_quote
  Maximum members per group entry. When the maximum is reached, a new
  group entry (line) is started in /etc/group (with the same name, same
  password, and same GID).

  The default value is 0, meaning that there are no limits in the number
  of members in a group.

  This feature (split group) permits to limit the length of lines in the
  group file. This is useful to make sure that lines for NIS groups are
  not larger than 1024 characters.

  If you need to enforce such limit, you can use 25.

  Note: split groups may not be supported by all tools (even in the
  Shadow toolsuite). You should not use this variable unless you really
  need it.
#+end_quote

*PASS_MAX_DAYS* (number)

#+begin_quote
  The maximum number of days a password may be used. If the password is
  older than this, a password change will be forced. If not specified,
  -1 will be assumed (which disables the restriction).
#+end_quote

*PASS_MIN_DAYS* (number)

#+begin_quote
  The minimum number of days allowed between password changes. Any
  password changes attempted sooner than this will be rejected. If not
  specified, -1 will be assumed (which disables the restriction).
#+end_quote

*PASS_WARN_AGE* (number)

#+begin_quote
  The number of days warning given before a password expires. A zero
  means warning is given only upon the day of expiration, a negative
  value means no warning is given. If not specified, no warning will be
  provided.
#+end_quote

*SUB_GID_MIN* (number), *SUB_GID_MAX* (number), *SUB_GID_COUNT* (number)

#+begin_quote
  If /etc/subuid exists, the commands *useradd* and *newusers* (unless
  the user already have subordinate group IDs) allocate *SUB_GID_COUNT*
  unused group IDs from the range *SUB_GID_MIN* to *SUB_GID_MAX* for
  each new user.

  The default values for *SUB_GID_MIN*, *SUB_GID_MAX*, *SUB_GID_COUNT*
  are respectively 100000, 600100000 and 65536.
#+end_quote

*SUB_UID_MIN* (number), *SUB_UID_MAX* (number), *SUB_UID_COUNT* (number)

#+begin_quote
  If /etc/subuid exists, the commands *useradd* and *newusers* (unless
  the user already have subordinate user IDs) allocate *SUB_UID_COUNT*
  unused user IDs from the range *SUB_UID_MIN* to *SUB_UID_MAX* for each
  new user.

  The default values for *SUB_UID_MIN*, *SUB_UID_MAX*, *SUB_UID_COUNT*
  are respectively 100000, 600100000 and 65536.
#+end_quote

*SYS_GID_MAX* (number), *SYS_GID_MIN* (number)

#+begin_quote
  Range of group IDs used for the creation of system groups by
  *useradd*, *groupadd*, or *newusers*.

  The default value for *SYS_GID_MIN* (resp. *SYS_GID_MAX*) is 101
  (resp. *GID_MIN*-1).
#+end_quote

*SYS_UID_MAX* (number), *SYS_UID_MIN* (number)

#+begin_quote
  Range of user IDs used for the creation of system users by *useradd*
  or *newusers*.

  The default value for *SYS_UID_MIN* (resp. *SYS_UID_MAX*) is 101
  (resp. *UID_MIN*-1).
#+end_quote

*UID_MAX* (number), *UID_MIN* (number)

#+begin_quote
  Range of user IDs used for the creation of regular users by *useradd*
  or *newusers*.

  The default value for *UID_MIN* (resp. *UID_MAX*) is 1000 (resp.
  60000).
#+end_quote

*UMASK* (number)

#+begin_quote
  The file mode creation mask is initialized to this value. If not
  specified, the mask will be initialized to 022.

  *useradd* and *newusers* use this mask to set the mode of the home
  directory they create if *HOME_MODE* is not set.

  It is also used by *login* to define users initial umask. Note that
  this mask can be overridden by the users GECOS line (if *QUOTAS_ENAB*
  is set) or by the specification of a limit with the /K/ identifier in
  *limits*(5).
#+end_quote

*USERGROUPS_ENAB* (boolean)

#+begin_quote
  Enable setting of the umask group bits to be the same as owner bits
  (examples: 022 -> 002, 077 -> 007) for non-root users, if the uid is
  the same as gid, and username is the same as the primary group name.

  If set to /yes/, *userdel* will remove the users group if it contains
  no more members, and *useradd* will create by default a group with the
  name of the user.
#+end_quote

* FILES
/etc/passwd

#+begin_quote
  User account information.
#+end_quote

/etc/shadow

#+begin_quote
  Secure user account information.
#+end_quote

/etc/group

#+begin_quote
  Group account information.
#+end_quote

/etc/gshadow

#+begin_quote
  Secure group account information.
#+end_quote

/etc/default/useradd

#+begin_quote
  Default values for account creation.
#+end_quote

/etc/skel/

#+begin_quote
  Directory containing default files.
#+end_quote

/etc/subgid

#+begin_quote
  Per user subordinate group IDs.
#+end_quote

/etc/subuid

#+begin_quote
  Per user subordinate user IDs.
#+end_quote

/etc/login.defs

#+begin_quote
  Shadow password suite configuration.
#+end_quote

* EXIT VALUES
The *useradd* command exits with the following values:

/0/

#+begin_quote
  success
#+end_quote

/1/

#+begin_quote
  cant update password file
#+end_quote

/2/

#+begin_quote
  invalid command syntax
#+end_quote

/3/

#+begin_quote
  invalid argument to option
#+end_quote

/4/

#+begin_quote
  UID already in use (and no *-o*)
#+end_quote

/6/

#+begin_quote
  specified group doesnt exist
#+end_quote

/9/

#+begin_quote
  username already in use
#+end_quote

/10/

#+begin_quote
  cant update group file
#+end_quote

/12/

#+begin_quote
  cant create home directory
#+end_quote

/14/

#+begin_quote
  cant update SELinux user mapping
#+end_quote

* SEE ALSO
*chfn*(1), *chsh*(1), *passwd*(1), *crypt*(3), *groupadd*(8),
*groupdel*(8), *groupmod*(8), *login.defs*(5), *newusers*(8),
*subgid*(5), *subuid*(5), *userdel*(8), *usermod*(8).
