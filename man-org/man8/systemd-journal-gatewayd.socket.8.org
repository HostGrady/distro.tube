#+TITLE: Manpages - systemd-journal-gatewayd.socket.8
#+DESCRIPTION: Linux manpage for systemd-journal-gatewayd.socket.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-journal-gatewayd.socket.8 is found in manpage for: [[../systemd-journal-gatewayd.service.8][systemd-journal-gatewayd.service.8]]