#+TITLE: Manpages - devlink-resource.8
#+DESCRIPTION: Linux manpage for devlink-resource.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
devlink-resource - devlink device resource configuration

* SYNOPSIS
*devlink* [ /OPTIONS/ ] *resource* { /COMMAND/ | *help* }

/OPTIONS/ := { *-v*[/erbose/] }

*devlink resource show* /DEV/

*devlink resource help*

*devlink resource set* /DEV/ *path*/ RESOURCE_PATH/ *size*/
RESOURCE_SIZE/

* DESCRIPTION
** devlink resource show - display devlink device's resosources
/DEV/ - specifies the devlink device to show.

Format is:

BUS_NAME/BUS_ADDRESS

** devlink resource set - sets resource size of specific resource
/DEV/ - specifies the devlink device.

- *path*/ RESOURCE_PATH/ :: Resource's path.

- *size*/ RESOURCE_SIZE/ :: The new resource's size.

* EXAMPLES
devlink resource show pci/0000:01:00.0

#+begin_quote
  Shows the resources of the specified devlink device.
#+end_quote

devlink resource set pci/0000:01:00.0 /kvd/linear 98304

#+begin_quote
  Sets the size of the specified resource for the specified devlink
  device.
#+end_quote

* SEE ALSO
*devlink*(8), *devlink-port*(8), *devlink-sb*(8),
*devlink-monitor*(8),\\

* AUTHOR
Arkadi Sharshevsky <arkadis@mellanox.com>
