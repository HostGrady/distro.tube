#+TITLE: Manpages - systemd-sysctl.8
#+DESCRIPTION: Linux manpage for systemd-sysctl.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-sysctl.8 is found in manpage for: [[../systemd-sysctl.service.8][systemd-sysctl.service.8]]