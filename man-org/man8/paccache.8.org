#+TITLE: Manpages - paccache.8
#+DESCRIPTION: Linux manpage for paccache.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
paccache - flexible pacman cache cleaning utility

* SYNOPSIS
/paccache/ <operation> [options] [targets...]

* DESCRIPTION
Paccache removes old packages from the pacman cache directory. By
default the last three versions of a package are kept.

* OPERATIONS
*-d, --dryrun*

#+begin_quote
  Perform a dry run, only finding candidate packages.
#+end_quote

*-m, --move <dir>*

#+begin_quote
  Move candidate packages from the cache directory to /dir/.
#+end_quote

*-r, --remove*

#+begin_quote
  Remove candidate packages from the cache directory.
#+end_quote

* OPTIONS
*-a, --arch <arch>*

#+begin_quote
  Scan for packages for a specific architecture. Default is to scan for
  all architectures.
#+end_quote

*--min-atime <age>*, *--min-mtime <age>*

#+begin_quote
  Only consider packages for removal with atime respectively mtime older
  than specified. The age can be given as /10d/, /1m/, /1y/, /1y1m/ etc.
#+end_quote

*-c, --cachedir <dir>*

#+begin_quote
  Specify a different cache directory. This option can be used more than
  once. Default is to use the cache directory configured in
  /pacman.conf/.
#+end_quote

*-f, --force*

#+begin_quote
  Apply force to /mv/ and /rm/ operations.
#+end_quote

*-h, --help*

#+begin_quote
  Display syntax and command line options.
#+end_quote

*-i, --ignore <pkgs>*

#+begin_quote
  Specify packages to ignore, comma-separated. Alternatively "-" can be
  used to read the package names from stdin, newline-delimited.
#+end_quote

*-k, --keep <num>*

#+begin_quote
  Specify how many versions of each package are kept in the cache
  directory, default is 3.
#+end_quote

*--nocolor*

#+begin_quote
  Remove color from the output.
#+end_quote

*-q, --quiet*

#+begin_quote
  Minimize the output.
#+end_quote

*-u, --uninstalled*

#+begin_quote
  Target uninstalled packages.
#+end_quote

*-v, --verbose*

#+begin_quote
  Increase verbosity, can be specified up to 3 times.
#+end_quote

*-z, --null*

#+begin_quote
  Use null delimiters for candidate names (only with -v and -vv).
#+end_quote

* SYSTEMD TIMER
The package cache can be cleaned periodically using the systemd timer
/paccache.timer/. If the timer is enabled the cache will be cleaned
weekly with paccache's default options.

* SEE ALSO
*pacman*(8), *pacman.conf*(5), *systemctl*(1)

* BUGS
Bugs? You must be kidding; there are no bugs in this software. But if we
happen to be wrong, send us an email with as much detail as possible to
pacman-contrib@lists.archlinux.org.

* AUTHORS
Current maintainers:

#+begin_quote
  ·

  Johannes Löthberg <johannes@kyriasis.com>
#+end_quote

#+begin_quote
  ·

  Daniel M. Capella <polyzen@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the
pacman-contrib.git repository.
