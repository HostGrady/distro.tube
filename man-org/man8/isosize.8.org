#+TITLE: Manpages - isosize.8
#+DESCRIPTION: Linux manpage for isosize.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
isosize - output the length of an iso9660 filesystem

* SYNOPSIS
*isosize* [options] /iso9660_image_file/

* DESCRIPTION
This command outputs the length of an iso9660 filesystem that is
contained in the specified file. This file may be a normal file or a
block device (e.g. //dev/hdd/ or //dev/sr0/). In the absence of any
options (and errors), it will output the size of the iso9660 filesystem
in bytes. This can now be a large number (>> 4 GB).

* OPTIONS
*-x*, *--sectors*

#+begin_quote
  Show the block count and block size in human-readable form. The output
  uses the term "sectors" for "blocks".
#+end_quote

*-d*, *--divisor* /number/

#+begin_quote
  Only has an effect when *-x* is not given. The value shown (if no
  errors) is the iso9660 file size in bytes divided by /number/. So if
  /number/ is the block size then the shown value will be the block
  count.

  The size of the file (or block device) holding an iso9660 filesystem
  can be marginally larger than the actual size of the iso9660
  filesystem. One reason for this is that cd writers are allowed to add
  "run out" sectors at the end of an iso9660 image.
#+end_quote

* EXIT STATUS
*0*

#+begin_quote
  success
#+end_quote

*1*

#+begin_quote
  generic failure, such as invalid usage
#+end_quote

*32*

#+begin_quote
  all failed
#+end_quote

*64*

#+begin_quote
  some failed
#+end_quote

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *isosize* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
