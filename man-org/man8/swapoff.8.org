#+TITLE: Manpages - swapoff.8
#+DESCRIPTION: Linux manpage for swapoff.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about swapoff.8 is found in manpage for: [[../swapon.8][swapon.8]]