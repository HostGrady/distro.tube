#+TITLE: Manpages - bus.8
#+DESCRIPTION: Linux manpage for bus.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about bus.8 is found in manpage for: [[../man8/les.8][man8/les.8]]