#+TITLE: Manpages - pam_warn.8
#+DESCRIPTION: Linux manpage for pam_warn.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_warn - PAM module which logs all PAM items if called

* SYNOPSIS
*pam_warn.so*

* DESCRIPTION
pam_warn is a PAM module that logs the service, terminal, user, remote
user and remote host to *syslog*(3). The items are not probed for, but
instead obtained from the standard PAM items. The module always returns
*PAM_IGNORE*, indicating that it does not want to affect the
authentication process.

* OPTIONS
This module does not recognise any options.

* MODULE TYPES PROVIDED
The *auth*, *account*, *password* and *session* module types are
provided.

* RETURN VALUES
PAM_IGNORE

#+begin_quote
  This module always returns PAM_IGNORE.
#+end_quote

* EXAMPLES

#+begin_quote
  #+begin_example
    #%PAM-1.0
    #
    # If we dont have config entries for a service, the
    # OTHER entries are used. To be secure, warn and deny
    # access to everything.
    other auth     required       pam_warn.so
    other auth     required       pam_deny.so
    other account  required       pam_warn.so
    other account  required       pam_deny.so
    other password required       pam_warn.so
    other password required       pam_deny.so
    other session  required       pam_warn.so
    other session  required       pam_deny.so
          
  #+end_example
#+end_quote

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_warn was written by Andrew G. Morgan <morgan@kernel.org>.
