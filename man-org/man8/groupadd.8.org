#+TITLE: Manpages - groupadd.8
#+DESCRIPTION: Linux manpage for groupadd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
groupadd - create a new group

* SYNOPSIS
*groupadd* [/options/] /group/

* DESCRIPTION
The *groupadd* command creates a new group account using the values
specified on the command line plus the default values from the system.
The new group will be entered into the system files as needed.

* OPTIONS
The options which apply to the *groupadd* command are:

*-f*, *--force*

#+begin_quote
  This option causes the command to simply exit with success status if
  the specified group already exists. When used with *-g*, and the
  specified GID already exists, another (unique) GID is chosen (i.e.
  *-g* is turned off).
#+end_quote

*-g*, *--gid* /GID/

#+begin_quote
  The numerical value of the groups ID. This value must be unique,
  unless the *-o* option is used. The value must be non-negative. The
  default is to use the smallest ID value greater than or equal to
  *GID_MIN* and greater than every other group.

  See also the *-r* option and the *GID_MAX* description.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help message and exit.
#+end_quote

*-K*, *--key* /KEY/=/VALUE/

#+begin_quote
  Overrides /etc/login.defs defaults (GID_MIN, GID_MAX and others).
  Multiple *-K* options can be specified.

  Example: *-K* /GID_MIN/=/100/ *-K* /GID_MAX/=/499/

  Note: *-K* /GID_MIN/=/10/,/GID_MAX/=/499/ doesnt work yet.
#+end_quote

*-o*, *--non-unique*

#+begin_quote
  This option permits to add a group with a non-unique GID.
#+end_quote

*-p*, *--password* /PASSWORD/

#+begin_quote
  The encrypted password, as returned by *crypt*(3). The default is to
  disable the password.

  *Note:* This option is not recommended because the password (or
  encrypted password) will be visible by users listing the processes.

  You should make sure the password respects the systems password
  policy.
#+end_quote

*-r*, *--system*

#+begin_quote
  Create a system group.

  The numeric identifiers of new system groups are chosen in the
  *SYS_GID_MIN*-*SYS_GID_MAX* range, defined in login.defs, instead of
  *GID_MIN*-*GID_MAX*.
#+end_quote

*-R*, *--root* /CHROOT_DIR/

#+begin_quote
  Apply changes in the /CHROOT_DIR/ directory and use the configuration
  files from the /CHROOT_DIR/ directory.
#+end_quote

*-P*, *--prefix* /PREFIX_DIR/

#+begin_quote
  Apply changes in the /PREFIX_DIR/ directory and use the configuration
  files from the /PREFIX_DIR/ directory. This option does not chroot and
  is intended for preparing a cross-compilation target. Some
  limitations: NIS and LDAP users/groups are not verified. PAM
  authentication is using the host files. No SELINUX support.
#+end_quote

* CONFIGURATION
The following configuration variables in /etc/login.defs change the
behavior of this tool:

*GID_MAX* (number), *GID_MIN* (number)

#+begin_quote
  Range of group IDs used for the creation of regular groups by
  *useradd*, *groupadd*, or *newusers*.

  The default value for *GID_MIN* (resp. *GID_MAX*) is 1000 (resp.
  60000).
#+end_quote

*MAX_MEMBERS_PER_GROUP* (number)

#+begin_quote
  Maximum members per group entry. When the maximum is reached, a new
  group entry (line) is started in /etc/group (with the same name, same
  password, and same GID).

  The default value is 0, meaning that there are no limits in the number
  of members in a group.

  This feature (split group) permits to limit the length of lines in the
  group file. This is useful to make sure that lines for NIS groups are
  not larger than 1024 characters.

  If you need to enforce such limit, you can use 25.

  Note: split groups may not be supported by all tools (even in the
  Shadow toolsuite). You should not use this variable unless you really
  need it.
#+end_quote

*SYS_GID_MAX* (number), *SYS_GID_MIN* (number)

#+begin_quote
  Range of group IDs used for the creation of system groups by
  *useradd*, *groupadd*, or *newusers*.

  The default value for *SYS_GID_MIN* (resp. *SYS_GID_MAX*) is 101
  (resp. *GID_MIN*-1).
#+end_quote

* FILES
/etc/group

#+begin_quote
  Group account information.
#+end_quote

/etc/gshadow

#+begin_quote
  Secure group account information.
#+end_quote

/etc/login.defs

#+begin_quote
  Shadow password suite configuration.
#+end_quote

* CAVEATS
Groupnames must start with a lower case letter or an underscore,
followed by lower case letters, digits, underscores, or dashes. They can
end with a dollar sign. In regular expression terms:
[a-z_][a-z0-9_-]*[$]?

Groupnames may only be up to 16 characters long.

You may not add a NIS or LDAP group. This must be performed on the
corresponding server.

If the groupname already exists in an external group database such as
NIS or LDAP, *groupadd* will deny the group creation request.

* EXIT VALUES
The *groupadd* command exits with the following values:

/0/

#+begin_quote
  success
#+end_quote

/2/

#+begin_quote
  invalid command syntax
#+end_quote

/3/

#+begin_quote
  invalid argument to option
#+end_quote

/4/

#+begin_quote
  GID not unique (when *-o* not used)
#+end_quote

/9/

#+begin_quote
  group name not unique
#+end_quote

/10/

#+begin_quote
  cant update group file
#+end_quote

* SEE ALSO
*chfn*(1), *chsh*(1), *passwd*(1), *gpasswd*(8), *groupdel*(8),
*groupmod*(8), *login.defs*(5), *useradd*(8), *userdel*(8),
*usermod*(8).
