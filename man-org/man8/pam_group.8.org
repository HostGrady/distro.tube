#+TITLE: Manpages - pam_group.8
#+DESCRIPTION: Linux manpage for pam_group.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_group - PAM module for group access

* SYNOPSIS
*pam_group.so*

* DESCRIPTION
The pam_group PAM module does not authenticate the user, but instead it
grants group memberships (in the credential setting phase of the
authentication module) to the user. Such memberships are based on the
service they are applying for.

By default rules for group memberships are taken from config file
/etc/security/group.conf.

This modules usefulness relies on the file-systems accessible to the
user. The point being that once granted the membership of a group, the
user may attempt to create a *setgid* binary with a restricted group
ownership. Later, when the user is not given membership to this group,
they can recover group membership with the precompiled binary. The
reason that the file-systems that the user has access to are so
significant, is the fact that when a system is mounted /nosuid/ the user
is unable to create or execute such a binary file. For this module to
provide any level of security, all file-systems that the user has write
access to should be mounted /nosuid/.

The pam_group module functions in parallel with the /etc/group file. If
the user is granted any groups based on the behavior of this module,
they are granted /in addition/ to those entries /etc/group (or
equivalent).

* OPTIONS
This module does not recognise any options.

* MODULE TYPES PROVIDED
Only the *auth* module type is provided.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  group membership was granted.
#+end_quote

PAM_ABORT

#+begin_quote
  Not all relevant data could be gotten.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_CRED_ERR

#+begin_quote
  Group membership was not granted.
#+end_quote

PAM_IGNORE

#+begin_quote
  *pam_sm_authenticate* was called which does nothing.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  The user is not known to the system.
#+end_quote

* FILES
/etc/security/group.conf

#+begin_quote
  Default configuration file
#+end_quote

* SEE ALSO
*group.conf*(5), *pam.d*(5), *pam*(8).

* AUTHORS
pam_group was written by Andrew G. Morgan <morgan@kernel.org>.
