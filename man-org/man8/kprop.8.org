#+TITLE: Manpages - kprop.8
#+DESCRIPTION: Linux manpage for kprop.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kprop - propagate a Kerberos V5 principal database to a replica server

* SYNOPSIS
*kprop* [*-r* /realm/] [*-f* /file/] [*-d*] [*-P* /port/] [*-s*
/keytab/] /replica_host/

* DESCRIPTION
kprop is used to securely propagate a Kerberos V5 database dump file
from the primary Kerberos server to a replica Kerberos server, which is
specified by /replica_host/. The dump file must be created by
kdb5_util(8).

* OPTIONS

#+begin_quote
  - *-r* /realm/ :: Specifies the realm of the primary server.

  - *-f* /file/ :: Specifies the filename where the dumped principal
    database file is to be found; by default the dumped database file is
    normally */var/lib/krb5kdc/replica_datatrans*.

  - *-P* /port/ :: Specifies the port to use to contact the kpropd(8)
    server on the remote host.

  - *-d* :: Prints debugging information.

  - *-s* /keytab/ :: Specifies the location of the keytab file.
#+end_quote

* ENVIRONMENT
See kerberos(7) for a description of Kerberos environment variables.

* SEE ALSO
kpropd(8), kdb5_util(8), krb5kdc(8), kerberos(7)

* AUTHOR
MIT

* COPYRIGHT
1985-2021, MIT
