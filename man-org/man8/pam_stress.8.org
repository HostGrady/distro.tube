#+TITLE: Manpages - pam_stress.8
#+DESCRIPTION: Linux manpage for pam_stress.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_stress - The stress-testing PAM module

* SYNOPSIS
*pam_stress.so* [debug] [no_warn] [use_first_pass] [try_first_pass]
[rootok] [expired] [fail_1] [fail_2] [prelim] [required]

* DESCRIPTION
The pam_stress PAM module is mainly intended to give the impression of
failing as a fully functioning module might.

* OPTIONS
*debug*

#+begin_quote
  Put lots of information in syslog. *NOTE* this option writes passwords
  to syslog, so dont use anything sensitive when testing.
#+end_quote

*no_warn*

#+begin_quote
  Do not give warnings about things (otherwise warnings are issued via
  the conversation function)
#+end_quote

*use_first_pass*

#+begin_quote
  Do not prompt for a password, for pam_sm_authentication function just
  use item PAM_AUTHTOK.
#+end_quote

*try_first_pass*

#+begin_quote
  Do not prompt for a password unless there has been no previous
  authentication token (item PAM_AUTHTOK is NULL)
#+end_quote

*rootok*

#+begin_quote
  This is intended for the pam_sm_chauthtok function and it instructs
  this function to permit root to change the users password without
  entering the old password.
#+end_quote

*expired*

#+begin_quote
  An argument intended for the account and chauthtok module parts. It
  instructs the module to act as if the users password has expired
#+end_quote

*fail_1*

#+begin_quote
  This instructs the module to make its first function fail.
#+end_quote

*fail_2*

#+begin_quote
  This instructs the module to make its second function (if there is
  one) fail.
#+end_quote

*prelim*

#+begin_quote
  For pam_sm_chauthtok, means fail on PAM_PRELIM_CHECK.
#+end_quote

*required*

#+begin_quote
  For pam_sm_chauthtok, means fail if the user hasnt already been
  authenticated by this module. (See stress_new_pwd data string in the
  NOTES.)
#+end_quote

* MODULE TYPES PROVIDED
All module types (*auth*, *account*, *password* and *session*) are
provided.

* RETURN VALUES
PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_PERM_DENIED

#+begin_quote
  Permission denied.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  Access to the system was denied.
#+end_quote

PAM_CONV_ERR

#+begin_quote
  Conversation failure.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The function passes all checks.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  The user is not known to the system.
#+end_quote

PAM_CRED_ERR

#+begin_quote
  Failure involving user credentials.
#+end_quote

PAM_NEW_AUTHTOK_REQD

#+begin_quote
  Authentication token is no longer valid; new one required.
#+end_quote

PAM_SESSION_ERR

#+begin_quote
  Session failure.
#+end_quote

PAM_TRY_AGAIN

#+begin_quote
  Failed preliminary check by service.
#+end_quote

PAM_AUTHTOK_LOCK_BUSY

#+begin_quote
  Authentication token lock busy.
#+end_quote

PAM_AUTHTOK_ERR

#+begin_quote
  Authentication token manipulation error.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  System error.
#+end_quote

* NOTES
This module uses the stress_new_pwd data string which tells
pam_sm_chauthtok that pam_sm_acct_mgmt says we need a new password. The
only possible value for this data string is yes.

* EXAMPLES

#+begin_quote
  #+begin_example
    #%PAM-1.0
    #
    # Any of the following will suffice
    account  required pam_stress.so
    auth     required pam_stress.so
    password required pam_stress.so
    session  required pam_stress.so
        
  #+end_example
#+end_quote

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8).

* AUTHORS
The pam_stress PAM module was developed by Andrew Morgan
<morgan@linux.kernel.org>. The man page for pam_stress was written by
Lucas Ramage <ramage.lucas@protonmail.com>.
