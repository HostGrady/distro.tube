#+TITLE: Manpages - pam_usertype.8
#+DESCRIPTION: Linux manpage for pam_usertype.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_usertype - check if the authenticated user is a system or regular
account

* SYNOPSIS
*pam_usertype.so* [/flag/...] {/condition/}

* DESCRIPTION
pam_usertype.so is designed to succeed or fail authentication based on
type of the account of the authenticated user. The type of the account
is decided with help of /SYS_UID_MIN/ and /SYS_UID_MAX/ settings in
//etc/login.defs/. One use is to select whether to load other modules
based on this test.

The module should be given only one condition as module argument.
Authentication will succeed only if the condition is met.

* OPTIONS
The following /flag/s are supported:

*use_uid*

#+begin_quote
  Evaluate conditions using the account of the user whose UID the
  application is running under instead of the user being authenticated.
#+end_quote

*audit*

#+begin_quote
  Log unknown users to the system log.
#+end_quote

Available /condition/s are:

*issystem*

#+begin_quote
  Succeed if the user is a system user.
#+end_quote

*isregular*

#+begin_quote
  Succeed if the user is a regular user.
#+end_quote

* MODULE TYPES PROVIDED
All module types (*account*, *auth*, *password* and *session*) are
provided.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  The condition was true.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_CONV_ERR

#+begin_quote
  The conversation method supplied by the application failed to obtain
  the username.
#+end_quote

PAM_INCOMPLETE

#+begin_quote
  The conversation method supplied by the application returned
  PAM_CONV_AGAIN.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  The condition was false.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  A service error occurred or the arguments cant be parsed correctly.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User was not found.
#+end_quote

* EXAMPLES
Skip remaining modules if the user is a system user:

#+begin_quote
  #+begin_example
    account sufficient pam_usertype.so issystem
        
  #+end_example
#+end_quote

* SEE ALSO
*login.defs*(5), *pam*(8)

* AUTHOR
Pavel Březina <pbrezina@redhat.com>

Information about pam_usertype.8 is found in manpage for: [[../is designed to succeed or fail authentication based on type of the account of the authenticated user\&. The type of the account is decided with help of
sufficient pam_usertype\&.so issystem][is designed to succeed or fail authentication based on type of the account of the authenticated user\&. The type of the account is decided with help of
sufficient pam_usertype\&.so issystem]]