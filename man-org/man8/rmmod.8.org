#+TITLE: Manpages - rmmod.8
#+DESCRIPTION: Linux manpage for rmmod.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rmmod - Simple program to remove a module from the Linux Kernel

* SYNOPSIS
*rmmod* [*-f*] [*-s*] [*-v*] [/modulename/]

* DESCRIPTION
*rmmod* is a trivial program to remove a module (when module unloading
support is provided) from the kernel. Most users will want to use
*modprobe*(8) with the *-r* option instead.

* OPTIONS
*-v*, *--verbose*

#+begin_quote
  Print messages about what the program is doing. Usually *rmmod* prints
  messages only if something goes wrong.
#+end_quote

*-f*, *--force*

#+begin_quote
  This option can be extremely dangerous: it has no effect unless
  CONFIG_MODULE_FORCE_UNLOAD was set when the kernel was compiled. With
  this option, you can remove modules which are being used, or which are
  not designed to be removed, or have been marked as unsafe (see
  *lsmod*(8)).
#+end_quote

*-s*, *--syslog*

#+begin_quote
  Send errors to syslog instead of standard error.
#+end_quote

*-V* *--version*

#+begin_quote
  Show version of program and exit.
#+end_quote

* COPYRIGHT
This manual page originally Copyright 2002, Rusty Russell, IBM
Corporation. Maintained by Jon Masters and others.

* SEE ALSO
*modprobe*(8), *insmod*(8), *lsmod*(8), *modinfo*(8) *depmod*(8)

* AUTHORS
*Jon Masters* <jcm@jonmasters.org>

#+begin_quote
  Developer
#+end_quote

*Lucas De Marchi* <lucas.de.marchi@gmail.com>

#+begin_quote
  Developer
#+end_quote
