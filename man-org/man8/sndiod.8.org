#+TITLE: Manpages - sndiod.8
#+DESCRIPTION: Linux manpage for sndiod.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

daemon is an intermediate layer between audio or MIDI programs and the
hardware. It performs the necessary audio processing to allow any
program to work on any supported hardware. By default,

accepts connections from programs running on the same system only; it
initializes only when programs are using its services, allowing

to consume a negligible amount of system resources the rest of the time.
Systems with no audio hardware can use

to keep hot-pluggable devices usable by default at virtually no cost.

operates as follows: it exposes at least one

that any number of audio programs can connect to and use as if it was
audio hardware. During playback,

receives audio data concurrently from all programs, mixes it and sends
the result to the hardware device. Similarly, during recording it
duplicates audio data recorded from the device and sends it to all
programs. Since audio data flows through the

process, it has the opportunity to process audio data on the fly:

Change the sound encoding to overcome incompatibilities between software
and hardware.

Route the sound from one channel to another, join stereo or split mono.

Control the per-application playback volume as well as the master
volume.

Monitor the sound being played, allowing one program to record what
other programs play.

Processing is configured on a per sub-device basis, meaning that the
sound of all programs connected to the same sub-device will be processed
according to the same configuration. Multiple sub-devices can be
defined, allowing multiple configurations to coexist. The user selects
the configuration a given program will use by selecting the sub-device
the program uses.

exposes MIDI thru boxes (hubs), allowing programs to send MIDI messages
to each other or to hardware MIDI ports in a uniform way.

Finally,

exposes a control MIDI port usable for:

Volume control.

Common clock source for audio and MIDI programs.

Start, stop and relocate groups of audio programs.

The options are as follows:

Control whether

opens the audio device or the MIDI port only when needed or keeps it
open all the time. If the flag is

then the audio device or MIDI port is kept open all the time, ensuring
no other program can steal it. If the flag is

then it's automatically closed, allowing other programs to have direct
access to the audio device, or the device to be disconnected. The
default is

The buffer size of the audio device in frames. A frame consists of one
sample for each channel in the stream. This is the number of frames that
will be buffered before being played and thus controls the playback
latency. The default is 7680 or twice the block size

if the block size is set.

The range of channel numbers for recording and playback directions,
respectively any client is allowed to use. This is a subset of the audio
device channels. The default is 0:1, i.e. stereo.

Enable debugging to standard error, and do not disassociate from the
controlling terminal. Can be specified multiple times to further
increase log verbosity.

Attempt to configure the device to use this encoding. The default is

Encoding names use the following scheme: signedness

or

followed by the precision in bits, the byte-order

or

the number of bytes per sample, and the alignment

or

Only the signedness and the precision are mandatory. Examples:

Specify an alternate device to use. If it doesn't work, the one given
with the last

or

options will be used. For instance, specifying a USB device following a
PCI device allows

to use the USB one preferably when it's connected and to fall back to
the PCI one when it's disconnected. Alternate devices may be switched
with the

control of the

utility.

Add this

audio device to devices used for playing and/or recording. Preceding
per-device options

apply to this device. Sub-devices

that are applied after will be attached to this device. Device mode and
parameters are determined from sub-devices attached to it. If no

option is used,

will use

Control whether program channels are joined or expanded if the number of
channels requested by a program is not equal to the device number of
channels. If the flag is

then client channels are routed to the corresponding device channel,
possibly discarding channels not present in the device. If the flag is

then a single client channel may be sent on multiple device channels, or
multiple client channels may be sent to a single device channel. For
instance, this feature could be used for mono to stereo conversions. The
default is

Specify a local network address

should listen on;

will listen on TCP port 11025+n, where n is the unit number specified
with

Without this option,

listens on the

socket only, and is not reachable from any network. If the option
argument is

then

will accept connections from any address. As the communication is not
secure, this option is only suitable for local networks where all hosts
and users are trusted.

Set the sub-device mode. Valid modes are

and

corresponding to playback, recording and monitoring. A monitoring stream
is a fake recording stream corresponding to the mix of all playback
streams. Multiple modes can be specified, separated by commas, but the
same sub-device cannot be used for both recording and monitoring. The
default is

(i.e. full-duplex).

Specify an alternate MIDI port to use. If it doesn't work, the one given
with the last

or

options will be used. For instance, this allows a USB MIDI controller to
be replaced without the need to restart programs using it.

Expose the given MIDI port. This allows multiple programs to share the
port. If no

option is used,

will use

Attempt to force the device to use this sample rate in Hertz. The
default is 48000.

Add

to the list of sub-devices to expose. This allows clients to use

instead of the physical audio device for audio input and output in order
to share the physical device with other clients. Defining multiple
sub-devices allows splitting a physical audio device into sub-devices
having different properties (e.g. channel ranges). The given

corresponds to the

part of the

device name string.

Select the way clients are controlled by MIDI Machine Control (MMC)
messages received by

If the mode is

(the default), then programs are not affected by MMC messages. If the
mode is

then programs are started synchronously by MMC start messages;
additionally, the server clock is exposed as MIDI Time Code (MTC)
messages allowing MTC-capable software or hardware to be synchronized to
audio programs.

Unit number. Each

server instance has a unique unit number, used in

device names. The default is 0.

Software volume attenuation of playback. The value must be between 1 and
127, corresponding to -42dB and -0dB attenuation in 1/3dB steps. Clients
inherit this parameter. Reducing the volume in advance allows a client's
volume to stay independent from the number of clients as long as their
number is small enough. 18 volume units (i.e. -6dB attenuation) allows
the number of playback programs to be doubled. The default is 127.

Control

behaviour when the maximum volume of the hardware is reached and a new
program starts playing. This happens only when volumes are not properly
set using the

option. If the flag is

then the master volume is automatically adjusted to avoid clipping. The
default is

The audio device block size in frames. This is the number of frames
between audio clock ticks, i.e. the clock resolution. If a sub-device is
created with the

option, and MTC is used for synchronization, the clock resolution must
be 96, 100 or 120 ticks per second for maximum accuracy. For instance,
100 ticks per second at 48000Hz corresponds to a 480 frame block size.
The default is 960 or half of the buffer size

if the buffer size is set.

On the command line, per-device parameters

must precede the device definition

and per-sub-device parameters

must precede the sub-device definition

Sub-device definitions

must follow the definition of the device

to which they are attached.

If no audio devices

are specified, settings are applied as if the default device is
specified. If no sub-devices

are specified for a device, a default sub-device is created attached to
it. If a device

is defined twice, both definitions are merged: parameters of the first
one are used but sub-devices

of both definitions are created. The default

device used by

is

and the default sub-device exposed by

is

If

is sent

or

it terminates. If

is sent

it reopens all audio devices and MIDI ports.

By default, when the program cannot accept recorded data fast enough or
cannot provide data to play fast enough, the program is paused, i.e.
samples that cannot be written are discarded and samples that cannot be
read are replaced by silence. If a sub-device is created with the

option, then recorded samples are discarded, but the same amount of
silence will be written once the program is unblocked, in order to reach
the right position in time. Similarly silence is played, but the same
amount of samples will be discarded once the program is unblocked. This
ensures proper synchronization between programs.

creates a MIDI port with the same name as the exposed audio sub-device
to which MIDI programs can connect.

exposes the audio device clock and allows audio device properties to be
controlled through MIDI.

A MIDI channel is assigned to each stream, and the volume is changed
using the standard volume controller (number 7). Similarly, when the
audio client changes its volume, the same MIDI controller message is
sent out; it can be used for instance for monitoring or as feedback for
motorized faders.

The master volume can be changed using the standard master volume system
exclusive message.

Streams created with the

option are controlled by the following MMC messages:

This message is ignored by audio

clients, but the given time position is sent to MIDI ports as an MTC

message forcing all MTC-slaves to relocate to the given position (see
below).

Put all streams in starting mode. In this mode,

waits for all streams to become ready to start, and then starts them
synchronously. Once started, new streams can be created

but they will be blocked until the next stop-to-start transition.

Put all streams in stopped mode (the default). In this mode, any stream
attempting to start playback or recording is paused. Client streams that
are already started are not affected until they stop and try to start
again.

Streams created with the

option export the

device clock using MTC, allowing non-audio software or hardware to be
synchronized to the audio stream. Maximum accuracy is achieved when the
number of blocks per second is equal to one of the standard MTC clock
rates (96, 100 and 120Hz). The following sample rates

and block sizes

are recommended:

44100Hz, 441 frames (MTC rate is 100Hz)

48000Hz, 400 frames (MTC rate is 120Hz)

48000Hz, 480 frames (MTC rate is 100Hz)

48000Hz, 500 frames (MTC rate is 96Hz)

For instance, the following command will create two devices: the default

and a MIDI-controlled

$ sndiod -r 48000 -z 400 -s default -t slave -s mmc

Streams connected to

behave normally, while streams connected to

wait for the MMC start signal and start synchronously. Regardless of
which device a stream is connected to, its playback volume knob is
exposed.

Start server using default parameters, creating an additional sub-device
for output to channels 2:3 only (rear speakers on most cards), exposing
the

and

devices:

$ sndiod -s default -c 2:3 -s rear

Start server creating the default sub-device with low volume and an
additional sub-device for high volume output, exposing the

and

devices:

$ sndiod -v 65 -s default -v 127 -s max

Start server configuring the audio device to use a 48kHz sample
frequency, 240-frame block size, and 2-block buffers. The corresponding
latency is 10ms, which is the time it takes the sound to propagate 3.5
meters.

$ sndiod -r 48000 -b 480 -z 240

Resampling is low quality; down-sampling especially should be avoided
when recording.

Processing is done using 16-bit arithmetic, thus samples with more than
16 bits are rounded. 16 bits (i.e. 97dB dynamic) are largely enough for
most applications though. Processing precision can be increased to
24-bit at compilation time though.

If

is used,

creates sub-devices to expose first and then opens the audio hardware on
demand. Technically, this allows

to attempt to use one of the sub-devices it exposes as an audio device,
creating a deadlock. There's nothing to prevent the user from shooting
himself in the foot by creating such a deadlock.
