#+TITLE: Manpages - rpcbind.8
#+DESCRIPTION: Linux manpage for rpcbind.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

utility is a server that converts

program numbers into universal addresses. It must be running on the host
to be able to make

calls on a server on that machine.

When an

service is started, it tells

the address at which it is listening, and the

program numbers it is prepared to serve. When a client wishes to make an

call to a given program number, it first contacts

on the server machine to determine the address where

requests should be sent.

The

utility should be started before any other RPC service. Normally,
standard

servers are started by port monitors, so

must be started before port monitors are invoked.

When

is started, it checks that certain name-to-address translation-calls
function correctly. If they fail, the network configuration databases
may be corrupt. Since

services cannot function correctly in this situation,

reports the condition and terminates.

The

utility can only be started by the super-user.

When debugging

do an abort on errors.

Run in debug mode. In this mode,

will log additional information during operation, and will abort on
certain errors if

is also specified. With this option, the name-to-address translation
consistency checks are shown in detail.

Do not fork and become a background process.

Specify specific IP addresses to bind to for UDP requests. This option
may be specified multiple times and can be used to restrict the
interfaces rpcbind will respond to. When specifying IP addresses with

will automatically add

and if IPv6 is enabled,

to the list. If no

option is specified,

will bind to

which could lead to problems on a multi-homed host due to

returning a UDP packet from a different IP address than it was sent to.
Note that when

is controlled via systemd's socket activation, the

option is ignored. In this case, you need to edit the

and

definitions in

instead.

mode. Allow calls to SET and UNSET from any host. Normally

accepts these requests only from the loopback interface for security
reasons. This change is necessary for programs that were compiled with
earlier versions of the rpc library and do not make those requests using
the loopback interface.

Turn on libwrap connection logging.

Cause

to change to the user daemon as soon as possible. This causes

to use non-privileged ports for outgoing connections, preventing
non-privileged clients from using

to connect to services from a privileged port.

Cause

to do a "warm start" by read a state file when

starts up. The state file is created when

terminates.

All RPC servers must be restarted if

is restarted.
