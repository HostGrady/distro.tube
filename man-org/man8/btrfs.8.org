#+TITLE: Manpages - btrfs.8
#+DESCRIPTION: Linux manpage for btrfs.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
btrfs - a toolbox to manage btrfs filesystems

* SYNOPSIS
*btrfs* /<command>/ [/<args>/]

* DESCRIPTION
The *btrfs* utility is a toolbox for managing btrfs filesystems. There
are command groups to work with subvolumes, devices, for whole
filesystem or other specific actions. See section *COMMANDS*.

There are also standalone tools for some tasks like *btrfs-convert* or
*btrfstune* that were separate historically and/or haven't been merged
to the main utility. See section /STANDALONE TOOLS/ for more details.

For other topics (mount options, etc) please refer to the separate
manual page *btrfs*(5).

* COMMAND SYNTAX
Any command name can be shortened so long as the shortened form is
unambiguous, however, it is recommended to use full command names in
scripts. All command groups have their manual page named
*btrfs-*/<group>/.

For example: it is possible to run *btrfs sub snaps* instead of *btrfs
subvolume snapshot*. But *btrfs file s* is not allowed, because *file s*
may be interpreted both as *filesystem show* and as *filesystem sync*.

If the command name is ambiguous, the list of conflicting options is
printed.

For an overview of a given command use /btrfs command --help/ or /btrfs
[command...] --help --full/ to print all available options.

* COMMANDS
*balance*

#+begin_quote
  Balance btrfs filesystem chunks across single or several devices.

  See *btrfs-balance*(8) for details.
#+end_quote

*check*

#+begin_quote
  Do off-line check on a btrfs filesystem.

  See *btrfs-check*(8) for details.
#+end_quote

*device*

#+begin_quote
  Manage devices managed by btrfs, including add/delete/scan and so on.

  See *btrfs-device*(8) for details.
#+end_quote

*filesystem*

#+begin_quote
  Manage a btrfs filesystem, including label setting/sync and so on.

  See *btrfs-filesystem*(8) for details.
#+end_quote

*inspect-internal*

#+begin_quote
  Debug tools for developers/hackers.

  See *btrfs-inspect-internal*(8) for details.
#+end_quote

*property*

#+begin_quote
  Get/set a property from/to a btrfs object.

  See *btrfs-property*(8) for details.
#+end_quote

*qgroup*

#+begin_quote
  Manage quota group(qgroup) for btrfs filesystem.

  See *btrfs-qgroup*(8) for details.
#+end_quote

*quota*

#+begin_quote
  Manage quota on btrfs filesystem like enabling/rescan and etc.

  See *btrfs-quota*(8) and *btrfs-qgroup*(8) for details.
#+end_quote

*receive*

#+begin_quote
  Receive subvolume data from stdin/file for restore and etc.

  See *btrfs-receive*(8) for details.
#+end_quote

*replace*

#+begin_quote
  Replace btrfs devices.

  See *btrfs-replace*(8) for details.
#+end_quote

*rescue*

#+begin_quote
  Try to rescue damaged btrfs filesystem.

  See *btrfs-rescue*(8) for details.
#+end_quote

*restore*

#+begin_quote
  Try to restore files from a damaged btrfs filesystem.

  See *btrfs-restore*(8) for details.
#+end_quote

*scrub*

#+begin_quote
  Scrub a btrfs filesystem.

  See *btrfs-scrub*(8) for details.
#+end_quote

*send*

#+begin_quote
  Send subvolume data to stdout/file for backup and etc.

  See *btrfs-send*(8) for details.
#+end_quote

*subvolume*

#+begin_quote
  Create/delete/list/manage btrfs subvolume.

  See *btrfs-subvolume*(8) for details.
#+end_quote

* STANDALONE TOOLS
New functionality could be provided using a standalone tool. If the
functionality proves to be useful, then the standalone tool is declared
obsolete and its functionality is copied to the main tool. Obsolete
tools are removed after a long (years) depreciation period.

Tools that are still in active use without an equivalent in *btrfs*:

*btrfs-convert*

#+begin_quote
  in-place conversion from ext2/3/4 filesystems to btrfs
#+end_quote

*btrfstune*

#+begin_quote
  tweak some filesystem properties on a unmounted filesystem
#+end_quote

*btrfs-select-super*

#+begin_quote
  rescue tool to overwrite primary superblock from a spare copy
#+end_quote

*btrfs-find-root*

#+begin_quote
  rescue helper to find tree roots in a filesystem
#+end_quote

Deprecated and obsolete tools:

*btrfs-debug-tree*

#+begin_quote
  moved to *btrfs inspect-internal dump-tree*. Removed from source
  distribution.
#+end_quote

*btrfs-show-super*

#+begin_quote
  moved to *btrfs inspect-internal dump-super*, standalone removed.
#+end_quote

*btrfs-zero-log*

#+begin_quote
  moved to *btrfs rescue zero-log*, standalone removed.
#+end_quote

For space-constrained environments, it's possible to build a single
binary with functionality of several standalone tools. This is following
the concept of busybox where the file name selects the functionality.
This works for symlinks or hardlinks. The full list can be obtained by
*btrfs help --box*.

* EXIT STATUS
*btrfs* returns a zero exit status if it succeeds. Non zero is returned
in case of failure.

* AVAILABILITY
*btrfs* is part of btrfs-progs. Please refer to the btrfs wiki
*http://btrfs.wiki.kernel.org* for further details.

* SEE ALSO
*btrfs*(5), *btrfs-balance*(8), *btrfs-check*(8), *btrfs-convert*(8),
*btrfs-device*(8), *btrfs-filesystem*(8), *btrfs-inspect-internal*(8),
*btrfs-property*(8), *btrfs-qgroup*(8), *btrfs-quota*(8),
*btrfs-receive*(8), *btrfs-replace*(8), *btrfs-rescue*(8),
*btrfs-restore*(8), *btrfs-scrub*(8), *btrfs-send*(8),
*btrfs-subvolume*(8), *btrfstune*(8), *mkfs.btrfs*(8)
