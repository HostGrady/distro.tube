#+TITLE: Manpages - wipefs.8
#+DESCRIPTION: Linux manpage for wipefs.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wipefs - wipe a signature from a device

* SYNOPSIS
*wipefs* [options] /device/...

*wipefs* [*--backup*] *-o* /offset device/...

*wipefs* [*--backup*] *-a* /device/...

* DESCRIPTION
*wipefs* can erase filesystem, raid or partition-table signatures (magic
strings) from the specified /device/ to make the signatures invisible
for libblkid. *wipefs* does not erase the filesystem itself nor any
other data from the device.

When used without any options, *wipefs* lists all visible filesystems
and the offsets of their basic signatures. The default output is subject
to change. So whenever possible, you should avoid using default outputs
in your scripts. Always explicitly define expected columns by using
*--output* /columns-list/ in environments where a stable output is
required.

*wipefs* calls the BLKRRPART ioctl when it has erased a partition-table
signature to inform the kernel about the change. The ioctl is called as
the last step and when all specified signatures from all specified
devices are already erased. This feature can be used to wipe content on
partitions devices as well as partition table on a disk device, for
example by *wipefs -a /dev/sdc1 /dev/sdc2 /dev/sdc*.

Note that some filesystems and some partition tables store more magic
strings on the device (e.g., FAT, ZFS, GPT). The *wipefs* command (since
v2.31) lists all the offset where a magic strings have been detected.

When option *-a* is used, all magic strings that are visible for
*libblkid*(3) are erased. In this case the *wipefs* scans the device
again after each modification (erase) until no magic string is found.

Note that by default *wipefs* does not erase nested partition tables on
non-whole disk devices. For this the option *--force* is required.

* OPTIONS
*-a*, *--all*

#+begin_quote
  Erase all available signatures. The set of erased signatures can be
  restricted with the *-t* option.
#+end_quote

*-b*, *--backup*

#+begin_quote
  Create a signature backup to the file
  /$HOME/wipefs-<devname>-<offset>.bak/. For more details see the
  *EXAMPLE* section.
#+end_quote

*-f*, *--force*

#+begin_quote
  Force erasure, even if the filesystem is mounted. This is required in
  order to erase a partition-table signature on a block device.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-J*, *--json*

#+begin_quote
  Use JSON output format.
#+end_quote

*--lock*[=/mode/]

#+begin_quote
  Use exclusive BSD lock for device or file it operates. The optional
  argument /mode/ can be *yes*, *no* (or 1 and 0) or *nonblock*. If the
  /mode/ argument is omitted, it defaults to *"yes"*. This option
  overwrites environment variable *$LOCK_BLOCK_DEVICE*. The default is
  not to use any lock at all, but it's recommended to avoid collisions
  with udevd or other tools.
#+end_quote

*-i*, *--noheadings*

#+begin_quote
  Do not print a header line.
#+end_quote

*-O*, *--output* /list/

#+begin_quote
  Specify which output columns to print. Use *--help* to get a list of
  all supported columns.
#+end_quote

*-n*, *--no-act*

#+begin_quote
  Causes everything to be done except for the *write*(2) call.
#+end_quote

*-o*, *--offset* /offset/

#+begin_quote
  Specify the location (in bytes) of the signature which should be
  erased from the device. The /offset/ number may include a "0x" prefix;
  then the number will be interpreted as a hex value. It is possible to
  specify multiple *-o* options.

  The /offset/ argument may be followed by the multiplicative suffixes
  KiB (=1024), MiB (=1024*1024), and so on for GiB, TiB, PiB, EiB, ZiB
  and YiB (the "iB" is optional, e.g., "K" has the same meaning as
  "KiB"), or the suffixes KB (=1000), MB (=1000*1000), and so on for GB,
  TB, PB, EB, ZB and YB.
#+end_quote

*-p*, *--parsable*

#+begin_quote
  Print out in parsable instead of printable format. Encode all
  potentially unsafe characters of a string to the corresponding hex
  value prefixed by '\x'.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  Suppress any messages after a successful signature wipe.
#+end_quote

*-t*, *--types* /list/

#+begin_quote
  Limit the set of printed or erased signatures. More than one type may
  be specified in a comma-separated list. The list or individual types
  can be prefixed with 'no' to specify the types on which no action
  should be taken. For more details see *mount*(8).
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

* ENVIRONMENT
LIBBLKID_DEBUG=all

#+begin_quote
  enables *libblkid*(3) debug output.
#+end_quote

LOCK_BLOCK_DEVICE=<mode>

#+begin_quote
  use exclusive BSD lock. The mode is "1" or "0". See *--lock* for more
  details.
#+end_quote

* EXAMPLES
*wipefs /dev/sda**

#+begin_quote
  Prints information about sda and all partitions on sda.
#+end_quote

*wipefs --all --backup /dev/sdb*

#+begin_quote
  Erases all signatures from the device //dev/sdb/ and creates a
  signature backup file /~/wipefs-sdb-<offset>.bak/ for each signature.
#+end_quote

*dd if=~/wipefs-sdb-0x00000438.bak of=/dev/sdb seek=$0x00000438 bs=1
conv=notrunc*

#+begin_quote
  Restores an ext2 signature from the backup file
  /~/wipefs-sdb-0x00000438.bak/.
#+end_quote

* AUTHORS
* SEE ALSO
*blkid*(8), *findfs*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *wipefs* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
