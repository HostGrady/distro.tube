#+TITLE: Manpages - chkfont.6
#+DESCRIPTION: Linux manpage for chkfont.6
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
chkfont - checks figlet 2.0 and up font files for format errors

* SYNOPSIS
*chkfont* [ /fontfile/ ]

* DESCRIPTION
This program checks figlet 2.0 and up font files for format errors. It
also looks for signs of common problems and gives warnings. chkfont does
not modify font files.

* EXAMPLES
To use *chkfont* on the "big" font

#+begin_quote
  *example% chkfont /usr/share/figlet/big.flf*
#+end_quote

* BUGS
Doesn't work on compressed font files.

* AUTHORS
chkfont was written by Glenn Chappell <ggc@uiuc.edu>

This manual page was written by Jonathon Abbott for the Debian Project.

* SEE ALSO
*figlet*(6), *showfigfonts*(6), *figlist*(6)
