#+TITLE: Manpages - elographics.4
#+DESCRIPTION: Linux manpage for elographics.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
elographics - Elographics input driver

* SYNOPSIS
*Section InputDevice*\\
* Identifier */idevname/**\\
* Driver elographics*\\
* Option Device */devpath/**\\
...\\
*EndSection*

* DESCRIPTION
*elographics* is an Xorg input driver for Elographics touchscreen
devices.

The *elographics* driver functions as a pointer input device, and may be
used as the X server's core pointer.

* SUPPORTED HARDWARE
E271-2210 and E271-2200 devices are supported. E281-2310 and compatible
devices are supported with some features unavailable.

* CONFIGURATION DETAILS
Please refer to xorg.conf(5) for general configuration details and for
options that can be used with all input drivers. This section only
covers configuration details specific to this driver.

The following driver options are supported:

- *Option Device */string/** :: The device that is attached to the
  touchscreen interface. Default is "/dev/ttyS1".

- *Option MinX */integer/** :: Set the minimum value for the touchscreen
  X axis. Default is 600.

- *Option MaxX */integer/** :: Set the maximum value for the touchscreen
  X axis. Default is 3000.

- *Option MinY */integer/** :: Set the minimum value for the touchscreen
  Y axis. Default is 600.

- *Option MaxY */integer/** :: Set the maximum value for the touchscreen
  Y axis. Default is 3000.

- *Option ScreenNo */integer/** :: The screen to attach to the
  touchscreen when running with multiple screens. Default is screen 0.

- *Option PortraitMode */string/** :: Set the X/Y axis orientation. The
  default is "Landscape" but you can rotate clockwise ("Portrait") or
  counter-clockwise ("PortraitCCW").

- *Option SwapXY */boolean/** :: Swap the X and Y axis on the display.
  Default is false.

- *Option UntouchDelay */integer/** :: The period that finger must be
  released for an untouch event to occur. Default: 5 (50ms).

- *Option ReportDelay */integer/** :: Delay between report packets.
  Default: 1 (10ms).

- *Option Model */string/** :: The touchscreen model. Default: unset.
  Supported models: "Sunit dSeries".

* SEE ALSO
Xorg(1), xorg.conf(5), Xserver(1), X(7).

* AUTHORS
Authors include: Patrick Lecoanet

This manpage was written by Lee Maguire on behalf of the Debian Project.
