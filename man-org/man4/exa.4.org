#+TITLE: Manpages - exa.4
#+DESCRIPTION: Linux manpage for exa.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
exa - new 2D acceleration architecture for X.Org

* DESCRIPTION
*EXA* provides a simple API for video drivers to implement for 2D
acceleration. It is a module loaded by drivers, and is not intended to
be loaded on its own. See your driver's manual page for how to enable
*EXA*.

The *EXA* architecture is designed to make accelerating the Render
extension simple and efficient, and results in various performance
tradeoffs compared to XAA. Some xorg.conf options are available for
debugging performance issues or driver rendering problems. They are not
intended for general use.

- *Option EXANoComposite */boolean/** :: Disables acceleration of the
  Composite operation, which is at the heart of the Render extension.
  Not related to the Composite extension. Default: No.

- *Option EXANoUploadToScreen */boolean/** :: Disables acceleration of
  uploading pixmap data to the framebuffer. Default: No.

- *Option EXANoDownloadFromScreen */boolean/** :: Disables acceleration
  of downloading of pixmap data from the framebuffer. *NOTE:* Not usable
  with drivers which rely on DownloadFromScreen succeeding. Default: No.

- *Option MigrationHeuristic */anystr/** :: Chooses an alternate pixmap
  migration heuristic, for debugging purposes. The default is intended
  to be the best performing one for general use, though others may help
  with specific use cases. Available options include always, greedy, and
  smart. Default: always.

* SEE ALSO
*Xorg*(1), *xorg.conf(5).*

* AUTHORS
Authors include: Keith Packard, Eric Anholt, Zack Rusin, and Michel
Dänzer
