#+TITLE: Manpages - xorg.conf.d.5
#+DESCRIPTION: Linux manpage for xorg.conf.d.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xorg.conf.d.5 is found in manpage for: [[../man5/xorg.conf.5][man5/xorg.conf.5]]