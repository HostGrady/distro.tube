#+TITLE: Manpages - Compose.5
#+DESCRIPTION: Linux manpage for Compose.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
Compose - X client mappings for multi-key input sequences

* DESCRIPTION
The X library, libX11, provides a simple input method for characters
beyond those represented on typical keyboards using sequences of key
strokes that are combined to enter a single character.

The compose file is searched for in the following order:

- If the environment variable *$XCOMPOSEFILE* is set, its value is used
  as the name of the Compose file.

- If the user's home directory has a file named /.XCompose/, it is used
  as the Compose file.

- The system provided compose file is used by mapping the locale to a
  compose file from the list in //usr/share/X11/locale/compose.dir/.

Compose files can use an *include* instruction. This allows local
modifications to be made to existing compose files without including all
of the content directly. For example, the system's iso8859-1 compose
file can be included with a line like this:

#+begin_quote
  *include */%S/iso8859-1/Compose/**
#+end_quote

There are several substitutions that can be made in the file name of the
include instruction:

- /%H/ :: expands to the user's home directory (the *$HOME* environment
  variable)

- /%L/ :: expands to the name of the locale specific Compose file (i.e.,
  //usr/share/X11/locale/<localename>/Compose/)

- /%S/ :: expands to the name of the system directory for Compose files
  (i.e., //usr/share/X11/locale/)

For example, you can include in your compose file the default Compose
file by using:

#+begin_quote
  *include %L*
#+end_quote

and then rewrite only the few rules that you need to change. New compose
rules can be added, and previous ones replaced.

* FILE FORMAT
Compose files are plain text files, with a separate line for each
compose sequence. Comments begin with *#* characters. Each compose
sequence specifies one or more events and a resulting input sequence,
with an optional comment at the end of the line:

#+begin_quote
  /EVENT/ [/EVENT/...] *:* /RESULT/ [*#* /COMMENT/]
#+end_quote

Each event consists of a specified input keysym, and optional modifier
states:

#+begin_quote
  [([*!*] ([*~*] /MODIFIER/)...) | *None*] *<*/keysym/*>*
#+end_quote

If the modifier list is preceded by *!* it must match exactly. MODIFIER
may be one of Ctrl, Lock, Caps, Shift, Alt or Meta. Each modifier may be
preceded by a *~* character to indicate that the modifier must not be
present. If *None* is specified, no modifier may be present.

The result specifies a string, keysym, or both, that the X client
receives as input when the sequence of events is input:

#+begin_quote
  /STRING/ | /keysym/ | /STRING/ /keysym/
#+end_quote

Keysyms are specified without the *XK_* prefix.

Strings may be direct text encoded in the locale for which the compose
file is to be used, or an escaped octal or hexadecimal character code.
Octal codes are specified as *\123* and hexadecimal codes as *\x3a*. It
is not necessary to specify in the right part of a rule a locale encoded
string in addition to the keysym name. If the string is omitted, Xlib
figures it out from the keysym according to the current locale. I.e., if
a rule looks like:

#+begin_quote
  *<dead_grave> <A> : \300 Agrave*
#+end_quote

the result of the composition is always the letter with the "\300" code.
But if the rule is:

#+begin_quote
  *<dead_grave> <A> : Agrave*
#+end_quote

the result depends on how Agrave is mapped in the current locale.

* ENVIRONMENT
- *XCOMPOSEFILE* :: File to use for compose sequences.

- *XCOMPOSECACHE* :: Directory to use for caching compiled compose
  files.

* FILES
- /$HOME/.XCompose/ :: User default compose file if XCOMPOSEFILE is not
  set.

- //usr/share/X11/locale/compose.dir/ :: File listing the compose file
  path to use for each locale.

- //usr/share/X11/locale/<localemapping>/Compose/ :: System default
  compose file for the locale, mapped via compose.dir.

- //var/cache/libx11/compose// :: System-wide cache directory for
  compiled compose files.

- /$HOME/.compose-cache// :: Per-user cache directory for compiled
  compose files.

* SEE ALSO
*XLookupString*(3), *XmbLookupString*(3), *XwcLookupString*(3),
*Xutf8LookupString*(3), *mkcomposecache*(1), *locale*(7).\\
/Xlib - C Language X Interface/
