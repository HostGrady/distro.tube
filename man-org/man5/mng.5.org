#+TITLE: Manpages - mng.5
#+DESCRIPTION: Linux manpage for mng.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mng - Multiple-image Network Graphics (MNG) format

* DESCRIPTION
MNG (Multiple-image Network Graphics) is the animation extension of the
popular PNG image-format. PNG (Portable Network Graphics) is an
extensible file format for the lossless, portable, well-compressed
storage of raster images.\\

MNG has advanced animation features which make it very useful as a full
replacement for GIF animations. These features allow animations that are
impossible with GIF or result in much smaller files as GIF.

As MNG builds on the same structure as PNG, it is robust, extensible and
free of patents. It retains the same clever file integrity checks as in
PNG.

MNG also embraces the lossy JPEG image-format in a sub-format named JNG,
which allows for alpha-transparency and color-correction on highly
compressed (photographic) images.

* SEE ALSO
/png(5)/, /jng(5)/, /libmng(3)/, /libpng(3)/, /zlib(3)/, deflate(5) ", "
zlib(5) ", " jpeg(5)

MNG 1.00, Februari 9, 2001:

#+begin_quote
  \\
  http://www.libpng.org/pub/mng
#+end_quote

* AUTHORS
This man page: Gerard Juyn

Multiple-image Network Graphics (MNG) Specification Version 1.00
(Februari 9, 2001): Glenn Randers-Pehrson and others
(png-list@ccrc.wustl.edu).

* COPYRIGHT NOTICE
The MNG-1.00 specification is copyright (c) 1998-2001 Glenn
Randers-Pehrson. See the specification for conditions of use and
distribution.
