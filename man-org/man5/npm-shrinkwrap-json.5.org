#+TITLE: Manpages - npm-shrinkwrap-json.5
#+DESCRIPTION: Linux manpage for npm-shrinkwrap-json.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*npm-shrinkwrap.json* - A publishable lockfile

** Description
*npm-shrinkwrap.json* is a file created by npm help *npm* shrinkwrap .
It is identical to *package-lock.json*, with one major caveat: Unlike
*package-lock.json*, *npm-shrinkwrap.json* may be included when
publishing a package.

The recommended use-case for *npm-shrinkwrap.json* is applications
deployed through the publishing process on the registry: for example,
daemons and command-line tools intended as global installs or
*devDependencies* . It's strongly discouraged for library authors to
publish this file, since that would prevent end users from having
control over transitive dependency updates.

If both *package-lock.json* and *npm-shrinkwrap.json* are present in a
package root, *npm-shrinkwrap.json* will be preferred over the
*package-lock.json* file.

For full details and description of the *npm-shrinkwrap.json* file
format, refer to the manual page for npm help package-lock.json.

** See also

#+begin_quote

  - npm help shrinkwrap

  - npm help package-lock.json

  - npm help package.json

  - npm help install
#+end_quote
