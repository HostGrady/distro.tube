#+TITLE: Manpages - repertoiremap.5
#+DESCRIPTION: Linux manpage for repertoiremap.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
repertoiremap - map symbolic character names to Unicode code points

* DESCRIPTION
A repertoire map defines mappings between symbolic character names
(mnemonics) and Unicode code points when compiling a locale with
*localedef*(1). Using a repertoire map is optional, it is needed only
when symbolic names are used instead of now preferred Unicode code
points.

** Syntax
The repertoiremap file starts with a header that may consist of the
following keywords:

- /comment_char/ :: is followed by a character that will be used as the
  comment character for the rest of the file. It defaults to the number
  sign (#).

- /escape_char/ :: is followed by a character that should be used as the
  escape character for the rest of the file to mark characters that
  should be interpreted in a special way. It defaults to the backslash
  (\).

The mapping section starts with the keyword /CHARIDS/ in the first
column.

The mapping lines have the following form:

- /<symbolic-name> <code-point> comment/ :: This defines exactly one
  mapping, /comment/ being optional.

The mapping section ends with the string /END CHARIDS/.

* FILES
- //usr/share/i18n/repertoiremaps/ :: Usual default repertoire map path.

* CONFORMING TO
POSIX.2.

* NOTES
Repertoire maps are deprecated in favor of Unicode code points.

* EXAMPLES
A mnemonic for the Euro sign can be defined as follows:

#+begin_example
  <Eu> <U20AC> EURO SIGN
#+end_example

* SEE ALSO
*locale*(1), *localedef*(1), *charmap*(5), *locale*(5)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
