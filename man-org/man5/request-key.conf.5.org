#+TITLE: Manpages - request-key.conf.5
#+DESCRIPTION: Linux manpage for request-key.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
request-key.conf - Instantiation handler configuration file

* DESCRIPTION
These files are used by the /sbin/request-key program to determine which
program it should run to instantiate a key.

request-key looks for the best match, reading all the following files:

#+begin_quote
  /etc/request-key.d/*.conf\\
  /etc/request-key.conf
#+end_quote

If it doesn't find a match, it will return an error and the kernel will
automatically negate the key.

The best match is defined as the line with the shortest wildcard skips,
ranking the columns in order left to right. If two lines have the same
length skips, then the first read is the one taken.

In the files, any blank line or line beginning with a hash mark '#' is
considered to be a comment and ignored.

All other lines are assumed to be command lines with a number of white
space separated fields:

<op> <type> <description> <callout-info> <prog> <arg1> <arg2> ...

The first four fields are used to match the parameters passed to
request-key by the kernel. /op/ is the operation type; currently the
only supported operation is "create".

/type/, /description/ and /callout-info/ match the three parameters
passed to *keyctl request2* or the *request_key()* system call. Each of
these may contain one asterisk '*' character as a wildcard anywhere
within the string.

Should a match be made, the program specified by <prog> will be exec'd.
This must have a fully qualified path name. argv[0] will be set from the
part of the program name that follows the last slash '/' character.

If the program name is prefixed with a pipe bar character '|', then the
program will be forked and exec'd attached to three pipes. The callout
information will be piped to it on it's stdin and the intended payload
data will be retrieved from its stdout. Anything sent to stderr will be
posted in syslog. If the program exits 0, then /sbin/request-key will
attempt to instantiate the key with the data read from stdout. If it
fails in any other way, then request-key will attempt to execute the
appropriate 'negate' operation command.

The program arguments can be substituted with various macros. Only
complete argument substitution is supported - macro substitutions can't
be embedded. All macros begin with a percent character '%'. An argument
beginning with two percent characters will have one of them discarded.

The following macros are supported:

#+begin_quote
  %o Operation type\\
  %k Key ID\\
  %t Key type\\
  %d Key description\\
  %c Callout information\\
  %u Key UID\\
  %g Key GID\\
  %T Requestor's thread keyring\\
  %P Requestor's process keyring\\
  %S Requestor's session keyring
#+end_quote

There's another macro substitution too that permits the interpolation of
the contents of a key:

#+begin_quote
  %{<type>:<description>}
#+end_quote

This performs a lookup for a key of the given type and description on
the requestor's keyrings, and if found, substitutes the contents for the
macro. If not found an error will be logged and the key under
construction will be negated.

* EXAMPLE
A basic file will be installed in the /etc. This will contain two
debugging lines that can be used to test the installation:

#+begin_quote
  create user debug:* negate /bin/keyctl negate %k 30 %S\\
  create user debug:loop:* * |/bin/cat\\
  create user debug:* * /usr/share/keyutils/request-key-debug.sh %k %d
  %c %S\\
  negate * * * /bin/keyctl negate %k 30 %S
#+end_quote

This is set up so that something like:

#+begin_quote
  keyctl request2 user debug:xxxx negate
#+end_quote

will create a negative user-defined key, something like:

#+begin_quote
  keyctl request2 user debug:yyyy spoon
#+end_quote

will create an instantiated user-defined key with "Debug spoon" as the
payload, and something like:

#+begin_quote
  keyctl request2 user debug:loop:zzzz abcdefghijkl
#+end_quote

will create an instantiated user-defined key with the callout
information as the payload.

* FILES
/etc/request-key.conf

\\

/etc/request-key.d/*.conf

* SEE ALSO
*keyctl*(1), *request-key.conf*(5)
