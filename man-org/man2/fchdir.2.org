#+TITLE: Manpages - fchdir.2
#+DESCRIPTION: Linux manpage for fchdir.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fchdir.2 is found in manpage for: [[../man2/chdir.2][man2/chdir.2]]