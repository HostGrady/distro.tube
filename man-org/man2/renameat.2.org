#+TITLE: Manpages - renameat.2
#+DESCRIPTION: Linux manpage for renameat.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about renameat.2 is found in manpage for: [[../man2/rename.2][man2/rename.2]]