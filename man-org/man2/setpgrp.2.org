#+TITLE: Manpages - setpgrp.2
#+DESCRIPTION: Linux manpage for setpgrp.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setpgrp.2 is found in manpage for: [[../man2/setpgid.2][man2/setpgid.2]]