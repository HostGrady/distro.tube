#+TITLE: Manpages - oldolduname.2
#+DESCRIPTION: Linux manpage for oldolduname.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about oldolduname.2 is found in manpage for: [[../man2/uname.2][man2/uname.2]]