#+TITLE: Manpages - rt_sigaction.2
#+DESCRIPTION: Linux manpage for rt_sigaction.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about rt_sigaction.2 is found in manpage for: [[../man2/sigaction.2][man2/sigaction.2]]