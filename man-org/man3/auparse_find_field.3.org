#+TITLE: Manpages - auparse_find_field.3
#+DESCRIPTION: Linux manpage for auparse_find_field.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_find_field - search for field name

* SYNOPSIS
*#include <auparse.h>*

const char *auparse_find_field(auparse_state_t *au, const char *name);

* DESCRIPTION
auparse_find_field will scan all records in an event to find the first
occurrence of the field name passed to it. Searching begins from the
cursor's current position. The field name is stored for subsequent
searching.

NOTE: auparse creates 2 psuedo fields that do not exist in the natural
record for SELinux AVC and USER_AVC decision and permissions. The field
names are seresult and seperms respectively.

* RETURN VALUE
Returns NULL field not found. If an error occurs errno will be set.
Otherwise, it returns a pointer to the text value associated with the
field.

* SEE ALSO
*auparse_first_record*(3), *auparse_find_field_next*(3).

* AUTHOR
Steve Grubb
