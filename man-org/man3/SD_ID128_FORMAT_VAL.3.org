#+TITLE: Manpages - SD_ID128_FORMAT_VAL.3
#+DESCRIPTION: Linux manpage for SD_ID128_FORMAT_VAL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about SD_ID128_FORMAT_VAL.3 is found in manpage for: [[../sd-id128.3][sd-id128.3]]