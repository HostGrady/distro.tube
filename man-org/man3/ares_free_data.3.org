#+TITLE: Manpages - ares_free_data.3
#+DESCRIPTION: Linux manpage for ares_free_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_free_data - Free data allocated by several c-ares functions

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_free_data(void *dataptr)

  cc file.c -lcares
#+end_example

* DESCRIPTION
The *ares_free_data(3)* function frees one or more data structures
allocated and returned by several c-ares functions. Specifically the
data returned by the following list of functions must be deallocated
using this function.

- *ares_get_servers(3)* :: When used to free the data returned by
  ares_get_servers(3) this will free the whole linked list of
  ares_addr_node structures returned by ares_get_servers(3).

- *ares_parse_srv_reply(3)* :: When used to free the data returned by
  ares_parse_srv_reply(3) this will free the whole linked list of
  ares_srv_reply structures returned by ares_parse_srv_reply(3), along
  with any additional storage associated with those structures.

- *ares_parse_mx_reply(3)* :: When used to free the data returned by
  ares_parse_mx_reply(3) this will free the whole linked list of
  ares_mx_reply structures returned by ares_parse_mx_reply(3), along
  with any additional storage associated with those structures.

- *ares_parse_txt_reply(3)* :: When used to free the data returned by
  ares_parse_txt_reply(3) this will free the whole linked list of
  ares_txt_reply structures returned by ares_parse_txt_reply(3), along
  with any additional storage associated with those structures.

- *ares_parse_soa_reply(3)* :: When used to free the data returned by
  ares_parse_soa_reply(3) this will free the ares_soa_reply structure,
  along with any additional storage associated with those structure.
  *ares_parse_uri_reply(3)* When used to free the data returned by
  ares_parse_uri_reply(3) this will free list of ares_uri_reply
  structures, along with any additional storage associated with those
  structure.

* RETURN VALUE
The ares_free_data() function does not return a value.

* AVAILABILITY
This function was first introduced in c-ares version 1.7.0.

* SEE ALSO
*ares_get_servers(3),* *ares_parse_srv_reply(3),*
*ares_parse_mx_reply(3),* *ares_parse_txt_reply(3),*
*ares_parse_soa_reply(3)*

* AUTHOR
Yang Tse

Copyright 1998 by the Massachusetts Institute of Technology.\\
Copyright (C) 2004-2010 by Daniel Stenberg.
