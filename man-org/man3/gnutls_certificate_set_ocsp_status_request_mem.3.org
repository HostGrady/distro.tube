#+TITLE: Manpages - gnutls_certificate_set_ocsp_status_request_mem.3
#+DESCRIPTION: Linux manpage for gnutls_certificate_set_ocsp_status_request_mem.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_certificate_set_ocsp_status_request_mem - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int
gnutls_certificate_set_ocsp_status_request_mem(gnutls_certificate_credentials_t
*/sc/*, const gnutls_datum_t * */resp_data/*, unsigned */idx/*,
gnutls_x509_crt_fmt_t */fmt/*);*

* ARGUMENTS
- gnutls_certificate_credentials_t sc :: is a credentials structure.

- const gnutls_datum_t * resp_data :: a memory buffer holding an OCSP
  response

- unsigned idx :: is a certificate index as returned by
  *gnutls_certificate_set_key()* and friends

- gnutls_x509_crt_fmt_t fmt :: is PEM or DER

* DESCRIPTION
This function sets the OCSP responses to be sent to the peer for the
certificate chain specified by /idx/ . When /fmt/ is set to PEM,
multiple responses can be loaded.

* NOTE
the ability to set multiple OCSP responses per credential structure via
the index /idx/ was added in version 3.5.6. To keep backwards
compatibility, it requires using *gnutls_certificate_set_flags()* with
the *GNUTLS_CERTIFICATE_API_V2* flag to make the set certificate
functions return an index usable by this function.

This function must be called after setting any certificates, and cannot
be used for certificates that are provided via a callback -- that is
when *gnutls_certificate_set_retrieve_function()* is used.

This function can be called multiple times when multiple responses which
apply to the certificate chain are available. If the response provided
does not match any certificates present in the chain, the code
*GNUTLS_E_OCSP_MISMATCH_WITH_CERTS* is returned. If the response is
already expired at the time of loading the code *GNUTLS_E_EXPIRED* is
returned.

* RETURNS
On success, the number of loaded responses is returned, otherwise a
negative error code.

* SINCE
3.6.3

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
