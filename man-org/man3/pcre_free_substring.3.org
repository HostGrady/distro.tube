#+TITLE: Manpages - pcre_free_substring.3
#+DESCRIPTION: Linux manpage for pcre_free_substring.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

*void pcre_free_substring(const char */stringptr/);*

*void pcre16_free_substring(PCRE_SPTR16 /stringptr/);*

*void pcre32_free_substring(PCRE_SPTR32 /stringptr/);*

* DESCRIPTION
This is a convenience function for freeing the store obtained by a
previous call to *pcre[16|32]_get_substring()* or
*pcre[16|32]_get_named_substring()*. Its only argument is a pointer to
the string.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
