#+TITLE: Manpages - libssh2_trace_sethandler.3
#+DESCRIPTION: Linux manpage for libssh2_trace_sethandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_trace_sethandler - set a trace output handler

* SYNOPSIS
#+begin_example
  #include <libssh2.h>

  typedef void (*libssh2_trace_handler_func)(LIBSSH2_SESSION *session,
                                             void* context,
                                             const char *data,
                                             size_t length);

  int libssh2_trace_sethandler(LIBSSH2_SESSION *session,
                               void* context,
                               libssh2_trace_handler_func callback);
#+end_example

* DESCRIPTION
libssh2_trace_sethandler installs a trace output handler for your
application. By default, when tracing has been switched on via a call to
libssh2_trace(), all output is written to stderr. By calling this method
and passing a function pointer that matches the
libssh2_trace_handler_func prototype, libssh2 will call back as it
generates trace output. This can be used to capture the trace output and
put it into a log file or diagnostic window. This function has no effect
unless libssh2 was built to support this option, and a typical "release
build" might not.

*context* can be used to pass arbitrary user defined data back into the
callback when invoked.

* AVAILABILITY
Added in libssh2 version 1.2.3
