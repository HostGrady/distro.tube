#+TITLE: Manpages - XtVaGetApplicationResources.3
#+DESCRIPTION: Linux manpage for XtVaGetApplicationResources.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtVaGetApplicationResources.3 is found in manpage for: [[../man3/XtGetApplicationResources.3][man3/XtGetApplicationResources.3]]