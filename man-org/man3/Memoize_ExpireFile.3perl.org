#+TITLE: Manpages - Memoize_ExpireFile.3perl
#+DESCRIPTION: Linux manpage for Memoize_ExpireFile.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Memoize::ExpireFile - test for Memoize expiration semantics

* DESCRIPTION
See Memoize::Expire.
