#+TITLE: Manpages - XErrorEvent.3
#+DESCRIPTION: Linux manpage for XErrorEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XErrorEvent - X error event structure

* STRUCTURES
The *XErrorEvent* structure contains:

#+begin_example
  typedef struct {
          int type;
          Display *display;       /* Display the event was read from */
          XID resourceid;         /* resource id */
          unsigned long serial;           /* serial number of failed request */
          unsigned char error_code;       /* error code of failed request */
          unsigned char request_code;     /* Major op-code of failed request */
          unsigned char minor_code;       /* Minor op-code of failed request */
  } XErrorEvent;
#+end_example

When you receive this event, the structure members are set as follows.

The serial member is the number of requests, starting from one, sent
over the network connection since it was opened. It is the number that
was the value of *NextRequest* immediately before the failing call was
made. The request_code member is a protocol request of the procedure
that failed, as defined in *X11/Xproto.h*.

* SEE ALSO
AllPlanes(3), XAnyEvent(3), XButtonEvent(3), XCreateWindowEvent(3),
XCirculateEvent(3), XCirculateRequestEvent(3), XColormapEvent(3),
XConfigureEvent(3), XConfigureRequestEvent(3), XCrossingEvent(3),
XDestroyWindowEvent(3), XExposeEvent(3), XFocusChangeEvent(3),
XGraphicsExposeEvent(3), XGravityEvent(3), XKeymapEvent(3),
XMapEvent(3), XMapRequestEvent(3), XPropertyEvent(3), XReparentEvent(3),
XResizeRequestEvent(3), XSelectionClearEvent(3), XSelectionEvent(3),
XSelectionRequestEvent(3), XUnmapEvent(3), XVisibilityEvent(3)\\
/Xlib - C Language X Interface/
