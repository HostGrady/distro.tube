#+TITLE: Manpages - SDL_AudioSpec.3
#+DESCRIPTION: Linux manpage for SDL_AudioSpec.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_AudioSpec - Audio Specification Structure

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    int freq;
    Uint16 format;
    Uint8 channels;
    Uint8 silence;
    Uint16 samples;
    Uint32 size;
    void (*callback)(void *userdata, Uint8 *stream, int len);
    void *userdata;
  } SDL_AudioSpec;
#+end_example

* STRUCTURE DATA
- *freq* :: Audio frequency in samples per second

- *format* :: Audio data format

- *channels* :: Number of channels: 1 mono, 2 stereo

- *silence* :: Audio buffer silence value (calculated)

- *samples* :: Audio buffer size in samples

- *size* :: Audio buffer size in bytes (calculated)

- *callback(..)* :: Callback function for filling the audio buffer

- *userdata* :: Pointer the user data which is passed to the callback
  function

* DESCRIPTION
The *SDL_AudioSpec* structure is used to describe the format of some
audio data. This structure is used by *SDL_OpenAudio* and *SDL_LoadWAV*.
While all fields are used by *SDL_OpenAudio* only *freq*, *format*,
*samples* and *channels* are used by *SDL_LoadWAV*. We will detail these
common members here.

- *freq* :: The number of samples sent to the sound device every second.
  Common values are 11025, 22050 and 44100. The higher the better.

- *format* :: Specifies the size and type of each sample element

- AUDIO_U8 :: 

- AUDIO_S8 :: 

- AUDIO_U16 or AUDIO_U16LSB :: 

- AUDIO_S16 or AUDIO_S16LSB :: 

- AUDIO_U16MSB :: 

- AUDIO_S16MSB :: 

- AUDIO_U16SYS :: 

- AUDIO_S16SYS :: 

- *channels* :: The number of seperate sound channels. 1 is mono (single
  channel), 2 is stereo (dual channel).

- *samples* :: When used with *SDL_OpenAudio* this refers to the size of
  the audio buffer in samples. A sample a chunk of audio data of the
  size specified in *format* mulitplied by the number of channels. When
  the *SDL_AudioSpec* is used with *SDL_LoadWAV* *samples* is set
  to 4096.

* SEE ALSO
*SDL_OpenAudio*, *SDL_LoadWAV*
