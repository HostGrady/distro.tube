#+TITLE: Manpages - __gnu_cxx__Relative_pointer_impl.3
#+DESCRIPTION: Linux manpage for __gnu_cxx__Relative_pointer_impl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::_Relative_pointer_impl< _Tp > - A storage policy for use with
_Pointer_adapter<> which stores the pointer's address as an offset value
which is relative to its own address.

* SYNOPSIS
\\

=#include <pointer.h>=

** Public Types
typedef _Tp *element_type*\\

** Public Member Functions
_Tp * *get* () const\\

bool *operator<* (const *_Relative_pointer_impl* &__rarg) const\\

bool *operator==* (const *_Relative_pointer_impl* &__rarg) const\\

void *set* (_Tp *__arg)\\

* Detailed Description
** "template<typename _Tp>
\\
class __gnu_cxx::_Relative_pointer_impl< _Tp >"A storage policy for use
with _Pointer_adapter<> which stores the pointer's address as an offset
value which is relative to its own address.

This is intended for pointers within shared memory regions which might
be mapped at different addresses by different processes. For null
pointers, a value of 1 is used. (0 is legitimate sometimes for nodes in
circularly linked lists) This value was chosen as the least likely to
generate an incorrect null, As there is no reason why any normal pointer
would point 1 byte into its own pointer address.

Definition at line *112* of file *pointer.h*.

* Member Typedef Documentation
** template<typename _Tp > typedef _Tp
*__gnu_cxx::_Relative_pointer_impl*< _Tp >::element_type
Definition at line *115* of file *pointer.h*.

* Member Function Documentation
** template<typename _Tp > _Tp * *__gnu_cxx::_Relative_pointer_impl*<
_Tp >::get () const= [inline]=
Definition at line *118* of file *pointer.h*.

** template<typename _Tp > bool *__gnu_cxx::_Relative_pointer_impl*< _Tp
>::operator< (const *_Relative_pointer_impl*< _Tp > & __rarg)
const= [inline]=
Definition at line *138* of file *pointer.h*.

** template<typename _Tp > bool *__gnu_cxx::_Relative_pointer_impl*< _Tp
>::operator== (const *_Relative_pointer_impl*< _Tp > & __rarg)
const= [inline]=
Definition at line *144* of file *pointer.h*.

** template<typename _Tp > void *__gnu_cxx::_Relative_pointer_impl*< _Tp
>::set (_Tp * __arg)= [inline]=
Definition at line *128* of file *pointer.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
