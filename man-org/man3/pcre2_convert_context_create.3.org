#+TITLE: Manpages - pcre2_convert_context_create.3
#+DESCRIPTION: Linux manpage for pcre2_convert_context_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  pcre2_convert_context *pcre2_convert_context_create(
   pcre2_general_context *gcontext);
#+end_example

* DESCRIPTION
This function is part of an experimental set of pattern conversion
functions. It creates and initializes a new convert context. If its
argument is NULL, *malloc()* is used to get the necessary memory;
otherwise the memory allocation function within the general context is
used. The result is NULL if the memory could not be obtained.

The pattern conversion functions are described in the *pcre2convert*
documentation.
