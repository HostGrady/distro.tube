#+TITLE: Manpages - TAP_Parser_YAMLish_Reader.3perl
#+DESCRIPTION: Linux manpage for TAP_Parser_YAMLish_Reader.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Parser::YAMLish::Reader - Read YAMLish data from iterator

* VERSION
Version 3.43

* SYNOPSIS
* DESCRIPTION
Note that parts of this code were derived from YAML::Tiny with the
permission of Adam Kennedy.

* METHODS
** Class Methods
/=new=/

The constructor =new= creates and returns an empty
=TAP::Parser::YAMLish::Reader= object.

my $reader = TAP::Parser::YAMLish::Reader->new;

** Instance Methods
/=read=/

my $got = $reader->read($iterator);

Read YAMLish from a TAP::Parser::Iterator and return the data structure
it represents.

/=get_raw=/

my $source = $reader->get_source;

Return the raw YAMLish source from the most recent =read=.

* AUTHOR
Andy Armstrong, <andy@hexten.net>

Adam Kennedy wrote YAML::Tiny which provided the template and many of
the YAML matching regular expressions for this module.

* SEE ALSO
YAML::Tiny, YAML, YAML::Syck, Config::Tiny, CSS::Tiny,
<http://use.perl.org/~Alias/journal/29427>

* COPYRIGHT
Copyright 2007-2011 Andy Armstrong.

Portions copyright 2006-2008 Adam Kennedy.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

The full text of the license can be found in the LICENSE file included
with this module.
