#+TITLE: Manpages - FcConfigBuildFonts.3
#+DESCRIPTION: Linux manpage for FcConfigBuildFonts.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigBuildFonts - Build font database

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigBuildFonts (FcConfig */config/*);*

* DESCRIPTION
Builds the set of available fonts for the given configuration. Note that
any changes to the configuration after this call have indeterminate
effects. Returns FcFalse if this operation runs out of memory. If
/config/ is NULL, the current configuration is used.
