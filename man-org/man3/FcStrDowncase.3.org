#+TITLE: Manpages - FcStrDowncase.3
#+DESCRIPTION: Linux manpage for FcStrDowncase.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrDowncase - create a lower case translation of a string

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 * FcStrDowncase (const FcChar8 */s/*);*

* DESCRIPTION
Allocates memory, copies /s/, converting upper case letters to lower
case and returns the allocated buffer.
