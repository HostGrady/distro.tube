#+TITLE: Manpages - gnutls_record_send_range.3
#+DESCRIPTION: Linux manpage for gnutls_record_send_range.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_record_send_range - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*ssize_t gnutls_record_send_range(gnutls_session_t */session/*, const
void * */data/*, size_t */data_size/*, const gnutls_range_st *
*/range/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

- const void * data :: contains the data to send.

- size_t data_size :: is the length of the data.

- const gnutls_range_st * range :: is the range of lengths in which the
  real data length must be hidden.

* DESCRIPTION
This function operates like *gnutls_record_send()* but, while
*gnutls_record_send()* adds minimal padding to each TLS record, this
function uses the TLS extra-padding feature to conceal the real data
size within the range of lengths provided. Some TLS sessions do not
support extra padding (e.g. stream ciphers in standard TLS or SSL3
sessions). To know whether the current session supports extra padding,
and hence length hiding, use the *gnutls_record_can_use_length_hiding()*
function.

* NOTE
This function currently is limited to blocking sockets.

* RETURNS
The number of bytes sent (that is data_size in a successful invocation),
or a negative error code.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
