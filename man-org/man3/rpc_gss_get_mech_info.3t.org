#+TITLE: Manpages - rpc_gss_get_mech_info.3t
#+DESCRIPTION: Linux manpage for rpc_gss_get_mech_info.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
This function returns the list of QOP names supported by the GSS_API
mechanism named "mech".

The name of a GSS_API mechanism. "kerberos_v5" is currently the only
supported mechanism.

Buffer in which maximum service type is planted

If the named GSS_API mechanism is recognized, a list of the supported
Qualities of Protection is returned. The maximum supported service type
for the mechanism is returned in

Otherwise

is returned.

Note: The returned QOP list is statically allocated memory. The caller
must not free this array.

The

function is part of libtirpc.

This manual page was written by
