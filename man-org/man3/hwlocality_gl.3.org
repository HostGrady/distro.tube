#+TITLE: Manpages - hwlocality_gl.3
#+DESCRIPTION: Linux manpage for hwlocality_gl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_gl - Interoperability with OpenGL displays

* SYNOPSIS
\\

** Functions
static *hwloc_obj_t* *hwloc_gl_get_display_osdev_by_port_device*
(*hwloc_topology_t* topology, unsigned port, unsigned device)\\

static *hwloc_obj_t* *hwloc_gl_get_display_osdev_by_name*
(*hwloc_topology_t* topology, const char *name)\\

static int *hwloc_gl_get_display_by_osdev* (*hwloc_topology_t* topology,
*hwloc_obj_t* osdev, unsigned *port, unsigned *device)\\

* Detailed Description
This interface offers ways to retrieve topology information about OpenGL
displays.

Only the NVIDIA display locality information is currently available,
using the NV-CONTROL X11 extension and the NVCtrl library.

* Function Documentation
** static int hwloc_gl_get_display_by_osdev (*hwloc_topology_t*
topology, *hwloc_obj_t* osdev, unsigned * port, unsigned *
device)= [inline]=, = [static]=
Get the OpenGL display port and device corresponding to the given hwloc
OS object. Retrieves the OpenGL display port (server) in =port= and
device (screen) in =screen= that correspond to the given hwloc OS device
object.

*Returns*

#+begin_quote
  =-1= if none could be found.
#+end_quote

The topology =topology= does not necessarily have to match the current
machine. For instance the topology may be an XML import of a remote
host. I/O devices detection and the GL component must be enabled in the
topology.

** static *hwloc_obj_t* hwloc_gl_get_display_osdev_by_name
(*hwloc_topology_t* topology, const char * name)= [inline]=, = [static]=
Get the hwloc OS device object corresponding to the OpenGL display given
by name.

*Returns*

#+begin_quote
  The hwloc OS device object describing the OpenGL display whose name is
  =name=, built as ':port.device' such as ':0.0' .

  =NULL= if none could be found.
#+end_quote

The topology =topology= does not necessarily have to match the current
machine. For instance the topology may be an XML import of a remote
host. I/O devices detection and the GL component must be enabled in the
topology.

*Note*

#+begin_quote
  The corresponding PCI device object can be obtained by looking at the
  OS device parent object (unless PCI devices are filtered out).
#+end_quote

** static *hwloc_obj_t* hwloc_gl_get_display_osdev_by_port_device
(*hwloc_topology_t* topology, unsigned port, unsigned
device)= [inline]=, = [static]=
Get the hwloc OS device object corresponding to the OpenGL display given
by port and device index.

*Returns*

#+begin_quote
  The hwloc OS device object describing the OpenGL display whose port
  (server) is =port= and device (screen) is =device=.

  =NULL= if none could be found.
#+end_quote

The topology =topology= does not necessarily have to match the current
machine. For instance the topology may be an XML import of a remote
host. I/O devices detection and the GL component must be enabled in the
topology.

*Note*

#+begin_quote
  The corresponding PCI device object can be obtained by looking at the
  OS device parent object (unless PCI devices are filtered out).
#+end_quote

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
