#+TITLE: Manpages - __gnu_pbds_detail_stored_data.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_stored_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::stored_data< _Tv, _Th, Store_Hash > - Primary
template for representation of stored data. Two types of data can be
stored: value and hash.

* SYNOPSIS
\\

=#include <types_traits.hpp>=

Inherits *__gnu_pbds::detail::stored_value< _Tv >*, and
*__gnu_pbds::detail::stored_hash< _Th >*.

** Public Types
typedef _Th *hash_type*\\

typedef _Tv *value_type*\\

** Public Attributes
hash_type *m_hash*\\

value_type *m_value*\\

* Detailed Description
** "template<typename _Tv, typename _Th, bool Store_Hash>
\\
struct __gnu_pbds::detail::stored_data< _Tv, _Th, Store_Hash >"Primary
template for representation of stored data. Two types of data can be
stored: value and hash.

Definition at line *95* of file *types_traits.hpp*.

* Member Typedef Documentation
** template<typename _Th > typedef _Th
*__gnu_pbds::detail::stored_hash*< _Th >::hash_type= [inherited]=
Definition at line *88* of file *types_traits.hpp*.

** template<typename _Tv > typedef _Tv
*__gnu_pbds::detail::stored_value*< _Tv >::value_type= [inherited]=
Definition at line *80* of file *types_traits.hpp*.

* Member Data Documentation
** template<typename _Th > hash_type *__gnu_pbds::detail::stored_hash*<
_Th >::m_hash= [inherited]=
Definition at line *89* of file *types_traits.hpp*.

** template<typename _Tv > value_type
*__gnu_pbds::detail::stored_value*< _Tv >::m_value= [inherited]=
Definition at line *81* of file *types_traits.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
