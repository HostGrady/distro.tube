#+TITLE: Manpages - Pod_Perldoc_ToChecker.3perl
#+DESCRIPTION: Linux manpage for Pod_Perldoc_ToChecker.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Perldoc::ToChecker - let Perldoc check Pod for errors

* SYNOPSIS
% perldoc -o checker SomeFile.pod No Pod errors in SomeFile.pod (or an
error report)

* DESCRIPTION
This is a plug-in class that allows Perldoc to use Pod::Simple::Checker
as a formatter class (or if that is not available, then Pod::Checker),
to check for errors in a given Pod file.

This is actually a Pod::Simple::Checker (or Pod::Checker) subclass, and
inherits all its options.

* SEE ALSO
Pod::Simple::Checker, Pod::Simple, Pod::Checker, Pod::Perldoc

* COPYRIGHT AND DISCLAIMERS
Copyright (c) 2002 Sean M. Burke. All rights reserved.

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

This program is distributed in the hope that it will be useful, but
without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

* AUTHOR
Current maintainer: Mark Allen =<mallen@cpan.org>=

Past contributions from: brian d foy =<bdfoy@cpan.org>= Adriano R.
Ferreira =<ferreira@cpan.org>=, Sean M. Burke =<sburke@cpan.org>=
