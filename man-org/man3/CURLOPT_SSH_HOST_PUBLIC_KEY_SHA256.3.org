#+TITLE: Manpages - CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256 - SHA256 hash of SSH server public
key

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256,
                            char *sha256);
#+end_example

* DESCRIPTION
Pass a char * pointing to a string containing a Base64-encoded SHA256
hash of the remote host's public key. The transfer will fail if the
given hash does not match the hash the remote host provides.

* DEFAULT
NULL

* PROTOCOLS
SCP and SFTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
    curl_easy_setopt(curl, CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256,
                     "NDVkMTQxMGQ1ODdmMjQ3MjczYjAyOTY5MmRkMjVmNDQ=");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.80.0 Requires the libssh2 back-end.

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_SSH_PUBLIC_KEYFILE*(3), *CURLOPT_SSH_AUTH_TYPES*(3),
