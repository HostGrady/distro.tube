#+TITLE: Manpages - __gnu_cxx__Caster.3
#+DESCRIPTION: Linux manpage for __gnu_cxx__Caster.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::_Caster< _ToType >

* SYNOPSIS
\\

=#include <cast.h>=

** Public Types
typedef *_ToType::element_type* * *type*\\

* Detailed Description
** "template<typename _ToType>
\\
struct __gnu_cxx::_Caster< _ToType >"These functions are here to allow
containers to support non standard pointer types. For normal pointers,
these resolve to the use of the standard cast operation. For other types
the functions will perform the appropriate cast to/from the custom
pointer class so long as that class meets the following conditions: 1)
has a typedef element_type which names tehe type it points to. 2) has a
get() const method which returns element_type*. 3) has a constructor
which can take one element_type* argument. This type supports the
semantics of the pointer cast operators (below.)

Definition at line *52* of file *cast.h*.

* Member Typedef Documentation
** template<typename _ToType > typedef *_ToType::element_type**
*__gnu_cxx::_Caster*< _ToType >::type
Definition at line *53* of file *cast.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
