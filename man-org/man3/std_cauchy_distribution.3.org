#+TITLE: Manpages - std_cauchy_distribution.3
#+DESCRIPTION: Linux manpage for std_cauchy_distribution.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::cauchy_distribution< _RealType > - A cauchy_distribution random
number distribution.

* SYNOPSIS
\\

=#include <random.h>=

** Classes
struct *param_type*\\

** Public Types
typedef _RealType *result_type*\\

** Public Member Functions
*cauchy_distribution* (_RealType __a, _RealType __b=1.0)\\

*cauchy_distribution* (const *param_type* &__p)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng, const
*param_type* &__p)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng, const *param_type* &__p)\\

_RealType *a* () const\\

_RealType *b* () const\\

*result_type* *max* () const\\
Returns the least upper bound value of the distribution.

*result_type* *min* () const\\
Returns the greatest lower bound value of the distribution.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng)\\
Generating functions.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng, const *param_type*
&__p)\\

*param_type* *param* () const\\
Returns the parameter set of the distribution.

void *param* (const *param_type* &__param)\\
Sets the parameter set of the distribution.

void *reset* ()\\
Resets the distribution state.

** Friends
bool *operator==* (const *cauchy_distribution* &__d1, const
*cauchy_distribution* &__d2)\\
Return true if two Cauchy distributions have the same parameters.

* Detailed Description
** "template<typename _RealType = double>
\\
class std::cauchy_distribution< _RealType >"A cauchy_distribution random
number distribution.

The formula for the normal probability mass function is $p(x|a,b) = (i b
(1 + (ac{x-a}{b})^2))^{-1}$

Definition at line *2856* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef _RealType
*std::cauchy_distribution*< _RealType >::*result_type*
The type of the range of the distribution.

Definition at line *2863* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double> *std::cauchy_distribution*<
_RealType >::*cauchy_distribution* ()= [inline]=
Definition at line *2898* of file *random.h*.

** template<typename _RealType = double> *std::cauchy_distribution*<
_RealType >::*cauchy_distribution* (_RealType __a, _RealType __b =
=1.0=)= [inline]=, = [explicit]=
Definition at line *2901* of file *random.h*.

** template<typename _RealType = double> *std::cauchy_distribution*<
_RealType >::*cauchy_distribution* (const *param_type* &
__p)= [inline]=, = [explicit]=
Definition at line *2906* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::cauchy_distribution*< _RealType >::__generate (_ForwardIterator
__f, _ForwardIterator __t, _UniformRandomNumberGenerator &
__urng)= [inline]=
Definition at line *2973* of file *random.h*.

** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::cauchy_distribution*< _RealType >::__generate (_ForwardIterator
__f, _ForwardIterator __t, _UniformRandomNumberGenerator & __urng, const
*param_type* & __p)= [inline]=
Definition at line *2980* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > void *std::cauchy_distribution*<
_RealType >::__generate (*result_type* * __f, *result_type* * __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *2987* of file *random.h*.

** template<typename _RealType = double> _RealType
*std::cauchy_distribution*< _RealType >::a () const= [inline]=
Definition at line *2921* of file *random.h*.

** template<typename _RealType = double> _RealType
*std::cauchy_distribution*< _RealType >::b () const= [inline]=
Definition at line *2925* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::cauchy_distribution*< _RealType >::max () const= [inline]=
Returns the least upper bound value of the distribution.

Definition at line *2954* of file *random.h*.

References *std::numeric_limits< _Tp >::max()*.

** template<typename _RealType = double> *result_type*
*std::cauchy_distribution*< _RealType >::min () const= [inline]=
Returns the greatest lower bound value of the distribution.

Definition at line *2947* of file *random.h*.

References *std::numeric_limits< _Tp >::lowest()*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::cauchy_distribution*< _RealType >::operator()
(_UniformRandomNumberGenerator & __urng)= [inline]=
Generating functions.

Definition at line *2962* of file *random.h*.

References *std::cauchy_distribution< _RealType >::operator()()*.

Referenced by *std::cauchy_distribution< _RealType >::operator()()*.

** template<typename _RealType > template<typename
_UniformRandomNumberGenerator > *cauchy_distribution*< _RealType
>::*result_type* *std::cauchy_distribution*< _RealType >::operator()
(_UniformRandomNumberGenerator & __urng, const *param_type* & __p)
Definition at line *2103* of file *bits/random.tcc*.

** template<typename _RealType = double> *param_type*
*std::cauchy_distribution*< _RealType >::param () const= [inline]=
Returns the parameter set of the distribution.

Definition at line *2932* of file *random.h*.

Referenced by *std::operator>>()*.

** template<typename _RealType = double> void
*std::cauchy_distribution*< _RealType >::param (const *param_type* &
__param)= [inline]=
Sets the parameter set of the distribution.

*Parameters*

#+begin_quote
  /__param/ The new parameter set of the distribution.
#+end_quote

Definition at line *2940* of file *random.h*.

** template<typename _RealType = double> void
*std::cauchy_distribution*< _RealType >::reset ()= [inline]=
Resets the distribution state.

Definition at line *2914* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> bool operator== (const
*cauchy_distribution*< _RealType > & __d1, const *cauchy_distribution*<
_RealType > & __d2)= [friend]=
Return true if two Cauchy distributions have the same parameters.

Definition at line *2997* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
