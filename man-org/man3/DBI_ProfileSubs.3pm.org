#+TITLE: Manpages - DBI_ProfileSubs.3pm
#+DESCRIPTION: Linux manpage for DBI_ProfileSubs.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBI::ProfileSubs - Subroutines for dynamic profile Path

* SYNOPSIS
DBI_PROFILE=&norm_std_n3 prog.pl

This is new and still experimental.

* TO DO
Define come kind of naming convention for the subs.
