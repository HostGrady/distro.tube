#+TITLE: Manpages - uuid_copy.3
#+DESCRIPTION: Linux manpage for uuid_copy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
uuid_copy - copy a UUID value

* SYNOPSIS
*#include <uuid.h>*

*void uuid_copy(uuid_t */dst/*, uuid_t */src/*;*

* DESCRIPTION
The *uuid_copy*/() function copies the UUID variable src to dst./

* RETURN VALUE
The copied UUID is returned in the location pointed to by /dst./

* AUTHORS
Theodore Y. Ts'o

* SEE ALSO
*uuid*/(3),/ *uuid_clear*/(3),/ *uuid_compare*/(3),/
*uuid_generate*/(3),/ *uuid_is_null*/(3),/ *uuid_parse*/(3),/
*uuid_unparse*/(3)/

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *libuuid*/ library is part of the util-linux package since version
2.15.1. It can be downloaded from / /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
