#+TITLE: Manpages - std_seed_seq.3
#+DESCRIPTION: Linux manpage for std_seed_seq.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::seed_seq - The seed_seq class generates sequences of seeds for
random number generators.

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef uint_least32_t *result_type*\\

** Public Member Functions
*seed_seq* () noexcept\\

template<typename _InputIterator > *seed_seq* (_InputIterator __begin,
_InputIterator __end)\\

*seed_seq* (const *seed_seq* &)=delete\\

template<typename _IntType > *seed_seq* (*std::initializer_list*<
_IntType > __il)\\

template<typename _RandomAccessIterator > void *generate*
(_RandomAccessIterator __begin, _RandomAccessIterator __end)\\

*seed_seq* & *operator=* (const *seed_seq* &)=delete\\

template<typename _OutputIterator > void *param* (_OutputIterator
__dest) const\\

size_t *size* () const noexcept\\

* Detailed Description
The seed_seq class generates sequences of seeds for random number
generators.

Definition at line *6065* of file *random.h*.

* Member Typedef Documentation
** typedef uint_least32_t *std::seed_seq::result_type*
The type of the seed vales.

Definition at line *6069* of file *random.h*.

* Constructor & Destructor Documentation
** std::seed_seq::seed_seq ()= [inline]=, = [noexcept]=
Default constructor.

Definition at line *6072* of file *random.h*.

** template<typename _IntType > std::seed_seq::seed_seq
(*std::initializer_list*< _IntType > __il)
Definition at line *3235* of file *bits/random.tcc*.

** template<typename _InputIterator > std::seed_seq::seed_seq
(_InputIterator __begin, _InputIterator __end)
Definition at line *3243* of file *bits/random.tcc*.

* Member Function Documentation
** template<typename _RandomAccessIterator > void
std::seed_seq::generate (_RandomAccessIterator __begin,
_RandomAccessIterator __end)
Definition at line *3252* of file *bits/random.tcc*.

** template<typename _OutputIterator > void std::seed_seq::param
(_OutputIterator __dest) const= [inline]=
Definition at line *6093* of file *random.h*.

** size_t std::seed_seq::size () const= [inline]=, = [noexcept]=
Definition at line *6088* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
