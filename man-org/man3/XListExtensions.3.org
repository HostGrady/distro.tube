#+TITLE: Manpages - XListExtensions.3
#+DESCRIPTION: Linux manpage for XListExtensions.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XListExtensions.3 is found in manpage for: [[../man3/XQueryExtension.3][man3/XQueryExtension.3]]