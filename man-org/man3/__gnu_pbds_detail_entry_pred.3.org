#+TITLE: Manpages - __gnu_pbds_detail_entry_pred.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_entry_pred.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::entry_pred< _VTp, Pred, _Alloc, No_Throw > - Entry
predicate primary class template.

* SYNOPSIS
\\

* Detailed Description
** "template<typename _VTp, typename Pred, typename _Alloc, bool
No_Throw>
\\
struct __gnu_pbds::detail::entry_pred< _VTp, Pred, _Alloc, No_Throw
>"Entry predicate primary class template.

Definition at line *50* of file *entry_pred.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
