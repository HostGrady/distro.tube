#+TITLE: Manpages - std_chrono_treat_as_floating_point.3
#+DESCRIPTION: Linux manpage for std_chrono_treat_as_floating_point.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::chrono::treat_as_floating_point< _Rep > - treat_as_floating_point

* SYNOPSIS
\\

Inherits *std::is_floating_point< _Rep >*.

* Detailed Description
** "template<typename _Rep>
\\
struct std::chrono::treat_as_floating_point< _Rep
>"treat_as_floating_point

Definition at line *268* of file *std/chrono*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
