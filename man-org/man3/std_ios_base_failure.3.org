#+TITLE: Manpages - std_ios_base_failure.3
#+DESCRIPTION: Linux manpage for std_ios_base_failure.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::ios_base::failure - These are thrown to indicate problems with io.

* SYNOPSIS
\\

=#include <ios_base.h>=

Inherits *std::exception*.

** Public Member Functions
*failure* (const char *__s, const *error_code* &=*error_code*{})\\

*failure* (const *string* &__s, const *error_code* &) noexcept\\

*failure* (const *string* &__str) throw ()\\

*error_code* *code* () const noexcept\\

virtual const char * *what* () const throw ()\\

* Detailed Description
These are thrown to indicate problems with io.

27.4.2.1.1 Class ios_base::failure

Definition at line *276* of file *ios_base.h*.

* Constructor & Destructor Documentation
** std::ios_base::failure::failure (const *string* & __s, const
*error_code* &)= [inline]=, = [explicit]=, = [noexcept]=
Definition at line *297* of file *ios_base.h*.

** std::ios_base::failure::failure (const char * __s, const *error_code*
& = *error_code*={}=*)*= [inline]=*, *= [explicit]=
Definition at line *302 of file ios_base.h.*

* Member Function Documentation
** *error_code std::ios_base::failure::code () const*= [inline]=*,
*= [noexcept]=
Definition at line *307 of file ios_base.h.*

** virtual const char * std::ios_base::failure::what ()
const= [virtual]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented from *std::exception.*

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
