#+TITLE: Manpages - SDL_QuitEvent.3
#+DESCRIPTION: Linux manpage for SDL_QuitEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_QuitEvent - Quit requested event

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    Uint8 type
  } SDL_QuitEvent;
#+end_example

* STRUCTURE DATA
- *type* :: *SDL_QUIT*

* DESCRIPTION
*SDL_QuitEvent* is a member of the *SDL_Event* union and is used whan an
event of type *SDL_QUIT* is reported.

As can be seen, the SDL_QuitEvent structure serves no useful purpose.
The event itself, on the other hand, is very important. If you filter
out or ignore a quit event then it is impossible for the user to close
the window. On the other hand, if you do accept a quit event then the
application window will be closed, and screen updates will still report
success event though the application will no longer be visible.

#+begin_quote
  *Note: *

  The macro *SDL_QuitRequested will return non-zero if a quit event is
  pending*
#+end_quote

* SEE ALSO
*SDL_Event*, *SDL_SetEventFilter*
