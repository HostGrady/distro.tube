#+TITLE: Manpages - RAND_cleanup.3ssl
#+DESCRIPTION: Linux manpage for RAND_cleanup.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
RAND_cleanup - erase the PRNG state

* SYNOPSIS
#include <openssl/rand.h> #if OPENSSL_API_COMPAT < 0x10100000L void
RAND_cleanup(void) #endif

* DESCRIPTION
Prior to OpenSSL 1.1.0, *RAND_cleanup()* released all resources used by
the PRNG. As of version 1.1.0, it does nothing and should not be called,
since no explicit initialisation or de-initialisation is necessary. See
*OPENSSL_init_crypto* (3).

* RETURN VALUES
*RAND_cleanup()* returns no value.

* SEE ALSO
*RAND* (7)

* HISTORY
*RAND_cleanup()* was deprecated in OpenSSL 1.1.0; do not use it. See
*OPENSSL_init_crypto* (3)

* COPYRIGHT
Copyright 2000-2019 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
