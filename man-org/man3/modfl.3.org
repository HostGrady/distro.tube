#+TITLE: Manpages - modfl.3
#+DESCRIPTION: Linux manpage for modfl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about modfl.3 is found in manpage for: [[../man3/modf.3][man3/modf.3]]