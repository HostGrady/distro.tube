#+TITLE: Manpages - XConfigureEvent.3
#+DESCRIPTION: Linux manpage for XConfigureEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XConfigureEvent - ConfigureNotify event structure

* STRUCTURES
The structure for *ConfigureNotify* events contains:

#+begin_example
  typedef struct {
          int type;       /* ConfigureNotify */
          unsigned long serial;   /* # of last request processed by server */
          Bool send_event;        /* true if this came from a SendEvent request */
          Display *display;       /* Display the event was read from */
          Window event;
          Window window;
          int x, y;
          int width, height;
          int border_width;
          Window above;
          Bool override_redirect;
  } XConfigureEvent;
#+end_example

When you receive this event, the structure members are set as follows.

The type member is set to the event type constant name that uniquely
identifies it. For example, when the X server reports a *GraphicsExpose*
event to a client application, it sends an *XGraphicsExposeEvent*
structure with the type member set to *GraphicsExpose*. The display
member is set to a pointer to the display the event was read on. The
send_event member is set to *True* if the event came from a *SendEvent*
protocol request. The serial member is set from the serial number
reported in the protocol but expanded from the 16-bit least-significant
bits to a full 32-bit value. The window member is set to the window that
is most useful to toolkit dispatchers.

The event member is set either to the reconfigured window or to its
parent, depending on whether *StructureNotify* or *SubstructureNotify*
was selected. The window member is set to the window whose size,
position, border, and/or stacking order was changed.

The x and y members are set to the coordinates relative to the parent
window's origin and indicate the position of the upper-left outside
corner of the window. The width and height members are set to the inside
size of the window, not including the border. The border_width member is
set to the width of the window's border, in pixels.

The above member is set to the sibling window and is used for stacking
operations. If the X server sets this member to *None*, the window whose
state was changed is on the bottom of the stack with respect to sibling
windows. However, if this member is set to a sibling window, the window
whose state was changed is placed on top of this sibling window.

The override_redirect member is set to the override-redirect attribute
of the window. Window manager clients normally should ignore this window
if the override_redirect member is *True*.

* SEE ALSO
XAnyEvent(3), XButtonEvent(3), XCreateWindowEvent(3),
XCirculateEvent(3), XCirculateRequestEvent(3), XColormapEvent(3),
XConfigureRequestEvent(3), XCrossingEvent(3), XDestroyWindowEvent(3),
XErrorEvent(3), XExposeEvent(3), XFocusChangeEvent(3),
XGraphicsExposeEvent(3), XGravityEvent(3), XKeymapEvent(3),
XMapEvent(3), XMapRequestEvent(3), XPropertyEvent(3), XReparentEvent(3),
XResizeRequestEvent(3), XSelectionClearEvent(3), XSelectionEvent(3),
XSelectionRequestEvent(3), XUnmapEvent(3), XVisibilityEvent(3)\\
/Xlib - C Language X Interface/
