#+TITLE: Manpages - CURLOPT_COOKIESESSION.3
#+DESCRIPTION: Linux manpage for CURLOPT_COOKIESESSION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_COOKIESESSION - start a new cookie session

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_COOKIESESSION, long
init);

* DESCRIPTION
Pass a long set to 1 to mark this as a new cookie "session". It will
force libcurl to ignore all cookies it is about to load that are
"session cookies" from the previous session. By default, libcurl always
stores and loads all cookies, independent if they are session cookies or
not. Session cookies are cookies without expiry date and they are meant
to be alive and existing for this "session" only.

A "session" is usually defined in browser land for as long as you have
your browser up, more or less.

* DEFAULT
0

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

    /* new "session", do not load session cookies */
    curl_easy_setopt(curl, CURLOPT_COOKIESESSION, 1L);

    /* get the (non session) cookies from this file */
    curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "/tmp/cookies.txt");

    ret = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Along with HTTP

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_COOKIEFILE*(3), *CURLOPT_COOKIE*(3),
