#+TITLE: Manpages - tanl.3
#+DESCRIPTION: Linux manpage for tanl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about tanl.3 is found in manpage for: [[../man3/tan.3][man3/tan.3]]