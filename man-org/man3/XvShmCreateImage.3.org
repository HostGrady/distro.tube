#+TITLE: Manpages - XvShmCreateImage.3
#+DESCRIPTION: Linux manpage for XvShmCreateImage.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XvShmCreateImage.3 is found in manpage for: [[../man3/XvCreateImage.3][man3/XvCreateImage.3]]