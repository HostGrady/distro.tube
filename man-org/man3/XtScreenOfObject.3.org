#+TITLE: Manpages - XtScreenOfObject.3
#+DESCRIPTION: Linux manpage for XtScreenOfObject.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtScreenOfObject.3 is found in manpage for: [[../man3/XtDisplay.3][man3/XtDisplay.3]]