#+TITLE: Manpages - xcb_xkb_indicator_state_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_xkb_indicator_state_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_xkb_indicator_state_notify_event_t -

* SYNOPSIS
*#include <xcb/xkb.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_xkb_indicator_state_notify_event_t {
      uint8_t         response_type;
      uint8_t         xkbType;
      uint16_t        sequence;
      xcb_timestamp_t time;
      uint8_t         deviceID;
      uint8_t         pad0[3];
      uint32_t        state;
      uint32_t        stateChanged;
      uint8_t         pad1[12];
  } xcb_xkb_indicator_state_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_XKB_INDICATOR_STATE_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- xkbType :: NOT YET DOCUMENTED.

- time :: NOT YET DOCUMENTED.

- deviceID :: NOT YET DOCUMENTED.

- state :: NOT YET DOCUMENTED.

- stateChanged :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xkb.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
