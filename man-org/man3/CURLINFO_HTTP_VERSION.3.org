#+TITLE: Manpages - CURLINFO_HTTP_VERSION.3
#+DESCRIPTION: Linux manpage for CURLINFO_HTTP_VERSION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_HTTP_VERSION - get the http version used in the connection

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_HTTP_VERSION, long
*p);

* DESCRIPTION
Pass a pointer to a long to receive the version used in the last http
connection. The returned value will be CURL_HTTP_VERSION_1_0,
CURL_HTTP_VERSION_1_1, CURL_HTTP_VERSION_2_0, CURL_HTTP_VERSION_3 or 0
if the version cannot be determined.

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    res = curl_easy_perform(curl);
    if(res == CURLE_OK) {
      long http_version;
      curl_easy_getinfo(curl, CURLINFO_HTTP_VERSION, &http_version);
    }
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.50.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLINFO_RESPONSE_CODE*(3), *curl_easy_getinfo*(3),
*curl_easy_setopt*(3),
