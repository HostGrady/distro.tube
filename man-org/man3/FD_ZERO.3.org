#+TITLE: Manpages - FD_ZERO.3
#+DESCRIPTION: Linux manpage for FD_ZERO.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about FD_ZERO.3 is found in manpage for: [[../man2/select.2][man2/select.2]]