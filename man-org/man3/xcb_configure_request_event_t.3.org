#+TITLE: Manpages - xcb_configure_request_event_t.3
#+DESCRIPTION: Linux manpage for xcb_configure_request_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_configure_request_event_t -

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_configure_request_event_t {
      uint8_t      response_type;
      uint8_t      stack_mode;
      uint16_t     sequence;
      xcb_window_t parent;
      xcb_window_t window;
      xcb_window_t sibling;
      int16_t      x;
      int16_t      y;
      uint16_t     width;
      uint16_t     height;
      uint16_t     border_width;
      uint16_t     value_mask;
  } xcb_configure_request_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_CONFIGURE_REQUEST/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- stack_mode :: NOT YET DOCUMENTED.

- parent :: NOT YET DOCUMENTED.

- window :: NOT YET DOCUMENTED.

- sibling :: NOT YET DOCUMENTED.

24. NOT YET DOCUMENTED.

25. NOT YET DOCUMENTED.

- width :: NOT YET DOCUMENTED.

- height :: NOT YET DOCUMENTED.

- border_width :: NOT YET DOCUMENTED.

- value_mask :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
