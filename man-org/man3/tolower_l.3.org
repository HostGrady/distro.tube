#+TITLE: Manpages - tolower_l.3
#+DESCRIPTION: Linux manpage for tolower_l.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about tolower_l.3 is found in manpage for: [[../man3/toupper.3][man3/toupper.3]]