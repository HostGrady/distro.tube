#+TITLE: Manpages - XmbufGetWindowAttributes.3
#+DESCRIPTION: Linux manpage for XmbufGetWindowAttributes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XmbufGetWindowAttributes.3 is found in manpage for: [[../man3/Xmbuf.3][man3/Xmbuf.3]]