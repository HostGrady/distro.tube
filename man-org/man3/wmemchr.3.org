#+TITLE: Manpages - wmemchr.3
#+DESCRIPTION: Linux manpage for wmemchr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wmemchr - search a wide character in a wide-character array

* SYNOPSIS
#+begin_example
  #include <wchar.h>

  wchar_t *wmemchr(const wchar_t *s, wchar_t c, size_t n);
#+end_example

* DESCRIPTION
The *wmemchr*() function is the wide-character equivalent of the
*memchr*(3) function. It searches the /n/ wide characters starting at
/s/ for the first occurrence of the wide character /c/.

* RETURN VALUE
The *wmemchr*() function returns a pointer to the first occurrence of
/c/ among the /n/ wide characters starting at /s/, or NULL if /c/ does
not occur among these.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface   | Attribute     | Value   |
| *wmemchr*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99.

* SEE ALSO
*memchr*(3), *wcschr*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
