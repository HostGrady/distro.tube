#+TITLE: Manpages - FcAtomicNewFile.3
#+DESCRIPTION: Linux manpage for FcAtomicNewFile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcAtomicNewFile - return new temporary file name

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 * FcAtomicNewFile (FcAtomic */atomic/*);*

* DESCRIPTION
Returns the filename for writing a new version of the file referenced by
/atomic/.
