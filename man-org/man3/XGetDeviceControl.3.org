#+TITLE: Manpages - XGetDeviceControl.3
#+DESCRIPTION: Linux manpage for XGetDeviceControl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGetDeviceControl, XChangeDeviceControl - query and change input device
controls

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  XDeviceControl *XGetDeviceControl( Display *display,
                                     XDevice *device,
                                     int *controlType);
#+end_example

#+begin_example
  int XChangeDeviceControl( Display *display,
                            XDevice *device,
                            int controlType,
                            XDeviceControl *control);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         Specifies the device whose control is to be interrogated
         or modified.
#+end_example

#+begin_example
  controlType
         Specifies the type of control to be interrogated or
         changed.
#+end_example

#+begin_example
  control
         Specifies the address of an XDeviceControl structure
         that contains the new values for the Device.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    These requests are provided to manipulate those input devices
    that support device control. A BadMatch error will be generated
    if the requested device does not support any device controls.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    Valid device control types that can be used with these requests
    include the following:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    DEVICE_RESOLUTION: Queries or changes the resolution of
    valuators on input devices.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XGetDeviceControl request returns a pointer to an
    XDeviceControl structure.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XGetDeviceControl can generate a BadDevice or BadMatch error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XChangeDeviceControl request modifies the values of one
    control on the specified device. The control is identified by
    the id field of the XDeviceControl structure that is passed
    with the request.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XChangeDeviceControl can generate a BadDevice, BadMatch, or
    BadValue error.
  #+end_example
#+end_quote

* STRUCTURES

#+begin_quote
  #+begin_example
    Each control is described by a structure specific to that control.
    These structures are defined in the file XInput.h.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XDeviceControl is a generic structure that contains two fields
    that are at the beginning of each class of control:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        XID class;
        int length;
    } XDeviceControl;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XDeviceResolutionState structure defines the information
    that is returned for device resolution for devices with
    valuators.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        XID control;
        int length;
        int num_valuators;
        int* resolutions;
        int* min_resolutions;
        int* max_resolutions;
    } XDeviceResolutionState;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XDeviceResolutionControl structure defines the attributes
    that can be controlled for keyboard Devices.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        XID control;
        int length;
        int first_valuator;
        int num_valuators;
        int* resolutions;
    } XDeviceResolutionControl;
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The specified device
           does not exist or has not been opened by this client via
           XOpenInputDevice. This error may also occur if some
           other client has caused the specified device to become
           the X keyboard or X pointer device via the
           XChangeKeyboardDevice or XChangePointerDevice requests.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadMatch
           This error may occur if an XGetDeviceControl request was
           made specifying a device that has no controls or an
           XChangeDeviceControl request was made with an
           XDeviceControl structure that contains an invalid Device
           type. It may also occur if an invalid combination of
           mask bits is specified ( DvKey but no DvAutoRepeatMode
           for keyboard Devices), or if an invalid KeySym is
           specified for a string Device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadValue
           Some numeric value falls outside the range of values
           accepted by the XChangeDeviceControl request. Unless a
           specific range is specified for an argument, the full
           range defined by the arguments type is accepted. Any
           argument defined as a set of alternatives can generate
           this error.
  #+end_example
#+end_quote
