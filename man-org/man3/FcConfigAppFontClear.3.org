#+TITLE: Manpages - FcConfigAppFontClear.3
#+DESCRIPTION: Linux manpage for FcConfigAppFontClear.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigAppFontClear - Remove all app fonts from font database

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcConfigAppFontClear (FcConfig */config/*);*

* DESCRIPTION
Clears the set of application-specific fonts. If /config/ is NULL, the
current configuration is used.
