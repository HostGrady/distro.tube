#+TITLE: Manpages - libssh2_channel_write_stderr.3
#+DESCRIPTION: Linux manpage for libssh2_channel_write_stderr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_channel_write_stderr - convenience macro for
/libssh2_channel_write_ex(3)/

* SYNOPSIS
#+begin_example
  #include <libssh2.h>

  ssize_t libssh2_channel_write_stderr(LIBSSH2_CHANNEL *channel, const char *buf, size_t buflen);
#+end_example

* DESCRIPTION
This is a macro defined in a public libssh2 header file that is using
the underlying function /libssh2_channel_write_ex(3)/.

* RETURN VALUE
See /libssh2_channel_write_ex(3)/

* ERRORS
See /libssh2_channel_write_ex(3)/

* SEE ALSO
*libssh2_channel_write_ex(3)*
