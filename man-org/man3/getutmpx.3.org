#+TITLE: Manpages - getutmpx.3
#+DESCRIPTION: Linux manpage for getutmpx.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getutmpx.3 is found in manpage for: [[../man3/getutmp.3][man3/getutmp.3]]