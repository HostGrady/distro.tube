#+TITLE: Manpages - humanize_number.3bsd
#+DESCRIPTION: Linux manpage for humanize_number.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

function formats the signed 64 bit quantity given in

into

A space and then

is appended to the end.

must be at least

bytes long.

If the formatted number (including

would be too long to fit into

then divide

by 1024 until it will. In this case, prefix

with the appropriate SI designator.

The prefixes are:

must be at least 4 plus the length of

in order to ensure a useful result is generated into

To use a specific prefix, specify this as

(Multiplier = 1024 ^ scale). This can not be combined with any of the

flags below.

The following flags may be passed in

Format the buffer using the lowest multiplier possible.

Return the prefix index number (the number of times

must be divided to fit) instead of formatting it to the buffer.

The following flags may be passed in

If the final result is less than 10, display it using one digit.

Do not put a space between

and the prefix.

Use 'B' (bytes) as prefix if the original result does not have a prefix.

Divide

with 1000 instead of 1024.

The

function parses the string representing an integral value given in

and stores the numerical value in the integer pointed to by

The provided string may hold one of the suffixes, which will be
interpreted and used to scale up its accompanying numerical value.

returns the number of characters stored in

(excluding the terminating NUL) upon success, or -1 upon failure. If

is specified, the prefix index number will be returned instead.

returns 0 if the string was parsed correctly. A -1 is returned to
indicate failure and an error code is stored in

will fail and no number will be stored in

if:

The string in

was empty or carried an unknown suffix.

The string in

represented a number that does not fit in

first appeared in

first appeared in
