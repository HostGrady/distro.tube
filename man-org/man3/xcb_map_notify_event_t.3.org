#+TITLE: Manpages - xcb_map_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_map_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_map_notify_event_t - a window was mapped

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_map_notify_event_t {
      uint8_t      response_type;
      uint8_t      pad0;
      uint16_t     sequence;
      xcb_window_t event;
      xcb_window_t window;
      uint8_t      override_redirect;
      uint8_t      pad1[3];
  } xcb_map_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_MAP_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- event :: The window which was mapped or its parent, depending on
  whether /StructureNotify/ or /SubstructureNotify/ was selected.

- window :: The window that was mapped.

- override_redirect :: Window managers should ignore this window if
  /override_redirect/ is 1.

* DESCRIPTION
* SEE ALSO
*xcb_generic_event_t*(3), *xcb_map_window*(3)

* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
