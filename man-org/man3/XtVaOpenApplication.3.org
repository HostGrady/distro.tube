#+TITLE: Manpages - XtVaOpenApplication.3
#+DESCRIPTION: Linux manpage for XtVaOpenApplication.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtVaOpenApplication.3 is found in manpage for: [[../man3/XtOpenApplication.3][man3/XtOpenApplication.3]]