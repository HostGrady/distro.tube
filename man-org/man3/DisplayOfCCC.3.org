#+TITLE: Manpages - DisplayOfCCC.3
#+DESCRIPTION: Linux manpage for DisplayOfCCC.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
DisplayOfCCC, VisualOfCCC, ScreenNumberOfCCC, ScreenWhitePointOfCCC,
ClientWhitePointOfCCC - Color Conversion Context macros

* SYNTAX
Display *DisplayOfCCC ( XcmsCCC /ccc/ );

Visual *VisualOfCCC ( XcmsCCC /ccc/ );

int ScreenNumberOfCCC ( XcmsCCC /ccc/ );

XcmsColor *ScreenWhitePointOfCCC ( XcmsCCC /ccc/ );

XcmsColor *ClientWhitePointOfCCC ( XcmsCCC /ccc/ );

* ARGUMENTS

300. Specifies the CCC.

* DESCRIPTION
The *DisplayOfCCC* macro returns the display associated with the
specified CCC.

The *VisualOfCCC* macro returns the visual associated with the specified
CCC.

The *ScreenNumberOfCCC* macro returns the number of the screen
associated with the specified CCC.

The *ScreenWhitePointOfCCC* macro returns the screen white point of the
screen associated with the specified CCC.

The *ClientWhitePointOfCCC* macro returns the client white point of the
screen associated with the specified CCC.

* SEE ALSO
XcmsCCCOfColormap(3), XcmsConvertColors(3), XcmsCreateCCC(3),
XcmsDefaultCCC(3), XcmsSetWhitePoint(3)\\
/Xlib - C Language X Interface/
