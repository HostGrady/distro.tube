#+TITLE: Manpages - std_binary_negate.3
#+DESCRIPTION: Linux manpage for std_binary_negate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::binary_negate< _Predicate > - One of the *negation functors*.

* SYNOPSIS
\\

=#include <stl_function.h>=

Inherits *std::binary_function< _Predicate::first_argument_type,
_Predicate::second_argument_type, bool >*.

** Public Types
typedef _Predicate::first_argument_type *first_argument_type*\\
=first_argument_type= is the type of the first argument

typedef bool *result_type*\\
=result_type= is the return type

typedef _Predicate::second_argument_type *second_argument_type*\\
=second_argument_type= is the type of the second argument

** Public Member Functions
constexpr *binary_negate* (const _Predicate &__x)\\

constexpr bool *operator()* (const typename
_Predicate::first_argument_type &__x, const typename
_Predicate::second_argument_type &__y) const\\

** Protected Attributes
_Predicate *_M_pred*\\

* Detailed Description
** "template<typename _Predicate>
\\
class std::binary_negate< _Predicate >"One of the *negation functors*.

Definition at line *1029* of file *stl_function.h*.

* Member Typedef Documentation
** typedef _Predicate::first_argument_type *std::binary_function*<
_Predicate::first_argument_type , _Predicate::second_argument_type ,
bool >::*first_argument_type*= [inherited]=
=first_argument_type= is the type of the first argument

Definition at line *121* of file *stl_function.h*.

** typedef bool *std::binary_function*< _Predicate::first_argument_type
, _Predicate::second_argument_type , bool >::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *127* of file *stl_function.h*.

** typedef _Predicate::second_argument_type *std::binary_function*<
_Predicate::first_argument_type , _Predicate::second_argument_type ,
bool >::*second_argument_type*= [inherited]=
=second_argument_type= is the type of the second argument

Definition at line *124* of file *stl_function.h*.

* Constructor & Destructor Documentation
** template<typename _Predicate > constexpr *std::binary_negate*<
_Predicate >::*binary_negate* (const _Predicate & __x)= [inline]=,
= [explicit]=, = [constexpr]=
Definition at line *1039* of file *stl_function.h*.

* Member Function Documentation
** template<typename _Predicate > constexpr bool *std::binary_negate*<
_Predicate >::operator() (const typename _Predicate::first_argument_type
& __x, const typename _Predicate::second_argument_type & __y)
const= [inline]=, = [constexpr]=
Definition at line *1043* of file *stl_function.h*.

* Member Data Documentation
** template<typename _Predicate > _Predicate *std::binary_negate*<
_Predicate >::_M_pred= [protected]=
Definition at line *1034* of file *stl_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
