#+TITLE: Manpages - uuid_is_null.3
#+DESCRIPTION: Linux manpage for uuid_is_null.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
uuid_is_null - compare the value of the UUID to the NULL value

* SYNOPSIS
*#include <uuid.h>*

*int uuid_is_null(uuid_t */uu/*);*

* DESCRIPTION
The *uuid_is_null*/() function compares the value of the supplied UUID
variable uu to the NULL value. If the value is equal to the NULL UUID, 1
is returned, otherwise 0 is returned./

* AUTHORS
Theodore Y. Ts'o

* SEE ALSO
*uuid*/(3),/ *uuid_clear*/(3),/ *uuid_compare*/(3),/ *uuid_copy*/(3),/
*uuid_generate*/(3),/ *uuid_time*/(3),/ *uuid_parse*/(3),/
*uuid_unparse*/(3)/

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *libuuid*/ library is part of the util-linux package since version
2.15.1. It can be downloaded from / /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
