#+TITLE: Manpages - HTTP_Headers_ETag.3pm
#+DESCRIPTION: Linux manpage for HTTP_Headers_ETag.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
HTTP::Headers::ETag

* VERSION
version 6.33

* AUTHOR
Gisle Aas <gisle@activestate.com>

* COPYRIGHT AND LICENSE
This software is copyright (c) 1994 by Gisle Aas.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
