#+TITLE: Manpages - Alien_Build_Plugin_Extract_ArchiveTar.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Extract_ArchiveTar.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Extract::ArchiveTar - Plugin to extract a tarball
using Archive::Tar

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Extract::ArchiveTar => ( format => tar.gz, );

* DESCRIPTION
Note: in most case you will want to use
Alien::Build::Plugin::Extract::Negotiate instead. It picks the
appropriate Extract plugin based on your platform and environment. In
some cases you may need to use this plugin directly instead.

This plugin extracts from an archive in tarball format (optionally
compressed by either gzip or bzip2) using Archive::Tar.

* PROPERTIES
** format
Gives a hint as to the expected format. This helps make sure the
prerequisites are set correctly, since compressed archives require extra
Perl modules to be installed.

* METHODS
** handles
Alien::Build::Plugin::Extract::ArchiveTar->handles($ext);
$plugin->handles($ext);

Returns true if the plugin is able to handle the archive of the given
format.

** available
Alien::Build::Plugin::Extract::ArchiveTar->available($ext);

Returns true if the plugin has what it needs right now to extract from
the given format

* SEE ALSO
Alien::Build::Plugin::Extract::Negotiate, Alien::Build, alienfile,
Alien::Build::MM, Alien

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
