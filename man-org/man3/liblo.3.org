#+TITLE: Manpages - liblo.3
#+DESCRIPTION: Linux manpage for liblo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
liblo - Defines the high-level API functions necessary to implement OSC
support. Should be adequate for most applications, but if you require
lower level control you can use the functions defined in
*lo_lowlevel.h*.

* SYNOPSIS
\\

** Data Structures
struct *lo_timetag*\\
A structure to store OSC TimeTag values.

union *lo_arg*\\
Union used to read values from incoming messages.

** Macros
#define *LO_TT_IMMEDIATE* ((*lo_timetag*){0U,1U})\\
A timetag constant representing 'now'.

** Enumerations
enum *lo_element_type* { *LO_ELEMENT_MESSAGE* = 1, *LO_ELEMENT_BUNDLE* =
2 }\\
An enumeration of bundle element types liblo can handle.

enum *lo_type* { *LO_INT32* = 'i', *LO_FLOAT* = 'f', *LO_STRING* = 's',
*LO_BLOB* = 'b', *LO_INT64* = 'h', *LO_TIMETAG* = 't', *LO_DOUBLE* =
'd', *LO_SYMBOL* = 'S', *LO_CHAR* = 'c', *LO_MIDI* = 'm', *LO_TRUE* =
'T', *LO_FALSE* = 'F', *LO_NIL* = 'N', *LO_INFINITUM* = 'I' }\\
An enumeration of the OSC types liblo can send and receive.

** Functions
*lo_address* *lo_address_new* (const char *host, const char *port)\\
Declare an OSC destination, given IP address and port number. Same as
*lo_address_new_with_proto()*, but using UDP.

*lo_address* *lo_address_new_with_proto* (int proto, const char *host,
const char *port)\\
Declare an OSC destination, given IP address and port number, specifying
protocol.

*lo_address* *lo_address_new_from_url* (const char *url)\\
Create a lo_address object from an OSC URL.

void *lo_address_free* (*lo_address* t)\\
Free the memory used by the lo_address object.

void *lo_address_set_ttl* (*lo_address* t, int ttl)\\
Set the Time-to-Live value for a given target address.

int *lo_address_get_ttl* (*lo_address* t)\\
Get the Time-to-Live value for a given target address.

int *lo_send* (*lo_address* targ, const char *path, const char
*type,...)\\
Send a OSC formatted message to the address specified.

int *lo_send_from* (*lo_address* targ, *lo_server* from, *lo_timetag*
ts, const char *path, const char *type,...)\\
Send a OSC formatted message to the address specified, from the same
socket as the specified server.

int *lo_send_timestamped* (*lo_address* targ, *lo_timetag* ts, const
char *path, const char *type,...)\\
Send a OSC formatted message to the address specified, scheduled to be
dispatch at some time in the future.

int *lo_address_errno* (*lo_address* a)\\
Return the error number from the last failed *lo_send()* or
*lo_address_new()* call.

const char * *lo_address_errstr* (*lo_address* a)\\
Return the error string from the last failed *lo_send()* or
*lo_address_new()* call.

*lo_blob* *lo_blob_new* (int32_t size, const void *data)\\
Create a new OSC blob type.

void *lo_blob_free* (*lo_blob* b)\\
Free the memory taken by a blob.

uint32_t *lo_blob_datasize* (*lo_blob* b)\\
Return the amount of valid data in a lo_blob object.

void * *lo_blob_dataptr* (*lo_blob* b)\\
Return a pointer to the start of the blob data to allow contents to be
changed.

void *lo_version* (char *verstr, int verstr_size, int *major, int
*minor, char *extra, int extra_size, int *lt_major, int *lt_minor, int
*lt_bug)\\
Get information on the version of liblo current in use.

* Detailed Description
Defines the high-level API functions necessary to implement OSC support.
Should be adequate for most applications, but if you require lower level
control you can use the functions defined in *lo_lowlevel.h*.

* Macro Definition Documentation
** #define LO_TT_IMMEDIATE ((*lo_timetag*){0U,1U})
A timetag constant representing 'now'.

Definition at line 151 of file lo_osc_types.h.

* Enumeration Type Documentation
** enum *lo_element_type*
An enumeration of bundle element types liblo can handle. The element of
a bundle can either be a message or an other bundle.

*Enumerator*

- /LO_ELEMENT_MESSAGE / :: bundle element is a message

- /LO_ELEMENT_BUNDLE / :: bundle element is a bundle

Definition at line 48 of file lo_osc_types.h.

** enum *lo_type*
An enumeration of the OSC types liblo can send and receive. The value of
the enumeration is the typechar used to tag messages and to specify
arguments with *lo_send()*/. /

*Enumerator*

- /LO_INT32 / :: 32 bit signed integer.

- /LO_FLOAT / :: 32 bit IEEE-754 float.

- /LO_STRING / :: Standard C, NULL terminated string.

- /LO_BLOB / :: OSC binary blob type. Accessed using the lo_blob_*()
  functions.

- /LO_INT64 / :: 64 bit signed integer.

- /LO_TIMETAG / :: OSC TimeTag type, represented by the *lo_timetag*/
  structure. /

- /LO_DOUBLE / :: 64 bit IEEE-754 double.

- /LO_SYMBOL / :: Standard C, NULL terminated, string. Used in systems
  which distinguish strings and symbols.

- /LO_CHAR / :: Standard C, 8 bit, char variable.

- /LO_MIDI / :: A 4 byte MIDI packet.

- /LO_TRUE / :: Sybol representing the value True.

- /LO_FALSE / :: Sybol representing the value False.

- /LO_NIL / :: Sybol representing the value Nil.

- /LO_INFINITUM / :: Sybol representing the value Infinitum.

Definition at line 61 of file lo_osc_types.h.

* Function Documentation
** int lo_address_errno (*lo_address*/ a)/
Return the error number from the last failed *lo_send()*/ or
/*lo_address_new()*/ call. /

** const char* lo_address_errstr (*lo_address*/ a)/
Return the error string from the last failed *lo_send()*/ or
/*lo_address_new()*/ call. /

** void lo_address_free (*lo_address*/ t)/
Free the memory used by the lo_address object.

** int lo_address_get_ttl (*lo_address*/ t)/
Get the Time-to-Live value for a given target address.

*Parameters*

#+begin_quote
  /t An OSC address. /
#+end_quote

*Returns*

#+begin_quote
  An integer specifying the scope of a multicast UDP message.
#+end_quote

** *lo_address*/ lo_address_new (const char * host, const char * port)/
Declare an OSC destination, given IP address and port number. Same as
*lo_address_new_with_proto()*/, but using UDP. /

*Parameters*

#+begin_quote
  /host An IP address or number, or NULL for the local machine. /\\
  /port a decimal port number or service name./
#+end_quote

The lo_address object may be used as the target of OSC messages.

Note: if you wish to receive replies from the target of this address,
you must first create a lo_server_thread or lo_server object which will
receive the replies. The last lo_server(_thread) object craeted will be
the receiver.

** *lo_address*/ lo_address_new_from_url (const char * url)/
Create a lo_address object from an OSC URL. example:
='osc.udp://localhost:4444/my/path/'=/ /

** *lo_address*/ lo_address_new_with_proto (int proto, const char *
host, const char * port)/
Declare an OSC destination, given IP address and port number, specifying
protocol.

*Parameters*

#+begin_quote
  /proto The protocol to use, must be one of LO_UDP, LO_TCP or LO_UNIX.
  /\\
  /host An IP address or number, or NULL for the local machine. /\\
  /port a decimal port number or service name./
#+end_quote

The lo_address object may be used as the target of OSC messages.

Note: if you wish to receive replies from the target of this address,
you must first create a lo_server_thread or lo_server object which will
receive the replies. The last lo_server(_thread) object created will be
the receiver.

** void lo_address_set_ttl (*lo_address*/ t, int ttl)/
Set the Time-to-Live value for a given target address. This is required
for sending multicast UDP messages. A value of 1 (the usual case) keeps
the message within the subnet, while 255 means a global, unrestricted
scope.

*Parameters*

#+begin_quote
  /t An OSC address. /\\
  /ttl An integer specifying the scope of a multicast UDP message. /
#+end_quote

** void* lo_blob_dataptr (*lo_blob*/ b)/
Return a pointer to the start of the blob data to allow contents to be
changed.

** uint32_t lo_blob_datasize (*lo_blob*/ b)/
Return the amount of valid data in a lo_blob object. If you want to know
the storage size, use *lo_arg_size()*/. /

** void lo_blob_free (*lo_blob*/ b)/
Free the memory taken by a blob.

** *lo_blob*/ lo_blob_new (int32_t size, const void * data)/
Create a new OSC blob type.

*Parameters*

#+begin_quote
  /size The amount of space to allocate in the blob structure. /\\
  /data The data that will be used to initialise the blob, should be
  size bytes long. /
#+end_quote

** int lo_send (*lo_address*/ targ, const char * path, const char *
type, ...)/
Send a OSC formatted message to the address specified.

*Parameters*

#+begin_quote
  /targ The target OSC address /\\
  /path The OSC path the message will be delivered to /\\
  /type The types of the data items in the message, types are defined in
  /*lo_osc_types.h*/ /\\
  /... The data values to be transmitted. The types of the arguments
  passed here must agree with the types specified in the type
  parameter./
#+end_quote

example:

#+begin_example
  lo_send(t, "/foo/bar", "ff", 0.1f, 23.0f);
#+end_example

*Returns*

#+begin_quote
  -1 on failure.
#+end_quote

** int lo_send_from (*lo_address*/ targ, /*lo_server*/ from,
/*lo_timetag*/ ts, const char * path, const char * type, ...)/
Send a OSC formatted message to the address specified, from the same
socket as the specified server.

*Parameters*

#+begin_quote
  /targ The target OSC address /\\
  /from The server to send message from (can be NULL to use new socket)
  /\\
  /ts The OSC timetag timestamp at which the message will be processed
  (can be LO_TT_IMMEDIATE if you don't want to attach a timetag) /\\
  /path The OSC path the message will be delivered to /\\
  /type The types of the data items in the message, types are defined in
  /*lo_osc_types.h*/ /\\
  /... The data values to be transmitted. The types of the arguments
  passed here must agree with the types specified in the type
  parameter./
#+end_quote

example:

#+begin_example
  serv = lo_server_new(NULL, err);
  lo_server_add_method(serv, "/reply", "ss", reply_handler, NULL);
  lo_send_from(t, serv, LO_TT_IMMEDIATE, "/foo/bar", "ff", 0.1f, 23.0f);
#+end_example

*Returns*

#+begin_quote
  on success, the number of bytes sent, or -1 on failure.
#+end_quote

** int lo_send_timestamped (*lo_address*/ targ, /*lo_timetag*/ ts, const
char * path, const char * type, ...)/
Send a OSC formatted message to the address specified, scheduled to be
dispatch at some time in the future.

*Parameters*

#+begin_quote
  /targ The target OSC address /\\
  /ts The OSC timetag timestamp at which the message will be processed
  /\\
  /path The OSC path the message will be delivered to /\\
  /type The types of the data items in the message, types are defined in
  /*lo_osc_types.h*/ /\\
  /... The data values to be transmitted. The types of the arguments
  passed here must agree with the types specified in the type
  parameter./
#+end_quote

example:

#+begin_example
  lo_timetag now;<br>
  lo_timetag_now(&now);<br>
  lo_send_timestamped(t, now, "/foo/bar", "ff", 0.1f, 23.0f);
#+end_example

*Returns*

#+begin_quote
  on success, the number of bytes sent, or -1 on failure.
#+end_quote

** void lo_version (char * verstr, int verstr_size, int * major, int *
minor, char * extra, int extra_size, int * lt_major, int * lt_minor, int
* lt_bug)
Get information on the version of liblo current in use. All parameters
are optional and can be given the value of 0 if that information is not
desired. For example, to get just the version as a string, call
lo_version(str, size, 0, 0, 0, 0, 0, 0, 0);

The 'lt' fields, called the ABI version, corresponds to libtool's
versioning system for binary interface compatibility, and is not related
to the library version number. This information is usually encoded in
the filename of the shared library.

Typically the string returned in 'verstr' should correspond with
$major.$minor$extra, e.g., '0.28rc'. If no 'extra' information is
present, e.g., '0.28', extra will given the null string.

*Parameters*

#+begin_quote
  /verstr A buffer to receive a string describing the library version.
  /\\
  /verstr_size Size of the buffer pointed to by string. /\\
  /major Location to receive the library major version. /\\
  /minor Location to receive the library minor version. /\\
  /extra Location to receive the library version extra string. /\\
  /extra_size Size of the buffer pointed to by extra. /\\
  /lt_major Location to receive the ABI major version. /\\
  /lt_minor Location to receive the ABI minor version. /\\
  /lt_bug Location to receive the ABI 'bugfix' version. /
#+end_quote

Referenced by lo::version().

* Author
Generated automatically by Doxygen for liblo from the source code.
