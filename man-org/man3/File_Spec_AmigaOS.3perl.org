#+TITLE: Manpages - File_Spec_AmigaOS.3perl
#+DESCRIPTION: Linux manpage for File_Spec_AmigaOS.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
File::Spec::AmigaOS - File::Spec for AmigaOS

* SYNOPSIS
require File::Spec::AmigaOS; # Done automatically by File::Spec # if
needed

* DESCRIPTION
Methods for manipulating file specifications.

* METHODS
- tmpdir :: Returns =$ENV={TMPDIR} or if that is unset, /t.

- file_name_is_absolute :: Returns true if there's a colon in the file
  name, or if it begins with a slash.

All the other methods are from File::Spec::Unix.
