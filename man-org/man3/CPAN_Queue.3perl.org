#+TITLE: Manpages - CPAN_Queue.3perl
#+DESCRIPTION: Linux manpage for CPAN_Queue.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
CPAN::Queue - internal queue support for CPAN.pm

* LICENSE
This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
