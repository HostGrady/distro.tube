#+TITLE: Manpages - XDirectionalDependentDrawing.3
#+DESCRIPTION: Linux manpage for XDirectionalDependentDrawing.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XDirectionalDependentDrawing.3 is found in manpage for: [[../man3/XFontsOfFontSet.3][man3/XFontsOfFontSet.3]]