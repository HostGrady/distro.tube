#+TITLE: Manpages - pcap_strerror.3pcap
#+DESCRIPTION: Linux manpage for pcap_strerror.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_strerror - convert an errno value to a string

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  const char *pcap_strerror(int error);
#+end_example

* DESCRIPTION
*pcap_strerror*() is provided in case *strerror*(3) isn't available. It
returns an error message string corresponding to /error/.

* SEE ALSO
*pcap*(3PCAP)
