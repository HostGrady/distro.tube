#+TITLE: Manpages - FcPatternFindIter.3
#+DESCRIPTION: Linux manpage for FcPatternFindIter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternFindIter - Set the iterator to point to the object in the
pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcPatternFindIter (const FcPattern */p/*, FcPatternIter
**/iter/*, const char **/object/*);*

* DESCRIPTION
Set /iter/ to point to /object/ in /p/ if any and returns FcTrue.
returns FcFalse otherwise.

* SINCE
version 2.13.1
