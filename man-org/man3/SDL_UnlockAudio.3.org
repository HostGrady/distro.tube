#+TITLE: Manpages - SDL_UnlockAudio.3
#+DESCRIPTION: Linux manpage for SDL_UnlockAudio.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_UnlockAudio - Unlock the callback function

* SYNOPSIS
*#include "SDL.h"*

*void SDL_UnlockAudio*(*void*)

* DESCRIPTION
Unlocks a previous *SDL_LockAudio* call.

* SEE ALSO
*SDL_OpenAudio*
