#+TITLE: Manpages - CPAN_Meta_Feature.3perl
#+DESCRIPTION: Linux manpage for CPAN_Meta_Feature.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
CPAN::Meta::Feature - an optional feature provided by a CPAN
distribution

* VERSION
version 2.150010

* DESCRIPTION
A CPAN::Meta::Feature object describes an optional feature offered by a
CPAN distribution and specified in the distribution's /META.json/ (or
/META.yml/) file.

For the most part, this class will only be used when operating on the
result of the =feature= or =features= methods on a CPAN::Meta object.

* METHODS
** new
my $feature = CPAN::Meta::Feature->new( $identifier => \%spec );

This returns a new Feature object. The =%spec= argument to the
constructor should be the same as the value of the =optional_feature=
entry in the distmeta. It must contain entries for =description= and
=prereqs=.

** identifier
This method returns the feature's identifier.

** description
This method returns the feature's long description.

** prereqs
This method returns the feature's prerequisites as a CPAN::Meta::Prereqs
object.

* BUGS
Please report any bugs or feature using the CPAN Request Tracker. Bugs
can be submitted through the web interface at
<http://rt.cpan.org/Dist/Display.html?Queue=CPAN-Meta>

When submitting a bug or request, please include a test-file or a patch
to an existing test-file that illustrates the bug or desired feature.

* AUTHORS

- David Golden <dagolden@cpan.org>

- Ricardo Signes <rjbs@cpan.org>

- Adam Kennedy <adamk@cpan.org>

* COPYRIGHT AND LICENSE
This software is copyright (c) 2010 by David Golden, Ricardo Signes,
Adam Kennedy and Contributors.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
