#+TITLE: Manpages - sd_bus_reply_method_return.3
#+DESCRIPTION: Linux manpage for sd_bus_reply_method_return.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_reply_method_return, sd_bus_reply_method_returnv - Reply to a
D-Bus method call

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_reply_method_return(sd_bus_message **/call/*, const char
**/types/*, ...);*

*int sd_bus_reply_method_returnv(sd_bus_message **/call/*, const char
**/types/*, va_list */ap/*);*

* DESCRIPTION
*sd_bus_reply_method_return()* sends a reply to the /call/ message. The
type string /types/ and the arguments that follow it must adhere to the
format described in *sd_bus_message_append*(3). If no reply is expected
to /call/, this function succeeds without sending a reply.

* RETURN VALUE
On success, this function returns a non-negative integer. On failure, it
returns a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  The input parameter /call/ is *NULL*.

  Message /call/ is not a method call message.

  Message /call/ is not attached to a bus.

  Message /m/ is not a method reply message.
#+end_quote

*-EPERM*

#+begin_quote
  Message /call/ has been sealed.
#+end_quote

*-ENOTCONN*

#+begin_quote
  The bus to which message /call/ is attached is not connected.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

In addition, any error returned by *sd_bus_send*(1) may be returned.

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_message_new_method_return*(3)
