#+TITLE: Manpages - std___detail__List_node_base.3
#+DESCRIPTION: Linux manpage for std___detail__List_node_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_List_node_base - Common part of a node in the list.

* SYNOPSIS
\\

=#include <stl_list.h>=

Inherited by *std::_List_node< _Tp >*, and
*std::__detail::_List_node_header*.

** Public Member Functions
void *_M_hook* (*_List_node_base* *const __position) noexcept\\

void *_M_reverse* () noexcept\\

void *_M_transfer* (*_List_node_base* *const __first, *_List_node_base*
*const __last) noexcept\\

void *_M_unhook* () noexcept\\

** Static Public Member Functions
static void *swap* (*_List_node_base* &__x, *_List_node_base* &__y)
noexcept\\

** Public Attributes
*_List_node_base* * *_M_next*\\

*_List_node_base* * *_M_prev*\\

* Detailed Description
Common part of a node in the list.

Definition at line *80* of file *stl_list.h*.

* Member Data Documentation
** *_List_node_base** std::__detail::_List_node_base::_M_next
Definition at line *82* of file *stl_list.h*.

** *_List_node_base** std::__detail::_List_node_base::_M_prev
Definition at line *83* of file *stl_list.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
