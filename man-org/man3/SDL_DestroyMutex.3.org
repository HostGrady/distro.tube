#+TITLE: Manpages - SDL_DestroyMutex.3
#+DESCRIPTION: Linux manpage for SDL_DestroyMutex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_DestroyMutex - Destroy a mutex

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*void SDL_DestroyMutex*(*SDL_mutex *mutex*);

* DESCRIPTION
Destroy a previously /created/ mutex.

* SEE ALSO
*SDL_CreateMutex*
