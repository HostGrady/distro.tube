#+TITLE: Manpages - edata.3
#+DESCRIPTION: Linux manpage for edata.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about edata.3 is found in manpage for: [[../man3/end.3][man3/end.3]]