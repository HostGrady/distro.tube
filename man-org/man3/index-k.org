#+TITLE: Man3 - K
#+DESCRIPTION: Man3 - K
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* K
#+begin_src bash :exports results
readarray -t starts_with_k < <(find . -type f -iname "k*" | sort)

for x in "${starts_with_k[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./keybound.3x.org][keybound.3x]]                   |
| [[file:./keyctl.3.org][keyctl.3]]                      |
| [[file:./keyctl_capabilities.3.org][keyctl_capabilities.3]]         |
| [[file:./keyctl_chown.3.org][keyctl_chown.3]]                |
| [[file:./keyctl_clear.3.org][keyctl_clear.3]]                |
| [[file:./keyctl_describe.3.org][keyctl_describe.3]]             |
| [[file:./keyctl_dh_compute.3.org][keyctl_dh_compute.3]]           |
| [[file:./keyctl_get_keyring_ID.3.org][keyctl_get_keyring_ID.3]]       |
| [[file:./keyctl_get_persistent.3.org][keyctl_get_persistent.3]]       |
| [[file:./keyctl_get_security.3.org][keyctl_get_security.3]]         |
| [[file:./keyctl_instantiate.3.org][keyctl_instantiate.3]]          |
| [[file:./keyctl_invalidate.3.org][keyctl_invalidate.3]]           |
| [[file:./keyctl_join_session_keyring.3.org][keyctl_join_session_keyring.3]] |
| [[file:./keyctl_link.3.org][keyctl_link.3]]                 |
| [[file:./keyctl_move.3.org][keyctl_move.3]]                 |
| [[file:./keyctl_pkey_decrypt.3.org][keyctl_pkey_decrypt.3]]         |
| [[file:./keyctl_pkey_encrypt.3.org][keyctl_pkey_encrypt.3]]         |
| [[file:./keyctl_pkey_query.3.org][keyctl_pkey_query.3]]           |
| [[file:./keyctl_pkey_sign.3.org][keyctl_pkey_sign.3]]            |
| [[file:./keyctl_pkey_verify.3.org][keyctl_pkey_verify.3]]          |
| [[file:./keyctl_read.3.org][keyctl_read.3]]                 |
| [[file:./keyctl_restrict_keyring.3.org][keyctl_restrict_keyring.3]]     |
| [[file:./keyctl_revoke.3.org][keyctl_revoke.3]]               |
| [[file:./keyctl_search.3.org][keyctl_search.3]]               |
| [[file:./keyctl_session_to_parent.3.org][keyctl_session_to_parent.3]]    |
| [[file:./keyctl_setperm.3.org][keyctl_setperm.3]]              |
| [[file:./keyctl_set_reqkey_keyring.3.org][keyctl_set_reqkey_keyring.3]]   |
| [[file:./keyctl_set_timeout.3.org][keyctl_set_timeout.3]]          |
| [[file:./keyctl_update.3.org][keyctl_update.3]]               |
| [[file:./keyctl_watch_key.3.org][keyctl_watch_key.3]]            |
| [[file:./key_decryptsession.3.org][key_decryptsession.3]]          |
| [[file:./key_defined.3x.org][key_defined.3x]]                |
| [[file:./key_encryptsession.3.org][key_encryptsession.3]]          |
| [[file:./key_gendes.3.org][key_gendes.3]]                  |
| [[file:./keyok.3x.org][keyok.3x]]                      |
| [[file:./key_secretkey_is_set.3.org][key_secretkey_is_set.3]]        |
| [[file:./key_setsecret.3.org][key_setsecret.3]]               |
| [[file:./killpg.3.org][killpg.3]]                      |
| [[file:./klogctl.3.org][klogctl.3]]                     |
