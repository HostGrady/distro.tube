#+TITLE: Manpages - pam_set_item.3
#+DESCRIPTION: Linux manpage for pam_set_item.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_set_item - set and update PAM information

* SYNOPSIS
#+begin_example
  #include <security/pam_modules.h>
#+end_example

*int pam_set_item(pam_handle_t **/pamh/*, int */item_type/*, const void
**/item/*);*

* DESCRIPTION
The *pam_set_item* function allows applications and PAM service modules
to access and to update PAM information of /item_type/. For this a copy
of the object pointed to by the /item/ argument is created. The
following /item_type/s are supported:

PAM_SERVICE

#+begin_quote
  The service name (which identifies that PAM stack that the PAM
  functions will use to authenticate the program).
#+end_quote

PAM_USER

#+begin_quote
  The username of the entity under whose identity service will be given.
  That is, following authentication, /PAM_USER/ identifies the local
  entity that gets to use the service. Note, this value can be mapped
  from something (eg., "anonymous") to something else (eg. "guest119")
  by any module in the PAM stack. As such an application should consult
  the value of /PAM_USER/ after each call to a PAM function.
#+end_quote

PAM_USER_PROMPT

#+begin_quote
  The string used when prompting for a users name. The default value for
  this string is a localized version of "login: ".
#+end_quote

PAM_TTY

#+begin_quote
  The terminal name: prefixed by /dev/ if it is a device file; for
  graphical, X-based, applications the value for this item should be the
  /$DISPLAY/ variable.
#+end_quote

PAM_RUSER

#+begin_quote
  The requesting user name: local name for a locally requesting user or
  a remote user name for a remote requesting user.

  Generally an application or module will attempt to supply the value
  that is most strongly authenticated (a local account before a remote
  one. The level of trust in this value is embodied in the actual
  authentication stack associated with the application, so it is
  ultimately at the discretion of the system administrator.

  /PAM_RUSER@PAM_RHOST/ should always identify the requesting user. In
  some cases, /PAM_RUSER/ may be NULL. In such situations, it is unclear
  who the requesting entity is.
#+end_quote

PAM_RHOST

#+begin_quote
  The requesting hostname (the hostname of the machine from which the
  /PAM_RUSER/ entity is requesting service). That is
  /PAM_RUSER@PAM_RHOST/ does identify the requesting user. In some
  applications, /PAM_RHOST/ may be NULL. In such situations, it is
  unclear where the authentication request is originating from.
#+end_quote

PAM_AUTHTOK

#+begin_quote
  The authentication token (often a password). This token should be
  ignored by all module functions besides *pam_sm_authenticate*(3) and
  *pam_sm_chauthtok*(3). In the former function it is used to pass the
  most recent authentication token from one stacked module to another.
  In the latter function the token is used for another purpose. It
  contains the currently active authentication token.
#+end_quote

PAM_OLDAUTHTOK

#+begin_quote
  The old authentication token. This token should be ignored by all
  module functions except *pam_sm_chauthtok*(3).
#+end_quote

PAM_CONV

#+begin_quote
  The pam_conv structure. See *pam_conv*(3).
#+end_quote

The following additional items are specific to Linux-PAM and should not
be used in portable applications:

PAM_FAIL_DELAY

#+begin_quote
  A function pointer to redirect centrally managed failure delays. See
  *pam_fail_delay*(3).
#+end_quote

PAM_XDISPLAY

#+begin_quote
  The name of the X display. For graphical, X-based applications the
  value for this item should be the /$DISPLAY/ variable. This value may
  be used independently of /PAM_TTY/ for passing the name of the
  display.
#+end_quote

PAM_XAUTHDATA

#+begin_quote
  A pointer to a structure containing the X authentication data required
  to make a connection to the display specified by /PAM_XDISPLAY/, if
  such information is necessary. See *pam_xauth_data*(3).
#+end_quote

PAM_AUTHTOK_TYPE

#+begin_quote
  The default action is for the module to use the following prompts when
  requesting passwords: "New UNIX password: " and "Retype UNIX password:
  ". The example word /UNIX/ can be replaced with this item, by default
  it is empty. This item is used by *pam_get_authtok*(3).
#+end_quote

For all /item_type/s, other than PAM_CONV and PAM_FAIL_DELAY, /item/ is
a pointer to a <NUL> terminated character string. In the case of
PAM_CONV, /item/ points to an initialized /pam_conv/ structure. In the
case of PAM_FAIL_DELAY, /item/ is a function pointer: *void
(*delay_fn)(int retval, unsigned usec_delay, void *appdata_ptr)*

Both, PAM_AUTHTOK and PAM_OLDAUTHTOK, will be reset before returning to
the application. Which means an application is not able to access the
authentication tokens.

* RETURN VALUES
PAM_BAD_ITEM

#+begin_quote
  The application attempted to set an undefined or inaccessible item.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Data was successful updated.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  The /pam_handle_t/ passed as first argument was invalid.
#+end_quote

* SEE ALSO
*pam_get_item*(3), *pam_strerror*(3)
