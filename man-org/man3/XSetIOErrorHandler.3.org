#+TITLE: Manpages - XSetIOErrorHandler.3
#+DESCRIPTION: Linux manpage for XSetIOErrorHandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetIOErrorHandler.3 is found in manpage for: [[../man3/XSetErrorHandler.3][man3/XSetErrorHandler.3]]