#+TITLE: Manpages - putgrent.3
#+DESCRIPTION: Linux manpage for putgrent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
putgrent - write a group database entry to a file

* SYNOPSIS
#+begin_example
  #define _GNU_SOURCE /* See feature_test_macros(7) */
  #include <grp.h>

  int putgrent(const struct group *restrict grp",FILE*restrict"stream);
#+end_example

* DESCRIPTION
The *putgrent*() function is the counterpart for *fgetgrent*(3). The
function writes the content of the provided /struct group/ into the
/stream/. The list of group members must be NULL-terminated or
NULL-initialized.

The /struct group/ is defined as follows:

#+begin_example
  struct group {
      char   *gr_name;      /* group name */
      char   *gr_passwd;    /* group password */
      gid_t   gr_gid;       /* group ID */
      char  **gr_mem;       /* group members */
  };
#+end_example

* RETURN VALUE
The function returns zero on success, and a nonzero value on error.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface    | Attribute     | Value   |
| *putgrent*() | Thread safety | MT-Safe |

* CONFORMING TO
This function is a GNU extension.

* SEE ALSO
*fgetgrent*(3), *getgrent*(3), *group*(5)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
