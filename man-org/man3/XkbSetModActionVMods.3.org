#+TITLE: Manpages - XkbSetModActionVMods.3
#+DESCRIPTION: Linux manpage for XkbSetModActionVMods.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbSetModActionVMods - Sets the vmods1 and vmods2 fields of act using
the vmods format of an Xkb modifier description

* SYNOPSIS
*void XkbSetModActionVMods* *( XkbAction */act/* ,* *unsigned short
*/vmods/* );*

* ARGUMENTS
- /- act/ :: action in which to set vmods

- /- vmods/ :: virtual mods to set

* DESCRIPTION
/XkbSetModActionVMods/ sets the /vmods1/ and /vmods2/ fields of /act/
using the /vmods/ format of an Xkb modifier description.

* NOTES
Despite the fact that the first parameter of these two macros is of type
XkbAction, these macros may be used only with Actions of type
XkbModAction and XkbISOAction.
