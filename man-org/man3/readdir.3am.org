#+TITLE: Manpages - readdir.3am
#+DESCRIPTION: Linux manpage for readdir.3am
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
readdir - directory input parser for gawk

* SYNOPSIS
@load "readdir"

* DESCRIPTION
The /readdir/ extension adds an input parser for directories.

When this extension is in use, instead of skipping directories named on
the command line (or with *getline*), they are read, with each entry
returned as a record.

The record consists of three fields. The first two are the inode number
and the filename, separated by a forward slash character. On systems
where the directory entry contains the file type, the record has a third
field which is a single letter indicating the type of the file: *f* for
file, *d* for directory, *b* for a block device, *c* for a character
device, *p* for a FIFO, *l* for a symbolic link, *s* for a socket.

On systems without the file type information, the extension falls back
to calling /stat/(2), in order to provide the information. Thus the
third field should never be *u*.

By default, if a directory cannot be opened (due to permission problems,
for example), /gawk/ will exit. As with regular files, this situation
can be handled using a *BEGINFILE* rule that checks *ERRNO* and prints
an error or otherwise handles the problem.

* EXAMPLE
#+begin_example
  @load "readdir"
  ...
  BEGIN { FS = "/" }
  { print "file name is", $2 }
#+end_example

* SEE ALSO
/GAWK: Effective AWK Programming/, /filefuncs/(3am), /fnmatch/(3am),
/fork/(3am), /inplace/(3am), /ordchr/(3am), /readfile/(3am),
/revoutput/(3am), /rwarray/(3am), /time/(3am).

/opendir/(3), /readdir/(3), /stat/(2).

* AUTHOR
Arnold Robbins, *arnold@skeeve.com*.

* COPYING PERMISSIONS
Copyright © 2012, 2013, 2018, 2019 Free Software Foundation, Inc.

Permission is granted to make and distribute verbatim copies of this
manual page provided the copyright notice and this permission notice are
preserved on all copies.

Permission is granted to process this file through troff and print the
results, provided the printed document carries copying permission notice
identical to this one except for the removal of this paragraph (this
paragraph not being relevant to the printed manual page).

Permission is granted to copy and distribute modified versions of this
manual page under the conditions for verbatim copying, provided that the
entire resulting derived work is distributed under the terms of a
permission notice identical to this one.

Permission is granted to copy and distribute translations of this manual
page into another language, under the above conditions for modified
versions, except that this permission notice may be stated in a
translation approved by the Foundation.
