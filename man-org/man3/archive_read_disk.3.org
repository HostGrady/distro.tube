#+TITLE: Manpages - archive_read_disk.3
#+DESCRIPTION: Linux manpage for archive_read_disk.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

These functions provide an API for reading information about objects on
disk. In particular, they provide an interface for populating

objects.

Allocates and initializes a

object suitable for reading object information from disk.

Configures various behavior options when reading entries from disk. The
flags field consists of a bitwise OR of one or more of the following
values:

Skip files and directories with the nodump file attribute (file flag)
set. By default, the nodump file attribute is ignored.

Mac OS X specific. Read metadata (ACLs and extended attributes) with

By default, metadata is read using

Do not read Access Control Lists. By default, ACLs are read from disk.

Do not read file attributes (file flags). By default, file attributes
are read from disk. See

or

for more information on file attributes.

Do not traverse mount points. By default, mount points are traversed.

Do not read extended file attributes (xattrs). By default, extended file
attributes are read from disk. See

or

for more information on extended file attributes.

Restore access time of traversed files. By default, access time of
traversed files is not restored.

This sets the mode used for handling symbolic links. The

mode follows all symbolic links. The

mode does not follow any symbolic links. The

mode currently behaves identically to the

mode.

Returns a user or group name given a gid or uid value. By default, these
always return a NULL string.

These allow you to override the functions used for user and group name
lookups. You may also provide a

pointer to a private data structure and a cleanup function for that
data. The cleanup function will be invoked when the

object is destroyed or when new lookup functions are registered.

This convenience function installs a standard set of user and group name
lookup functions. These functions use

and

to convert ids to names, defaulting to NULL if the names cannot be
looked up. These functions also implement a simple memory cache to
reduce the number of calls to

and

Populates a

object with information about a particular file. The

object must have already been created with

and at least one of the source path or path fields must already be set.
(If both are set, the source path will be used.)

Information is read from disk using the path name from the

object. If a file descriptor is provided, some information will be
obtained using that file descriptor, on platforms that support the
appropriate system calls.

If a pointer to a

is provided, information from that structure will be used instead of
reading from the disk where appropriate. This can provide performance
benefits in scenarios where

information has already been read from the disk as a side effect of some
other operation. (For example, directory traversal libraries often
provide this information.)

Where necessary, user and group ids are converted to user and group
names using the currently-registered lookup functions above. This
affects the file ownership fields and ACL values in the

object.

More information about the

object and the overall design of the library can be found in the

overview.

The following illustrates basic usage of the library by showing how to
use it to copy an item on disk into an archive.

void file_to_archive(struct archive *a, const char *name) { char
buff[8192]; size_t bytes_read; struct archive *ard; struct archive_entry
*entry; int fd;

ard = archive_read_disk_new();
archive_read_disk_set_standard_lookup(ard); entry = archive_entry_new();
fd = open(name, O_RDONLY); if (fd < 0) return;
archive_entry_copy_pathname(entry, name);
archive_read_disk_entry_from_file(ard, entry, fd, NULL);
archive_write_header(a, entry); while ((bytes_read = read(fd, buff,
sizeof(buff))) > 0) archive_write_data(a, buff, bytes_read);
archive_write_finish_entry(a); archive_read_free(ard);
archive_entry_free(entry); }

Most functions return

(zero) on success, or one of several negative error codes for errors.
Specific error codes include:

for operations that might succeed if retried,

for unusual conditions that do not prevent further operations, and

for serious errors that make remaining operations impossible.

returns a pointer to a newly-allocated

object or NULL if the allocation failed for any reason.

and

return

pointers to the textual name or NULL if the lookup failed for any
reason. The returned pointer points to internal storage that may be
reused on the next call to either of these functions; callers should
copy the string if they need to continue accessing it.

Detailed error codes and textual descriptions are available from the

and

functions.

The

library first appeared in

The

interface was added to

and first appeared in

The

library was written by

The

user name and group name lookup functions are not the defaults because

and

are sometimes too large for particular applications. The current design
allows the application author to use a more compact implementation when
appropriate.

The full list of metadata read from disk by

is necessarily system-dependent.

The

function reads as much information as it can from disk. Some method
should be provided to limit this so that clients who do not need ACLs,
for instance, can avoid the extra work needed to look up such
information.

This API should provide a set of methods for walking a directory tree.
That would make it a direct parallel of the

API. When such methods are implemented, the

symbolic link mode will make sense.
