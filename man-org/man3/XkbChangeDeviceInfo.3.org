#+TITLE: Manpages - XkbChangeDeviceInfo.3
#+DESCRIPTION: Linux manpage for XkbChangeDeviceInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbChangeDeviceInfo - Update the server's description of a device with
the changes noted in an XkbDeviceChangesRec

* SYNOPSIS
*Bool XkbChangeDeviceInfo* *( Display **/dpy/* ,* *XkbDeviceInfoPtr
*/device_info/* ,* *XkbDeviceChangesPtr */changes/* );*

* ARGUMENTS
- /- dpy/ :: connection to X server

- /- device_info/ :: local copy of device state and configuration

- /- changes/ :: note specifying changes in device_info

* DESCRIPTION
/XkbChangeDeviceInfo/ updates the server's description of the device
specified in /device_info->device_spec/ with the changes specified in
/changes/ and contained in /device_info./ The update is made by an
/XkbSetDeviceInfo/ request.

* STRUCTURES
Changes to an Xkb extension device may be tracked by listening to
XkbDeviceExtensionNotify events and accumulating the changes in an
XkbDeviceChangesRec structure. The changes noted in the structure may
then be used in subsequent operations to update either a server
configuration or a local copy of an Xkb extension device configuration.
The changes structure is defined as follows:

#+begin_example

  typedef struct _XkbDeviceChanges {
      unsigned int         changed;        /* bits indicating what has changed */
      unsigned short       first_btn;      /* number of first button which changed, if any */
      unsigned short       num_btns;       /* number of buttons that have changed */
      XkbDeviceLedChangesRec leds;
  } XkbDeviceChangesRec,*XkbDeviceChangesPtr;
#+end_example

* SEE ALSO
*XkbSetDeviceInfo*(3)
