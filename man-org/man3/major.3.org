#+TITLE: Manpages - major.3
#+DESCRIPTION: Linux manpage for major.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about major.3 is found in manpage for: [[../man3/makedev.3][man3/makedev.3]]