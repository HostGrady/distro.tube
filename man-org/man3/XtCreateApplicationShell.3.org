#+TITLE: Manpages - XtCreateApplicationShell.3
#+DESCRIPTION: Linux manpage for XtCreateApplicationShell.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtCreateApplicationShell - create top-level widget instance

* SYNTAX
#include <X11/Intrinsic.h>

Widget XtCreateApplicationShell(const char */name/, WidgetClass
/widget_class/, ArgList /args/, Cardinal /num_args/);

* ARGUMENTS
- name :: Specifies the name of the shell.

- args :: Specifies the argument list to override any other resource
  specifications.

- num_args :: Specifies the number of arguments in the argument list.

* DESCRIPTION
The procedure *XtCreateApplicationShell* calls *XtAppCreateShell* with
the /application/ NULL, the application class passed to *XtInitialize*,
and the default application context created by *XtInitialize*. This
routine has been replaced by *XtAppCreateShell*.

* SEE ALSO
XtAppCreateShell(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
