#+TITLE: Manpages - pcre2_jit_stack_create.3
#+DESCRIPTION: Linux manpage for pcre2_jit_stack_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  pcre2_jit_stack *pcre2_jit_stack_create(PCRE2_SIZE startsize,
   PCRE2_SIZE maxsize, pcre2_general_context *gcontext);
#+end_example

* DESCRIPTION
This function is used to create a stack for use by the code compiled by
the JIT compiler. The first two arguments are a starting size for the
stack, and a maximum size to which it is allowed to grow. The final
argument is a general context, for memory allocation functions, or NULL
for standard memory allocation. The result can be passed to the JIT
run-time code by calling *pcre2_jit_stack_assign()* to associate the
stack with a compiled pattern, which can then be processed by
*pcre2_match()* or *pcre2_jit_match()*. A maximum stack size of 512KiB
to 1MiB should be more than enough for any pattern. For more details,
see the *pcre2jit* page.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
