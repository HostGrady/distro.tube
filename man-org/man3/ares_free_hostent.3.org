#+TITLE: Manpages - ares_free_hostent.3
#+DESCRIPTION: Linux manpage for ares_free_hostent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_free_hostent - Free host structure allocated by ares functions

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_free_hostent(struct hostent *host)
#+end_example

* DESCRIPTION
The /ares_free_hostent/ function frees a *struct hostent* allocated by
one of the functions /ares_parse_a_reply(3)/,
/ares_parse_aaaa_reply(3)/, or /ares_parse_ptr_reply(3)/.

* NOTES
It is not necessary (and is not correct) to free the host structure
passed to the callback functions for /ares_gethostbyname(3)/ or
/ares_gethostbyaddr(3)/. c-ares will automatically free such host
structures when the callback returns.

* SEE ALSO
*ares_parse_a_reply*(3), *ares_parse_aaaa_reply*(3),
*ares_parse_ptr_reply*(3), *ares_parse_ns_reply*(3)

* AUTHOR
Greg Hudson, MIT Information Systems\\
Copyright 1998 by the Massachusetts Institute of Technology.
