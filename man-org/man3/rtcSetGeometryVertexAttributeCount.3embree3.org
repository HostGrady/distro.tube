#+TITLE: Manpages - rtcSetGeometryVertexAttributeCount.3embree3
#+DESCRIPTION: Linux manpage for rtcSetGeometryVertexAttributeCount.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetGeometryVertexAttributeCount - sets the number of vertex
    attributes of the geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcSetGeometryVertexAttributeCount(
    RTCGeometry geometry,
    unsigned int vertexAttributeCount
  );
#+end_example

** DESCRIPTION
The =rtcSetGeometryVertexAttributeCount= function sets the number of
slots (=vertexAttributeCount= parameter) for vertex attribute buffers
(=RTC_BUFFER_TYPE_VERTEX_ATTRIBUTE=) that can be used for the specified
geometry (=geometry= parameter).

This function is supported only for triangle meshes
(=RTC_GEOMETRY_TYPE_TRIANGLE=), quad meshes (=RTC_GEOMETRY_TYPE_QUAD=),
curves (=RTC_GEOMETRY_TYPE_CURVE=), points (=RTC_GEOMETRY_TYPE_POINT=),
and subdivision geometries (=RTC_GEOMETRY_TYPE_SUBDIVISION=).

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewGeometry], [RTCBufferType]
