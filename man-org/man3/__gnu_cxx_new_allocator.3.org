#+TITLE: Manpages - __gnu_cxx_new_allocator.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_new_allocator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::new_allocator< _Tp > - An allocator that uses global new, as
per [20.4].

* SYNOPSIS
\\

=#include <new_allocator.h>=

** Public Types
typedef const _Tp * *const_pointer*\\

typedef const _Tp & *const_reference*\\

typedef std::ptrdiff_t *difference_type*\\

typedef _Tp * *pointer*\\

typedef *std::true_type* *propagate_on_container_move_assignment*\\

typedef _Tp & *reference*\\

typedef std::size_t *size_type*\\

typedef _Tp *value_type*\\

** Public Member Functions
constexpr *new_allocator* (const *new_allocator* &) noexcept\\

template<typename _Tp1 > constexpr *new_allocator* (const
*new_allocator*< _Tp1 > &) noexcept\\

const_pointer *address* (const_reference __x) const noexcept\\

pointer *address* (reference __x) const noexcept\\

_Tp * *allocate* (size_type __n, const void *=static_cast< const void *
>(0))\\

template<typename _Up , typename... _Args> void *construct* (_Up *__p,
_Args &&... __args) noexcept(*std::is_nothrow_constructible*< _Up,
_Args... >::value)\\

void *deallocate* (_Tp *__p, size_type __t)\\

template<typename _Up > void *destroy* (_Up *__p)
noexcept(*std::is_nothrow_destructible*< _Up >::value)\\

size_type *max_size* () const noexcept\\

** Friends
template<typename _Up > constexpr friend bool *operator!=* (const
*new_allocator* &, const *new_allocator*< _Up > &) noexcept\\

template<typename _Up > constexpr friend bool *operator==* (const
*new_allocator* &, const *new_allocator*< _Up > &) noexcept\\

* Detailed Description
** "template<typename _Tp>
\\
class __gnu_cxx::new_allocator< _Tp >"An allocator that uses global new,
as per [20.4].

This is precisely the allocator defined in the C++ Standard.

- all allocation calls operator new

- all deallocation calls operator delete

*Template Parameters*

#+begin_quote
  /_Tp/ Type of allocated object.
#+end_quote

Definition at line *55* of file *new_allocator.h*.

* Member Typedef Documentation
** template<typename _Tp > typedef const _Tp*
*__gnu_cxx::new_allocator*< _Tp >::const_pointer
Definition at line *63* of file *new_allocator.h*.

** template<typename _Tp > typedef const _Tp&
*__gnu_cxx::new_allocator*< _Tp >::const_reference
Definition at line *65* of file *new_allocator.h*.

** template<typename _Tp > typedef std::ptrdiff_t
*__gnu_cxx::new_allocator*< _Tp >::difference_type
Definition at line *60* of file *new_allocator.h*.

** template<typename _Tp > typedef _Tp* *__gnu_cxx::new_allocator*< _Tp
>::pointer
Definition at line *62* of file *new_allocator.h*.

** template<typename _Tp > typedef *std::true_type*
*__gnu_cxx::new_allocator*< _Tp
>::*propagate_on_container_move_assignment*
Definition at line *75* of file *new_allocator.h*.

** template<typename _Tp > typedef _Tp& *__gnu_cxx::new_allocator*< _Tp
>::reference
Definition at line *64* of file *new_allocator.h*.

** template<typename _Tp > typedef std::size_t
*__gnu_cxx::new_allocator*< _Tp >::size_type
Definition at line *59* of file *new_allocator.h*.

** template<typename _Tp > typedef _Tp *__gnu_cxx::new_allocator*< _Tp
>::value_type
Definition at line *58* of file *new_allocator.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > constexpr *__gnu_cxx::new_allocator*< _Tp
>::*new_allocator* ()= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *79* of file *new_allocator.h*.

** template<typename _Tp > constexpr *__gnu_cxx::new_allocator*< _Tp
>::*new_allocator* (const *new_allocator*< _Tp > &)= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *82* of file *new_allocator.h*.

** template<typename _Tp > template<typename _Tp1 > constexpr
*__gnu_cxx::new_allocator*< _Tp >::*new_allocator* (const
*new_allocator*< _Tp1 > &)= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *86* of file *new_allocator.h*.

** template<typename _Tp > *__gnu_cxx::new_allocator*< _Tp
>::~*new_allocator* ()= [inline]=, = [noexcept]=
Definition at line *89* of file *new_allocator.h*.

* Member Function Documentation
** template<typename _Tp > const_pointer *__gnu_cxx::new_allocator*< _Tp
>::address (const_reference __x) const= [inline]=, = [noexcept]=
Definition at line *96* of file *new_allocator.h*.

** template<typename _Tp > pointer *__gnu_cxx::new_allocator*< _Tp
>::address (reference __x) const= [inline]=, = [noexcept]=
Definition at line *92* of file *new_allocator.h*.

** template<typename _Tp > _Tp * *__gnu_cxx::new_allocator*< _Tp
>::allocate (size_type __n, const void * =
=static_cast<const void*>(0)=)= [inline]=
Definition at line *103* of file *new_allocator.h*.

** template<typename _Tp > template<typename _Up , typename... _Args>
void *__gnu_cxx::new_allocator*< _Tp >::construct (_Up * __p, _Args
&&... __args)= [inline]=, = [noexcept]=
Definition at line *154* of file *new_allocator.h*.

** template<typename _Tp > void *__gnu_cxx::new_allocator*< _Tp
>::deallocate (_Tp * __p, size_type __t)= [inline]=
Definition at line *126* of file *new_allocator.h*.

** template<typename _Tp > template<typename _Up > void
*__gnu_cxx::new_allocator*< _Tp >::destroy (_Up * __p)= [inline]=,
= [noexcept]=
Definition at line *160* of file *new_allocator.h*.

** template<typename _Tp > size_type *__gnu_cxx::new_allocator*< _Tp
>::max_size () const= [inline]=, = [noexcept]=
Definition at line *148* of file *new_allocator.h*.

* Friends And Related Function Documentation
** template<typename _Tp > template<typename _Up > constexpr friend bool
operator!= (const *new_allocator*< _Tp > &, const *new_allocator*< _Up >
&)= [friend]=
Definition at line *184* of file *new_allocator.h*.

** template<typename _Tp > template<typename _Up > constexpr friend bool
operator== (const *new_allocator*< _Tp > &, const *new_allocator*< _Up >
&)= [friend]=
Definition at line *177* of file *new_allocator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
