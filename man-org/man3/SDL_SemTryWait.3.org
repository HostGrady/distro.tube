#+TITLE: Manpages - SDL_SemTryWait.3
#+DESCRIPTION: Linux manpage for SDL_SemTryWait.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SemTryWait - Attempt to lock a semaphore but don't suspend the
thread.

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*int SDL_SemTryWait*(*SDL_sem *sem*);

* DESCRIPTION
*SDL_SemTryWait* is a non-blocking varient of *SDL_SemWait*. If the
value of the semaphore pointed to by *sem* is positive it will
atomically decrement the semaphore value and return 0, otherwise it will
return *SDL_MUTEX_TIMEOUT* instead of suspending the thread.

After *SDL_SemTryWait* is successful, the semaphore can be released and
its count atomically incremented by a successful call to /SDL_SemPost/.

* RETURN VALUE
Returns *0* if the semaphore was successfully locked or either
*SDL_MUTEX_TIMEOUT* or *-1* if the thread would have suspended or there
was an error, respectivly.

If the semaphore was not successfully locked, the semaphore will be
unchanged.

* EXAMPLES
#+begin_example
  res = SDL_SemTryWait(my_sem);

  if (res == SDL_MUTEX_TIMEOUT) {
          return TRY_AGAIN;
  }
  if (res == -1) {
          return WAIT_ERROR;
  }

  ...

  SDL_SemPost(my_sem);
#+end_example

* SEE ALSO
*SDL_CreateSemaphore*, *SDL_DestroySemaphore*, *SDL_SemWait*,
*SDL_SemWaitTimeout*, *SDL_SemPost*, *SDL_SemValue*
