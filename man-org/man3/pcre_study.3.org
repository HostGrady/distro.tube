#+TITLE: Manpages - pcre_study.3
#+DESCRIPTION: Linux manpage for pcre_study.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

#+begin_example
  pcre_extra *pcre_study(const pcre *code, int options,
   const char **errptr);

  pcre16_extra *pcre16_study(const pcre16 *code, int options,
   const char **errptr);

  pcre32_extra *pcre32_study(const pcre32 *code, int options,
   const char **errptr);
#+end_example

* DESCRIPTION
This function studies a compiled pattern, to see if additional
information can be extracted that might speed up matching. Its arguments
are:

/code/ A compiled regular expression /options/ Options for
*pcre[16|32]_study()* /errptr/ Where to put an error message

If the function succeeds, it returns a value that can be passed to
*pcre[16|32]_exec()* or *pcre[16|32]_dfa_exec()* via their /extra/
arguments.

If the function returns NULL, either it could not find any additional
information, or there was an error. You can tell the difference by
looking at the error value. It is NULL in first case.

The only option is PCRE_STUDY_JIT_COMPILE. It requests just-in-time
compilation if possible. If PCRE has been compiled without JIT support,
this option is ignored. See the *pcrejit* page for further details.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
