#+TITLE: Manpages - std___detail__Map_base.3
#+DESCRIPTION: Linux manpage for std___detail__Map_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Map_base< _Key, _Value, _Alloc, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, _Unique_keys >

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherited by *std::_Hashtable< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >*.

* Detailed Description
** "template<typename _Key, typename _Value, typename _Alloc, typename
_ExtractKey, typename _Equal, typename _Hash, typename _RangeHash,
typename _Unused, typename _RehashPolicy, typename _Traits, bool
_Unique_keys = _Traits::__unique_keys::value>
\\
struct std::__detail::_Map_base< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, _Unique_keys
>"Primary class template _Map_base.

If the hashtable has a value type of the form pair<T1, T2> and a key
extraction policy (_ExtractKey) that returns the first part of the pair,
the hashtable gets a mapped_type typedef. If it satisfies those criteria
and also has unique keys, then it also gets an operator[].

Definition at line *642* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
