#+TITLE: Manpages - FcPatternFilter.3
#+DESCRIPTION: Linux manpage for FcPatternFilter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternFilter - Filter the objects of pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcPattern * FcPatternFilter (FcPattern */p/*, const FcObjectSet
**/os/*);*

* DESCRIPTION
Returns a new pattern that only has those objects from /p/ that are in
/os/. If /os/ is NULL, a duplicate of /p/ is returned.
