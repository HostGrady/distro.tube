#+TITLE: Manpages - unibi_from_file.3
#+DESCRIPTION: Linux manpage for unibi_from_file.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_from_file - read a terminfo entry from a file

* SYNOPSIS
#include <unibilium.h> unibi_term *unibi_from_file(const char *file);

* DESCRIPTION
This function opens /file/, then calls =unibi_from_fd=.

* RETURN VALUE
See *unibi_from_fd* (3).

* SEE ALSO
*unibilium.h* (3), *unibi_from_fd* (3), *unibi_destroy* (3)
