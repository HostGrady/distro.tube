#+TITLE: Manpages - io_uring_queue_exit.3
#+DESCRIPTION: Linux manpage for io_uring_queue_exit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
io_uring_queue_exit - tear down io_uring submission and completion
queues

* SYNOPSIS
#+begin_example
  #include <liburing.h>

  void io_uring_queue_exit(struct io_uring * ring );
#+end_example

* DESCRIPTION
*io_uring_queue_exit*(3) will release all resources acquired and
initialized by *io_uring_queue_init*(3). It first unmaps the memory
shared between the application and the kernel and then closes the
io_uring file descriptor.

* RETURN VALUE
None

* SEE ALSO
*io_uring_setup*(2), *mmap*(2), *io_uring_queue_init*(3)
