#+TITLE: Manpages - XcupStoreColors.3
#+DESCRIPTION: Linux manpage for XcupStoreColors.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XcupStoreColors - initialize shareable colormap entries at specific
locations

* SYNOPSIS
#+begin_example
  cc [ flag ... ] file ... -lXext [ library ... ]
  #include <X11/extensions/Xcup.h>
  Status XcupStoreColors ( Display *display , Colormap colormap ,
  XColor *colors_in_out , int ncolors );
#+end_example

* ARGUMENTS
- display :: Specifies the connection to the X server

- colormap :: Specifies the colormap

- colors_in_out :: Specifies and returns the values actually used in the
  colormap

- ncolors :: Specifies the number of items in colors_in_out

* DESCRIPTION
The / XcupStoreColors / function changes the colormap entries of the
pixel values in the pixel members of the XColor structures. The colormap
entries are allocated as if an AllocColor has been used instead, i.e.
the colors are read-only (shareable). / XcupStoreColors / returns the
number of colors that were successfully allocated in the colormap.

A / Value / error is generated if a pixel is not a valid index into the
colormap. A / BadMatch / error is generated if the colormap does not
belong to a GrayScale, PseudoColor, or DirectColor visual.

Applications which allocate many colors in a screen's default colormap,
e.g. a color-cube or a gray-ramp, should allocate them with
/ XCupStoreColors /. By using XCupStoreColors the colors will be
allocated sharable (read-only) and any other application which allocates
the same color will share that color cell.

* SEE ALSO
*XcupQueryVersion*(3Xext), *XcupGetReservedColormapEntries*(3Xext),\\
/Colormap Utilization Policy and Extension/
