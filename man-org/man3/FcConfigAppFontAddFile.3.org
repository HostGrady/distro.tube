#+TITLE: Manpages - FcConfigAppFontAddFile.3
#+DESCRIPTION: Linux manpage for FcConfigAppFontAddFile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigAppFontAddFile - Add font file to font database

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigAppFontAddFile (FcConfig */config/*, const FcChar8
**/file/*);*

* DESCRIPTION
Adds an application-specific font to the configuration. Returns FcFalse
if the fonts cannot be added (due to allocation failure or no fonts
found). Otherwise returns FcTrue. If /config/ is NULL, the current
configuration is used.
