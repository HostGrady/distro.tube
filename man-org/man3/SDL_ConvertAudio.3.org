#+TITLE: Manpages - SDL_ConvertAudio.3
#+DESCRIPTION: Linux manpage for SDL_ConvertAudio.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_ConvertAudio - Convert audio data to a desired audio format.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_ConvertAudio*(*SDL_AudioCVT *cvt*);

* DESCRIPTION
*SDL_ConvertAudio* takes one parameter, *cvt*, which was previously
initilized. Initilizing a *SDL_AudioCVT* is a two step process. First of
all, the structure must be passed to *SDL_BuildAudioCVT* along with
source and destination format parameters. Secondly, the *cvt*->*buf* and
*cvt*->*len* fields must be setup. *cvt*->*buf* should point to the
audio data and *cvt*->*len* should be set to the length of the audio
data in bytes. Remember, the length of the buffer pointed to by *buf*
show be *len***len_mult* bytes in length.

Once the *SDL_AudioCVT*structure is initilized then we can pass it to
*SDL_ConvertAudio*, which will convert the audio data pointer to by
*cvt*->*buf*. If *SDL_ConvertAudio* returned *0* then the conversion was
completed successfully, otherwise *-1* is returned.

If the conversion completed successfully then the converted audio data
can be read from *cvt*->*buf*. The amount of valid, converted, audio
data in the buffer is equal to *cvt*->*len***cvt*->*len_ratio*.

* EXAMPLES
#+begin_example
  /* Converting some WAV data to hardware format */
  void my_audio_callback(void *userdata, Uint8 *stream, int len);

  SDL_AudioSpec *desired, *obtained;
  SDL_AudioSpec wav_spec;
  SDL_AudioCVT  wav_cvt;
  Uint32 wav_len;
  Uint8 *wav_buf;
  int ret;

  /* Allocated audio specs */
  desired=(SDL_AudioSpec *)malloc(sizeof(SDL_AudioSpec));
  obtained=(SDL_AudioSpec *)malloc(sizeof(SDL_AudioSpec));

  /* Set desired format */
  desired->freq=22050;
  desired->format=AUDIO_S16LSB;
  desired->samples=8192;
  desired->callback=my_audio_callback;
  desired->userdata=NULL;

  /* Open the audio device */
  if ( SDL_OpenAudio(desired, obtained) < 0 ){
    fprintf(stderr, "Couldn't open audio: %s
  ", SDL_GetError());
    exit(-1);
  }
          
  free(desired);

  /* Load the test.wav */
  if( SDL_LoadWAV("test.wav", &wav_spec, &wav_buf, &wav_len) == NULL ){
    fprintf(stderr, "Could not open test.wav: %s
  ", SDL_GetError());
    SDL_CloseAudio();
    free(obtained);
    exit(-1);
  }
                                              
  /* Build AudioCVT */
  ret = SDL_BuildAudioCVT(&wav_cvt,
                          wav_spec.format, wav_spec.channels, wav_spec.freq,
                          obtained->format, obtained->channels, obtained->freq);

  /* Check that the convert was built */
  if(ret==-1){
    fprintf(stderr, "Couldn't build converter!
  ");
    SDL_CloseAudio();
    free(obtained);
    SDL_FreeWAV(wav_buf);
  }

  /* Setup for conversion */
  wav_cvt.buf=(Uint8 *)malloc(wav_len*wav_cvt.len_mult);
  wav_cvt.len=wav_len;
  memcpy(wav_cvt.buf, wav_buf, wav_len);

  /* We can delete to original WAV data now */
  SDL_FreeWAV(wav_buf);

  /* And now we're ready to convert */
  SDL_ConvertAudio(&wav_cvt);

  /* do whatever */
  .
  .
  .
  .
#+end_example

* SEE ALSO
*SDL_BuildAudioCVT*, *SDL_AudioCVT*
