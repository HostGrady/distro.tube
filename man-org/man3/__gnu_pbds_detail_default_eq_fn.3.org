#+TITLE: Manpages - __gnu_pbds_detail_default_eq_fn.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_default_eq_fn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::default_eq_fn< Key > - Primary template,
default_eq_fn.

* SYNOPSIS
\\

=#include <standard_policies.hpp>=

** Public Types
typedef *std::equal_to*< Key > *type*\\
Dispatched type.

* Detailed Description
** "template<typename Key>
\\
struct __gnu_pbds::detail::default_eq_fn< Key >"Primary template,
default_eq_fn.

Definition at line *67* of file *standard_policies.hpp*.

* Member Typedef Documentation
** template<typename Key > typedef *std::equal_to*<Key>
*__gnu_pbds::detail::default_eq_fn*< Key >::*type*
Dispatched type.

Definition at line *70* of file *standard_policies.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
