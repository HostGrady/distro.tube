#+TITLE: Manpages - getdelim.3
#+DESCRIPTION: Linux manpage for getdelim.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getdelim.3 is found in manpage for: [[../man3/getline.3][man3/getline.3]]