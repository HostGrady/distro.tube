#+TITLE: Manpages - XML_LibXML_ErrNo.3pm
#+DESCRIPTION: Linux manpage for XML_LibXML_ErrNo.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
XML::LibXML::ErrNo - Structured Errors

* DESCRIPTION
This module is based on xmlerror.h libxml2 C header file. It defines
symbolic constants for all libxml2 error codes. Currently libxml2 uses
over 480 different error codes. See also XML::LibXML::Error.

* AUTHORS
Matt Sergeant, Christian Glahn, Petr Pajas

* VERSION
2.0207

* COPYRIGHT
2001-2007, AxKit.com Ltd.

2002-2006, Christian Glahn.

2006-2009, Petr Pajas.

* LICENSE
This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
