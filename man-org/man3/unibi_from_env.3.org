#+TITLE: Manpages - unibi_from_env.3
#+DESCRIPTION: Linux manpage for unibi_from_env.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_from_env - read the terminfo entry specified by TERM

* SYNOPSIS
#include <unibilium.h> unibi_term *unibi_from_env(void);

* DESCRIPTION
This function calls =unibi_from_term= with the value of the environment
variable =TERM=.

* RETURN VALUE
See *unibi_from_term* (3).

* SEE ALSO
*unibilium.h* (3), *unibi_from_term* (3), *unibi_destroy* (3)
