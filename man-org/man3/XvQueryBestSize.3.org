#+TITLE: Manpages - XvQueryBestSize.3
#+DESCRIPTION: Linux manpage for XvQueryBestSize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XvQueryBestSize - determine the optimum drawable region size

* SYNOPSIS
*#include <X11/extensions/Xvlib.h>*

#+begin_example
  int XvQueryBestSize(Display *dpy, XvPort port, Bool motion,
   unsigned int vw, unsigned int vh,
   unsigned int dw, unsigned int dh,
   unsigned int *p_dw, unsigned int *p_dh);
#+end_example

* ARGUMENTS
- dpy :: Specifies the connection to the X server.

- port :: Defines the port associated with the display and adaptor for
  which the optimum size is to be calculated.

- motion :: Specifies /True/ if the destination size needs to support
  full motion, and /False/ if the destination size need only support
  still images.

- vw,vh :: Specifies the size of the source video region desired.

- dw,dh :: Specifies the size of the destination drawable region
  desired.

- p_dw,p_dh :: Pointers to where the closest destination sizes supported
  by the server are returned.

* DESCRIPTION
Some ports may be able to scale incoming or outgoing video.
*XvQueryBestSize*(3) returns the size of the closest destination region
that is supported by the adaptor. The returned size is guaranteed to be
smaller than the requested size if a smaller size is supported.

* RETURN VALUES
- [Success] :: Returned if *XvQueryBestSize*(3) completed successfully.

- [XvBadExtension] :: Returned if the Xv extension is unavailable.

- [XvBadAlloc] :: Returned if *XvQueryBestSize*(3) failed to allocate
  memory to process the request.

* DIAGNOSTICS
- [XvBadPort] :: Generated if the requested port does not exist.\\
