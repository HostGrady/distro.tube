#+TITLE: Manpages - DBI_Gofer_Serializer_DataDumper.3pm
#+DESCRIPTION: Linux manpage for DBI_Gofer_Serializer_DataDumper.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBI::Gofer::Serializer::DataDumper - Gofer serialization using
DataDumper

* SYNOPSIS
$serializer = DBI::Gofer::Serializer::DataDumper->new(); $string =
$serializer->serialize( $data );

* DESCRIPTION
Uses DataDumper to serialize. Deserialization is not supported. The
output of this class is only meant for human consumption.

See also DBI::Gofer::Serializer::Base.
