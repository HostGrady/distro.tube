#+TITLE: Manpages - rtcDetachGeometry.3embree3
#+DESCRIPTION: Linux manpage for rtcDetachGeometry.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcDetachGeometry - detaches a geometry from the scene
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcDetachGeometry(RTCScene scene, unsigned int geomID);
#+end_example

** DESCRIPTION
This function detaches a geometry identified by its geometry ID
(=geomID= argument) from a scene (=scene= argument). When detached, the
geometry is no longer contained in the scene.

This function is thread-safe, thus multiple threads can detach
geometries from a scene at the same time.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcAttachGeometry], [rtcAttachGeometryByID]
