#+TITLE: Manpages - pcre2_substring_list_free.3
#+DESCRIPTION: Linux manpage for pcre2_substring_list_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

*void pcre2_substring_list_free(PCRE2_SPTR */list/);*

* DESCRIPTION
This is a convenience function for freeing the store obtained by a
previous call to *pcre2substring_list_get()*. Its only argument is a
pointer to the list of string pointers. If the argument is NULL, the
function returns immediately, without doing anything.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
