#+TITLE: Manpages - XkbComputeSectionBounds.3
#+DESCRIPTION: Linux manpage for XkbComputeSectionBounds.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbComputeSectionBounds - Update the bounding box of a section

* SYNOPSIS
*Bool XkbComputeSectionBounds* *( XkbGeometryPtr */geom/* ,*
*XkbSectionPtr */section/* );*

* ARGUMENTS
- /- geom/ :: geometry that contains the section

- /- section/ :: section to be examined and updated

* DESCRIPTION
If you add or delete a row to or from a section, or if you change the
geometry of any of the rows in that section, you may need to update the
bounding box for that section. /XkbComputeSectionBounds/ examines all
the rows of the /section/ and updates the bounding box of that section
so that it contains all rows. /XkbComputeSectionBounds/ returns False if
any of the arguments is NULL; otherwise, it returns True.
