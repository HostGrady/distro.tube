#+TITLE: Manpages - MPI_Comm_create_errhandler.3
#+DESCRIPTION: Linux manpage for MPI_Comm_create_errhandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Comm_create_errhandler * - Creates an error handler that can be
attached to communicators.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Comm_create_errhandler(MPI_Comm_errhandler_function *function,
  	MPI_Errhandler *errhandler)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_COMM_CREATE_ERRHANDLER(FUNCTION, ERRHANDLER, IERROR)
  	EXTERNAL	FUNCTION
  	INTEGER	ERRHANDLER, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Comm_create_errhandler(comm_errhandler_fn, errhandler, ierror)
  	PROCEDURE(MPI_Comm_errhandler_function) :: comm_errhandler_fn
  	TYPE(MPI_Errhandler), INTENT(OUT) :: errhandler
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  static MPI::Errhandler
  	MPI::Comm::Create_errhandler(MPI::Comm::Errhandler_function*
  	function)
#+end_example

* DEPRECATED TYPE NAME NOTE
MPI-2.2 deprecated the MPI_Comm_errhandler_fn and
MPI::Comm::Errhandler_fn types in favor of MPI_Comm_errhandler_function
and MPI::Comm::Errhandler_function, respectively. Open MPI supports both
names (indeed, the _fn names are typedefs to the _function names).

* INPUT PARAMETER
- function :: User-defined error handling procedure (function).

* OUTPUT PARAMETERS
- errhandler :: MPI error handler (handle).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_Comm_create_errhandler creates an error handler that can be attached
to communicators. This function is identical to MPI_Errhandler_create,
the use of which is deprecated.

In C, the user routine should be a function of type
MPI_Comm_errhandler_function, which is defined as

#+begin_example
      typedef void MPI_Comm_errhandler_function(MPI_Comm *, int *, ...);
#+end_example

The first argument is the communicator in use. The second is the error
code to be returned by the MPI routine that raised the error. This
typedef replaces MPI_Handler_function, the use of which is deprecated.

In Fortran, the user routine should be of this form:

#+begin_example
      SUBROUTINE COMM_ERRHANDLER_FUNCTION(COMM, ERROR_CODE, ...)
          INTEGER COMM, ERROR_CODE
#+end_example

In C++, the user routine should be of this form:

#+begin_example
      typedef void MPI::Comm::Errhandler_function(MPI_Comm &, int *, ...);
#+end_example

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
