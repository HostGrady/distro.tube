#+TITLE: Manpages - SDL_CondWaitTimeout.3
#+DESCRIPTION: Linux manpage for SDL_CondWaitTimeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CondWaitTimeout - Wait on a condition variable, with timeout

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*int SDL_CondWaitTimeout*(*SDL_cond *cond, SDL_mutex *mutex, Uint32
ms*);

* DESCRIPTION
Wait on the condition variable *cond* for, at most, *ms* milliseconds.
*mut* is unlocked so it must be locked when the function is called.
Returns *SDL_MUTEX_TIMEDOUT* if the condition is not signalled in the
allotted time, *0* if it was signalled or *-1* on an error.

* SEE ALSO
*SDL_CondWait*
