#+TITLE: Manpages - FcLangSetDel.3
#+DESCRIPTION: Linux manpage for FcLangSetDel.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetDel - delete a language from a langset

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcLangSetDel (FcLangSet */ls/*, const FcChar8 **/lang/*);*

* DESCRIPTION
/lang/ is removed from /ls/. /lang/ should be of the form Ll-Tt where Ll
is a two or three letter language from ISO 639 and Tt is a territory
from ISO 3166.

* SINCE
version 2.9.0
