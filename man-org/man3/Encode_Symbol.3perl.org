#+TITLE: Manpages - Encode_Symbol.3perl
#+DESCRIPTION: Linux manpage for Encode_Symbol.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Encode::Symbol - Symbol Encodings

* SYNOPSIS
use Encode qw/encode decode/; $symbol = encode("symbol", $utf8); # loads
Encode::Symbol implicitly $utf8 = decode("", $symbol); # ditto

* ABSTRACT
This module implements symbol and dingbats encodings. Encodings
supported are as follows.

Canonical Alias Description
--------------------------------------------------------------------
symbol dingbats AdobeZDingbat AdobeSymbol MacDingbats

* DESCRIPTION
To find out how to use this module in detail, see Encode.

* SEE ALSO
Encode
