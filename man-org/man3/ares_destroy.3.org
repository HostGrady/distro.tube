#+TITLE: Manpages - ares_destroy.3
#+DESCRIPTION: Linux manpage for ares_destroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_destroy - Destroy a resolver channel

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_destroy(ares_channel channel)
#+end_example

* DESCRIPTION
The *ares_destroy(3)* function destroys the name service channel
identified by /channel/, freeing all memory and closing all sockets used
by the channel.

*ares_destroy(3)* invokes the callbacks for each pending query on the
channel, passing a status of /ARES_EDESTRUCTION/. These calls give the
callbacks a chance to clean up any state which might have been stored in
their arguments. A callback must not add new requests to a channel being
destroyed.

* SEE ALSO
*ares_init*(3), *ares_cancel*(3)

* AUTHOR
Greg Hudson, MIT Information Systems\\
Copyright 1998 by the Massachusetts Institute of Technology.
