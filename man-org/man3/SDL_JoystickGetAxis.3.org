#+TITLE: Manpages - SDL_JoystickGetAxis.3
#+DESCRIPTION: Linux manpage for SDL_JoystickGetAxis.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoystickGetAxis - Get the current state of an axis

* SYNOPSIS
*#include "SDL.h"*

*Sint16 SDL_JoystickGetAxis*(*SDL_Joystick *joystick, int axis*);

* DESCRIPTION
*SDL_JoystickGetAxis* returns the current state of the given *axis* on
the given *joystick*.

On most modern joysticks the X axis is usually represented by *axis* 0
and the Y axis by *axis* 1. The value returned by *SDL_JoystickGetAxis*
is a signed integer (-32768 to 32768) representing the current position
of the *axis*, it maybe necessary to impose certain tolerances on these
values to account for jitter. It is worth noting that some joysticks use
axes 2 and 3 for extra buttons.

* RETURN VALUE
Returns a 16-bit signed integer representing the current position of the
*axis*.

* EXAMPLES
#+begin_example
  Sint16 x_move, y_move;
  SDL_Joystick *joy1;
  .
  .
  x_move=SDL_JoystickGetAxis(joy1, 0);
  y_move=SDL_JoystickGetAxis(joy1, 1);
#+end_example

* SEE ALSO
*SDL_JoystickNumAxes*
