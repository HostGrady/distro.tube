#+TITLE: Manpages - XtName.3
#+DESCRIPTION: Linux manpage for XtName.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtName - obtain widget's name

* SYNTAX
#include <X11/Intrinsic.h>

String XtName(Widget /w/);

* ARGUMENTS

23. Specifies the widget.

* DESCRIPTION
*XtName* returns the widget's name.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
