#+TITLE: Manpages - XNoExposeEvent.3
#+DESCRIPTION: Linux manpage for XNoExposeEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XNoExposeEvent.3 is found in manpage for: [[../man3/XGraphicsExposeEvent.3][man3/XGraphicsExposeEvent.3]]