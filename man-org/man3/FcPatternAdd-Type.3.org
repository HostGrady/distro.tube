#+TITLE: Manpages - FcPatternAdd-Type.3
#+DESCRIPTION: Linux manpage for FcPatternAdd-Type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternAddInteger, FcPatternAddDouble, FcPatternAddString,
FcPatternAddMatrix, FcPatternAddCharSet, FcPatternAddBool,
FcPatternAddFTFace, FcPatternAddLangSet, FcPatternAddRange - Add a typed
value to a pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcPatternAddInteger (FcPattern */p/*, const char **/object/*, int
*/i/*);*

FcBool FcPatternAddDouble (FcPattern */p/*, const char **/object/*,
double */d/*);*

FcBool FcPatternAddString (FcPattern */p/*, const char **/object/*,
const FcChar8 **/s/*);*

FcBool FcPatternAddMatrix (FcPattern */p/*, const char **/object/*,
const FcMatrix **/m/*);*

FcBool FcPatternAddCharSet (FcPattern */p/*, const char **/object/*,
const FcCharSet **/c/*);*

FcBool FcPatternAddBool (FcPattern */p/*, const char **/object/*, FcBool
*/b/*);*

FcBool FcPatternAddFTFace (FcPattern */p/*, const char **/object/*,
const FT_Face*/f/*);*

FcBool FcPatternAddLangSet (FcPattern */p/*, const char **/object/*,
const FcLangSet **/l/*);*

FcBool FcPatternAddRange (FcPattern */p/*, const char **/object/*, const
FcRange **/r/*);*

* DESCRIPTION
These are all convenience functions that insert objects of the specified
type into the pattern. Use these in preference to FcPatternAdd as they
will provide compile-time typechecking. These all append values to any
existing list of values. *FcPatternAddRange* are available since
2.11.91.
