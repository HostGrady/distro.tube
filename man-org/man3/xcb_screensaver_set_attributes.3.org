#+TITLE: Manpages - xcb_screensaver_set_attributes.3
#+DESCRIPTION: Linux manpage for xcb_screensaver_set_attributes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_screensaver_set_attributes -

* SYNOPSIS
*#include <xcb/screensaver.h>*

** Request function
xcb_void_cookie_t *xcb_screensaver_set_attributes*(xcb_connection_t
*/conn/, xcb_drawable_t /drawable/, int16_t /x/, int16_t /y/, uint16_t
/width/, uint16_t /height/, uint16_t /border_width/, uint8_t /_class/,
uint8_t /depth/, xcb_visualid_t /visual/, uint32_t /value_mask/, const
void */value_list/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- drawable :: TODO: NOT YET DOCUMENTED.

24. TODO: NOT YET DOCUMENTED.

25. TODO: NOT YET DOCUMENTED.

- width :: TODO: NOT YET DOCUMENTED.

- height :: TODO: NOT YET DOCUMENTED.

- border_width :: TODO: NOT YET DOCUMENTED.

- _class :: TODO: NOT YET DOCUMENTED.

- depth :: TODO: NOT YET DOCUMENTED.

- visual :: TODO: NOT YET DOCUMENTED.

- value_mask :: TODO: NOT YET DOCUMENTED.

- value_list :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_screensaver_set_attributes_checked/. See *xcb-requests(3)* for
details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from screensaver.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
