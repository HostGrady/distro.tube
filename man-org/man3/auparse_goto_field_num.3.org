#+TITLE: Manpages - auparse_goto_field_num.3
#+DESCRIPTION: Linux manpage for auparse_goto_field_num.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_goto_field_num - move field cursor to specific field

* SYNOPSIS
*#include <auparse.h>*

int auparse_goto_field_num(auparse_state_t *au, unsigned int num);

* DESCRIPTION
auparse_goto_field_num will move the internal library cursors to point
to a specific field number in the current record. Fields within the same
record are numbered starting from 0. This is generally not needed but
there are some cases where one may want precise control over the exact
field being looked at.

* RETURN VALUE
Returns 0 on error or 1 for success.

* SEE ALSO
*auparse_get_field_num*(3),*auparse_goto_record_num*(3).

* AUTHOR
Steve Grubb
