#+TITLE: Manpages - Test2_EventFacet.3perl
#+DESCRIPTION: Linux manpage for Test2_EventFacet.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::EventFacet - Base class for all event facets.

* DESCRIPTION
Base class for all event facets.

* METHODS
- $key = $facet_class->facet_key() :: This will return the key for the
  facet in the facet data hash.

- $bool = $facet_class->is_list() :: This will return true if the facet
  should be in a list instead of a single item.

- $clone = $facet->clone() :: 

- $clone = $facet->clone(%replace) :: 

This will make a shallow clone of the facet. You may specify fields to
override as arguments.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
