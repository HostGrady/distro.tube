#+TITLE: Manpages - FcMatrixRotate.3
#+DESCRIPTION: Linux manpage for FcMatrixRotate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcMatrixRotate - Rotate a matrix

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcMatrixRotate (FcMatrix */matrix/*, double */cos/*, double
*/sin/*);*

* DESCRIPTION
*FcMatrixRotate* rotates /matrix/ by the angle who's sine is /sin/ and
cosine is /cos/. This is done by multiplying by the matrix:

#+begin_example
    cos -sin
    sin  cos
#+end_example
