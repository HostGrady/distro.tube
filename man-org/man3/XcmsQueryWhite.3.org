#+TITLE: Manpages - XcmsQueryWhite.3
#+DESCRIPTION: Linux manpage for XcmsQueryWhite.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XcmsQueryWhite.3 is found in manpage for: [[../man3/XcmsQueryBlack.3][man3/XcmsQueryBlack.3]]