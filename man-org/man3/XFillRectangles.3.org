#+TITLE: Manpages - XFillRectangles.3
#+DESCRIPTION: Linux manpage for XFillRectangles.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFillRectangles.3 is found in manpage for: [[../man3/XFillRectangle.3][man3/XFillRectangle.3]]