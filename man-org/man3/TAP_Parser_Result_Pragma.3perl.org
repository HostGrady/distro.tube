#+TITLE: Manpages - TAP_Parser_Result_Pragma.3perl
#+DESCRIPTION: Linux manpage for TAP_Parser_Result_Pragma.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Parser::Result::Pragma - TAP pragma token.

* VERSION
Version 3.43

* DESCRIPTION
This is a subclass of TAP::Parser::Result. A token of this class will be
returned if a pragma is encountered.

TAP version 13 pragma +strict, -foo

Pragmas are only supported from TAP version 13 onwards.

* OVERRIDDEN METHODS
Mainly listed here to shut up the pitiful screams of the pod coverage
tests. They keep me awake at night.

- =as_string=

- =raw=

** Instance Methods
/=pragmas=/

if ( =$result=->is_pragma ) { =@pragmas= = =$result=->pragmas; }
