#+TITLE: Manpages - __gnu_pbds_cc_hash_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_cc_hash_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::cc_hash_tag - Collision-chaining hash.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::basic_hash_tag*.

* Detailed Description
Collision-chaining hash.

Definition at line *141* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
