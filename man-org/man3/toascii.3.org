#+TITLE: Manpages - toascii.3
#+DESCRIPTION: Linux manpage for toascii.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
toascii - convert character to ASCII

* SYNOPSIS
#+begin_example
  #include <ctype.h>

  int toascii(int c);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*toascii*():

#+begin_example
      _XOPEN_SOURCE
          || /* Glibc since 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _SVID_SOURCE || _BSD_SOURCE
#+end_example

* DESCRIPTION
*toascii*() converts /c/ to a 7-bit /unsigned char/ value that fits into
the ASCII character set, by clearing the high-order bits.

* RETURN VALUE
The value returned is that of the converted character.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface   | Attribute     | Value   |
| *toascii*() | Thread safety | MT-Safe |

* CONFORMING TO
SVr4, BSD, POSIX.1-2001. POSIX.1-2008 marks *toascii*() as obsolete,
noting that it cannot be used portably in a localized application.

* BUGS
Many people will be unhappy if you use this function. This function will
convert accented letters into random characters.

* SEE ALSO
*isascii*(3), *tolower*(3), *toupper*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
