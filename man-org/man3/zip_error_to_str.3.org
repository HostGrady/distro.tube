#+TITLE: Manpages - zip_error_to_str.3
#+DESCRIPTION: Linux manpage for zip_error_to_str.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The function

is deprecated; use

and

instead.

Replace

char buf[BUFSIZE]; zip_error_to_str(buf, sizeof(buf), ze, se);
printf("%s", buf);

with

zip_error_t error; zip_error_init_with_code(&error, ze); printf("%s",
zip_error_strerror(&error)); zip_error_fini(&error);

was added in libzip 0.6. In libzip 0.10 the type of

was changed from

to

It was deprecated in libzip 1.0, use

and

instead.

and
