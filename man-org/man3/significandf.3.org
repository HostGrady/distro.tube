#+TITLE: Manpages - significandf.3
#+DESCRIPTION: Linux manpage for significandf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about significandf.3 is found in manpage for: [[../man3/significand.3][man3/significand.3]]