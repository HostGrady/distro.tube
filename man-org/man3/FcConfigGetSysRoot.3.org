#+TITLE: Manpages - FcConfigGetSysRoot.3
#+DESCRIPTION: Linux manpage for FcConfigGetSysRoot.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigGetSysRoot - Obtain the system root directory

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

const FcChar8 * FcConfigGetSysRoot (const FcConfig */config/*);*

* DESCRIPTION
Obtains the system root directory in 'config' if available. All files
(including file properties in patterns) obtained from this 'config' are
relative to this system root directory.

This function isn't MT-safe. *FcConfigReference* must be called before
using this and then *FcConfigDestroy* when the return value is no longer
referenced.

* SINCE
version 2.10.92
