#+TITLE: Manpages - TIFFFieldName.3tiff
#+DESCRIPTION: Linux manpage for TIFFFieldName.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFFieldName - Get TIFF field name from field information

* SYNOPSIS
*#include <tiffio.h>*

*const char* TIFFFieldName(const TIFFField* */fip/*)*

* DESCRIPTION
*TIFFFieldName* returns the textual name for a TIFF field.

/fip/ is a field information pointer previously returned by
*TIFFFindField*, *TIFFFieldWithTag*, or *TIFFFieldWithName*.\\

* RETURN VALUES
\\
*TIFFFieldName* returns a constant C string.\\

* SEE ALSO
*libtiff*(3TIFF),

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
