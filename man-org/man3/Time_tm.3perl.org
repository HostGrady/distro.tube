#+TITLE: Manpages - Time_tm.3perl
#+DESCRIPTION: Linux manpage for Time_tm.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Time::tm - internal object used by Time::gmtime and Time::localtime

* SYNOPSIS
Don't use this module directly.

* DESCRIPTION
This module is used internally as a base class by Time::localtime And
Time::gmtime functions. It creates a Time::tm struct object which is
addressable just like's C's tm structure from /time.h/; namely with sec,
min, hour, mday, mon, year, wday, yday, and isdst.

This class is an internal interface only.

* AUTHOR
Tom Christiansen
