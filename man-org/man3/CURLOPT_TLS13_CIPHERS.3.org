#+TITLE: Manpages - CURLOPT_TLS13_CIPHERS.3
#+DESCRIPTION: Linux manpage for CURLOPT_TLS13_CIPHERS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TLS13_CIPHERS - ciphers suites to use for TLS 1.3

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TLS13_CIPHERS, char
*list);

* DESCRIPTION
Pass a char *, pointing to a null-terminated string holding the list of
cipher suites to use for the TLS 1.3 connection. The list must be
syntactically correct, it consists of one or more cipher suite strings
separated by colons.

you will find more details about cipher lists on this URL:

https://curl.se/docs/ssl-ciphers.html

This option is currently used only when curl is built to use OpenSSL
1.1.1 or later. If you are using a different SSL backend you can try
setting TLS 1.3 cipher suites by using the CURLOPT_SSL_CIPHER_LIST
option.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL, use internal default

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_TLS13_CIPHERS,
                     "TLS13-CHACHA20-POLY1305-SHA256");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.61.0. Available when built with OpenSSL >= 1.1.1.

* RETURN VALUE
Returns CURLE_OK if supported, CURLE_NOT_BUILT_IN otherwise.

* SEE ALSO
*CURLOPT_SSL_CIPHER_LIST*(3), *CURLOPT_SSLVERSION*(3),
*CURLOPT_PROXY_SSL_CIPHER_LIST*(3), *CURLOPT_PROXY_TLS13_CIPHERS*(3),
*CURLOPT_PROXY_SSLVERSION*(3), *CURLOPT_USE_SSL*(3),
