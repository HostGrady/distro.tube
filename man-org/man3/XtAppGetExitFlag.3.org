#+TITLE: Manpages - XtAppGetExitFlag.3
#+DESCRIPTION: Linux manpage for XtAppGetExitFlag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtAppGetExitFlag.3 is found in manpage for: [[../man3/XtAppSetExitFlag.3][man3/XtAppSetExitFlag.3]]