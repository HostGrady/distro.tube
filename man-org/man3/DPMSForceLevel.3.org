#+TITLE: Manpages - DPMSForceLevel.3
#+DESCRIPTION: Linux manpage for DPMSForceLevel.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
DPMSForceLevel - forces a DPMS capable display into the specified power
level

* SYNOPSIS
#+begin_example
  cc [ flag ... ] file ... -lXext [ library ... ]
  #include <X11/extensions/dpms.h>
  Status DPMSForceLevel ( Display *display , CARD16 level  );
#+end_example

* ARGUMENTS
- /display/ :: Specifies the connection to the X server

- /level/ :: Specifies the level to force power to

* DESCRIPTION
The /DPMSForceLevel/ function forces a DPMS capable display into the
specified power level. The /level/ must be one of following four states:
DPMSModeOn, DPMSModeStandby, DPMSModeSuspend, or DPMSModeOff. Values
other than these will result in a BadValue error. If DPMS is disabled on
the display, trying to set the power level on the display will result in
a BadMatch protocol error.

* RETURN VALUES
- TRUE :: The /DPMSForceLevel/ function always returns TRUE.

* ERRORS
- BadValue :: A level other than DPMSModeOn, DPMSModeStandby,
  DPMSModeSuspend, or DPMSModeOff was specified.

DPMS is disabled on the specified display.

* SEE ALSO
*DPMSCapable*(3), *DPMSInfo*(3), *DPMSGetTimeouts*(3),
*DPMSSetTimeouts*(3)
