#+TITLE: Manpages - FcLangSetAdd.3
#+DESCRIPTION: Linux manpage for FcLangSetAdd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetAdd - add a language to a langset

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcLangSetAdd (FcLangSet */ls/*, const FcChar8 **/lang/*);*

* DESCRIPTION
/lang/ is added to /ls/. /lang/ should be of the form Ll-Tt where Ll is
a two or three letter language from ISO 639 and Tt is a territory from
ISO 3166.
