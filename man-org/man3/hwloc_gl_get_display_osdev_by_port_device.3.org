#+TITLE: Manpages - hwloc_gl_get_display_osdev_by_port_device.3
#+DESCRIPTION: Linux manpage for hwloc_gl_get_display_osdev_by_port_device.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_gl_get_display_osdev_by_port_device.3 is found in manpage for: [[../man3/hwlocality_gl.3][man3/hwlocality_gl.3]]