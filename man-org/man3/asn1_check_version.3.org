#+TITLE: Manpages - asn1_check_version.3
#+DESCRIPTION: Linux manpage for asn1_check_version.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
asn1_check_version - API function

* SYNOPSIS
*#include <libtasn1.h>*

*const char * asn1_check_version(const char * */req_version/*);*

* ARGUMENTS
- const char * req_version :: Required version number, or *NULL*.

* DESCRIPTION
Check that the version of the library is at minimum the requested one
and return the version string; return *NULL* if the condition is not
satisfied. If a *NULL* is passed to this function, no check is done, but
the version string is simply returned.

See *ASN1_VERSION* for a suitable /req_version/ string.

* RETURNS
Version string of run-time library, or *NULL* if the run-time library
does not meet the required version number.

* COPYRIGHT
Copyright © 2006-2021 Free Software Foundation, Inc..\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libtasn1* is maintained as a Texinfo manual.
If the *info* and *libtasn1* programs are properly installed at your
site, the command

#+begin_quote
  *info libtasn1*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libtasn1/manual/*
#+end_quote
