#+TITLE: Manpages - endfsent.3
#+DESCRIPTION: Linux manpage for endfsent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about endfsent.3 is found in manpage for: [[../man3/getfsent.3][man3/getfsent.3]]