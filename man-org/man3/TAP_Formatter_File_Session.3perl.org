#+TITLE: Manpages - TAP_Formatter_File_Session.3perl
#+DESCRIPTION: Linux manpage for TAP_Formatter_File_Session.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Formatter::File::Session - Harness output delegate for file output

* VERSION
Version 3.43

* DESCRIPTION
This provides file orientated output formatting for TAP::Harness. It is
particularly important when running with parallel tests, as it ensures
that test results are not interleaved, even when run verbosely.

* METHODS
** result
Stores results for later output, all together.

** close_test
When the test file finishes, outputs the summary, together.
