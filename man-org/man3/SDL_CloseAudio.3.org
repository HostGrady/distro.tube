#+TITLE: Manpages - SDL_CloseAudio.3
#+DESCRIPTION: Linux manpage for SDL_CloseAudio.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CloseAudio - Shuts down audio processing and closes the audio
device.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_CloseAudio*(*void*)

* DESCRIPTION
This function shuts down audio processing and closes the audio device.

* SEE ALSO
*SDL_OpenAudio*
