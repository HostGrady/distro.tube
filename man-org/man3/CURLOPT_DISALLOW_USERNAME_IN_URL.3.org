#+TITLE: Manpages - CURLOPT_DISALLOW_USERNAME_IN_URL.3
#+DESCRIPTION: Linux manpage for CURLOPT_DISALLOW_USERNAME_IN_URL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_DISALLOW_USERNAME_IN_URL - disallow specifying username in the
url

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle,
CURLOPT_DISALLOW_USERNAME_IN_URL, long disallow);

* DESCRIPTION
A long parameter set to 1 tells the library to not allow URLs that
include a username.

* DEFAULT
0 (disabled) - user names are allowed by default.

* PROTOCOLS
Several

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {

    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_setopt(curl, CURLOPT_DISALLOW_USERNAME_IN_URL, 1L);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.61.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

curl_easy_perform() will return CURLE_LOGIN_DENIED if this option is
enabled and a URL containing a username is specified.

* SEE ALSO
*libcurl-security*(3), *,*CURLOPT_PROTOCOLS*(3)*
