#+TITLE: Manpages - systempapername.3
#+DESCRIPTION: Linux manpage for systempapername.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systempapername, defaultpapername, systempapersizefile,
defaultpapersizefile - return names for managing system paper
information

* SYNOPSYS
#+begin_example
  #include <paper.h>

  const char* systempapername(void)
  const char* defaultpapername(void)

  const char* systempapersizefile(void)
  const char* defaultpapersizefile(void)
#+end_example

* DESCRIPTION
*systempapername()* returns the name of the default paper to be used by
a program. This name is obtained by looking in that order at the
*PAPERCONF* environment variable, at the contents of the file returned
by *systempapersizefile()* or by using *defaultpapername()* as a last
chance.

*defaultpapername()* returns the name of the default paper to use if it
is impossible to get its name from the environment.

*systempapersizefile()* returns the path of the papersize file to use.
This path is obtained by looking at the *PAPERCONF* environment variable
or by calling *defaultpapersizefile()*.

*defaultpapersizefile* returns the path of the default papersize file.

* ENVIRONMENT
- *PAPERSIZE* :: Paper size to use regardless of what the papersize file
  contains.

- *PAPERCONF* :: Full path to a file containing the paper size to use.

* FILES
- */etc/papersize* :: Contains the name of the system-wide default paper
  size to be used.

* SEE ALSO
*papersize*(5)
