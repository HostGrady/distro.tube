#+TITLE: Manpages - ccosh.3
#+DESCRIPTION: Linux manpage for ccosh.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ccosh, ccoshf, ccoshl - complex hyperbolic cosine

* SYNOPSIS
#+begin_example
  #include <complex.h>

  double complex ccosh(double complex z);
  float complex ccoshf(float complex z);
  long double complex ccoshl(long double complex z);

  Link with -lm.
#+end_example

* DESCRIPTION
These functions calculate the complex hyperbolic cosine of /z/.

The complex hyperbolic cosine function is defined as:

#+begin_example
      ccosh(z) = (exp(z)+exp(-z))/2
#+end_example

* VERSIONS
These functions first appeared in glibc in version 2.1.

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*cabs*(3), *cacosh*(3), *csinh*(3), *ctanh*(3), *complex*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
