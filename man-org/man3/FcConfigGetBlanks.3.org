#+TITLE: Manpages - FcConfigGetBlanks.3
#+DESCRIPTION: Linux manpage for FcConfigGetBlanks.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigGetBlanks - Get config blanks

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBlanks * FcConfigGetBlanks (FcConfig */config/*);*

* DESCRIPTION
FcBlanks is deprecated. This function always returns NULL.
