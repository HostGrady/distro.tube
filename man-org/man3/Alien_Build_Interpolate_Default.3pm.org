#+TITLE: Manpages - Alien_Build_Interpolate_Default.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Interpolate_Default.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Interpolate::Default - Default interpolator for
Alien::Build

* VERSION
version 2.44

* CONSTRUCTOR
** new
my $intr = Alien::Build::Interpolate::Default->new;

* HELPERS
** ar
%{ar}

The ar command.

** bison
%{bison}

Requires: Alien::bison 0.17 if not already in =PATH=.

** bzip2
%{bzip2}

Requires: Alien::Libbz2 0.04 if not already in =PATH=.

** cc
%{cc}

The C Compiler used to build Perl

** cmake
%{cmake}

Requires: Alien::CMake 0.07 if cmake is not already in =PATH=.

Deprecated: Use the Alien::Build::Plugin::Build::CMake plugin instead
(which will replace this helper with one that works with Alien::cmake3
that works better).

** cp
%{cp}

The copy command.

** devnull
%{devnull}

The null device, if available. On Unix style operating systems this will
be =/dev/null= on Windows it is =NUL=.

** flex
%{flex}

Requires: Alien::flex 0.08 if not already in =PATH=.

** gmake
%{gmake}

Requires: Alien::gmake 0.11

Deprecated: use Alien::Build::Plugin::Build::Make instead.

** install
%{install}

The Unix =install= command. Not normally available on Windows.

** ld
%{ld}

The linker used to build Perl

** m4
%{m4}

Requires: Alien::m4 0.08

Alien::m4 should pull in a version of =m4= that will work with
Autotools.

** make
%{make}

Make. On Unix this will be the same make used by Perl. On Windows this
will be =gmake= or =nmake= if those are available, and only =dmake= if
the first two are not available.

** make_path
%{make_path}

Make directory, including all parent directories as needed. This is
usually =mkdir -p= on Unix and simply =md= on windows.

** nasm
%{nasm}

Requires: Alien::nasm 0.11 if not already in the =PATH=.

** patch
%{patch}

Requires: Alien::patch 0.09 if not already in the =PATH=.

On Windows this will normally render =patch --binary=, which makes patch
work like it does on Unix.

** perl
%{perl}

Requires: Devel::FindPerl

** pkgconf
%{pkgconf}

Requires: Alien::pkgconf 0.06.

** cwd
%{cwd}

** sh
%{sh}

Unix style command interpreter (/bin/sh).

Deprecated: use the Alien::Build::Plugin::Build::MSYS plugin instead.

** rm
%{rm}

The remove command

** xz
%{xz}

Requires: Alien::xz 0.02 if not already in the =PATH=.

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
