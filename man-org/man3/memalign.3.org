#+TITLE: Manpages - memalign.3
#+DESCRIPTION: Linux manpage for memalign.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about memalign.3 is found in manpage for: [[../man3/posix_memalign.3][man3/posix_memalign.3]]