#+TITLE: Manpages - XtSetTypeConverter.3
#+DESCRIPTION: Linux manpage for XtSetTypeConverter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtSetTypeConverter.3 is found in manpage for: [[../man3/XtAppSetTypeConverter.3][man3/XtAppSetTypeConverter.3]]