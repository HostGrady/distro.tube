#+TITLE: Manpages - Encode_MIME_Name.3perl
#+DESCRIPTION: Linux manpage for Encode_MIME_Name.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Encode::MIME::NAME -- internally used by Encode

* SEE ALSO
I18N::Charset
