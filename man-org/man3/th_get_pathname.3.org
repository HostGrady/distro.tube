#+TITLE: Manpages - th_get_pathname.3
#+DESCRIPTION: Linux manpage for th_get_pathname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
th_get_pathname, th_get_uid, th_get_gid, th_get_mode, th_get_crc,
th_get_size, th_get_mtime, th_get_devmajor, th_get_devminor,
th_get_linkname - extract individual fields of a tar header

TH_ISREG, TH_ISLNK, TH_ISSYM, TH_ISCHR, TH_ISBLK, TH_ISDIR, TH_ISFIFO -
determine what kind of file a tar header refers to

TH_ISLONGNAME, TH_ISLONGLINK - determine whether the GNU extensions are
in use

* SYNOPSIS
*#include <libtar.h>*

*char *th_get_linkname(TAR **/t/*);*

*char *th_get_pathname(TAR **/t/*);*

*mode_t th_get_mode(TAR **/t/*);*

*uid_t th_get_uid(TAR **/t/*);*

*gid_t th_get_gid(TAR **/t/*);*

*int th_get_crc(TAR **/t/*);*

*off_t th_get_size(TAR **/t/*);*

*time_t th_get_mtime(TAR **/t/*);*

*major_t th_get_devmajor(TAR **/t/*);*

*minor_t th_get_devminor(TAR **/t/*);*

*int TH_ISREG(TAR **/t/*);*

*int TH_ISLNK(TAR **/t/*);*

*int TH_ISSYM(TAR **/t/*);*

*int TH_ISCHR(TAR **/t/*);*

*int TH_ISBLK(TAR **/t/*);*

*int TH_ISDIR(TAR **/t/*);*

*int TH_ISFIFO(TAR **/t/*);*

*int TH_ISLONGNAME(TAR **/t/*);*

*int TH_ISLONGLINK(TAR **/t/*);*

* VERSION
This man page documents version 1.2 of *libtar*.

* DESCRIPTION
The *th_get_**() functions extract individual fields from the current
tar header associated with the /TAR/ handle /t/.

The *TH_IS**() macros are used to evaluate what kind of file is pointed
to by the current tar header associated with the /TAR/ handle /t/.

The *TH_ISLONGNAME*() and *TH_ISLONGLINK*() macros evaluate whether or
not the GNU extensions are used by the current tar header associated
with the /TAR/ handle /t/. This is only relevant if the *TAR_GNU* option
was used when /tar_open/() was called.

* SEE ALSO
*tar_open*(3)
