#+TITLE: Manpages - XkbFreeServerMap.3
#+DESCRIPTION: Linux manpage for XkbFreeServerMap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbFreeServerMap - Free memory used by the server member of an
XkbDescRec structure

* SYNOPSIS
*void XkbFreeServerMap* *( XkbDescPtr */xkb/* ,* *unsigned int
*/which/* ,* *Bool */free_all/* );*

* ARGUMENTS
- /- xkb/ :: keyboard description containing server map to free

- /- which/ :: mask identifying components of map to free

- /- free_all/ :: True => free all server map components and server
  itself

* DESCRIPTION
The /XkbFreeServerMap/ function frees the specified components of server
map in the XkbDescRec structure specified by the /xkb/ parameter and
sets the corresponding structure component values to NULL. The /which
parameter specifies a combination of the server map masks and is a/
bitwise inclusive OR of the masks listed in Table 1. If /free_all/ is
True, /which/ is ignored and /XkbFreeServerMap/ frees every non-NULL
structure component in the server map, frees the XkbServerMapRec
structure referenced by the /server/ member of the /xkb/ parameter, and
sets the /server/ member to NULL.

TABLE

* STRUCTURES
The complete description of an Xkb keyboard is given by an XkbDescRec.
The component structures in the XkbDescRec represent the major Xkb
components.

#+begin_example
  typedef struct {
     struct _XDisplay * display;      /* connection to X server */
     unsigned short     flags;        /* private to Xkb, do not modify */
     unsigned short     device_spec;  /* device of interest */
     KeyCode            min_key_code; /* minimum keycode for device */
     KeyCode            max_key_code; /* maximum keycode for device */
     XkbControlsPtr     ctrls;        /* controls */
     XkbServerMapPtr    server;       /* server keymap */
     XkbClientMapPtr    map;          /* client keymap */
     XkbIndicatorPtr    indicators;   /* indicator map */
     XkbNamesPtr        names;        /* names for all components */
     XkbCompatMapPtr    compat;       /* compatibility map */
     XkbGeometryPtr     geom;         /* physical geometry of keyboard */
  } XkbDescRec, *XkbDescPtr;
#+end_example

The /display/ field points to an X display structure. The /flags field
is private to the library: modifying/ /flags/ may yield unpredictable
results. The /device_spec/ field specifies the device identifier of the
keyboard input device, or XkbUseCoreKeyboard, which specifies the core
keyboard device. The /min_key_code/ and /max_key_code/ fields specify
the least and greatest keycode that can be returned by the keyboard.

Each structure component has a corresponding mask bit that is used in
function calls to indicate that the structure should be manipulated in
some manner, such as allocating it or freeing it. These masks and their
relationships to the fields in the XkbDescRec are shown in Table 2.

TABLE

The Xkb server map contains the information the server needs to
interpret key events and is of type XkbServerMapRec:

#+begin_example
  #define XkbNumVirtualMods          16

  typedef struct {                    /* Server Map */
      unsigned short    num_acts;     /* # of occupied entries in acts */
      unsigned short    size_acts;    /* # of entries in acts */
      XkbAction *       acts;         /* linear 2d tables of key actions, 1 per keycode */
      XkbBehavior *     behaviors;    /* key behaviors,1 per keycode */
      unsigned short *  key_acts;     /* index into acts, 1 per keycode */
      unsigned char *   explicit;     /* explicit overrides of core remapping, 1 per key */
      unsigned char     vmods[XkbNumVirtualMods]; /* real mods bound to virtual mods */
      unsigned short *  vmodmap;      /* virtual mods bound to key, 1 per keycode*/
  } XkbServerMapRec, *XkbServerMapPtr;
#+end_example
