#+TITLE: Manpages - acl_error.3
#+DESCRIPTION: Linux manpage for acl_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function converts an ACL error code such as returned by the

function to a text message describing the error condition. In the
“POSIX” locale,

returns the following descriptions for the error codes.

“Multiple entries”

“Duplicate entries”

“Missing or wrong entry”

“Invalid entry type”

The

function returns a text message if the error code is recognized, and a
value of

otherwise.

This is a non-portable, Linux specific extension to the ACL manipulation
functions defined in IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned).

Written by
