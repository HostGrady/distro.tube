#+TITLE: Manpages - SDL_JoystickIndex.3
#+DESCRIPTION: Linux manpage for SDL_JoystickIndex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoystickIndex - Get the index of an SDL_Joystick.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_JoystickIndex*(*SDL_Joystick *joystick*);

* DESCRIPTION
Returns the index of a given *SDL_Joystick* structure.

* RETURN VALUE
Index number of the joystick.

* SEE ALSO
*SDL_JoystickOpen*
