#+TITLE: Manpages - sd_event_source_set_destroy_callback.3
#+DESCRIPTION: Linux manpage for sd_event_source_set_destroy_callback.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_event_source_set_destroy_callback,
sd_event_source_get_destroy_callback, sd_event_destroy_t - Define the
callback function for resource cleanup

* SYNOPSIS
#+begin_example
  #include <systemd/sd-event.h>
#+end_example

*typedef int (*sd_event_destroy_t)(void **/userdata/*);*

*int sd_event_source_set_destroy_callback(sd_event_source **/source/*,
sd_event_destroy_t */callback/*);*

*int sd_event_source_get_destroy_callback(sd_event_source **/source/*,
sd_event_destroy_t **/callback/*);*

* DESCRIPTION
*sd_event_source_set_destroy_callback()* sets /callback/ as the callback
function to be called right before the event source object /source/ is
deallocated. The /userdata/ pointer from the event source object will be
passed as the /userdata/ parameter. This pointer can be set by an
argument to the constructor functions, see *sd_event_add_io*(3), or
directly, see *sd_event_source_set_userdata*(3). This callback function
is called even if /userdata/ is *NULL*. Note that this callback is
invoked at a time where the event source object itself is already
invalidated, and executing operations or taking new references to the
event source object is not permissible.

*sd_event_source_get_destroy_callback()* returns the current callback
for /source/ in the /callback/ parameter.

* RETURN VALUE
On success, *sd_event_source_set_destroy_callback()* returns 0 or a
positive integer. On failure, it returns a negative errno-style error
code.

*sd_event_source_get_destroy_callback()* returns positive if the destroy
callback function is set, 0 if not. On failure, returns a negative
errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  The /source/ parameter is *NULL*.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-event*(3), *sd_event_add_io*(3),
*sd_event_add_time*(3), *sd_event_add_signal*(3),
*sd_event_add_child*(3), *sd_event_add_inotify*(3),
*sd_event_add_defer*(3), *sd_event_source_set_userdata*(3)
