#+TITLE: Manpages - fltk.3
#+DESCRIPTION: Linux manpage for fltk.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fltk - the fast light tool kit

* SYNOPSIS
The Fast Light Tool Kit ("FLTK", pronounced "fulltick") is a C++
graphical user interface toolkit for the X Window System, MacOS(r), and
Microsoft Windows(r) that supports OpenGL(r). It was originally
developed by Mr. Bill Spitzak and is currently maintained by a small
group of developers across the world with a central repository in the
US.

FLTK is provides under the terms of the GNU Library General Public
License, with the following exceptions:

#+begin_quote
  1. Modifications to the FLTK configure script, config header file, and
  makefiles by themselves to support a specific platform do not
  constitute a modified or derivative work.

  The authors do request that such modifications be contributed to the
  FLTK project - send all contributions through the "Software Trouble
  Report" on the following page:

  http://www.fltk.org/str.php

  2. Widgets that are subclassed from FLTK widgets do not constitute a
  derivative work.

  3. Static linking of applications and widgets to the FLTK library does
  not constitute a derivative work and does not require the author to
  provide source code for the application or widget, use the shared FLTK
  libraries, or link their applications or widgets against a
  user-supplied version of FLTK.

  If you link the application or widget to a modified version of FLTK,
  then the changes to FLTK must be provided under the terms of the LGPL
  in sections 1, 2, and 4.

  4. You do not have to provide a copy of the FLTK license with programs
  that are linked to the FLTK library, nor do you have to identify the
  FLTK license in your program or documentation as required by section 6
  of the LGPL.

  However, programs must still identify their use of FLTK. The following
  example statement can be included in user documentation to satisfy
  this requirement:

  [program/widget] is based in part on the work of the FLTK project
  (http://www.fltk.org).
#+end_quote

* SEE ALSO
fltk-config(1), fluid(1)\\
FLTK Programming Manual\\
FLTK Web Site, http://www.fltk.org/

* AUTHORS
Bill Spitzak and others.
