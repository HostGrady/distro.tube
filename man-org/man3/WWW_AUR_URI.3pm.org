#+TITLE: Manpages - WWW_AUR_URI.3pm
#+DESCRIPTION: Linux manpage for WWW_AUR_URI.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
WWW::AUR::URI - Generate dynamic URIs for accessing the AUR

* SYNOPSIS
use WWW::AUR::URI qw(:all); my $uri = pkgfile_uri(perl-www-aur); $uri =
pkgbuild_uri(perl-www-aur); $uri = pkgsubmit_uri(); $uri = pkg_uri(SB =>
n, O => 1234, https => YUP); # Override Scheme to use http... local
$WWW::AUR::URI::Scheme = http; my $httpsuri = pkgfile_uri(perl-www-aur);
print $httpsuri # "http://aur.archlinux.org/..."

* DESCRIPTION
This is a collection of functions used internally by other modules in
the WWW-AUR distribution. For advanced users only. Well... sorta.

* EXPORTS
This module exports nothing by default. You must explicitly import
functions or import the =all= tag to import all functions.

use WWW::AUR::URI qw( pkgfile_uri pkgbuild_uri pkgsubmit_uri pkg_uri
rpc_uri ); use WWW::AUR::URI qw( :all );

* FUNCTIONS
** pkgfile_uri
$URI = pkgfile_uri( $PKGNAME )

- $PKGNAME :: The name of the package.

- $URI :: The standard URI to the source package tarball.

** pkgbuild_uri
$URI = pkgbuild_uri( $PKGNAME )

- $PKGNAME :: The name of the package.

- $URI :: The standard URI to the conveniently extracted PKGBUILD file.

** pkg_uri
$URI = pkg_uri( %QUERY_PARAMS )

This generates a URI for the <https://aur.archlinux.org/packages/>
webpage. The one that shows package information and comments, etc.

- %QUERY_PARAMS :: You can supply whatever query parameters that you
  want. You might want to look at the AUR's HTML source to learn how
  they work.

- $URI :: The URI to /packages// with query parameters appended.

** pkgsubmit_uri
$URI = pkgsubmit_uri( )

This usually returns <https://aur.archlinux.org/submit/> but can be
changed with =$WWW::AUR::HOST= and =$WWW::AUR::URI::Scheme=.

- $URI :: The URI used for submitting packages.

** rpc_uri
$URI = rpc_uri( $METHOD, @ARGS )

Generates a URI for the <http://aur.archlinux.org/rpc.php> page.

- $METHOD :: The RPC method to use. Must be either /search/, /info/,
  /multiinfo/, or /msearch/.

1. The RPC arguments to give to the method. /multiinfo/ is the only
   method that uses all of the arguments given. The other methods only
   use the first argument and ignore the rest.

- $URI :: The URI to the rpc.php page with query parameters in place.

* PACKAGE VARIABLES
** Scheme
This scalar contains the URI scheme to be inserted into all generated
URIs. The default value is https.

- Example ::  # This prints a URI starting with http:// local
  $WWW::AUR::URI::Scheme = http; print pkgbuild_uri(perl-www-aur), "\n";

* SEE ALSO
WWW::AUR

* AUTHOR
Justin Davis, =<juster at cpan dot org>=

* BUGS
Please email me any bugs you find. I will try to fix them as quick as I
can.

* SUPPORT
Send me an email if you have any questions or need help.

* LICENSE AND COPYRIGHT
Copyright 2014 Justin Davis.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.
