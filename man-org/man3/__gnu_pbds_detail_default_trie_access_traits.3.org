#+TITLE: Manpages - __gnu_pbds_detail_default_trie_access_traits.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_default_trie_access_traits.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::default_trie_access_traits< Key > - Primary
template, default_trie_access_traits.

* SYNOPSIS
\\

* Detailed Description
** "template<typename Key>
\\
struct __gnu_pbds::detail::default_trie_access_traits< Key >"Primary
template, default_trie_access_traits.

Definition at line *135* of file *standard_policies.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
