#+TITLE: Manpages - unibi_dump.3
#+DESCRIPTION: Linux manpage for unibi_dump.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_dump - convert a terminal object to compiled terminfo data

* SYNOPSIS
#include <unibilium.h> size_t unibi_dump(const unibi_term *ut, char *p,
size_t n);

* DESCRIPTION
This function creates a compiled terminfo entry from /ut/. The output is
written to /p/, which must have room for at least /n/ bytes.

If all numeric properties in /ut/ have values that fit in 15 bits, the
traditional ncurses terminfo format is used (starting with the bytes
=1A 01=), otherwise the newer wide integer format (starting with the
bytes =1E 02=) is used.

* RETURN VALUE
=unibi_dump= returns the number of bytes required to store the terminfo
data. If this exceeds /n/, nothing is written to /p/. If the terminal
object can't be represented in terminfo format (e.g. because the string
table would be too large), the return value is =SIZE_MAX=.

* ERRORS
- "EINVAL" :: /ut/ can't be converted to terminfo format.

- "EFAULT" :: The resulting terminfo entry would be longer than /n/
  bytes.

* SEE ALSO
*unibilium.h* (3), *unibi_destroy* (3), *unibi_from_mem* (3)
