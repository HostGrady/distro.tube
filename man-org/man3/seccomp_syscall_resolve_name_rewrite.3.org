#+TITLE: Manpages - seccomp_syscall_resolve_name_rewrite.3
#+DESCRIPTION: Linux manpage for seccomp_syscall_resolve_name_rewrite.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about seccomp_syscall_resolve_name_rewrite.3 is found in manpage for: [[../man3/seccomp_syscall_resolve_name.3][man3/seccomp_syscall_resolve_name.3]]