#+TITLE: Manpages - pcre2_set_newline.3
#+DESCRIPTION: Linux manpage for pcre2_set_newline.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_set_newline(pcre2_compile_context *ccontext,
   uint32_t value);
#+end_example

* DESCRIPTION
This function sets the newline convention within a compile context. This
specifies which character(s) are recognized as newlines when compiling
and matching patterns. The second argument must be one of:

PCRE2_NEWLINE_CR Carriage return only PCRE2_NEWLINE_LF Linefeed only
PCRE2_NEWLINE_CRLF CR followed by LF only PCRE2_NEWLINE_ANYCRLF Any of
the above PCRE2_NEWLINE_ANY Any Unicode newline sequence
PCRE2_NEWLINE_NUL The NUL character (binary zero)

The result is zero for success or PCRE2_ERROR_BADDATA if the second
argument is invalid.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
