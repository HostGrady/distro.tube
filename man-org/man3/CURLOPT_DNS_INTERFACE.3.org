#+TITLE: Manpages - CURLOPT_DNS_INTERFACE.3
#+DESCRIPTION: Linux manpage for CURLOPT_DNS_INTERFACE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_DNS_INTERFACE - interface to speak DNS over

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DNS_INTERFACE, char
*ifname);

* DESCRIPTION
Pass a char * as parameter. Set the name of the network interface that
the DNS resolver should bind to. This must be an interface name (not an
address). Set this option to NULL to use the default setting (do not
bind to a specific interface).

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
    curl_easy_setopt(curl, CURLOPT_DNS_INTERFACE, "eth0");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.33.0. This option also requires that libcurl was built with a
resolver backend that supports this operation. The c-ares backend is the
only such one.

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_NOT_BUILT_IN if support was disabled at compile-time.

* SEE ALSO
*CURLOPT_DNS_SERVERS*(3), *CURLOPT_DNS_LOCAL_IP4*(3),
