#+TITLE: Manpages - archive_write_finish_entry.3
#+DESCRIPTION: Linux manpage for archive_write_finish_entry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

Close out the entry just written. In particular, this writes out the
final padding required by some formats. Ordinarily, clients never need
to call this, as it is called automatically by

and

as needed. For

handles, this flushes pending file attribute changes like modification
time.

This function returns

on success, or one of several non-zero error codes for errors. Specific
error codes include:

for operations that might succeed if retried,

for unusual conditions that do not prevent further operations, and

for serious errors that make remaining operations impossible.

Detailed error codes and textual descriptions are available from the

and

functions.
