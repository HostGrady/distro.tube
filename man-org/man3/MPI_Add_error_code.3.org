#+TITLE: Manpages - MPI_Add_error_code.3
#+DESCRIPTION: Linux manpage for MPI_Add_error_code.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Add_error_code* - Creates a new error code associated with
/errorclass/

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Add_error_code(int errorclass, int *errorcode)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_ADD_ERROR_CODE(ERRORCLASS, ERRORCODE, IERROR)
  	INTEGER  ERRORCLASS, ERRORCODE, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Add_error_code(errorclass, errorcode, ierror)
  	INTEGER, INTENT(IN) :: errorclass
  	INTEGER, INTENT(OUT) :: errorcode
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  int MPI::Add_error_code(int errorclass, int* errorcode)
#+end_example

* INPUT PARAMETER
- errorclass :: MPI error class (integer).

* OUTPUT PARAMETERS
- errorcode :: Error code returned by an MPI routine or an MPI error
  class (integer).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
Creates a new error code associated with /errorclass/ and returns its
value in /errorcode/.

* NOTES
No function is provided to free error codes, as it is not expected that
an application will create them in significant numbers.

The value returned is always greater than or equal to MPI_ERR_LASTCODE.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
#+begin_example
  MPI_Add_error_class
  MPI_Error_class

#+end_example
