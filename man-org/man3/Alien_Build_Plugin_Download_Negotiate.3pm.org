#+TITLE: Manpages - Alien_Build_Plugin_Download_Negotiate.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Download_Negotiate.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Download::Negotiate - Download negotiation plugin

* VERSION
version 2.44

* SYNOPSIS
use alienfile; share { start_url http://ftp.gnu.org/gnu/make; plugin
Download => ( filter => qr/^make-.*\.tar\.gz$/, version =>
qr/([0-9\.]+)/, ); };

* DESCRIPTION
This is a negotiator plugin for downloading packages from the internet.
This plugin picks the best Fetch, Decode and Prefer plugins to do the
actual work. Which plugins are picked depend on the properties you
specify, your platform and environment. It is usually preferable to use
a negotiator plugin rather than the Fetch, Decode and Prefer plugins
directly from your alienfile.

* PROPERTIES
** url
[DEPRECATED] use =start_url= instead.

The Initial URL for your package. This may be a directory listing
(either in HTML or ftp listing format) or the final tarball intended to
be downloaded.

** filter
This is a regular expression that lets you filter out files that you do
not want to consider downloading. For example, if the directory listing
contained tarballs and readme files like this:

foo-1.0.0.tar.gz foo-1.0.0.readme

You could specify a filter of =qr/\.tar\.gz$/= to make sure only
tarballs are considered for download.

** version
Regular expression to parse out the version from a filename. The regular
expression should store the result in =$1=.

Note: if you provide a =version= property, this plugin will assume that
you will be downloading an initial index to select package downloads
from. Depending on the protocol (and typically this is the case for http
and HTML) that may bring in additional dependencies. If start_url points
to a tarball or other archive directly (without needing to do through an
index selection process), it is recommended that you not specify this
property.

** ssl
If your initial URL does not need SSL, but you know ahead of time that a
subsequent request will need it (for example, if your directory listing
is on =http=, but includes links to =https= URLs), then you can set this
property to true, and the appropriate Perl SSL modules will be loaded.

** passive
If using FTP, attempt a passive mode transfer first, before trying an
active mode transfer.

** bootstrap_ssl
If set to true, then the download negotiator will avoid using plugins
that have a dependency on Net::SSLeay, or other Perl SSL modules. The
intent for this option is to allow OpenSSL to be alienized and be a
useful optional dependency for Net::SSLeay.

The implementation may improve over time, but as of this writing, this
option relies on you having a working =curl= or =wget= with SSL support
in your =PATH=.

** prefer
How to sort candidates for selection. This should be one of three types
of values:

- code reference :: This will be used as the prefer hook.

- true value :: Use Alien::Build::Plugin::Prefer::SortVersions.

- false value :: Don't set any preference at all. A hook must be
  installed, or another prefer plugin specified.

** decoder
Override the detected decoder.

* METHODS
** pick
my($fetch, @decoders) = $plugin->pick;

Returns the fetch plugin and any optional decoders that should be used.

* SEE ALSO
Alien::Build::Plugin::Prefer::BadVersion,
Alien::Build::Plugin::Prefer::GoodVersion

Alien::Build, alienfile, Alien::Build::MM, Alien

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
