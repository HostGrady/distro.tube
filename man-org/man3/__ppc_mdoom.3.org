#+TITLE: Manpages - __ppc_mdoom.3
#+DESCRIPTION: Linux manpage for __ppc_mdoom.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about __ppc_mdoom.3 is found in manpage for: [[../man3/__ppc_yield.3][man3/__ppc_yield.3]]