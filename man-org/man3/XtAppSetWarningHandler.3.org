#+TITLE: Manpages - XtAppSetWarningHandler.3
#+DESCRIPTION: Linux manpage for XtAppSetWarningHandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtAppSetWarningHandler.3 is found in manpage for: [[../man3/XtAppError.3][man3/XtAppError.3]]