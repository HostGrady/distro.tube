#+TITLE: Manpages - XtError.3
#+DESCRIPTION: Linux manpage for XtError.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtError, XtSetErrorHandler, XtSetWarningHandler, XtWarning - low-level
error handlers

* SYNTAX
#include <X11/Intrinsic.h>

void XtError(const char */message/);

void XtSetErrorHandler(XtErrorHandler /handler/);

void XtSetWarningHandler(XtErrorHandler /handler/);

void XtWarning(const char */message/);

* ARGUMENTS
- message :: Specifies the nonfatal error message that is to be
  reported.

- handler :: Specifies the new fatal error procedure, which should not
  return, or the nonfatal error procedure, which usually returns.

- message :: Specifies the message that is to be reported.

* DESCRIPTION
The *XtError* function has been superceded by *XtAppError*.

The *XtSetErrorHandler* function has been superceded by
*XtAppSetErrorHandler*.

The *XtSetWarningHandler* function has been superceded by
*XtAppSetWarningHandler*.

The *XtWarning* function has been superceded by *XtAppWarning*.

* SEE ALSO
*XtAppError*(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
