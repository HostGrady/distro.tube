#+TITLE: Manpages - std_student_t_distribution.3
#+DESCRIPTION: Linux manpage for std_student_t_distribution.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::student_t_distribution< _RealType > - A student_t_distribution
random number distribution.

* SYNOPSIS
\\

=#include <random.h>=

** Classes
struct *param_type*\\

** Public Types
typedef _RealType *result_type*\\

** Public Member Functions
*student_t_distribution* (_RealType __n)\\

*student_t_distribution* (const *param_type* &__p)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng, const
*param_type* &__p)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng, const *param_type* &__p)\\

*result_type* *max* () const\\
Returns the least upper bound value of the distribution.

*result_type* *min* () const\\
Returns the greatest lower bound value of the distribution.

_RealType *n* () const\\

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng)\\
Generating functions.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng, const *param_type*
&__p)\\

*param_type* *param* () const\\
Returns the parameter set of the distribution.

void *param* (const *param_type* &__param)\\
Sets the parameter set of the distribution.

void *reset* ()\\
Resets the distribution state.

** Friends
template<typename _RealType1 , typename _CharT , typename _Traits >
*std::basic_ostream*< _CharT, _Traits > & *operator<<*
(*std::basic_ostream*< _CharT, _Traits > &__os, const
*std::student_t_distribution*< _RealType1 > &__x)\\
Inserts a student_t_distribution random number distribution =__x= into
the output stream =__os=.

bool *operator==* (const *student_t_distribution* &__d1, const
*student_t_distribution* &__d2)\\
Return true if two Student t distributions have the same parameters and
the sequences that would be generated are equal.

template<typename _RealType1 , typename _CharT , typename _Traits >
*std::basic_istream*< _CharT, _Traits > & *operator>>*
(*std::basic_istream*< _CharT, _Traits > &__is,
*std::student_t_distribution*< _RealType1 > &__x)\\
Extracts a student_t_distribution random number distribution =__x= from
the input stream =__is=.

* Detailed Description
** "template<typename _RealType = double>
\\
class std::student_t_distribution< _RealType >"A student_t_distribution
random number distribution.

The formula for the normal probability mass function is: �

Definition at line *3296* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef _RealType
*std::student_t_distribution*< _RealType >::*result_type*
The type of the range of the distribution.

Definition at line *3303* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double> *std::student_t_distribution*<
_RealType >::*student_t_distribution* ()= [inline]=
Definition at line *3333* of file *random.h*.

** template<typename _RealType = double> *std::student_t_distribution*<
_RealType >::*student_t_distribution* (_RealType __n)= [inline]=,
= [explicit]=
Definition at line *3336* of file *random.h*.

** template<typename _RealType = double> *std::student_t_distribution*<
_RealType >::*student_t_distribution* (const *param_type* &
__p)= [inline]=, = [explicit]=
Definition at line *3341* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::student_t_distribution*< _RealType >::__generate (_ForwardIterator
__f, _ForwardIterator __t, _UniformRandomNumberGenerator &
__urng)= [inline]=
Definition at line *3414* of file *random.h*.

** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::student_t_distribution*< _RealType >::__generate (_ForwardIterator
__f, _ForwardIterator __t, _UniformRandomNumberGenerator & __urng, const
*param_type* & __p)= [inline]=
Definition at line *3421* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > void *std::student_t_distribution*<
_RealType >::__generate (*result_type* * __f, *result_type* * __t,
_UniformRandomNumberGenerator & __urng)= [inline]=
Definition at line *3428* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > void *std::student_t_distribution*<
_RealType >::__generate (*result_type* * __f, *result_type* * __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *3434* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::student_t_distribution*< _RealType >::max () const= [inline]=
Returns the least upper bound value of the distribution.

Definition at line *3388* of file *random.h*.

References *std::numeric_limits< _Tp >::max()*.

** template<typename _RealType = double> *result_type*
*std::student_t_distribution*< _RealType >::min () const= [inline]=
Returns the greatest lower bound value of the distribution.

Definition at line *3381* of file *random.h*.

References *std::numeric_limits< _Tp >::lowest()*.

** template<typename _RealType = double> _RealType
*std::student_t_distribution*< _RealType >::n () const= [inline]=
Definition at line *3359* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::student_t_distribution*< _RealType >::operator()
(_UniformRandomNumberGenerator & __urng)= [inline]=
Generating functions.

Definition at line *3396* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::student_t_distribution*< _RealType >::operator()
(_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *3401* of file *random.h*.

** template<typename _RealType = double> *param_type*
*std::student_t_distribution*< _RealType >::param () const= [inline]=
Returns the parameter set of the distribution.

Definition at line *3366* of file *random.h*.

** template<typename _RealType = double> void
*std::student_t_distribution*< _RealType >::param (const *param_type* &
__param)= [inline]=
Sets the parameter set of the distribution.

*Parameters*

#+begin_quote
  /__param/ The new parameter set of the distribution.
#+end_quote

Definition at line *3374* of file *random.h*.

** template<typename _RealType = double> void
*std::student_t_distribution*< _RealType >::reset ()= [inline]=
Resets the distribution state.

Definition at line *3349* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> template<typename _RealType1 ,
typename _CharT , typename _Traits > *std::basic_ostream*< _CharT,
_Traits > & operator<< (*std::basic_ostream*< _CharT, _Traits > & __os,
const *std::student_t_distribution*< _RealType1 > & __x)= [friend]=
Inserts a student_t_distribution random number distribution =__x= into
the output stream =__os=.

*Parameters*

#+begin_quote
  /__os/ An output stream.\\
  /__x/ A student_t_distribution random number distribution.
#+end_quote

*Returns*

#+begin_quote
  The output stream with the state of =__x= inserted or in an error
  state.
#+end_quote

** template<typename _RealType = double> bool operator== (const
*student_t_distribution*< _RealType > & __d1, const
*student_t_distribution*< _RealType > & __d2)= [friend]=
Return true if two Student t distributions have the same parameters and
the sequences that would be generated are equal.

Definition at line *3445* of file *random.h*.

** template<typename _RealType = double> template<typename _RealType1 ,
typename _CharT , typename _Traits > *std::basic_istream*< _CharT,
_Traits > & operator>> (*std::basic_istream*< _CharT, _Traits > & __is,
*std::student_t_distribution*< _RealType1 > & __x)= [friend]=
Extracts a student_t_distribution random number distribution =__x= from
the input stream =__is=.

*Parameters*

#+begin_quote
  /__is/ An input stream.\\
  /__x/ A student_t_distribution random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The input stream with =__x= extracted or in an error state.
#+end_quote

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
