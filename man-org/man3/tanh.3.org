#+TITLE: Manpages - tanh.3
#+DESCRIPTION: Linux manpage for tanh.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tanh, tanhf, tanhl - hyperbolic tangent function

* SYNOPSIS
#+begin_example
  #include <math.h>

  double tanh(double x);
  float tanhf(float x);
  long double tanhl(long double x);
#+end_example

Link with /-lm/.

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*tanhf*(), *tanhl*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L
          || /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

* DESCRIPTION
These functions return the hyperbolic tangent of /x/, which is defined
mathematically as:

#+begin_example
      tanh(x) = sinh(x) / cosh(x)
#+end_example

* RETURN VALUE
On success, these functions return the hyperbolic tangent of /x/.

If /x/ is a NaN, a NaN is returned.

If /x/ is +0 (-0), +0 (-0) is returned.

If /x/ is positive infinity (negative infinity), +1 (-1) is returned.

* ERRORS
No errors occur.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *tanh*(), *tanhf*(), *tanhl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

The variant returning /double/ also conforms to SVr4, 4.3BSD, C89.

* SEE ALSO
*acosh*(3), *asinh*(3), *atanh*(3), *cosh*(3), *ctanh*(3), *sinh*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
