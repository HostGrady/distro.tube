#+TITLE: Manpages - ldns_key_list_push_key.3
#+DESCRIPTION: Linux manpage for ldns_key_list_push_key.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_key_list_push_key, ldns_key_list_pop_key - manipulate ldns_key_list

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

bool ldns_key_list_push_key(ldns_key_list *key_list, ldns_key *key);

ldns_key* ldns_key_list_pop_key(ldns_key_list *key_list);

* DESCRIPTION
/ldns_key_list_push_key/() pushes a key to a keylist .br *key_list*: the
key_list to push to .br *key*: the key to push .br Returns false on
error, otherwise true

/ldns_key_list_pop_key/() pops the last rr from a keylist .br
*key_list*: the rr_list to pop from .br Returns NULL if nothing to pop.
Otherwise the popped RR

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_key/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
