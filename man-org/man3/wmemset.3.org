#+TITLE: Manpages - wmemset.3
#+DESCRIPTION: Linux manpage for wmemset.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wmemset - fill an array of wide-characters with a constant wide
character

* SYNOPSIS
#+begin_example
  #include <wchar.h>

  wchar_t *wmemset(wchar_t *wcs, wchar_t wc, size_t n);
#+end_example

* DESCRIPTION
The *wmemset*() function is the wide-character equivalent of the
*memset*(3) function. It fills the array of /n/ wide-characters starting
at /wcs/ with /n/ copies of the wide character /wc/.

* RETURN VALUE
*wmemset*() returns /wcs/.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface   | Attribute     | Value   |
| *wmemset*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99.

* SEE ALSO
*memset*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
