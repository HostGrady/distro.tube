#+TITLE: Manpages - archive_write_free.3
#+DESCRIPTION: Linux manpage for archive_write_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

Always returns

This marks the archive object as being unusable; after calling this
function, the only call that can succeed is

to release the resources. This can be used to speed recovery when the
archive creation must be aborted. Note that the created archive is
likely to be malformed in this case;

Complete the archive and invoke the close callback.

This is a deprecated synonym for

Invokes

if necessary, then releases all resources. If you need detailed
information about

failures, you should be careful to call it separately, as you cannot
obtain error information after

returns.

These functions return

on success, or

Detailed error codes and textual descriptions are available from the

and

functions.
