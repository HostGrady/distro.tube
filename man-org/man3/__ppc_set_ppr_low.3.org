#+TITLE: Manpages - __ppc_set_ppr_low.3
#+DESCRIPTION: Linux manpage for __ppc_set_ppr_low.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about __ppc_set_ppr_low.3 is found in manpage for: [[../man3/__ppc_set_ppr_med.3][man3/__ppc_set_ppr_med.3]]