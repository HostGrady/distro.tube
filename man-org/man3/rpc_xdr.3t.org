#+TITLE: Manpages - rpc_xdr.3t
#+DESCRIPTION: Linux manpage for rpc_xdr.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These routines are used for describing the RPC messages in XDR language.
They should normally be used by those who do not want to use the RPC
package directly. These routines return

if they succeed,

otherwise.

See

for the definition of the

data structure.

Used to translate between RPC reply messages and their external
representation. It includes the status of the RPC call in the XDR
language format. In the case of success, it also includes the call
results.

Used for describing

operating system credentials. It includes machine-name, uid, gid list,
etc.

Used for describing RPC call header messages. It encodes the static part
of the call message header in the XDR language format. It includes
information such as transaction ID, RPC version number, program and
version number.

Used for describing RPC call messages. This includes all the RPC call
information such as transaction ID, RPC version number, program number,
version number, authentication information, etc. This is normally used
by servers to determine information about the client RPC call.

Used for describing RPC opaque authentication information messages.

Used for describing RPC reply messages. It encodes the rejected RPC
message in the XDR language format. The message could be rejected either
because of version number mis-match or because of authentication errors.

Used for describing RPC reply messages. It translates between the RPC
reply message and its external representation. This reply could be
either an acceptance, rejection or

These functions are part of libtirpc.
