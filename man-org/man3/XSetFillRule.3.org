#+TITLE: Manpages - XSetFillRule.3
#+DESCRIPTION: Linux manpage for XSetFillRule.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetFillRule.3 is found in manpage for: [[../man3/XSetFillStyle.3][man3/XSetFillStyle.3]]