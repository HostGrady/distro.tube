#+TITLE: Manpages - __gnu_parallel__Nothing.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__Nothing.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_Nothing - Functor doing nothing.

* SYNOPSIS
\\

=#include <for_each_selectors.h>=

** Public Member Functions
template<typename _It > void *operator()* (_It __i)\\
Functor execution.

* Detailed Description
Functor doing nothing.

For some __reduction tasks (this is not a function object, but is passed
as __selector __dummy parameter.

Definition at line *288* of file *for_each_selectors.h*.

* Member Function Documentation
** template<typename _It > void __gnu_parallel::_Nothing::operator()
(_It __i)= [inline]=
Functor execution.

*Parameters*

#+begin_quote
  /__i/ iterator referencing object.
#+end_quote

Definition at line *294* of file *for_each_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
