#+TITLE: Manpages - zmq_poll.3
#+DESCRIPTION: Linux manpage for zmq_poll.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_poll - input/output multiplexing

* SYNOPSIS
*int zmq_poll (zmq_pollitem_t */*items/*, int */nitems/*, long
*/timeout/*);*

* DESCRIPTION
The /zmq_poll()/ function provides a mechanism for applications to
multiplex input/output events in a level-triggered fashion over a set of
sockets. Each member of the array pointed to by the /items/ argument is
a *zmq_pollitem_t* structure. The /nitems/ argument specifies the number
of items in the /items/ array. The *zmq_pollitem_t* structure is defined
as follows:

#+begin_quote
  #+begin_example
    typedef struct
    {
        void *socket;
        int fd;
        short events;
        short revents;
    } zmq_pollitem_t;
  #+end_example
#+end_quote

For each *zmq_pollitem_t* item, /zmq_poll()/ shall examine either the
0MQ socket referenced by /socket/ *or* the standard socket specified by
the file descriptor /fd/, for the event(s) specified in /events/. If
both /socket/ and /fd/ are set in a single *zmq_pollitem_t*, the 0MQ
socket referenced by /socket/ shall take precedence and the value of
/fd/ shall be ignored.

For each *zmq_pollitem_t* item, /zmq_poll()/ shall first clear the
/revents/ member, and then indicate any requested events that have
occurred by setting the bit corresponding to the event condition in the
/revents/ member.

If none of the requested events have occurred on any *zmq_pollitem_t*
item, /zmq_poll()/ shall wait /timeout/ milliseconds for an event to
occur on any of the requested items. If the value of /timeout/ is 0,
/zmq_poll()/ shall return immediately. If the value of /timeout/ is -1,
/zmq_poll()/ shall block indefinitely until a requested event has
occurred on at least one *zmq_pollitem_t*.

The /events/ and /revents/ members of *zmq_pollitem_t* are bit masks
constructed by OR'ing a combination of the following event flags:

*ZMQ_POLLIN*

#+begin_quote
  For 0MQ sockets, at least one message may be received from the
  /socket/ without blocking. For standard sockets this is equivalent to
  the /POLLIN/ flag of the /poll()/ system call and generally means that
  at least one byte of data may be read from /fd/ without blocking.
#+end_quote

*ZMQ_POLLOUT*

#+begin_quote
  For 0MQ sockets, at least one message may be sent to the /socket/
  without blocking. For standard sockets this is equivalent to the
  /POLLOUT/ flag of the /poll()/ system call and generally means that at
  least one byte of data may be written to /fd/ without blocking.
#+end_quote

*ZMQ_POLLERR*

#+begin_quote
  For standard sockets, this flag is passed through /zmq_poll()/ to the
  underlying /poll()/ system call and generally means that some sort of
  error condition is present on the socket specified by /fd/. For 0MQ
  sockets this flag has no effect if set in /events/, and shall never be
  returned in /revents/ by /zmq_poll()/.
#+end_quote

*ZMQ_POLLPRI*

#+begin_quote
  For 0MQ sockets this flags is of no use. For standard sockets this
  means there is urgent data to read. Refer to the POLLPRI flag for more
  informations. For file descriptor, refer to your use case: as an
  example, GPIO interrupts are signaled through a POLLPRI event. This
  flag has no effect on Windows.
#+end_quote

#+begin_quote
  \\

  *Note*

  \\

  The /zmq_poll()/ function may be implemented or emulated using
  operating system interfaces other than /poll()/, and as such may be
  subject to the limits of those interfaces in ways not defined in this
  documentation.
#+end_quote

* THREAD SAFETY
The *zmq_pollitem_t* array must only be used by the thread which will/is
calling /zmq_poll/.

If a socket is contained in multiple *zmq_pollitem_t* arrays, each owned
by a different thread, the socket itself needs to be thead-safe (Server,
Client, ...). Otherwise, behaviour is undefined.

* RETURN VALUE
Upon successful completion, the /zmq_poll()/ function shall return the
number of *zmq_pollitem_t* structures with events signaled in /revents/
or 0 if no events have been signaled. Upon failure, /zmq_poll()/ shall
return -1 and set /errno/ to one of the values defined below.

* ERRORS
*ETERM*

#+begin_quote
  At least one of the members of the /items/ array refers to a /socket/
  whose associated 0MQ /context/ was terminated.
#+end_quote

*EFAULT*

#+begin_quote
  The provided /items/ was not valid (NULL).
#+end_quote

*EINTR*

#+begin_quote
  The operation was interrupted by delivery of a signal before any
  events were available.
#+end_quote

* EXAMPLE
*Polling indefinitely for input events on both a 0MQ socket and a
standard socket.*.

#+begin_quote
  #+begin_example
    zmq_pollitem_t items [2];
    /* First item refers to 0MQ socket socket */
    items[0].socket = socket;
    items[0].events = ZMQ_POLLIN;
    /* Second item refers to standard socket fd */
    items[1].socket = NULL;
    items[1].fd = fd;
    items[1].events = ZMQ_POLLIN;
    /* Poll for events indefinitely */
    int rc = zmq_poll (items, 2, -1);
    assert (rc >= 0);
    /* Returned events will be stored in items[].revents */
  #+end_example
#+end_quote

* SEE ALSO
*zmq_socket*(3) *zmq_send*(3) *zmq_recv*(3) *zmq*(7)

Your operating system documentation for the /poll()/ system call.

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
