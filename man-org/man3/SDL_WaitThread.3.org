#+TITLE: Manpages - SDL_WaitThread.3
#+DESCRIPTION: Linux manpage for SDL_WaitThread.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WaitThread - Wait for a thread to finish.

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*void SDL_WaitThread*(*SDL_Thread *thread, int *status*);

* DESCRIPTION
Wait for a thread to finish (timeouts are not supported).

* RETURN VALUE
The return code for the thread function is placed in the area pointed to
by *status*, if *status* is not *NULL*.

* SEE ALSO
*SDL_CreateThread*
