#+TITLE: Manpages - XIGrabDevice.3
#+DESCRIPTION: Linux manpage for XIGrabDevice.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XIGrabDevice, XIUngrabDevice - grab or ungrab the device.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput2.h>
#+end_example

#+begin_example
  Status XIGrabDevice( Display *display,
                       int deviceid,
                       Window grab_window,
                       Time time,
                       Cursor cursor,
                       int grab_mode,
                       int paired_device_mode,
                       Bool owner_events,
                       XIEventMask *mask);
#+end_example

#+begin_example
  Status XIUngrabDevice( Display *display,
                         int deviceid,
                         Time time);
#+end_example

#+begin_example
  cursor
         Specifies the cursor image to display for the duration
         of the grab.
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  deviceid
         Specifies the device that should be grabbed or
         ungrabbed.
#+end_example

#+begin_example
  grab_mode, paired_device_mode
         The grab mode for this device and (if applicable) the
         paired device.
#+end_example

#+begin_example
  grab_window
         The grab window.
#+end_example

#+begin_example
  mask
         Event mask.
#+end_example

#+begin_example
  owner_events
         True if events are to be reported normally.
#+end_example

#+begin_example
  time
         A valid timestamp or CurrentTime.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    XIGrabDevice actively grabs control of the device. Further
    device events are reported only to the grabbing client.
    XIGrabDevice overrides any active device grab by this client.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If the device is an attached slave device, the device is
    automatically detached from the master device and reattached to
    the same master device when client ungrabs the device. If the
    master device is removed while the device is floating as a
    result of a grab, the device remains floating once the grab
    deactivates.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If owner_events is False, all generated device events are
    reported with respect to grab_window if selected. If
    owner_events is True and if a generated device event would
    normally be reported to this client, it is reported normally;
    otherwise, the event is reported with respect to the
    grab_window, and is only reported if specified in the event
    mask.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If the grab_mode argument is XIGrabModeAsync, device event
    processing continues as usual. If the device is currently
    frozen by this client, then processing of device events is
    resumed. If the grab_mode argument is XIGrabModeSync, the state
    of the device (as seen by client applications) appears to
    freeze, and the X server generates no further device events
    until the grabbing client issues a releasing XIAllowEvents call
    or until the device grab is released. Actual device changes are
    not lost while the device is frozen; they are simply queued in
    the server for later processing.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If paired_device_mode is XIGrabModeAsync, processing of events
    from the paired master device is unaffected by activation of
    the grab. If paired_device_mode is XIGrabModeSync, the state of
    the paired master device (as seen by client applications)
    appears to freeze, and the X server generates no further events
    from this device until the grabbing client issues a releasing
    XIAllowEvents call or until the device grab is released. Actual
    events are not lost while the devices are frozen; they are
    simply queued in the server for later processing. If the device
    is a slave device paired_device_mode is ignored.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If the device is actively grabbed by some other client,
    XIGrabDevice fails and returns AlreadyGrabbed. If grab_window
    is not viewable, it fails and returns GrabNotViewable. If the
    device is frozen by an active grab of another client, it fails
    and returns GrabFrozen. If the specified time is earlier than
    the last-device-grab time or later than the current X server
    time, it fails and returns GrabInvalidTime. Otherwise, the
    last-device-grab time is set to the specified time. CurrentTime
    is replaced by the current X server time.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If cursor is not None this cursor is displayed until the client
    calls XIUngrabDevice.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIGrabDevice can generate BadDevice, BadValue, and BadWindow
    errors.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XIUngrabDevice request releases the device and any queued
    events if this client has it actively grabbed from either
    XIGrabDevice or XIGrabKey or XIGrabButton. If other devices are
    frozen by the grab, XUngrabDevice thaws them. XUngrabDevice
    does not release the device and any queued events if the
    specified time is earlier than the last-device-grab time or is
    later than the current X server time. The X server
    automatically performs an XIUngrabDevice request if the event
    window for an active device grab becomes not viewable.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIUngrabDevice can generate a BadDevice error.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadValue
           A value is outside of the permitted range.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The device does not
           exist or is not a appropriate for the type of change.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadMatch
           The window is not viewable.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadWindow
           A value for a Window argument does not name a defined
           Window.
  #+end_example
#+end_quote

* BUGS

#+begin_quote
  #+begin_example
    The protocol headers for XI 2.0 did not provide
    XIGrabModeAsync or XIGrabModeSync. Use GrabModeSync and
    GrabModeAsync instead, respectively.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XIAllowEvents(3)
  #+end_example
#+end_quote
