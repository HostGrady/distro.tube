#+TITLE: Manpages - clock_getcpuclockid.3
#+DESCRIPTION: Linux manpage for clock_getcpuclockid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
clock_getcpuclockid - obtain ID of a process CPU-time clock

* SYNOPSIS
*#include <time.h>*

#+begin_example

  int clock_getcpuclockid(pid_t pid, clockid_t *clockid);
#+end_example

Link with /-lrt/ (only for glibc versions before 2.17).

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*clock_getcpuclockid*():

#+begin_example
      _POSIX_C_SOURCE >= 200112L
#+end_example

* DESCRIPTION
The *clock_getcpuclockid*() function obtains the ID of the CPU-time
clock of the process whose ID is /pid/, and returns it in the location
pointed to by /clockid/. If /pid/ is zero, then the clock ID of the
CPU-time clock of the calling process is returned.

* RETURN VALUE
On success, *clock_getcpuclockid*() returns 0; on error, it returns one
of the positive error numbers listed in ERRORS.

* ERRORS
- *ENOSYS* :: The kernel does not support obtaining the per-process
  CPU-time clock of another process, and /pid/ does not specify the
  calling process.

- *EPERM* :: The caller does not have permission to access the CPU-time
  clock of the process specified by /pid/. (Specified in POSIX.1-2001;
  does not occur on Linux unless the kernel does not support obtaining
  the per-process CPU-time clock of another process.)

- *ESRCH* :: There is no process with the ID /pid/.

* VERSIONS
The *clock_getcpuclockid*() function is available in glibc since version
2.2.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface               | Attribute     | Value   |
| *clock_getcpuclockid*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008.

* NOTES
Calling *clock_gettime*(2) with the clock ID obtained by a call to
*clock_getcpuclockid*() with a /pid/ of 0, is the same as using the
clock ID *CLOCK_PROCESS_CPUTIME_ID*.

* EXAMPLES
The example program below obtains the CPU-time clock ID of the process
whose ID is given on the command line, and then uses *clock_gettime*(2)
to obtain the time on that clock. An example run is the following:

#+begin_example
  $ ./a.out 1 # Show CPU clock of init process
  CPU-time clock for PID 1 is 2.213466748 seconds
#+end_example

** Program source
#+begin_example
  #define _XOPEN_SOURCE 600
  #include <stdint.h>
  #include <stdio.h>
  #include <unistd.h>
  #include <stdlib.h>
  #include <time.h>

  int
  main(int argc, char *argv[])
  {
      clockid_t clockid;
      struct timespec ts;

      if (argc != 2) {
          fprintf(stderr, "%s <process-ID>\n", argv[0]);
          exit(EXIT_FAILURE);
      }

      if (clock_getcpuclockid(atoi(argv[1]), &clockid) != 0) {
          perror("clock_getcpuclockid");
          exit(EXIT_FAILURE);
      }

      if (clock_gettime(clockid, &ts) == -1) {
          perror("clock_gettime");
          exit(EXIT_FAILURE);
      }

      printf("CPU-time clock for PID %s is %jd.%09ld seconds\n",
              argv[1], (intmax_t) ts.tv_sec, ts.tv_nsec);
      exit(EXIT_SUCCESS);
  }
#+end_example

* SEE ALSO
*clock_getres*(2), *timer_create*(2), *pthread_getcpuclockid*(3),
*time*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
