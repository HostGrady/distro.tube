#+TITLE: Manpages - SDL_EventState.3
#+DESCRIPTION: Linux manpage for SDL_EventState.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_EventState - This function allows you to set the state of processing
certain events.

* SYNOPSIS
*#include "SDL.h"*

*Uint8 SDL_EventState*(*Uint8 type, int state*);

* DESCRIPTION
This function allows you to set the state of processing certain event
*type*'s.

If *state* is set to *SDL_IGNORE*, that event *type* will be
automatically dropped from the event queue and will not be filtered.

If *state* is set to *SDL_ENABLE*, that event *type* will be processed
normally.

If *state* is set to *SDL_QUERY*, *SDL_EventState* will return the
current processing state of the specified event *type*.

A list of event *type*'s can be found in the *SDL_Event* section.

* SEE ALSO
*SDL_Event*
