#+TITLE: Manpages - std___detail__BracketMatcher.3
#+DESCRIPTION: Linux manpage for std___detail__BracketMatcher.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_BracketMatcher< _TraitsT, __icase, __collate > - Matches
a character range (bracket expression)

* SYNOPSIS
\\

=#include <regex_compiler.h>=

** Public Types
typedef _TraitsT::char_class_type *_CharClassT*\\

typedef _TransT::_CharT *_CharT*\\

typedef _TraitsT::string_type *_StringT*\\

typedef _TransT::_StrTransT *_StrTransT*\\

typedef _RegexTranslator< _TraitsT, __icase, __collate > *_TransT*\\

** Public Member Functions
*_BracketMatcher* (bool __is_non_matching, const _TraitsT &__traits)\\

void *_M_add_char* (_CharT __c)\\

void *_M_add_character_class* (const _StringT &__s, bool __neg)\\

_StringT *_M_add_collate_element* (const _StringT &__s)\\

void *_M_add_equivalence_class* (const _StringT &__s)\\

void *_M_make_range* (_CharT __l, _CharT __r)\\

void *_M_ready* ()\\

bool *operator()* (_CharT __ch) const\\

* Detailed Description
** "template<typename _TraitsT, bool __icase, bool __collate>
\\
struct std::__detail::_BracketMatcher< _TraitsT, __icase, __collate
>"Matches a character range (bracket expression)

Definition at line *413* of file *regex_compiler.h*.

* Member Typedef Documentation
** template<typename _TraitsT , bool __icase, bool __collate> typedef
_TraitsT::char_class_type *std::__detail::_BracketMatcher*< _TraitsT,
__icase, __collate >::_CharClassT
Definition at line *420* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> typedef
_TransT::_CharT *std::__detail::_BracketMatcher*< _TraitsT, __icase,
__collate >::_CharT
Definition at line *417* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> typedef
_TraitsT::string_type *std::__detail::_BracketMatcher*< _TraitsT,
__icase, __collate >::_StringT
Definition at line *419* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> typedef
_TransT::_StrTransT *std::__detail::_BracketMatcher*< _TraitsT, __icase,
__collate >::_StrTransT
Definition at line *418* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> typedef
_RegexTranslator<_TraitsT, __icase, __collate>
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::_TransT
Definition at line *416* of file *regex_compiler.h*.

* Constructor & Destructor Documentation
** template<typename _TraitsT , bool __icase, bool __collate>
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::*_BracketMatcher* (bool __is_non_matching, const _TraitsT &
__traits)= [inline]=
Definition at line *423* of file *regex_compiler.h*.

* Member Function Documentation
** template<typename _TraitsT , bool __icase, bool __collate> void
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::_M_add_char (_CharT __c)= [inline]=
Definition at line *437* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> void
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::_M_add_character_class (const _StringT & __s, bool __neg)= [inline]=
Definition at line *472* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> _StringT
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::_M_add_collate_element (const _StringT & __s)= [inline]=
Definition at line *444* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> void
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::_M_add_equivalence_class (const _StringT & __s)= [inline]=
Definition at line *457* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> void
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::_M_make_range (_CharT __l, _CharT __r)= [inline]=
Definition at line *488* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> void
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::_M_ready ()= [inline]=
Definition at line *499* of file *regex_compiler.h*.

** template<typename _TraitsT , bool __icase, bool __collate> bool
*std::__detail::_BracketMatcher*< _TraitsT, __icase, __collate
>::operator() (_CharT __ch) const= [inline]=
Definition at line *430* of file *regex_compiler.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
