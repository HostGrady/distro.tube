#+TITLE: Manpages - XvStopVideo.3
#+DESCRIPTION: Linux manpage for XvStopVideo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XvStopVideo - stop active video

* SYNOPSIS
*#include <X11/extensions/Xvlib.h>*

#+begin_example
  int XvStopVideo(Display *dpy, XvPortID port, Drawable draw);
#+end_example

* ARGUMENTS
- dpy :: Specifies the connection to the X server.

- port :: Specifies the port for which video is to be stopped.

- draw :: Specifies the drawable associated with the named port.

* DESCRIPTION
*XvStopVideo*(3) stops active video for the specified port and drawable.
If the port is not processing video, or if it is processing video in a
different drawable, the request is ignored. When video is stopped a
*XvVideoNotify*(3) event with detail XvStopped is generated for the
associated drawable.

* RETURN VALUES
- [Success] :: Returned if *XvStopVideo*(3) completed successfully.

- [XvBadExtension] :: Returned if the Xv extension is unavailable.

- [XvBadAlloc] :: Returned if *XvStopVideo*(3) failed to allocate memory
  to process the request.

* DIAGNOSTICS
- [XvBadPort] :: Generated if the requested port does not exist.

- [BadDrawable] :: Generated if the requested drawable does not exist.

* SEE ALSO
*XvGetVideo*(3), *XvPutVideo*(3), *XvVideoNotify*(3)
