#+TITLE: Manpages - CURLOPT_SSH_AUTH_TYPES.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSH_AUTH_TYPES.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSH_AUTH_TYPES - auth types for SFTP and SCP

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_AUTH_TYPES, long
bitmask);

* DESCRIPTION
Pass a long set to a bitmask consisting of one or more of
CURLSSH_AUTH_PUBLICKEY, CURLSSH_AUTH_PASSWORD, CURLSSH_AUTH_HOST,
CURLSSH_AUTH_KEYBOARD and CURLSSH_AUTH_AGENT.

Set /CURLSSH_AUTH_ANY/ to let libcurl pick a suitable one. Currently
CURLSSH_AUTH_HOST has no effect. If CURLSSH_AUTH_AGENT is used, libcurl
attempts to connect to ssh-agent or pageant and let the agent attempt
the authentication.

* DEFAULT
None

* PROTOCOLS
SFTP and SCP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
    curl_easy_setopt(curl, CURLOPT_SSH_AUTH_TYPES,
                     CURLSSH_AUTH_PUBLICKEY | CURLSSH_AUTH_KEYBOARD);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
CURLSSH_AUTH_HOST was added in 7.16.1, CURLSSH_AUTH_AGENT was added in
7.28.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_SSH_HOST_PUBLIC_KEY_MD5*(3), *CURLOPT_SSH_PUBLIC_KEYFILE*(3),
