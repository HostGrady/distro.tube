#+TITLE: Manpages - scalbf.3
#+DESCRIPTION: Linux manpage for scalbf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about scalbf.3 is found in manpage for: [[../man3/scalb.3][man3/scalb.3]]