#+TITLE: Manpages - ffi.3
#+DESCRIPTION: Linux manpage for ffi.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libffi, -lffi

The foreign function interface provides a mechanism by which a function
can generate a call to another function at runtime without requiring
knowledge of the called function's interface at compile time.
