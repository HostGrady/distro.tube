#+TITLE: Manpages - va_copy.3
#+DESCRIPTION: Linux manpage for va_copy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about va_copy.3 is found in manpage for: [[../man3/stdarg.3][man3/stdarg.3]]