#+TITLE: Manpages - gnutls_pkcs11_privkey_get_pk_algorithm.3
#+DESCRIPTION: Linux manpage for gnutls_pkcs11_privkey_get_pk_algorithm.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pkcs11_privkey_get_pk_algorithm - API function

* SYNOPSIS
*#include <gnutls/pkcs11.h>*

*int gnutls_pkcs11_privkey_get_pk_algorithm(gnutls_pkcs11_privkey_t
*/key/*, unsigned int * */bits/*);*

* ARGUMENTS
- gnutls_pkcs11_privkey_t key :: should contain a
  *gnutls_pkcs11_privkey_t* type

- unsigned int * bits :: if bits is non null it will hold the size of
  the parameters' in bits

* DESCRIPTION
This function will return the public key algorithm of a private key.

* RETURNS
a member of the *gnutls_pk_algorithm_t* enumeration on success, or a
negative error code on error.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
