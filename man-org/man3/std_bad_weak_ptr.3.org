#+TITLE: Manpages - std_bad_weak_ptr.3
#+DESCRIPTION: Linux manpage for std_bad_weak_ptr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bad_weak_ptr - Exception possibly thrown by =shared_ptr=.

* SYNOPSIS
\\

=#include <shared_ptr_base.h>=

Inherits *std::exception*.

** Public Member Functions
virtual char const * *what* () const noexcept\\

* Detailed Description
Exception possibly thrown by =shared_ptr=.

Definition at line *82* of file *shared_ptr_base.h*.

* Member Function Documentation
** virtual char const * std::bad_weak_ptr::what () const= [virtual]=,
= [noexcept]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented from *std::exception*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
