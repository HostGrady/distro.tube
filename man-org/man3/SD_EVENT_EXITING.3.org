#+TITLE: Manpages - SD_EVENT_EXITING.3
#+DESCRIPTION: Linux manpage for SD_EVENT_EXITING.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about SD_EVENT_EXITING.3 is found in manpage for: [[../sd_event_wait.3][sd_event_wait.3]]