#+TITLE: Manpages - auparse_get_type_name.3
#+DESCRIPTION: Linux manpage for auparse_get_type_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_get_type_name - get record's type translation

* SYNOPSIS
*#include <auparse.h>*

const char *auparse_get_type_name(auparse_state_t *au);

* DESCRIPTION
The auparse_get_type_name function will return the text representation
of the name of the current record type.

* RETURN VALUE
The auparse_get_type_name function returns NULL on error; otherwise a
pointer to a string.

* SEE ALSO
*auparse_get_type*(3),*auparse_next_record*(3).

* AUTHOR
Steve Grubb
