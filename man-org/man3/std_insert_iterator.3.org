#+TITLE: Manpages - std_insert_iterator.3
#+DESCRIPTION: Linux manpage for std_insert_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::insert_iterator< _Container > - Turns assignment into insertion.

* SYNOPSIS
\\

=#include <stl_iterator.h>=

Inherits *std::iterator< output_iterator_tag, void, void, void, void >*.

** Public Types
typedef _Container *container_type*\\
A nested typedef for the type of whatever container you used.

typedef void *difference_type*\\
Distance between iterators is represented as this type.

typedef *output_iterator_tag* *iterator_category*\\
One of the *tag types*.

typedef void *pointer*\\
This type represents a pointer-to-value_type.

typedef void *reference*\\
This type represents a reference-to-value_type.

typedef void *value_type*\\
The type 'pointed to' by the iterator.

** Public Member Functions
constexpr *insert_iterator* (_Container &__x, _Iter __i)\\

constexpr *insert_iterator* & *operator** ()\\
Simply returns *this.

constexpr *insert_iterator* & *operator++* ()\\
Simply returns *this. (This iterator does not /move/.)

constexpr *insert_iterator* & *operator++* (int)\\
Simply returns *this. (This iterator does not /move/.)

constexpr *insert_iterator* & *operator=* (const typename
_Container::value_type &__value)\\

constexpr *insert_iterator* & *operator=* (typename
_Container::value_type &&__value)\\

** Protected Attributes
_Container * *container*\\

_Iter *iter*\\

* Detailed Description
** "template<typename _Container>
\\
class std::insert_iterator< _Container >"Turns assignment into
insertion.

These are output iterators, constructed from a container-of-T. Assigning
a T to the iterator inserts it in the container at the iterator's
position, rather than overwriting the value at that position.

(Sequences will actually insert a /copy/ of the value before the
iterator's position.)

Tip: Using the inserter function to create these iterators can save
typing.

Definition at line *838* of file *bits/stl_iterator.h*.

* Member Typedef Documentation
** template<typename _Container > typedef _Container
*std::insert_iterator*< _Container >::*container_type*
A nested typedef for the type of whatever container you used.

Definition at line *857* of file *bits/stl_iterator.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*difference_type*= [inherited]=
Distance between iterators is represented as this type.

Definition at line *134* of file *stl_iterator_base_types.h*.

** typedef *output_iterator_tag* *std::iterator*< *output_iterator_tag*
, void , void , void , void >::*iterator_category*= [inherited]=
One of the *tag types*.

Definition at line *130* of file *stl_iterator_base_types.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*pointer*= [inherited]=
This type represents a pointer-to-value_type.

Definition at line *136* of file *stl_iterator_base_types.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*reference*= [inherited]=
This type represents a reference-to-value_type.

Definition at line *138* of file *stl_iterator_base_types.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*value_type*= [inherited]=
The type 'pointed to' by the iterator.

Definition at line *132* of file *stl_iterator_base_types.h*.

* Constructor & Destructor Documentation
** template<typename _Container > constexpr *std::insert_iterator*<
_Container >::*insert_iterator* (_Container & __x, _Iter
__i)= [inline]=, = [constexpr]=
The only way to create this iterator is with a container and an initial
position (a normal iterator into the container).

Definition at line *870* of file *bits/stl_iterator.h*.

* Member Function Documentation
** template<typename _Container > constexpr *insert_iterator* &
*std::insert_iterator*< _Container >::operator* ()= [inline]=,
= [constexpr]=
Simply returns *this.

Definition at line *927* of file *bits/stl_iterator.h*.

** template<typename _Container > constexpr *insert_iterator* &
*std::insert_iterator*< _Container >::operator++ ()= [inline]=,
= [constexpr]=
Simply returns *this. (This iterator does not /move/.)

Definition at line *933* of file *bits/stl_iterator.h*.

** template<typename _Container > constexpr *insert_iterator* &
*std::insert_iterator*< _Container >::operator++ (int)= [inline]=,
= [constexpr]=
Simply returns *this. (This iterator does not /move/.)

Definition at line *939* of file *bits/stl_iterator.h*.

** template<typename _Container > constexpr *insert_iterator* &
*std::insert_iterator*< _Container >::operator= (const typename
_Container::value_type & __value)= [inline]=, = [constexpr]=
*Parameters*

#+begin_quote
  /__value/ An instance of whatever type container_type::const_reference
  is; presumably a reference-to-const T for container<T>.
#+end_quote

*Returns*

#+begin_quote
  This iterator, for chained operations.
#+end_quote

This kind of iterator maintains its own position in the container.
Assigning a value to the iterator will insert the value into the
container at the place before the iterator.

The position is maintained such that subsequent assignments will insert
values immediately after one another. For example,

#+begin_example
  // vector v contains A and Z

  insert_iterator i (v, ++v.begin());
  i = 1;
  i = 2;
  i = 3;

  // vector v contains A, 1, 2, 3, and Z
#+end_example

Definition at line *907* of file *bits/stl_iterator.h*.

** template<typename _Container > constexpr *insert_iterator* &
*std::insert_iterator*< _Container >::operator= (typename
_Container::value_type && __value)= [inline]=, = [constexpr]=
Definition at line *916* of file *bits/stl_iterator.h*.

* Member Data Documentation
** template<typename _Container > _Container* *std::insert_iterator*<
_Container >::container= [protected]=
Definition at line *851* of file *bits/stl_iterator.h*.

** template<typename _Container > _Iter *std::insert_iterator*<
_Container >::iter= [protected]=
Definition at line *852* of file *bits/stl_iterator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
