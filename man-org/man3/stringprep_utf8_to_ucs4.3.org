#+TITLE: Manpages - stringprep_utf8_to_ucs4.3
#+DESCRIPTION: Linux manpage for stringprep_utf8_to_ucs4.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stringprep_utf8_to_ucs4 - API function

* SYNOPSIS
*#include <stringprep.h>*

*uint32_t * stringprep_utf8_to_ucs4(const char * */str/*, ssize_t
*/len/*, size_t * */items_written/*);*

* ARGUMENTS
- const char * str :: a UTF-8 encoded string

- ssize_t len :: the maximum length of /str/ to use. If /len/ < 0, then
  the string is nul-terminated.

- size_t * items_written :: location to store the number of characters
  in the result, or *NULL*.

* DESCRIPTION
Convert a string from UTF-8 to a 32-bit fixed width representation as
UCS-4. The function now performs error checking to verify that the input
is valid UTF-8 (before it was documented to not do error checking).

Return value: a pointer to a newly allocated UCS-4 string. This value
must be deallocated by the caller.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
