#+TITLE: Manpages - auparse_destroy.3
#+DESCRIPTION: Linux manpage for auparse_destroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_destroy - release instance of parser

* SYNOPSIS
*#include <auparse.h>*

*void auparse_destroy(auparse_state_t *au);*

*void auparse_destroy_ext(auparse_state_t *au, auparse_destroy_what_t
what);*

* DESCRIPTION
*auparse_destroy* frees all data structures and closes file descriptors.

*auparse_destroy_ext* frees data structures based on what. What can be
AUPARSE_DESTROY_ALL to release everything or AUPARSE_DESTROY_COMMON to
release everything but the uid and gid lookup cache.

* RETURN VALUE
None.

* SEE ALSO
*auparse_init*(3), *auparse_reset*(3).

* AUTHOR
Steve Grubb
