#+TITLE: Manpages - getaliasent.3
#+DESCRIPTION: Linux manpage for getaliasent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getaliasent.3 is found in manpage for: [[../man3/setaliasent.3][man3/setaliasent.3]]