#+TITLE: Manpages - XWithdrawWindow.3
#+DESCRIPTION: Linux manpage for XWithdrawWindow.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XWithdrawWindow.3 is found in manpage for: [[../man3/XIconifyWindow.3][man3/XIconifyWindow.3]]