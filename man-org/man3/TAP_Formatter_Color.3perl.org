#+TITLE: Manpages - TAP_Formatter_Color.3perl
#+DESCRIPTION: Linux manpage for TAP_Formatter_Color.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Formatter::Color - Run Perl test scripts with color

* VERSION
Version 3.43

* DESCRIPTION
Note that this harness is /experimental/. You may not like the colors
I've chosen and I haven't yet provided an easy way to override them.

This test harness is the same as TAP::Harness, but test results are
output in color. Passing tests are printed in green. Failing tests are
in red. Skipped tests are blue on a white background and TODO tests are
printed in white.

If Term::ANSIColor cannot be found (and Win32::Console::ANSI if running
under Windows) tests will be run without color.

* SYNOPSIS
use TAP::Formatter::Color; my $harness = TAP::Formatter::Color->new(
\%args ); $harness->runtests(@tests);

* METHODS
** Class Methods
/=new=/

The constructor returns a new =TAP::Formatter::Color= object. If
Term::ANSIColor is not installed, returns undef.

/=can_color=/

Test::Formatter::Color->can_color()

Returns a boolean indicating whether or not this module can actually
generate colored output. This will be false if it could not load the
modules needed for the current platform.

/=set_color=/

Set the output color.
