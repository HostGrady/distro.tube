#+TITLE: Manpages - fgetpos.3
#+DESCRIPTION: Linux manpage for fgetpos.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fgetpos.3 is found in manpage for: [[../man3/fseek.3][man3/fseek.3]]