#+TITLE: Manpages - xdr_enum.3
#+DESCRIPTION: Linux manpage for xdr_enum.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xdr_enum.3 is found in manpage for: [[../man3/xdr.3][man3/xdr.3]]