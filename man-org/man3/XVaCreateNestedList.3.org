#+TITLE: Manpages - XVaCreateNestedList.3
#+DESCRIPTION: Linux manpage for XVaCreateNestedList.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XVaCreateNestedList - allocate a nested variable argument list

* SYNTAX
typedef void * XVaNestedList;

XVaNestedList XVaCreateNestedList ( int /dummy/ , ... );

* ARGUMENTS
- dummy :: Specifies an unused argument (required by ANSI C).

- ... :: Specifies the variable length argument list.

* DESCRIPTION
The *XVaCreateNestedList* function allocates memory and copies its
arguments into a single list pointer, which may be used as a value for
arguments requiring a list value. Any entries are copied as specified.
Data passed by reference is not copied; the caller must ensure data
remains valid for the lifetime of the nested list. The list should be
freed using *XFree* when it is no longer needed.

* SEE ALSO
/Xlib - C Language X Interface/
