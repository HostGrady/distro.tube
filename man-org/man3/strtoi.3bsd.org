#+TITLE: Manpages - strtoi.3bsd
#+DESCRIPTION: Linux manpage for strtoi.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

function converts the string in

to an

value. The

function uses internally

and ensures that the result is always in the range [

]. In adddition it always places

on success or a conversion status in the

argument, avoiding the

gymnastics the other functions require. The

argument can be

if conversion status is to be ignored.

The string may begin with an arbitrary amount of white space (as
determined by

followed by a single optional

or

sign. If

is zero or 16, the string may then include a

or

prefix, and the number will be read in base 16; otherwise, a zero

is taken as 10 (decimal) unless the next character is

in which case it is taken as 8 (octal).

The remainder of the string is converted to a

value in the obvious manner, stopping at the first character which is
not a valid digit in the given base. (In bases above 10, the letter

in either upper or lower case represents 10,

represents 11, and so forth, with

representing 35.)

If

is non-nil,

stores the address of the first invalid character in

If there were no digits at all, however,

stores the original value of

in

(Thus, if

is not

but

is

on return, the entire string was valid.)

The

function always returns the closest value in the range specified by the

and

arguments.

The

value is guaranteed to be left unchanged.

Errors are stored as the conversion status in the

argument.

The following example will always return a number in

range no matter what the input is, and warn if the conversion failed.

int e; intmax_t lval = strtoi(buf, NULL, 0, 1, 99, &e); if (e) warnc(e,
"conversion of `%s' to a number failed, using %jd", buf, lval);

The string did not contain any characters that were converted.

The

is not between 2 and 36 and does not contain the special value 0.

The string contained non-numeric characters that did not get converted.
In this case,

points to the first unconverted character.

The given string was out of range; the value converted has been clamped;
or the range given was invalid, i.e.

>

The

function is a

extension.

The

function first appeared in

introduced the

function for the same purpose, but the interface makes it impossible to
properly differentiate illegal returns.

Ignores the current locale.
