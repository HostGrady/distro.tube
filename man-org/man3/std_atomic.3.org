#+TITLE: Manpages - std_atomic.3
#+DESCRIPTION: Linux manpage for std_atomic.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::atomic< _Tp > - Generic atomic type, primary class template.

* SYNOPSIS
\\

** Public Types
using *value_type* = _Tp\\

** Public Member Functions
constexpr *atomic* (_Tp __i) noexcept\\

*atomic* (const *atomic* &)=delete\\

bool *compare_exchange_strong* (_Tp &__e, _Tp __i, *memory_order*
__m=memory_order_seq_cst) noexcept\\

bool *compare_exchange_strong* (_Tp &__e, _Tp __i, *memory_order*
__m=memory_order_seq_cst) volatile noexcept\\

bool *compare_exchange_strong* (_Tp &__e, _Tp __i, *memory_order* __s,
*memory_order* __f) noexcept\\

bool *compare_exchange_strong* (_Tp &__e, _Tp __i, *memory_order* __s,
*memory_order* __f) volatile noexcept\\

bool *compare_exchange_weak* (_Tp &__e, _Tp __i, *memory_order*
__m=memory_order_seq_cst) noexcept\\

bool *compare_exchange_weak* (_Tp &__e, _Tp __i, *memory_order*
__m=memory_order_seq_cst) volatile noexcept\\

bool *compare_exchange_weak* (_Tp &__e, _Tp __i, *memory_order* __s,
*memory_order* __f) noexcept\\

bool *compare_exchange_weak* (_Tp &__e, _Tp __i, *memory_order* __s,
*memory_order* __f) volatile noexcept\\

_Tp *exchange* (_Tp __i, *memory_order* __m=memory_order_seq_cst)
noexcept\\

_Tp *exchange* (_Tp __i, *memory_order* __m=memory_order_seq_cst)
volatile noexcept\\

bool *is_lock_free* () const noexcept\\

bool *is_lock_free* () const volatile noexcept\\

_Tp *load* (*memory_order* __m=memory_order_seq_cst) const noexcept\\

_Tp *load* (*memory_order* __m=memory_order_seq_cst) const volatile
noexcept\\

*operator _Tp* () const noexcept\\

*operator _Tp* () const volatile noexcept\\

_Tp *operator=* (_Tp __i) noexcept\\

_Tp *operator=* (_Tp __i) volatile noexcept\\

*atomic* & *operator=* (const *atomic* &) volatile=delete\\

*atomic* & *operator=* (const *atomic* &)=delete\\

void *store* (_Tp __i, *memory_order* __m=memory_order_seq_cst)
noexcept\\

void *store* (_Tp __i, *memory_order* __m=memory_order_seq_cst) volatile
noexcept\\

** Static Public Attributes
static constexpr bool *is_always_lock_free*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::atomic< _Tp >"Generic atomic type, primary class template.

*Template Parameters*

#+begin_quote
  /_Tp/ Type to be made atomic, must be trivially copyable.
#+end_quote

Definition at line *196* of file *atomic*.

* Member Typedef Documentation
** template<typename _Tp > using *std::atomic*< _Tp >::value_type = _Tp
Definition at line *198* of file *atomic*.

* Constructor & Destructor Documentation
** template<typename _Tp > constexpr *std::atomic*< _Tp >::*atomic* (_Tp
__i)= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *231* of file *atomic*.

* Member Function Documentation
** template<typename _Tp > bool *std::atomic*< _Tp
>::compare_exchange_strong (_Tp & __e, _Tp __i, *memory_order* __m =
=memory_order_seq_cst=)= [inline]=, = [noexcept]=
Definition at line *372* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp
>::compare_exchange_strong (_Tp & __e, _Tp __i, *memory_order* __m =
=memory_order_seq_cst=) volatile= [inline]=, = [noexcept]=
Definition at line *378* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp
>::compare_exchange_strong (_Tp & __e, _Tp __i, *memory_order* __s,
*memory_order* __f)= [inline]=, = [noexcept]=
Definition at line *352* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp
>::compare_exchange_strong (_Tp & __e, _Tp __i, *memory_order* __s,
*memory_order* __f) volatile= [inline]=, = [noexcept]=
Definition at line *362* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp
>::compare_exchange_weak (_Tp & __e, _Tp __i, *memory_order* __m =
=memory_order_seq_cst=)= [inline]=, = [noexcept]=
Definition at line *340* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp
>::compare_exchange_weak (_Tp & __e, _Tp __i, *memory_order* __m =
=memory_order_seq_cst=) volatile= [inline]=, = [noexcept]=
Definition at line *346* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp
>::compare_exchange_weak (_Tp & __e, _Tp __i, *memory_order* __s,
*memory_order* __f)= [inline]=, = [noexcept]=
Definition at line *320* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp
>::compare_exchange_weak (_Tp & __e, _Tp __i, *memory_order* __s,
*memory_order* __f) volatile= [inline]=, = [noexcept]=
Definition at line *330* of file *atomic*.

** template<typename _Tp > _Tp *std::atomic*< _Tp >::exchange (_Tp __i,
*memory_order* __m = =memory_order_seq_cst=)= [inline]=, = [noexcept]=
Definition at line *299* of file *atomic*.

** template<typename _Tp > _Tp *std::atomic*< _Tp >::exchange (_Tp __i,
*memory_order* __m = =memory_order_seq_cst=) volatile= [inline]=,
= [noexcept]=
Definition at line *309* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp >::is_lock_free ()
const= [inline]=, = [noexcept]=
Definition at line *248* of file *atomic*.

** template<typename _Tp > bool *std::atomic*< _Tp >::is_lock_free ()
const volatile= [inline]=, = [noexcept]=
Definition at line *256* of file *atomic*.

** template<typename _Tp > _Tp *std::atomic*< _Tp >::load
(*memory_order* __m = =memory_order_seq_cst=) const= [inline]=,
= [noexcept]=
Definition at line *281* of file *atomic*.

** template<typename _Tp > _Tp *std::atomic*< _Tp >::load
(*memory_order* __m = =memory_order_seq_cst=) const volatile= [inline]=,
= [noexcept]=
Definition at line *290* of file *atomic*.

** template<typename _Tp > *std::atomic*< _Tp >::operator _Tp ()
const= [inline]=, = [noexcept]=
Definition at line *233* of file *atomic*.

** template<typename _Tp > *std::atomic*< _Tp >::operator _Tp () const
volatile= [inline]=, = [noexcept]=
Definition at line *236* of file *atomic*.

** template<typename _Tp > _Tp *std::atomic*< _Tp >::operator= (_Tp
__i)= [inline]=, = [noexcept]=
Definition at line *240* of file *atomic*.

** template<typename _Tp > _Tp *std::atomic*< _Tp >::operator= (_Tp __i)
volatile= [inline]=, = [noexcept]=
Definition at line *244* of file *atomic*.

** template<typename _Tp > void *std::atomic*< _Tp >::store (_Tp __i,
*memory_order* __m = =memory_order_seq_cst=)= [inline]=, = [noexcept]=
Definition at line *269* of file *atomic*.

** template<typename _Tp > void *std::atomic*< _Tp >::store (_Tp __i,
*memory_order* __m = =memory_order_seq_cst=) volatile= [inline]=,
= [noexcept]=
Definition at line *275* of file *atomic*.

* Member Data Documentation
** template<typename _Tp > constexpr bool *std::atomic*< _Tp
>::is_always_lock_free= [static]=, = [constexpr]=
Definition at line *264* of file *atomic*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
