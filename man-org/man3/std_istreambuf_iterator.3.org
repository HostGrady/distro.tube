#+TITLE: Manpages - std_istreambuf_iterator.3
#+DESCRIPTION: Linux manpage for std_istreambuf_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::istreambuf_iterator< _CharT, _Traits > - Provides input iterator
semantics for streambufs.

* SYNOPSIS
\\

=#include <streambuf_iterator.h>=

Inherits *std::iterator< input_iterator_tag, _CharT, _Traits::off_type,
_CharT *, _CharT >*.

** Public Types
typedef _Traits::off_type *difference_type*\\
Distance between iterators is represented as this type.

typedef *input_iterator_tag* *iterator_category*\\
One of the *tag types*.

typedef _CharT * *pointer*\\
This type represents a pointer-to-value_type.

typedef _CharT *reference*\\
This type represents a reference-to-value_type.

typedef _CharT *value_type*\\
The type 'pointed to' by the iterator.

\\

typedef _CharT *char_type*\\
Public typedefs.

typedef _Traits *traits_type*\\
Public typedefs.

typedef _Traits::int_type *int_type*\\
Public typedefs.

typedef *basic_streambuf*< _CharT, _Traits > *streambuf_type*\\
Public typedefs.

typedef *basic_istream*< _CharT, _Traits > *istream_type*\\
Public typedefs.

** Public Member Functions
constexpr *istreambuf_iterator* () noexcept\\
Construct end of input stream iterator.

*istreambuf_iterator* (const *istreambuf_iterator* &) noexcept=default\\

*istreambuf_iterator* (*istream_type* &__s) noexcept\\
Construct start of input stream iterator.

*istreambuf_iterator* (*streambuf_type* *__s) noexcept\\
Construct start of streambuf iterator.

bool *equal* (const *istreambuf_iterator* &__b) const\\
Return true both iterators are end or both are not end.

*char_type* *operator** () const\\
Return the current character pointed to by iterator. This returns
streambuf.sgetc(). It cannot be assigned. NB: The result of operator*()
on an end of stream is undefined.

*istreambuf_iterator* & *operator++* ()\\
Advance the iterator. Calls streambuf.sbumpc().

*istreambuf_iterator* *operator++* (int)\\
Advance the iterator. Calls streambuf.sbumpc().

*istreambuf_iterator* & *operator=* (const *istreambuf_iterator* &)
noexcept=default\\

** Friends
template<bool _IsMove, typename _CharT2 > __gnu_cxx::__enable_if<
__is_char< _CharT2 >::__value, _CharT2 * >::__type *__copy_move_a2*
(*istreambuf_iterator*< _CharT2 >, *istreambuf_iterator*< _CharT2 >,
_CharT2 *)\\

template<typename _CharT2 , typename _Size > __gnu_cxx::__enable_if<
__is_char< _CharT2 >::__value, _CharT2 * >::__type *__copy_n_a*
(*istreambuf_iterator*< _CharT2 >, _Size, _CharT2 *, bool)\\

template<typename _CharT2 , typename _Distance > __gnu_cxx::__enable_if<
__is_char< _CharT2 >::__value, void >::__type *advance*
(*istreambuf_iterator*< _CharT2 > &, _Distance)\\

template<typename _CharT2 > __gnu_cxx::__enable_if< __is_char< _CharT2
>::__value, *ostreambuf_iterator*< _CharT2 > >::__type *copy*
(*istreambuf_iterator*< _CharT2 >, *istreambuf_iterator*< _CharT2 >,
*ostreambuf_iterator*< _CharT2 >)\\

template<typename _CharT2 > __gnu_cxx::__enable_if< __is_char< _CharT2
>::__value, *istreambuf_iterator*< _CharT2 > >::__type *find*
(*istreambuf_iterator*< _CharT2 >, *istreambuf_iterator*< _CharT2 >,
const _CharT2 &)\\

* Detailed Description
** "template<typename _CharT, typename _Traits>
\\
class std::istreambuf_iterator< _CharT, _Traits >"Provides input
iterator semantics for streambufs.

Definition at line *50* of file *streambuf_iterator.h*.

* Member Typedef Documentation
** template<typename _CharT , typename _Traits > typedef _CharT
*std::istreambuf_iterator*< _CharT, _Traits >::*char_type*
Public typedefs.

Definition at line *66* of file *streambuf_iterator.h*.

** typedef _Traits::off_type *std::iterator*< *input_iterator_tag* ,
_CharT , _Traits::off_type , _CharT * , _CharT
>::*difference_type*= [inherited]=
Distance between iterators is represented as this type.

Definition at line *134* of file *stl_iterator_base_types.h*.

** template<typename _CharT , typename _Traits > typedef
_Traits::int_type *std::istreambuf_iterator*< _CharT, _Traits
>::*int_type*
Public typedefs.

Definition at line *68* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > typedef
*basic_istream*<_CharT, _Traits> *std::istreambuf_iterator*< _CharT,
_Traits >::*istream_type*
Public typedefs.

Definition at line *70* of file *streambuf_iterator.h*.

** typedef *input_iterator_tag* *std::iterator*< *input_iterator_tag* ,
_CharT , _Traits::off_type , _CharT * , _CharT
>::*iterator_category*= [inherited]=
One of the *tag types*.

Definition at line *130* of file *stl_iterator_base_types.h*.

** typedef _CharT * *std::iterator*< *input_iterator_tag* , _CharT ,
_Traits::off_type , _CharT * , _CharT >::*pointer*= [inherited]=
This type represents a pointer-to-value_type.

Definition at line *136* of file *stl_iterator_base_types.h*.

** typedef _CharT *std::iterator*< *input_iterator_tag* , _CharT ,
_Traits::off_type , _CharT * , _CharT >::*reference*= [inherited]=
This type represents a reference-to-value_type.

Definition at line *138* of file *stl_iterator_base_types.h*.

** template<typename _CharT , typename _Traits > typedef
*basic_streambuf*<_CharT, _Traits> *std::istreambuf_iterator*< _CharT,
_Traits >::*streambuf_type*
Public typedefs.

Definition at line *69* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > typedef _Traits
*std::istreambuf_iterator*< _CharT, _Traits >::*traits_type*
Public typedefs.

Definition at line *67* of file *streambuf_iterator.h*.

** typedef _CharT *std::iterator*< *input_iterator_tag* , _CharT ,
_Traits::off_type , _CharT * , _CharT >::*value_type*= [inherited]=
The type 'pointed to' by the iterator.

Definition at line *132* of file *stl_iterator_base_types.h*.

* Constructor & Destructor Documentation
** template<typename _CharT , typename _Traits > constexpr
*std::istreambuf_iterator*< _CharT, _Traits >::*istreambuf_iterator*
()= [inline]=, = [constexpr]=, = [noexcept]=
Construct end of input stream iterator.

Definition at line *114* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits >
*std::istreambuf_iterator*< _CharT, _Traits >::*istreambuf_iterator*
(*istream_type* & __s)= [inline]=, = [noexcept]=
Construct start of input stream iterator.

Definition at line *129* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits >
*std::istreambuf_iterator*< _CharT, _Traits >::*istreambuf_iterator*
(*streambuf_type* * __s)= [inline]=, = [noexcept]=
Construct start of streambuf iterator.

Definition at line *133* of file *streambuf_iterator.h*.

* Member Function Documentation
** template<typename _CharT , typename _Traits > bool
*std::istreambuf_iterator*< _CharT, _Traits >::equal (const
*istreambuf_iterator*< _CharT, _Traits > & __b) const= [inline]=
Return true both iterators are end or both are not end.

Definition at line *193* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > *char_type*
*std::istreambuf_iterator*< _CharT, _Traits >::operator* ()
const= [inline]=
Return the current character pointed to by iterator. This returns
streambuf.sgetc(). It cannot be assigned. NB: The result of operator*()
on an end of stream is undefined.

Definition at line *145* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > *istreambuf_iterator* &
*std::istreambuf_iterator*< _CharT, _Traits >::operator++ ()= [inline]=
Advance the iterator. Calls streambuf.sbumpc().

Definition at line *161* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > *istreambuf_iterator*
*std::istreambuf_iterator*< _CharT, _Traits >::operator++
(int)= [inline]=
Advance the iterator. Calls streambuf.sbumpc().

Definition at line *175* of file *streambuf_iterator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
