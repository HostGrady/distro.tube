#+TITLE: Manpages - Date_Language.3pm
#+DESCRIPTION: Linux manpage for Date_Language.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Date::Language - Language specific date formating and parsing

* SYNOPSIS
use Date::Language; my $lang = Date::Language->new(German);
$lang->time2str("%a %b %e %T %Y\n", time);

* DESCRIPTION
Date::Language provides objects to parse and format dates for specific
languages. Available languages are

Afar French Russian_cp1251 Amharic Gedeo Russian_koi8r Austrian German
Sidama Brazilian Greek Somali Chinese Hungarian Spanish Chinese_GB
Icelandic Swedish Czech Italian Tigrinya Danish Norwegian
TigrinyaEritrean Dutch Oromo TigrinyaEthiopian English Romanian Turkish
Finnish Russian Bulgarian Occitan

* METHODS
- time2str :: See time2str in Date::Format

- strftime :: See strftime in Date::Format

- ctime :: See ctime in Date::Format

- asctime :: See asctime in Date::Format

- str2time :: See str2time in Date::Parse

- strptime :: See strptime in Date::Parse
