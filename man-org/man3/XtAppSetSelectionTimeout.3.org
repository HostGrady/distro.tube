#+TITLE: Manpages - XtAppSetSelectionTimeout.3
#+DESCRIPTION: Linux manpage for XtAppSetSelectionTimeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtAppSetSelectionTimeout.3 is found in manpage for: [[../man3/XtAppGetSelectionTimeout.3][man3/XtAppGetSelectionTimeout.3]]