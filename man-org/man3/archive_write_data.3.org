#+TITLE: Manpages - archive_write_data.3
#+DESCRIPTION: Linux manpage for archive_write_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

Write data corresponding to the header just written.

Write data corresponding to the header just written. This is like

except that it performs a seek on the file being written to the
specified offset before writing the data. This is useful when restoring
sparse files from archive formats that support sparse files. Returns
number of bytes written or -1 on error. (Note: This is currently not
supported for

handles, only for

handles.

This function returns the number of bytes actually written, or a
negative error code on error.

Detailed error codes and textual descriptions are available from the

and

functions.

In libarchive 3.x, this function sometimes returns zero on success
instead of returning the number of bytes written. Specifically, this
occurs when writing to an

handle. Clients should treat any value less than zero as an error and
consider any non-negative value as success.
