#+TITLE: Manpages - XtAppAddActions.3
#+DESCRIPTION: Linux manpage for XtAppAddActions.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtAppAddActions - register an action table

* SYNTAX
#include <X11/Intrinsic.h>

void XtAppAddActions(XtAppContext /app_context/, XtActionList /actions/,
Cardinal /num_actions/);

* ARGUMENTS
- app_context :: Specifies the application context.

- actions :: Specifies the action table to register.

- num_args :: Specifies the number of entries in this action table.

* DESCRIPTION
The *XtAppAddActions* function adds the specified action table and
registers it with the translation manager.

* SEE ALSO
XtParseTranslationTable(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
