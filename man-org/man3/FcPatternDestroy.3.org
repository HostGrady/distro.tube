#+TITLE: Manpages - FcPatternDestroy.3
#+DESCRIPTION: Linux manpage for FcPatternDestroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternDestroy - Destroy a pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcPatternDestroy (FcPattern */p/*);*

* DESCRIPTION
Decrement the pattern reference count. If all references are gone,
destroys the pattern, in the process destroying all related values.
