#+TITLE: Manpages - SSL_SESSION_get0_id_context.3ssl
#+DESCRIPTION: Linux manpage for SSL_SESSION_get0_id_context.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_SESSION_get0_id_context, SSL_SESSION_set1_id_context - get and set
the SSL ID context associated with a session

* SYNOPSIS
#include <openssl/ssl.h> const unsigned char
*SSL_SESSION_get0_id_context(const SSL_SESSION *s, unsigned int *len)
int SSL_SESSION_set1_id_context(SSL_SESSION *s, const unsigned char
*sid_ctx, unsigned int sid_ctx_len);

* DESCRIPTION
See *SSL_CTX_set_session_id_context* (3) for further details on session
ID contexts.

*SSL_SESSION_get0_id_context()* returns the ID context associated with
the SSL/TLS session *s*. The length of the ID context is written to
**len* if *len* is not NULL.

The value returned is a pointer to an object maintained within *s* and
should not be released.

*SSL_SESSION_set1_id_context()* takes a copy of the provided ID context
given in *sid_ctx* and associates it with the session *s*. The length of
the ID context is given by *sid_ctx_len* which must not exceed
SSL_MAX_SID_CTX_LENGTH bytes.

* RETURN VALUES
*SSL_SESSION_set1_id_context()* returns 1 on success or 0 on error.

* SEE ALSO
*ssl* (7), *SSL_set_session_id_context* (3)

* HISTORY
The *SSL_SESSION_get0_id_context()* function was added in OpenSSL 1.1.0.

* COPYRIGHT
Copyright 2015-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
