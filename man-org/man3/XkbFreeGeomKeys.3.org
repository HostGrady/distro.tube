#+TITLE: Manpages - XkbFreeGeomKeys.3
#+DESCRIPTION: Linux manpage for XkbFreeGeomKeys.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbFreeGeomKeys - Free geometry keys

* SYNOPSIS
*void XkbFreeGeomKeys* *( XkbRowPtr */row/* ,* *int */first/* ,* *int
*/count/* ,* *Bool */free_all/* );*

* ARGUMENTS
- /- row/ :: row in which keys should be freed

- /- first/ :: first key to be freed

- /- count/ :: number of keys to be freed

- /- free_all/ :: True => all keys are freed

* DESCRIPTION
If /free_all/ is True, all keys are freed regardless of the value of
/first/ or /count./ Otherwise, /count/ keys are freed beginning with the
one specified by /first./
