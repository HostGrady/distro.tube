#+TITLE: Manpages - std_filesystem_recursive_directory_iterator.3
#+DESCRIPTION: Linux manpage for std_filesystem_recursive_directory_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::filesystem::recursive_directory_iterator - Iterator type for
recursively traversing a directory hierarchy.

* SYNOPSIS
\\

=#include <fs_dir.h>=

** Public Types
typedef ptrdiff_t *difference_type*\\

typedef *input_iterator_tag* *iterator_category*\\

typedef const *directory_entry* * *pointer*\\

typedef const *directory_entry* & *reference*\\

typedef *directory_entry* *value_type*\\

** Public Member Functions
*recursive_directory_iterator* (const *path* &__p)\\

*recursive_directory_iterator* (const *path* &__p, directory_options
__options)\\

*recursive_directory_iterator* (const *path* &__p, directory_options
__options, *error_code* &__ec)\\

*recursive_directory_iterator* (const *path* &__p, *error_code* &__ec)\\

*recursive_directory_iterator* (const *recursive_directory_iterator*
&)=default\\

*recursive_directory_iterator* (*recursive_directory_iterator*
&&)=default\\

int *depth* () const noexcept\\

void *disable_recursion_pending* () noexcept\\

*recursive_directory_iterator* & *increment* (*error_code* &__ec)\\

const *directory_entry* & *operator** () const noexcept\\

*recursive_directory_iterator* & *operator++* ()\\

*__directory_iterator_proxy* *operator++* (int)\\

const *directory_entry* * *operator->* () const noexcept\\

*recursive_directory_iterator* & *operator=* (const
*recursive_directory_iterator* &__rhs) noexcept\\

*recursive_directory_iterator* & *operator=*
(*recursive_directory_iterator* &&__rhs) noexcept\\

directory_options *options* () const noexcept\\

void *pop* ()\\

void *pop* (*error_code* &)\\

bool *recursion_pending* () const noexcept\\

** Friends
bool *operator!=* (const *recursive_directory_iterator* &__lhs, const
*recursive_directory_iterator* &__rhs) noexcept\\

bool *operator==* (const *recursive_directory_iterator* &__lhs, const
*recursive_directory_iterator* &__rhs) noexcept\\

** Related Functions
(Note that these are not member functions.)

\\

*recursive_directory_iterator* *begin* (*recursive_directory_iterator*
__iter) noexcept\\
Enable range-based =for= using recursive_directory_iterator.

*recursive_directory_iterator* *end* (*recursive_directory_iterator*)
noexcept\\
Return a past-the-end recursive_directory_iterator.

* Detailed Description
Iterator type for recursively traversing a directory hierarchy.

Definition at line *461* of file *bits/fs_dir.h*.

* Member Typedef Documentation
** typedef ptrdiff_t
std::filesystem::recursive_directory_iterator::difference_type
Definition at line *465* of file *bits/fs_dir.h*.

** typedef *input_iterator_tag*
*std::filesystem::recursive_directory_iterator::iterator_category*
Definition at line *468* of file *bits/fs_dir.h*.

** typedef const *directory_entry**
*std::filesystem::recursive_directory_iterator::pointer*
Definition at line *466* of file *bits/fs_dir.h*.

** typedef const *directory_entry*&
*std::filesystem::recursive_directory_iterator::reference*
Definition at line *467* of file *bits/fs_dir.h*.

** typedef *directory_entry*
*std::filesystem::recursive_directory_iterator::value_type*
Definition at line *464* of file *bits/fs_dir.h*.

* Constructor & Destructor Documentation
** std::filesystem::recursive_directory_iterator::recursive_directory_iterator
(const *path* & __p)= [inline]=, = [explicit]=
Definition at line *473* of file *bits/fs_dir.h*.

** std::filesystem::recursive_directory_iterator::recursive_directory_iterator
(const *path* & __p, directory_options __options)= [inline]=
Definition at line *476* of file *bits/fs_dir.h*.

** std::filesystem::recursive_directory_iterator::recursive_directory_iterator
(const *path* & __p, directory_options __options, *error_code* &
__ec)= [inline]=
Definition at line *479* of file *bits/fs_dir.h*.

** std::filesystem::recursive_directory_iterator::recursive_directory_iterator
(const *path* & __p, *error_code* & __ec)= [inline]=
Definition at line *483* of file *bits/fs_dir.h*.

* Member Function Documentation
** *__directory_iterator_proxy*
std::filesystem::recursive_directory_iterator::operator++
(int)= [inline]=
Definition at line *510* of file *bits/fs_dir.h*.

** const *directory_entry* *
std::filesystem::recursive_directory_iterator::operator-> ()
const= [inline]=, = [noexcept]=
Definition at line *499* of file *bits/fs_dir.h*.

* Friends And Related Function Documentation
** bool operator!= (const *recursive_directory_iterator* & __lhs, const
*recursive_directory_iterator* & __rhs)= [friend]=
Definition at line *534* of file *bits/fs_dir.h*.

** bool operator== (const *recursive_directory_iterator* & __lhs, const
*recursive_directory_iterator* & __rhs)= [friend]=
Definition at line *526* of file *bits/fs_dir.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
