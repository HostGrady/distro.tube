#+TITLE: Manpages - EVP_PKEY_CTX_new.3ssl
#+DESCRIPTION: Linux manpage for EVP_PKEY_CTX_new.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_PKEY_CTX_new, EVP_PKEY_CTX_new_id, EVP_PKEY_CTX_dup,
EVP_PKEY_CTX_free - public key algorithm context functions

* SYNOPSIS
#include <openssl/evp.h> EVP_PKEY_CTX *EVP_PKEY_CTX_new(EVP_PKEY *pkey,
ENGINE *e); EVP_PKEY_CTX *EVP_PKEY_CTX_new_id(int id, ENGINE *e);
EVP_PKEY_CTX *EVP_PKEY_CTX_dup(EVP_PKEY_CTX *ctx); void
EVP_PKEY_CTX_free(EVP_PKEY_CTX *ctx);

* DESCRIPTION
The *EVP_PKEY_CTX_new()* function allocates public key algorithm context
using the algorithm specified in *pkey* and ENGINE *e*.

The *EVP_PKEY_CTX_new_id()* function allocates public key algorithm
context using the algorithm specified by *id* and ENGINE *e*. It is
normally used when no *EVP_PKEY* structure is associated with the
operations, for example during parameter generation of key generation
for some algorithms.

*EVP_PKEY_CTX_dup()* duplicates the context *ctx*.

*EVP_PKEY_CTX_free()* frees up the context *ctx*. If *ctx* is NULL,
nothing is done.

* NOTES
The *EVP_PKEY_CTX* structure is an opaque public key algorithm context
used by the OpenSSL high-level public key API. Contexts *MUST NOT* be
shared between threads: that is it is not permissible to use the same
context simultaneously in two threads.

* RETURN VALUES
*EVP_PKEY_CTX_new()*, *EVP_PKEY_CTX_new_id()*, *EVP_PKEY_CTX_dup()*
returns either the newly allocated *EVP_PKEY_CTX* structure of *NULL* if
an error occurred.

*EVP_PKEY_CTX_free()* does not return a value.

* SEE ALSO
*EVP_PKEY_new* (3)

* HISTORY
These functions were added in OpenSSL 1.0.0.

* COPYRIGHT
Copyright 2006-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
